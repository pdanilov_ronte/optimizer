<?php

namespace Base\UsersBundle\Security;

use FOS\UserBundle\Security\UserProvider;


class EmailUserProvider extends UserProvider
{
    /**
     * {@inheritDoc}
     */
    protected function findUser($email)
    {
        return $this->userManager->findUserByEmail($email);
    }
}
