<?php

namespace Base\UsersBundle\Security;

use Base\UsersBundle\Entity\Account;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Security\UserProvider;
use FOS\UserBundle\Model\UserInterface;
use Jmikola\AutoLogin\User\AutoLoginUserProviderInterface;
use Jmikola\AutoLogin\Exception\AutoLoginTokenNotFoundException;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;


class AutoUserProvider extends UserProvider implements AutoLoginUserProviderInterface
{
    use ContainerAwareTrait;

    /**
     * {@inheritDoc}
     */
    protected function findUser($email)
    {
        return $this->userManager->findUserByEmail($email);
    }

    /**
     * Loads the user for the given auto-login token.
     *
     * This method must throw AutoLoginTokenNotFoundException if the user is not
     * found.
     *
     * @param string $key
     * @return UserInterface
     * @throws AutoLoginTokenNotFoundException if the user is not found
     */
    public function loadUserByAutoLoginToken($key)
    {
        /** @var EntityManager $om */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $time = time();

        /** @var Account $account */
        $account = $em->getRepository('BaseUsersBundle:Account')->findOneBy(['autoLoginToken' => $key]);
        if (empty($account)) {
            throw new AutoLoginTokenNotFoundException('Account not found');
        }

        if ($time >= $account->getAutoLoginTokenExpiresAt()) {
            $account->setAutoLoginToken(null);
            $account->setAutoLoginTokenExpiresAt(null);
            $em->persist($account);
            $em->flush($account);
            throw new AutoLoginTokenNotFoundException('Token expired');
        }

        return $account;
    }
}
