<?php

namespace Base\UsersBundle\Service;


class UidProvider
{
    /**
     * Generate unique user id
     *
     * @param int $a Unique numeric id
     * @return string
     */
    public function generateUID($a)
    {
        $n = dechex($this->generateNumber($a));

        $bl = bin2hex(openssl_random_pseudo_bytes(2));
        $br = bin2hex(openssl_random_pseudo_bytes(2));

        return sprintf('%04s-%04s-%08s', $bl, $br, $n);
    }

    private function generateNumber($a)
    {
        $b = bin2hex(openssl_random_pseudo_bytes(1));
        $d = intval($b, 16);
        $r = $d / 255;
        $mx = 98 / 255;
        $mn = 1 / 255;
        $v = round(($r * $mx + $mn) * 255);
        $s = sprintf('%s%06s', dechex($v), dechex($a));

        $x = intval($s, 16);
        $y = $this->permuteReverse32($x);

        return $y;
    }

    private function permuteReverse32($x)
    {
        // Maximum prime number less than unsigned 32 integer
        static $prime = 4294967291;

        if ($x > $prime) {
            throw new \Exception('Maximum allowed number for the permutation is 4294967291');
        }

        $residue = ($x * $x) % $prime;

        return ($x <= ($prime / 2)) ? $residue : ($prime - $residue);
    }
}
