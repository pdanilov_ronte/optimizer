<?php

namespace Base\UsersBundle\EventListener;

use Base\UsersBundle\Entity\Account;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;


/**
 * @class   ChangePasswordListener
 */
class ChangePasswordListener implements EventSubscriberInterface
{
    /**
     * @type UserManagerInterface
     */
    private $userManager;

    /**
     * @type EntityManager
     */
    private $entityManager;

    /**
     * @type EncoderFactoryInterface
     */
    private $encoderFactory;


    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::CHANGE_PASSWORD_INITIALIZE => 'onChangePasswordInitialize',
            FOSUserEvents::CHANGE_PASSWORD_SUCCESS    => 'onChangePasswordSuccess'
        );
    }

    public function onChangePasswordInitialize(GetResponseUserEvent $event)
    {
        /** @type Account $account */
        $account = $event->getUser();

        $data = $event->getRequest()->request->all();

        $encoder = $this->encoderFactory->getEncoder($account);

        $valid = $encoder->isPasswordValid($account->getPassword(), $data['current'], $account->getSalt());

        if (!$valid) {

            $response = new JsonResponse([
                'status' => 'error',
                'error'  => 'Current password is not valid',
                'field'  => 'current'
            ], 401);

            return $event->setResponse($response);
        }

        $account->setPlainPassword($data['new']);


//        $serializer = new Serializer([new ObjectNormalizer()]);

        try {

            $this->userManager->updatePassword($account);
            $this->entityManager->persist($account);
            $this->entityManager->flush();

            $response = new JsonResponse([
                'status' => 'success',
                'check' => 'persist'
            ], 200);

            $event->setResponse($response);

        } catch (\Exception $e) {

            $response = new JsonResponse([
                'status' => 'error',
                'error'  => $e->getMessage()
            ], 400);

            $event->setResponse($response);
        }
    }

    public function onChangePasswordSuccess(FormEvent $event)
    {
        $response = new JsonResponse([
            'status' => 'success'
        ], 200);

        $event->setResponse($response);
    }

    public function setUserManager(UserManagerInterface $manager)
    {
        $this->userManager = $manager;
    }

    public function setEntityManager(EntityManager $manager) {
        $this->entityManager = $manager;
    }

    public function setEncoderFactory(EncoderFactoryInterface $factory)
    {
        $this->encoderFactory = $factory;
    }
}
