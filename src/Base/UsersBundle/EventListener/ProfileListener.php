<?php

namespace Base\UsersBundle\EventListener;

use Base\UsersBundle\Entity\Account;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class ProfileListener implements EventSubscriberInterface
{
    /**
     * @type UserManagerInterface
     */
    private $userManager;


    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::PROFILE_EDIT_INITIALIZE => 'onProfileEditInitialize'
        );
    }

    public function onProfileEditInitialize(GetResponseUserEvent $event)
    {
        /** @type Account $account */
        $account = $event->getUser();

        $data = $event->getRequest()->request->all();

        $account->setName($data['name']);
        $account->setEmail($data['email']);

        try {

            $this->userManager->updateUser($account);

            $serializer = new Serializer([new ObjectNormalizer()]);

            $response = new JsonResponse([
                'status'  => 'success',
                'account' => $serializer->normalize($account)
            ], 200);

            $event->setResponse($response);

        } catch (\Exception $e) {

            $event->setResponse(new JsonResponse([
                'status' => 'error',
                'error'  => $e->getMessage()
            ], 400));
        }
    }


    public function setUserManager(UserManagerInterface $manager)
    {
        $this->userManager = $manager;
    }
}
