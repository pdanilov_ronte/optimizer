<?php

namespace Base\UsersBundle\EventListener;

use Base\UsersBundle\Entity\Account;
use Base\UsersBundle\Entity\Invite;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserManagerInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Base\UsersBundle\Service\UidProvider;


/**
 * @class RegistrationListener
 */
class RegistrationListener implements EventSubscriberInterface
{
    /**
     * @type UserManagerInterface
     */
    private $userManager;

    /**
     * @type EntityManager
     */
    private $entityManager;

    /**
     * @type UidProvider
     */
    private $uidProvider;


    /**
     * @type TokenStorage
     */
    private $tokenStorage;

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_INITIALIZE => 'onRegistrationInitialize',
            FOSUserEvents::REGISTRATION_COMPLETED  => 'onRegistrationCompleted'
        );
    }

    /**
     * Craft Account object manually
     *
     * @param GetResponseUserEvent $event
     */
    public function onRegistrationInitialize(GetResponseUserEvent $event)
    {
        /** @type Account $account */
        $account = $event->getUser();

        $data = $event->getRequest()->request->all();

        $uid = null;

        // @todo use forms service for validation
        if (!isset($data['email']) || empty($data['email'])) {

            $response = new JsonResponse([
                'status' => 'error',
                'error'  => 'E-mail must not be empty'
            ], 400);

            return $event->setResponse($response);
        }

        // @todo check invitation code
        if (!isset($data['code']) || empty($data['code'])) {

            $response = new JsonResponse([
                'status' => 'error',
                'error'  => 'Invitation code must not be empty'
            ], 400);

            return $event->setResponse($response);
        }
        $code = $data['code'];

        /** @type Invite $invite */
        $invite = $this->entityManager->getRepository('BaseUsersBundle:Invite')
            ->findOneBy(['code' => $code]);

        if (empty($invite)) {
            $response = new JsonResponse([
                'status' => 'error',
                'error'  => 'Invalid invitation code'
            ], 400);

            return $event->setResponse($response);
        }

        // @todo decide with error message for this check
        if (strtolower($invite->getSentTo()) !== strtolower($data['email'])) {
            $response = new JsonResponse([
                'status' => 'error',
                'error'  => 'Invalid invitation code'
            ], 400);

            return $event->setResponse($response);
        }

        if (!isset($data['password']) || empty($data['password'])) {

            $response = new JsonResponse([
                'status' => 'error',
                'error'  => 'Password must not be empty'
            ], 400);

            return $event->setResponse($response);
        }

        // Setup account fields
        $account->setUsername($data['email']);
        $account->setEmail($data['email']);
        $account->setPlainPassword($data['password']);

        // Check that the email is not yet registered
        $this->userManager->updateCanonicalFields($account);
        $result = $this->entityManager->getRepository('BaseUsersBundle:Account')
            ->findOneBy([
                'emailCanonical' => $account->getEmailCanonical()
            ]);

        // If it is, return with error
        if (!empty($result)) {

            $response = new JsonResponse([
                'status' => 'error',
                'error'  => 'Sorry, the specified E-mail is already registered'
            ], 400);

            return $event->setResponse($response);
        }

        // Otherwise, try to create new account
        try {


            $this->userManager->updateUser($account);

            // Generate UID and update account
            $uid = $this->uidProvider->generateUID($account->getId());
            $account->setUid($uid);
            $this->entityManager->persist($account);

            // update attached invitation code entity
            $invite->setAttachedTo($account);
            $this->entityManager->persist($invite);
            $this->entityManager->flush();

            $token = new UsernamePasswordToken($account, null, 'main', $account->getRoles());

            $this->tokenStorage->setToken($token);

            $serializer = new Serializer([new ObjectNormalizer()]);

            $response = new JsonResponse([
                'status'  => 'success',
                'account' => $serializer->normalize($account)
            ], 200);

        } catch (\Exception $e) {

            $response = new JsonResponse([
                'status' => 'error',
                'error'  => $e->getMessage()
            ], 400);
        }

        $event->setResponse($response);
    }

    /**
     * @param FilterUserResponseEvent $event
     */
    public function onRegistrationCompleted(FilterUserResponseEvent $event)
    {
        $account = $event->getUser();
        $token = new UsernamePasswordToken($account, null, 'main', $account->getRoles());

        $this->tokenStorage->setToken($token);
    }

    public function setUserManager(UserManagerInterface $manager)
    {
        $this->userManager = $manager;
    }

    public function setEntityManager(EntityManager $em)
    {
        $this->entityManager = $em;
    }

    public function setUidProvider(UidProvider $provider)
    {
        $this->uidProvider = $provider;
    }

    public function setTokenStorage($storage)
    {
        $this->tokenStorage = $storage;
    }
}
