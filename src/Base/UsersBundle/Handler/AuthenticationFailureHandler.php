<?php


namespace Base\UsersBundle\Handler;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationFailureHandler;


class AuthenticationFailureHandler extends DefaultAuthenticationFailureHandler
{
    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        // @todo remove default handler completely
        if (1 || $request->isXmlHttpRequest()) {
            $response = new JsonResponse();
            $response->setData([
                'status' => 'failure',
                'error' => $exception->getMessage()
            ]);
            $response->setStatusCode(401);
            return $response;
        }

        return parent::onAuthenticationFailure($request, $exception);
    }
}
