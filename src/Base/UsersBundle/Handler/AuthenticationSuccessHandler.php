<?php


namespace Base\UsersBundle\Handler;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class AuthenticationSuccessHandler extends DefaultAuthenticationSuccessHandler
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;


    /**
     * @param Request $request
     * @param TokenInterface $token
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $this->tokenStorage->setToken($token);

        $serializer = new Serializer([new ObjectNormalizer()]);

        // @todo remove default handler completely
        if (1 || $request->isXmlHttpRequest()) {
            return new JsonResponse([
                'status' => 'success',
                'user' => $serializer->normalize($token->getUser())
            ], 200);
        }

        return parent::onAuthenticationSuccess($request, $token);
    }

    public function setTokenStorage(TokenStorageInterface $storage)
    {
        $this->tokenStorage = $storage;
    }
}
