<?php

namespace Base\UsersBundle\DependencyInjection;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Base\UsersBundle\Entity\Account;


trait AccountAwareTrait
{
    /**
     * @return Account
     */
    protected function getAccount()
    {
        /** @var TokenStorage $storage */
        /** @noinspection PhpUndefinedMethodInspection */
        $storage = $this->container->get('security.token_storage');

        /** @var TokenInterface $token */
        $token = $storage->getToken();

        /** @var Account $account */
        $account = $token->getUser();

        return $account;
    }
}
