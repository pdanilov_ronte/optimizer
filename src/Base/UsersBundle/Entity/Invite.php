<?php

namespace Base\UsersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Invite
 *
 * @ORM\Table(name="invite")
 * @ORM\Entity()
 */
class Invite
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=16, unique=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="sent_to", type="string", length=64, nullable=true)
     */
    private $sentTo;

    /**
     * @var Account
     *
     * @ORM\OneToOne(targetEntity="Account")
     * @ORM\JoinColumn(name="attached_to", unique=true, nullable=true)
     */
    private $attachedTo;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Invite
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getSentTo()
    {
        return $this->sentTo;
    }

    /**
     * @param string $sentTo
     * @return Invite
     */
    public function setSentTo($sentTo)
    {
        $this->sentTo = $sentTo;

        return $this;
    }

    /**
     * @return Account
     */
    public function getAttachedTo()
    {
        return $this->attachedTo;
    }

    /**
     * @param Account $attachedTo
     * @return Invite
     */
    public function setAttachedTo($attachedTo)
    {
        $this->attachedTo = $attachedTo;

        return $this;
    }
}

