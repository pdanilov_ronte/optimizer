<?php

namespace Base\UsersBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;


/**
 * Account
 *
 * @ORM\Table(name="account")
 * @ORM\Entity(repositoryClass="Base\UsersBundle\Entity\AccountRepository")
 * @ExclusionPolicy("all")
 */
class Account extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose()
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     * @Expose()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="uid", type="string", unique=true, nullable=true, length=18)
     */
    private $uid;

    /**
     * @var string
     *
     * @ORM\Column(name="auto_login_token", type="string", unique=true, nullable=true, length=32)
     */
    private $autoLoginToken;

    /**
     * @var integer
     *
     * @ORM\Column(name="auto_login_token_expires_at", type="bigint", nullable=true)
     */
    private $autoLoginTokenExpiresAt;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Account
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     * @return Account
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * @return string
     */
    public function getAutoLoginToken()
    {
        return $this->autoLoginToken;
    }

    /**
     * @param string $autoLoginToken
     */
    public function setAutoLoginToken($autoLoginToken)
    {
        $this->autoLoginToken = $autoLoginToken;
    }

    /**
     * @return int
     */
    public function getAutoLoginTokenExpiresAt()
    {
        return $this->autoLoginTokenExpiresAt;
    }

    /**
     * @param int $autoLoginTokenExpiresAt
     */
    public function setAutoLoginTokenExpiresAt($autoLoginTokenExpiresAt)
    {
        $this->autoLoginTokenExpiresAt = $autoLoginTokenExpiresAt;
    }
}
