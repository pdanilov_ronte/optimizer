<?php

namespace Base\ExperimentsBundle\Doctrine\Filter;


use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;


/**
 * @class   AccountScopeFilter
 * @package Base\ExperimentsBundle\Doctrine\Filter
 * @author  PM:/ <pm@spiral.ninja>
 */
class AccountScopeFilter extends SQLFilter
{
    private static $restricted = [
        'Experiment'       => 'Base\ExperimentsBundle\Entity\Experiment',
        'Variation'        => 'Base\ExperimentsBundle\Entity\Variation',
        'VariationSnippet' => 'Base\ExperimentsBundle\Entity\VariationSnippet',
        'SnippetValue'     => 'Base\ExperimentsBundle\Entity\SnippetValue',
        'VariationCode'    => 'Base\ExperimentsBundle\Entity\VariationCode',
        'ExperimentUrl'    => 'Base\ExperimentsBundle\Entity\Experiment',
        'Goal'             => 'Base\ExperimentsBundle\Entity\Goal',
        'GoalDestination'  => 'Base\ExperimentsBundle\Entity\GoalDestination',
    ];

    /**
     * Gets the SQL query part to add to a query.
     *
     * @param ClassMetaData $targetEntity
     * @param string $targetTableAlias
     *
     * @return string The constraint SQL if there is available, empty string otherwise.
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if (!in_array($targetEntity->name, static::$restricted)) {
            return '';
        }

        $account_id = $this->getParameter('account_id');

        return sprintf('%s.account_id = %s', $targetTableAlias, $account_id);
    }
}
