<?php

namespace Base\ExperimentsBundle\Service\FindVariant;

use Base\ExperimentsBundle\Entity\Experiment;
use Base\ExperimentsBundle\Entity\Variation;
use Base\ExperimentsBundle\Service\VariantAttachment;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceProvider;


class FindVariantProvider extends AbstractServiceProvider
{
    /**
     * @param FindVariantRequest $request
     */
    public function findVariant(FindVariantRequest $request)
    {
        $data = $request->getRequestData()->getData();

        if (!isset($data['id']) || empty($data['id'])) {
            $request->operationFailed(
                new ServiceException('Parameter "id" must be set in request data')
            );
        }

        try {
            /** @type Registry $doctrine */
            $doctrine = $this->container->get('doctrine');
            $variant = $doctrine
                ->getManager()
                ->getRepository('BaseExperimentsBundle:Variation')
                ->find($data['id']);
        } catch (\Exception $e) {
            $request->operationFailed(
                new ServiceException($e->getMessage())
            );
            return;
        }

        if (empty($variant)) {
            $request->variantNotFound();
        } else {
            $attachment = new VariantAttachment($variant);
            $request->variantFound($attachment);
        }
    }

    /**
     * @param FindVariantRequest $request
     */
    public function findVariantByNumber(FindVariantRequest $request)
    {
        $data = $request->getRequestData()->getData();

        if (!isset($data['experiment_id']) || empty($data['experiment_id'])) {
            $request->operationFailed(
                new ServiceException('Parameter `experiment_id` must be set in request data')
            );
            return;
        }

        if (!isset($data['number'])) {
            $request->operationFailed(
                new ServiceException('Parameter `number` must be set in request data')
            );
            return;
        }

        /** @type Registry $doctrine */
        $doctrine = $this->container->get('doctrine');
        /** @type EntityManager $em */
        $em = $doctrine->getManager();

        try {
            /** @type Experiment $experiment */
            $experiment = $em->getRepository('BaseExperimentsBundle:Experiment')
                ->find($data['experiment_id']);
        } catch (\Exception $e) {
            $request->operationFailed(
                new ServiceException($e->getMessage())
            );
            return;
        }

        if (empty($experiment)) {
            $request->operationFailed(
                new ServiceException(sprintf('Experiment id %s not found', $data['experiment_id']))
            );
            return;
        }

        /**
         * This is temporary high-risk solution and should be replaced on the next prototype.
         * @todo: Unify VariantBase and Variant for similar contexts.
         */
        /*if ($data['number'] == 0) {
            $base = $experiment->getActiveBase();
            $variant = new Variant();
            $code = new VariantCode();
            $code->setValue($base->getCode()->getValue());
            $variant->setCode($code);
            $attachment = new VariantAttachment($variant);
            $request->variantFound($attachment);
            return;
        }*/

        try {
            $variants = $em->getRepository('BaseExperimentsBundle:Variation')
                ->findBy([
                    'experiment' => $experiment,
                    'number' => $data['number']
                ]);
        } catch (\Exception $e) {
            $request->operationFailed(
                new ServiceException($e->getMessage())
            );
            return;
        }

        if (empty($variants)) {
            $request->variantNotFound();
        } elseif (count($variants) > 1) {
            $request->operationFailed(
                new ServiceException(
                    sprintf('More than one variant result found for experiment %s and variant number %s',
                        $data['experiment_id'], $data['number']))
            );
        } else {
            /** @type Variation $variant */
            $variant = $variants[0];
            $attachment = new VariantAttachment($variant);
            $request->variantFound($attachment);
        }
    }
}
