<?php

namespace Base\ExperimentsBundle\Service\FindVariant;

use Base\ExperimentsBundle\Service\VariantAttachment;
use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceRequest;


class FindVariantRequest extends AbstractServiceRequest
{
    /**
     * @type VariantAttachment
     */
    protected $attachment;

    /**
     * @type bool
     */
    protected $found;


    /**
     * @param VariantAttachment $result
     */
    public function variantFound(VariantAttachment $result)
    {
        $this->setAttachment($result);
        $this->found = true;
    }

    /**
     *
     */
    public function variantNotFound()
    {
        $this->found = false;
    }

    /**
     * @return bool
     */
    public function isFound()
    {
        return (bool) $this->found;
    }

    /**
     * @param ServiceException $error
     */
    public function operationFailed(ServiceException $error)
    {
        $this->setException($error);
    }

    /**
     * Returns variant attachment if present.
     * Overwritten to specialize the method hint.
     *
     * @return VariantAttachment|null
     */
    public function getAttachment()
    {
        return parent::getAttachment();
    }
}
