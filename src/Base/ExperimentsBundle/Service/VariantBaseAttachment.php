<?php

namespace Base\ExperimentsBundle\Service;

use Base\ExperimentsBundle\Entity\VariantBase;
use Shared\SoaBundle\Service\ServiceRequestAttachment;


class VariantBaseAttachment extends ServiceRequestAttachment
{
    /**
     * @var VariantBase
     */
    private $base;


    /**
     * @param VariantBase|null $base optional
     */
    public function __construct(VariantBase $base = null)
    {
        if (isset($base)) {
            $this->setBase($base);
        }
    }

    /**
     * @return VariantBase
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * @param VariantBase $base
     */
    public function setBase($base)
    {
        $this->base = $base;
    }
}
