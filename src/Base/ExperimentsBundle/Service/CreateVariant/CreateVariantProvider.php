<?php

namespace Base\ExperimentsBundle\Service\CreateVariant;

use Base\ExperimentsBundle\Entity\Variant;
use Base\ExperimentsBundle\Entity\VariantBase;
use Base\ExperimentsBundle\Entity\VariantCode;
use Base\ExperimentsBundle\Service\VariantAttachment;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceProvider;


class CreateVariantProvider extends AbstractServiceProvider
{
    /**
     * @param CreateVariantRequest $request
     * @return void
     */
    public function createVariant(CreateVariantRequest $request)
    {
        $data = $request->getRequestData()->getData();

        if (!isset($data['base_id']) && empty($data['base_id'])) {
            $request->createFailed(
                new ServiceException('Parameter "base_id" must be set in request data')
            );
        }

        try {
            /** @type Registry $doctrine */
            $doctrine = $this->container->get('doctrine');
            /** @type VariantBase $base */
            $base = $doctrine
                ->getManager()
                ->getRepository('BaseExperimentsBundle:VariantBase')
                ->find($data['base_id']);
        } catch (\Exception $e) {
            $request->createFailed(
                new ServiceException($e->getMessage())
            );
            return;
        }

        if (empty($base)) {
            $request->createFailed(
                new ServiceException(sprintf('Variant base with id %s is not found', $data['base_id']))
            );
            return;
        }

        $variant = new Variant();

        $variant->setBase($base);
        $variant->setTitle($data['title']);
        /** By default, set the `number` value to last position in the resulting list */
        $variant->setNumber(count($base->getVariants()) + 1);

        $code = new VariantCode();
        $code->setValue($data['code']);
        $variant->setCode($code);

        try {
            /** @type Registry $doctrine */
            $doctrine = $this->container->get('doctrine');
            $em = $doctrine->getManager();
            $em->persist($variant);
            $em->flush();
        } catch (\Exception $e) {
            $request->createFailed(
                new ServiceException($e->getMessage())
            );
            return;
        }

        $attachment = new VariantAttachment($variant);
        $request->variantCreated($attachment);
    }
}
