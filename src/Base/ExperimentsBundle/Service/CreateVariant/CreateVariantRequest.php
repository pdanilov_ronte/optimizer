<?php

namespace Base\ExperimentsBundle\Service\CreateVariant;

use Base\ExperimentsBundle\Service\VariantAttachment;
use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceRequest;


class CreateVariantRequest extends AbstractServiceRequest
{
    /**
     * @param VariantAttachment $result
     */
    public function variantCreated(VariantAttachment $result)
    {
        $this->setAttachment($result);
    }

    /**
     * @param ServiceException $error
     */
    public function createFailed(ServiceException $error)
    {
        $this->setException($error);
    }

    /**
     * Returns variant attachment if present.
     * Overwritten to specialize the method hint.
     *
     * @return VariantAttachment|null
     */
    public function getAttachment()
    {
        return parent::getAttachment();
    }
}
