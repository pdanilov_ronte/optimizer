<?php

namespace Base\ExperimentsBundle\Service\CreateVariant;

use Shared\SoaBundle\Service\AbstractServiceRequester;


class CreateVariantService extends AbstractServiceRequester
{
    /**
     * @param array $data
     * @return CreateVariantRequest
     */
    public function createVariant(array $data)
    {
        $request = $this->createRequest($data);
        $provider = $this->createProvider();

        $provider->createVariant($request);

        return $request;
    }

    /**
     * @param array $data
     * @return CreateVariantRequest
     */
    protected function createRequest(array $data)
    {
        $request = new CreateVariantRequest();
        parent::prepareRequest($request, $data);

        return $request;
    }

    /**
     * @return CreateVariantProvider
     */
    protected function createProvider()
    {
        $provider = new CreateVariantProvider();
        parent::prepareProvider($provider);

        return $provider;
    }
}
