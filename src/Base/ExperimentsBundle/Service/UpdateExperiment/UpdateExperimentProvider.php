<?php

namespace Base\ExperimentsBundle\Service\UpdateExperiment;

use Base\ExperimentsBundle\Entity\Experiment;
use Base\ExperimentsBundle\Service\ExperimentAttachment;
use Doctrine\ORM\EntityManager;
use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceProvider;


class UpdateExperimentProvider extends AbstractServiceProvider
{
    /**
     * @param UpdateExperimentRequest $request
     */
    public function updateExperiment($request)
    {
        /** @type EntityManager $em */
        $em = $this->container->get('doctrine')->getManager();

        $data = $request->getRequestData()->getData();

        if (!isset($data['id']) || empty($data['id'])) {
            $request->updateFailed(
                new ServiceException('Parameter "id" must be set in request data')
            );
        }

        /** @type Experiment $experiment */
        $experiment = $em->getRepository('BaseExperimentsBundle:Experiment')->find($data['id']);

        if (empty($experiment)) {
            $request->updateFailed(
                new ServiceException(sprintf('Experiment id %s not found', $data['id']))
            );
            return;
        }

        $experiment->setTitle($data['title']);


        if (isset($data['method'])) {
            switch ($data['method']) {
                case Experiment::METHOD_WEIGHT:
                case 'weights':
                    $experiment->setMethod(Experiment::METHOD_WEIGHT);
                    break;
                case Experiment::METHOD_SEQ:
                case 'sequence':
                    $experiment->setMethod(Experiment::METHOD_SEQ);
                    break;
                case Experiment::METHOD_RTO:
                case 'rto':
                    $experiment->setMethod(Experiment::METHOD_RTO);
                    break;
                default:
                    $request->updateFailed(
                        new ServiceException(sprintf('Invalid value (%s) of "method" parameter passed', $data['method']))
                    );

                    return;
            }
        }

        try {
            $em->persist($experiment);
            $em->flush();
        } catch (\Exception $e) {
            $request->updateFailed(
                new ServiceException($e->getMessage())
            );

            return;
        }

        $attachment = new ExperimentAttachment($experiment);
        $request->experimentUpdated($attachment);
    }
}
