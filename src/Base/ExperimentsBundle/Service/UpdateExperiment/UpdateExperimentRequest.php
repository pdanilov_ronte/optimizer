<?php

namespace Base\ExperimentsBundle\Service\UpdateExperiment;

use Base\ExperimentsBundle\Service\ExperimentAttachment;
use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceRequest;


class UpdateExperimentRequest extends AbstractServiceRequest
{
    /**
     * @param ExperimentAttachment $result
     */
    public function experimentUpdated(ExperimentAttachment $result)
    {
        $this->setAttachment($result);
    }

    /**
     * @param ServiceException $error
     */
    public function updateFailed(ServiceException $error)
    {
        $this->setException($error);
    }

    /**
     * @return ExperimentAttachment|null
     */
    public function getAttachment()
    {
        return parent::getAttachment();
    }
}
