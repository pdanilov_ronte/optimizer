<?php

namespace Base\ExperimentsBundle\Service\UpdateExperiment;

use Shared\SoaBundle\Service\AbstractServiceRequester;


class UpdateExperimentService extends AbstractServiceRequester
{
    /**
     * @param array $data
     * @return UpdateExperimentRequest
     */
    public function updateExperiment(array $data)
    {
        $request = $this->createRequest($data);
        $provider = $this->createProvider();

        $provider->updateExperiment($request);

        return $request;
    }

    /**
     * @param array $data
     * @return UpdateExperimentRequest
     */
    protected function createRequest(array $data)
    {
        $request = new UpdateExperimentRequest();
        parent::prepareRequest($request, $data);

        return $request;
    }

    /**
     * @return UpdateExperimentProvider
     */
    protected function createProvider()
    {
        $provider = new UpdateExperimentProvider();
        parent::prepareProvider($provider);

        return $provider;
    }
}
