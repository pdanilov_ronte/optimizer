<?php

namespace Base\ExperimentsBundle\Service\FindExperiment;

use Base\ExperimentsBundle\Service\ExperimentAttachment;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceProvider;


class FindExperimentProvider extends AbstractServiceProvider
{
    /**
     * @param FindExperimentRequest $request
     */
    public function findExperiment(FindExperimentRequest $request)
    {
        $data = $request->getRequestData()->getData();

        if (!isset($data['id']) || empty($data['id'])) {
            $request->operationFailed(
                new ServiceException('Parameter "id" must be set in request data')
            );
        }

        try {
            /** @type Registry $doctrine */
            $doctrine = $this->container->get('doctrine');
            $experiment = $doctrine
                ->getManager()
                ->getRepository('BaseExperimentsBundle:Experiment')
                ->find($data['id']);
        } catch (\Exception $e) {
            $request->operationFailed(
                new ServiceException($e->getMessage())
            );
            return;
        }

        if (empty($experiment)) {
            $request->experimentNotFound();
        } else {
            $attachment = new ExperimentAttachment($experiment);
            $request->experimentFound($attachment);
        }
    }
}
