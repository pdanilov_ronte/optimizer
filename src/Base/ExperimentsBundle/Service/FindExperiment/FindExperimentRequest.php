<?php

namespace Base\ExperimentsBundle\Service\FindExperiment;

use Base\ExperimentsBundle\Service\ExperimentAttachment;
use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceRequest;


class FindExperimentRequest extends AbstractServiceRequest
{
    /**
     * @type ExperimentAttachment
     */
    protected $attachment;

    /**
     * @type bool
     */
    protected $found;


    /**
     * @param ExperimentAttachment $result
     */
    public function experimentFound($result)
    {
        $this->setAttachment($result);
        $this->found = true;
    }

    /**
     *
     */
    public function experimentNotFound()
    {
        $this->found = false;
    }

    /**
     * @return bool
     */
    public function isFound()
    {
        return (bool) $this->found;
    }

    /**
     * @param ServiceException $error
     */
    public function operationFailed(ServiceException $error)
    {
        $this->setException($error);
    }

    /**
     * Returns experiment attachment if present.
     * Overwritten to specialize the method hint.
     *
     * @return ExperimentAttachment|null
     */
    public function getAttachment()
    {
        return parent::getAttachment();
    }
}
