<?php


namespace Base\ExperimentsBundle\Service\FindExperiment;

use Shared\SoaBundle\Service\AbstractServiceRequester;


class FindExperimentService extends AbstractServiceRequester
{
    /**
     * @param $data
     * @return FindExperimentRequest
     */
    public function findExperiment($data)
    {
        $request = $this->createRequest($data);
        $provider = $this->createProvider();

        $provider->findExperiment($request);

        return $request;
    }

    /**
     * @param array $data
     * @return FindExperimentRequest
     */
    protected function createRequest(array $data)
    {
        $request = new FindExperimentRequest();
        parent::prepareRequest($request, $data);

        return $request;
    }

    /**
     * @return FindExperimentProvider
     */
    protected function createProvider()
    {
        $provider = new FindExperimentProvider();
        parent::prepareProvider($provider);

        return $provider;
    }
}
