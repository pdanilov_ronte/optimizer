<?php

namespace Base\ExperimentsBundle\Service;

use Base\ExperimentsBundle\Entity\Experiment;
use Shared\SoaBundle\Service\ServiceRequestAttachment;


class ExperimentAttachment extends ServiceRequestAttachment
{
    /**
     * @var Experiment
     */
    private $experiment;


    /**
     * @param Experiment|null $experiment optional
     */
    public function __construct(Experiment $experiment = null)
    {
        if (isset($experiment)) {
            $this->setExperiment($experiment);
        }
    }

    /**
     * @return Experiment
     */
    public function getExperiment()
    {
        return $this->experiment;
    }

    /**
     * @param Experiment $experiment
     */
    public function setExperiment($experiment)
    {
        $this->experiment = $experiment;
    }
}
