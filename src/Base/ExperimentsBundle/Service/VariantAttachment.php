<?php

namespace Base\ExperimentsBundle\Service;

use Base\ExperimentsBundle\Entity\Variation;
use Shared\SoaBundle\Service\ServiceRequestAttachment;


class VariantAttachment extends ServiceRequestAttachment
{
    /**
     * @var Variation
     */
    private $variation;


    /**
     * @param Variation|null $variation optional
     */
    public function __construct(Variation $variation = null)
    {
        if (isset($variation)) {
            $this->setVariation($variation);
        }
    }

    /**
     * @return Variation
     */
    public function getVariation()
    {
        return $this->variation;
    }

    /**
     * @param Variation $variation
     */
    public function setVariation($variation)
    {
        $this->variation = $variation;
    }
}
