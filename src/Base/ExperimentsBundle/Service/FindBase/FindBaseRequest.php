<?php

namespace Base\ExperimentsBundle\Service\FindBase;

use Base\ExperimentsBundle\Service\VariantBaseAttachment;
use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceRequest;


class FindBaseRequest extends AbstractServiceRequest
{
    /**
     * @type VariantBaseAttachment
     */
    protected $attachment;

    /**
     * @type bool
     */
    protected $found;


    /**
     * @param VariantBaseAttachment $result
     */
    public function baseFound($result)
    {
        $this->setAttachment($result);
        $this->found = true;
    }

    /**
     *
     */
    public function baseNotFound()
    {
        $this->found = false;
    }

    /**
     * @return bool
     */
    public function isFound()
    {
        return (bool) $this->found;
    }

    /**
     * @param ServiceException $error
     */
    public function operationFailed(ServiceException $error)
    {
        $this->setException($error);
    }

    /**
     * Returns variant base attachment if present.
     * Overwritten to specialize the method hint.
     *
     * @return VariantBaseAttachment|null
     */
    public function getAttachment()
    {
        return parent::getAttachment();
    }
}
