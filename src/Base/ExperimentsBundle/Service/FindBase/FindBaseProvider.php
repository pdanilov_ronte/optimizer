<?php

namespace Base\ExperimentsBundle\Service\FindBase;

use Base\ExperimentsBundle\Service\VariantBaseAttachment;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceProvider;


class FindBaseProvider extends AbstractServiceProvider
{
    /**
     * @param FindBaseRequest $request
     */
    public function findBase(FindBaseRequest $request)
    {
        $data = $request->getRequestData()->getData();

        if (!isset($data['id']) || empty($data['id'])) {
            $request->operationFailed(
                new ServiceException('Parameter "id" must be set in request data')
            );
        }

        try {
            /** @type Registry $doctrine */
            $doctrine = $this->container->get('doctrine');
            $base = $doctrine
                ->getManager()
                ->getRepository('BaseExperimentsBundle:VariantBase')
                ->find($data['id']);
        } catch (\Exception $e) {
            $request->operationFailed(
                new ServiceException($e->getMessage())
            );
            return;
        }

        if (empty($base)) {
            $request->baseNotFound();
        } else {
            $attachment = new VariantBaseAttachment($base);
            $request->baseFound($attachment);
        }
    }
}
