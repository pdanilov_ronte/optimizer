<?php

namespace Base\ExperimentsBundle\Service\FindBase;

use Shared\SoaBundle\Service\AbstractServiceRequester;


class FindBaseService extends AbstractServiceRequester
{
    /**
     * @param array $data
     * @return FindBaseRequest
     */
    public function findBase(array $data)
    {
        $request = $this->createRequest($data);
        $provider = $this->createProvider();

        $provider->findBase($request);

        return $request;
    }

    /**
     * @param array $data
     * @return FindBaseRequest
     */
    protected function createRequest(array $data)
    {
        $request = new FindBaseRequest();
        parent::prepareRequest($request, $data);

        return $request;
    }

    /**
     * @return FindBaseProvider
     */
    protected function createProvider()
    {
        $provider = new FindBaseProvider();
        parent::prepareProvider($provider);

        return $provider;
    }
}
