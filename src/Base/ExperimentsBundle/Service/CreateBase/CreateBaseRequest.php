<?php

namespace Base\ExperimentsBundle\Service\CreateBase;

use Base\ExperimentsBundle\Service\VariantBaseAttachment;
use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceRequest;


class CreateBaseRequest extends AbstractServiceRequest
{
    /**
     * @param VariantBaseAttachment $result
     */
    public function baseCreated(VariantBaseAttachment $result)
    {
        $this->setAttachment($result);
    }

    /**
     * @param ServiceException $error
     */
    public function createFailed(ServiceException $error)
    {
        $this->setException($error);
    }

    /**
     * Returns variant base attachment if present.
     * Overwritten to specialize the method hint.
     *
     * @return VariantBaseAttachment|null
     */
    public function getAttachment()
    {
        return parent::getAttachment();
    }
}
