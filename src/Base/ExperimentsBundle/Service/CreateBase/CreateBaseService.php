<?php

namespace Base\ExperimentsBundle\Service\CreateBase;

use Shared\SoaBundle\Service\AbstractServiceRequester;


class CreateBaseService extends AbstractServiceRequester
{
    /**
     * @param array $data
     * @return CreateBaseRequest
     */
    public function createBase(array $data)
    {
        $request = $this->createRequest($data);
        $provider = $this->createProvider();

        $provider->createBase($request);

        return $request;
    }

    /**
     * @param array $data
     * @return CreateBaseRequest
     */
    protected function createRequest(array $data)
    {
        $request = new CreateBaseRequest();
        parent::prepareRequest($request, $data);

        return $request;
    }

    /**
     * @return CreateBaseProvider
     */
    protected function createProvider()
    {
        $provider = new CreateBaseProvider();
        parent::prepareProvider($provider);

        return $provider;
    }
}
