<?php

namespace Base\ExperimentsBundle\Service\CreateBase;

use Base\ExperimentsBundle\Entity\Experiment;
use Base\ExperimentsBundle\Entity\VariantBase;
use Base\ExperimentsBundle\Entity\VariantCode;
use Base\ExperimentsBundle\Service\VariantBaseAttachment;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceProvider;


class CreateBaseProvider extends AbstractServiceProvider
{
    /**
     * @param CreateBaseRequest $request
     * @return void
     */
    public function createBase($request)
    {
        $data = $request->getRequestData()->getData();
        $base = new VariantBase();

        if (!isset($data['experiment_id']) && empty($data['experiment_id'])) {
            $request->createFailed(
                new ServiceException('Parameter "experiment_id" must be set in request data')
            );
        }

        if (!isset($data['code']) && empty($data['code'])) {
            $request->createFailed(
                new ServiceException('Parameter "code" must be set in request data')
            );
        }

        try {
            /** @type Registry $doctrine */
            $doctrine = $this->container->get('doctrine');
            /** @type Experiment $experiment */
            $experiment = $doctrine
                ->getManager()
                ->getRepository('BaseExperimentsBundle:Experiment')
                ->find($data['experiment_id']);
        } catch (\Exception $e) {
            $request->createFailed(
                new ServiceException($e->getMessage())
            );
            return;
        }

        if (empty($experiment)) {
            $request->createFailed(
                new ServiceException(sprintf('Experiment with id %s is not found', $data['experiment_id']))
            );
            return;
        }

        $base->setTitle($data['title']);
        $base->setSelector($data['selector']);

        $code = new VariantCode();
        $code->setValue($data['code']);
        $base->setCode($code);

        $experiment->setBase($base);

        try {
            /** @type Registry $doctrine */
            $doctrine = $this->container->get('doctrine');
            $em = $doctrine->getManager();
            $em->persist($base);
            $em->persist($experiment);
            $em->flush();
        } catch (\Exception $e) {
            $request->createFailed(
                new ServiceException($e->getMessage())
            );
            return;
        }

        $attachment = new VariantBaseAttachment($base);
        $request->baseCreated($attachment);
    }
}
