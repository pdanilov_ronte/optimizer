<?php

namespace Base\ExperimentsBundle\Service\CreateExperiment;

use Base\ExperimentsBundle\Service\ExperimentAttachment;
use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceRequest;


class CreateExperimentRequest extends AbstractServiceRequest
{
    /**
     * @param ExperimentAttachment $result
     */
    public function experimentCreated(ExperimentAttachment $result)
    {
        $this->setAttachment($result);
    }

    /**
     * @param ServiceException $error
     */
    public function createFailed(ServiceException $error)
    {
        $this->setException($error);
    }

    /**
     * Returns experiment attachment if present.
     * Overwritten to specialize the method hint.
     *
     * @return ExperimentAttachment|null
     */
    public function getAttachment()
    {
        return parent::getAttachment();
    }
}
