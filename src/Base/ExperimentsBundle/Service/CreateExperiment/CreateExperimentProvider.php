<?php

namespace Base\ExperimentsBundle\Service\CreateExperiment;

use Base\ExperimentsBundle\Entity\Experiment;
use Base\ExperimentsBundle\Service\ExperimentAttachment;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceProvider;


class CreateExperimentProvider extends AbstractServiceProvider
{
    /**
     * @param CreateExperimentRequest $request
     * @return void
     */
    public function createExperiment($request)
    {
        $data = $request->getRequestData()->getData();
        $experiment = new Experiment();

        $experiment->setTitle($data['title']);

        $experiment->setState(Experiment::STATE_NEW);

        if (!isset($data['type']) || empty($data['type'])) {
            $request->createFailed(
                new ServiceException('Parameter "type" must be set in request data')
            );

            return;
        }

        switch ($data['type']) {
            case Experiment::TYPE_AB:
            case 'ab':
                $experiment->setType(Experiment::TYPE_AB);
                break;
            case Experiment::TYPE_MULTI:
            case 'multi':
                $experiment->setType(Experiment::TYPE_MULTI);
                break;
            default:
                $request->createFailed(
                    new ServiceException(sprintf('Invalid value (%s) of "type" parameter passed', $data['type']))
                );

                return;
        }

        if (isset($data['method'])) {
            switch ($data['method']) {
                case Experiment::METHOD_WEIGHT:
                case 'weights':
                    $experiment->setMethod(Experiment::METHOD_WEIGHT);
                    break;
                case Experiment::METHOD_SEQ:
                case 'sequence':
                    $experiment->setMethod(Experiment::METHOD_SEQ);
                    break;
                case Experiment::METHOD_RTO:
                case 'rto':
                    $experiment->setMethod(Experiment::METHOD_RTO);
                    break;
                default:
                    $request->createFailed(
                        new ServiceException(sprintf('Invalid value (%s) of "method" parameter passed', $data['method']))
                    );

                    return;
            }
        }

        try {
            /** @type Registry $doctrine */
            $doctrine = $this->container->get('doctrine');
            $em = $doctrine->getManager();
            $em->persist($experiment);
            $em->flush();
        } catch (\Exception $e) {
            $request->createFailed(
                new ServiceException($e->getMessage())
            );

            return;
        }

        $attachment = new ExperimentAttachment($experiment);
        $request->experimentCreated($attachment);
    }
}
