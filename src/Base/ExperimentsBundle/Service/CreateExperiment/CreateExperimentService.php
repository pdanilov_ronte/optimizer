<?php

namespace Base\ExperimentsBundle\Service\CreateExperiment;

use Shared\SoaBundle\Service\AbstractServiceRequester;


class CreateExperimentService extends AbstractServiceRequester
{
    /**
     * @param array $data
     * @return CreateExperimentRequest
     */
    public function createExperiment(array $data)
    {
        $request = $this->createRequest($data);
        $provider = $this->createProvider();

        $provider->createExperiment($request);

        return $request;
    }

    /**
     * @param array $data
     * @return CreateExperimentRequest
     */
    protected function createRequest(array $data)
    {
        $request = new CreateExperimentRequest();
        parent::prepareRequest($request, $data);

        return $request;
    }

    /**
     * @return CreateExperimentProvider
     */
    protected function createProvider()
    {
        $provider = new CreateExperimentProvider();
        parent::prepareProvider($provider);

        return $provider;
    }
}
