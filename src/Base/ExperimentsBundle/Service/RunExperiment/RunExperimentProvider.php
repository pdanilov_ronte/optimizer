<?php

namespace Base\ExperimentsBundle\Service\RunExperiment;

use Base\ExperimentsBundle\Entity\Experiment;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceProvider;


class RunExperimentProvider extends AbstractServiceProvider
{
    /**
     * @param RunExperimentRequest $request
     * @return void
     */
    public function runExperiment($request)
    {
        $data = $request->getRequestData()->getData();

        if (!isset($data['id']) || empty($data['id'])) {
            $request->runFailed(
                new ServiceException('Parameter "id" must be set in request data')
            );
        }

        try {
            /** @type Registry $doctrine */
            $doctrine = $this->container->get('doctrine');
            /** @type Experiment $experiment */
            $experiment = $doctrine
                ->getManager()
                ->getRepository('BaseExperimentsBundle:Experiment')
                ->find($data['id']);
        } catch (\Exception $e) {
            $request->runFailed(
                new ServiceException($e->getMessage())
            );

            return;
        }

        if (empty($experiment)) {
            $request->experimentNotFound();
        } elseif ($experiment->getState() != Experiment::STATE_NEW) {
            $request->alreadyRunning();
        } else {

            $experiment->setState(Experiment::STATE_RUNNING);

            try {
                $doctrine->getManager()->persist($experiment);
                $doctrine->getManager()->flush();
            } catch (\Exception $e) {
                $request->runFailed(
                    new ServiceException($e->getMessage())
                );

                return;
            }

            $request->runSuccessful();
        }
    }
}
