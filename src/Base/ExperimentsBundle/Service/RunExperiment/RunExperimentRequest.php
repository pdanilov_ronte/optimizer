<?php

namespace Base\ExperimentsBundle\Service\RunExperiment;

use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceRequest;


class RunExperimentRequest extends AbstractServiceRequest
{
    /**
     * @type bool
     */
    protected $notFound;

    /**
     * @type bool
     */
    protected $success;

    /**
     * @type bool
     */
    protected $wasRunning;


    /**
     *
     */
    public function experimentNotFound()
    {
        $this->notFound = true;
    }

    /**
     * @return bool
     */
    public function isNotFound()
    {
        return (bool) $this->notFound;
    }

    /**
     * Callback when experiment set to "running" state successfully.
     */
    public function runSuccessful()
    {
        $this->success = true;
    }

    /**
     * @return bool
     */
    public function isSuccessful()
    {
        return (bool) $this->success;
    }

    /**
     * Callback when experiment is already in "running" state.
     */
    public function alreadyRunning()
    {
        $this->wasRunning = true;
    }


    public function wasRunning()
    {
        return (bool) $this->wasRunning;
    }

    /**
     * @param ServiceException $error
     */
    public function runFailed(ServiceException $error)
    {
        $this->setException($error);
    }
}
