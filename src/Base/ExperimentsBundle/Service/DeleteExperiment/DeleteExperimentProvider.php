<?php

namespace Base\ExperimentsBundle\Service\DeleteExperiment;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceProvider;


class DeleteExperimentProvider extends AbstractServiceProvider
{
    /**
     * @param DeleteExperimentRequest $request
     */
    public function deleteExperiment($request)
    {
        $data = $request->getRequestData()->getData();

        if (!isset($data['id']) || empty($data['id'])) {
            $request->deleteFailed(
                new ServiceException('Parameter "id" must be set in request data')
            );
        }

        try {

            /** @type Registry $doctrine */
            $doctrine = $this->container->get('doctrine');
            $em = $doctrine->getManager();

            $experiment = $em->getRepository('BaseExperimentsBundle:Experiment')->find($data['id']);
            $em->remove($experiment);
            $em->flush();

        } catch (\Exception $e) {
            $request->deleteFailed(
                new ServiceException($e->getMessage())
            );
            return;
        }

        $request->experimentDeleted();
    }
}
