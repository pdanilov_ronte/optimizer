<?php

namespace Base\ExperimentsBundle\Service\DeleteExperiment;

use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceRequest;


class DeleteExperimentRequest extends AbstractServiceRequest
{
    /**
     * @type bool
     */
    protected $deleted;


    /**
     *
     */
    public function experimentDeleted()
    {
        $this->deleted = true;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return (bool) $this->deleted;
    }

    /**
     * @param ServiceException $error
     */
    public function deleteFailed($error)
    {
        $this->setException($error);
    }
}
