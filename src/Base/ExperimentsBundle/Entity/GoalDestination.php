<?php

namespace Base\ExperimentsBundle\Entity;

use Base\UsersBundle\Entity\Account;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="goal_destination")
 * @ORM\Entity()
 * @ExclusionPolicy("all")
 */
class GoalDestination
{
    /**
     * @var integer
     *
     * @Expose()
     * @Groups({"static", "details", "complete"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Expose()
     * @Groups({"static", "details", "complete"})
     * @ORM\Column(name="value", type="string", length=255, nullable=true)
     */
    private $value;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Base\UsersBundle\Entity\Account")
     */
    private $account;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     * @return GoalDestination
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    function __toString()
    {
        return $this->getValue();
    }
}
