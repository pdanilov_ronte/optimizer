<?php

namespace Base\ExperimentsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

use Base\UsersBundle\Entity\Account;


/**
 * @ORM\Entity
 * @ORM\Table(uniqueConstraints={
 *     @UniqueConstraint(name="variation_snippet_unique", columns={"variation_id", "snippet_id"})
 * })
 * @ExclusionPolicy("all")
 */
class SnippetValue
{
    /**
     * @var integer
     *
     * @Expose()
     * @Groups({"static", "details", "complete"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Expose()
     * @Groups({"static", "details", "complete"})
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;

    /**
     * @var Variation
     *
     * @ORM\ManyToOne(targetEntity="Variation", inversedBy="snippetValues")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $variation;

    /**
     * @var VariationSnippet
     *
     * @ORM\ManyToOne(targetEntity="VariationSnippet", inversedBy="values")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $snippet;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Base\UsersBundle\Entity\Account")
     * @ORM\JoinColumn(nullable=false)
     */
    private $account;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getVariation()
    {
        return $this->variation;
    }

    /**
     * @param mixed $variation
     * @return SnippetValue
     */
    public function setVariation($variation)
    {
        $this->variation = $variation;

        return $this;
    }

    /**
     * @return VariationSnippet
     */
    public function getSnippet()
    {
        return $this->snippet;
    }

    /**
     * @param VariationSnippet $snippet
     * @return SnippetValue
     */
    public function setSnippet($snippet)
    {
        $this->snippet = $snippet;

        return $this;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return SnippetValue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     * @return SnippetValue
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }
}
