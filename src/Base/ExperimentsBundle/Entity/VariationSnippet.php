<?php

namespace Base\ExperimentsBundle\Entity;

use Base\UsersBundle\Entity\Account;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

/**
 * VariationSnippet
 *
 * @ORM\Table(uniqueConstraints={
 *     @UniqueConstraint(name="experiment_number_unique", columns={"experiment_id", "number"})
 * })
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class VariationSnippet
{
    /**
     * @var integer
     *
     * @Expose()
     * @Groups({"static", "details", "complete"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint")
     */
    private $type = self::TYPE_CODE;

    /**
     * @var Experiment
     *
     * @ORM\ManyToOne(targetEntity="Experiment")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $experiment;

    /**
     * @var integer
     *
     * @Expose()
     * @Groups({"static", "details", "complete"})
     * @ORM\Column(name="number", type="smallint", nullable=true)
     */
    private $number;

    /**
     * @var string
     *
     * @Expose()
     * @Groups({"static", "details", "complete"})
     * @ORM\Column(name="title", type="string", length=32, nullable=true)
     */
    private $title;

    /**
     * @var SnippetValue[]
     *
     * @Expose()
     * @Groups({"static", "lazy", "complete"})
     * @ORM\OneToMany(targetEntity="SnippetValue", mappedBy="snippet")
     */
    private $values;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Base\UsersBundle\Entity\Account")
     * @ORM\JoinColumn(nullable=false)
     */
    private $account;


    const TYPE_UNKNOWN = 0;
    const TYPE_CODE = 1;
    const TYPE_REGEXP = 2;
    const TYPE_SELECTOR = 3;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return VariationSnippet
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return VariationSnippet
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return VariationSnippet
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Experiment
     */
    public function getExperiment()
    {
        return $this->experiment;
    }

    /**
     * @param Experiment $experiment
     * @return VariationSnippet
     */
    public function setExperiment($experiment)
    {
        $this->experiment = $experiment;

        return $this;
    }

    /**
     * @return SnippetValue[]
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param SnippetValue[] $values
     * @return VariationSnippet
     */
    public function setValues($values)
    {
        $this->values = $values;

        return $this;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     * @return VariationSnippet
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }
}
