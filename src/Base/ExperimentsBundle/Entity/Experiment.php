<?php

namespace Base\ExperimentsBundle\Entity;

use Base\UsersBundle\Entity\Account;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Exclude;
use /** @noinspection PhpUnusedAliasInspection */
    JMS\Serializer\Annotation\ExclusionPolicy;
use /** @noinspection PhpUnusedAliasInspection */
    JMS\Serializer\Annotation\Expose;
use /** @noinspection PhpUnusedAliasInspection */
    JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Inline;
use JMS\Serializer\Annotation\Type;


/**
 * @ORM\Table(name="experiment")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @ExclusionPolicy("all")
 */
class Experiment
{
    /**
     * @var integer
     *
     * @Expose()
     * @Groups({"static", "list", "details", "complete"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Expose()
     * @Groups({"static", "list", "details", "complete"})
     * @ORM\Column(name="title", type="string", length=64)
     */
    private $title;

    /**
     * @var int
     *
     * @Groups({"static", "list", "details", "complete"})
     * @ORM\Column(name="type", type="smallint")
     */
    private $type = self::TYPE_AB;

    /**
     * @var int
     *
     * @Expose()
     * @Groups({"static", "list", "details", "complete"})
     * @ORM\Column(name="state", type="smallint")
     */
    private $state;

    /**
     * @var bool
     *
     * @Groups({"static", "list", "details", "complete"})
     * @ORM\Column(name="pristine", type="boolean", options={"default": true})
     */
    private $pristine = true;

    /**
     * @var int
     *
     * @Groups({"static", "list", "details", "complete"})
     * @ORM\Column(name="method", type="smallint", nullable=false, options={"default":0})
     */
    private $method = self::METHOD_WEIGHT;

    /**
     * @var int
     *
     * @Groups({"static", "list", "details", "complete"})
     * @ORM\Column(name="mode", type="smallint", nullable=false, options={"default":0})
     */
    private $mode = self::MODE_TRANSIENT;

    /**
     * @var \DateTime
     *
     * @Expose()
     * @Groups({"static", "list", "details", "complete"})
     * @ORM\Column(name="startedAt", type="datetime", nullable=true)
     */
    private $startedAt;


    /**
     * @var ExperimentUrl[]
     *
     * @Expose()
     * @Groups({"static", "complete"})
     * @ORM\OneToMany(targetEntity="ExperimentUrl", mappedBy="experiment", cascade={"persist", "remove"})
     */
    private $urls;

    /**
     * @var Variation[]
     *
     * @Expose()
     * @Groups({"static", "list", "details", "complete"})
     * @ORM\OneToMany(targetEntity="Variation", mappedBy="experiment", cascade={"persist", "remove"})
     */
    private $variations;

    /**
     * @var VariationSnippet[]
     *
     * @Expose()
     * @Groups({"static", "complete"})
     * @ORM\OneToMany(targetEntity="VariationSnippet", mappedBy="experiment", cascade={"remove","persist"})
     */
    private $snippets;

    /**
     * @var Goal[]
     *
     * @Expose()
     * @Groups({"static", "list", "details", "complete"})
     * @ORM\OneToMany(targetEntity="Goal", mappedBy="experiment", cascade={"persist", "remove"})
     */
    private $goals;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Base\UsersBundle\Entity\Account")
     */
    private $account;

    /**
     * Experiment types constants
     */
    const TYPE_AB = 1;
    const TYPE_MULTI = 2;

    private static $typeMap = [
        Experiment::TYPE_AB    => 'ab',
        Experiment::TYPE_MULTI => 'multi'
    ];

    /**
     * Experiment states constants
     */
    const STATE_NEW = 1;
    const STATE_RUNNING = 2;
    const STATE_PAUSED = 3;

    private static $stateMap = [
        Experiment::STATE_NEW     => 'new',
        Experiment::STATE_RUNNING => 'running',
        Experiment::STATE_PAUSED  => 'paused'
    ];

    /**
     * Variant decision methods constants
     */
    const METHOD_UNKNOWN = 0;
    const METHOD_WEIGHT = 1;
    const METHOD_SEQ = 2;
    const METHOD_RTO = 3;

    private static $methodMap = [
        Experiment::METHOD_WEIGHT => 'weights',
        Experiment::METHOD_SEQ    => 'sequence',
        Experiment::METHOD_RTO    => 'rto'
    ];

    /**
     * Variation persistence mode
     */
    const MODE_UNKNOWN = 0;
    const MODE_PERSISTENT = 1;
    const MODE_TRANSIENT = 2;
    const MODE_HYBRID = 3;
    const MODE_MANUAL = 4;

    private static $modeMap = [
        Experiment::MODE_UNKNOWN    => 'unknown',
        Experiment::MODE_PERSISTENT => 'persistent',
        Experiment::MODE_TRANSIENT  => 'transient',
        Experiment::MODE_HYBRID     => 'hybrid',
        Experiment::MODE_MANUAL     => 'manual',
    ];


    public function __construct()
    {
        $this->urls = new ArrayCollection();
        $this->variations = new ArrayCollection();
        $this->goals = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Experiment
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return Experiment
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Maps type constant value to convenience type name
     *
     * @param int $type
     * @return string|null
     */
    public static function mapType($type)
    {
        return isset(static::$typeMap[ $type ])
            ? static::$typeMap[ $type ]
            : null;
    }

    /**
     * Maps convenience type name to integer constant value
     *
     * @param string $name
     * @return int|null
     */
    public static function reverseType($name)
    {
        $reverse = array_flip(static::$typeMap);

        return isset($reverse[ $name ])
            ? $reverse[ $name ]
            : null;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param int $state
     * @return $this
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPristine()
    {
        return $this->pristine;
    }

    /**
     * @param mixed $pristine
     * @return Experiment
     */
    public function setPristine($pristine)
    {
        $this->pristine = $pristine;

        return $this;
    }

    /**
     * Maps state constant value to convenience state name
     *
     * @param int $state
     * @return string|null
     */
    public static function mapState($state)
    {
        return isset(static::$stateMap[ $state ])
            ? static::$stateMap[ $state ]
            : null;
    }

    /**
     * Maps convenience state name to integer constant value
     *
     * @param string $name
     * @return int|null
     */
    public static function reverseState($name)
    {
        $reverse = array_flip(static::$stateMap);

        return isset($reverse[ $name ])
            ? $reverse[ $name ]
            : null;
    }

    /**
     * @return int
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param int $method
     * @return Experiment
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @return int
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param int $mode
     * @return Experiment
     */
    public function setMode($mode)
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartedAt()
    {
        return $this->startedAt;
    }

    /**
     * @param \DateTime $startedAt
     * @return Experiment
     */
    public function setStartedAt($startedAt)
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    /**
     * Maps method constant value to convenience method name
     *
     * @param int $type
     * @return string|null
     */
    public static function mapMethod($type)
    {
        return isset(static::$methodMap[ $type ])
            ? static::$methodMap[ $type ]
            : null;
    }

    /**
     * Maps convenience method name to integer constant value
     *
     * @param string $name
     * @return int|null
     */
    public static function reverseMethod($name)
    {
        $reverse = array_flip(static::$methodMap);

        return isset($reverse[ $name ])
            ? $reverse[ $name ]
            : null;
    }

    /**
     * @param $mode
     * @return string|null
     */
    public static function mapMode($mode)
    {
        return isset(static::$modeMap[ $mode ])
            ? static::$modeMap[ $mode ]
            : null;
    }

    /**
     * @return ExperimentUrl[]
     */
    public function getUrls()
    {
        return $this->urls;
    }

    /**
     * @param array $urls
     * @return Experiment
     */
    public function setUrls($urls)
    {
        $this->urls = $urls;

        return $this;
    }

    /**
     * @param ExperimentUrl $url
     * @return Experiment
     */
    public function addUrl(ExperimentUrl $url)
    {
        $url->setExperiment($this);
        $this->urls[] = $url;

        return $this;
    }

    /**
     * @param Variation $variation
     * @return Experiment
     */
    public function addVariation(Variation $variation)
    {
        $variation->setExperiment($this);
        $this->variations[] = $variation;

        return $this;
    }

    /**
     * @return Variation[]
     */
    public function getVariations()
    {
        return $this->variations;
    }

    /**
     * @return Variation|null
     */
    public function getControlVariation()
    {
        foreach ($this->variations as $variation) {
            if ($variation->getNumber() == 0) {
                return $variation;
            }
        }

        return null;
    }

    /**
     * @return VariationSnippet[]
     */
    public function getSnippets()
    {
        return $this->snippets;
    }

    /**
     * @param VariationSnippet[] $snippets
     * @return Experiment
     */
    public function setSnippets($snippets)
    {
        $this->snippets = $snippets;

        return $this;
    }

    /**
     * @param Goal $goal
     * @return Experiment
     */
    public function addGoal(Goal $goal)
    {
        $goal->setExperiment($this);
        $this->goals[] = $goal;

        return $this;
    }

    /**
     * @return Goal[]
     */
    public function getGoals()
    {
        return $this->goals;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     * @return Experiment
     */
    public function setAccount(Account $account)
    {
        $this->account = $account;

        return $this;
    }
}
