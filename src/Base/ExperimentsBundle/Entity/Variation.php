<?php

namespace Base\ExperimentsBundle\Entity;

use Base\UsersBundle\Entity\Account;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;


/**
 * @ORM\Table(name="variation")
 * @ORM\Entity()
 * @ExclusionPolicy("all")
 */
class Variation
{
    /**
     * @var integer
     *
     * @Expose()
     * @Groups({"static", "list", "details", "complete"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Expose()
     * @Groups({"static", "list", "details", "complete"})
     * @ORM\Column(name="title", type="string", nullable=true)
     * @Expose
     */
    private $title = '';

    /**
     * @var int
     *
     * @Expose()
     * @Groups({"static", "list", "details", "complete"})
     * @ORM\Column(name="number", type="smallint", nullable=false)
     */
    private $number;

    /**
     * @var float
     *
     * @Expose()
     * @Groups({"static", "details", "complete"})
     * @ORM\Column(name="weight", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $weight;

    /**
     * @var bool
     *
     * @Expose()
     * @Groups({"static", "details", "complete"})
     * @ORM\Column(name="enabled", type="boolean", options={"default"=1}, nullable=true)
     */
    private $enabled = true;

    /**
     * @var int
     *
     * @Expose()
     * @Groups({"static", "details", "complete"})
     * @ORM\Column(name="state", type="integer", options={"default"=0}, nullable=true)
     */
    private $state;

    /**
     * @var SnippetValue[]
     *
     * @ORM\OneToMany(targetEntity="SnippetValue", mappedBy="variation")
     */
    private $snippetValues;

    /**
     * @var Experiment
     *
     * @ORM\ManyToOne(targetEntity="Experiment", inversedBy="variations")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $experiment;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Base\UsersBundle\Entity\Account")
     */
    private $account;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Variation
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param int $number
     * @return Variation
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     * @return Variation
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     * @return Variation
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param int $state
     * @return Variation
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return SnippetValue[]
     */
    public function getSnippetValues()
    {
        return $this->snippetValues;
    }

    /**
     * @param SnippetValue[] $snippetValues
     * @return Variation
     */
    public function setSnippetValues($snippetValues)
    {
        $this->snippetValues = $snippetValues;

        return $this;
    }

    /**
     * @return Experiment
     */
    public function getExperiment()
    {
        return $this->experiment;
    }

    /**
     * @param Experiment $experiment
     * @return Variation
     */
    public function setExperiment($experiment)
    {
        $this->experiment = $experiment;

        return $this;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     * @return Variation
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }
}
