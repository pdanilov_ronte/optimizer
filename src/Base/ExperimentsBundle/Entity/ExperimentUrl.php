<?php

namespace Base\ExperimentsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

use Base\UsersBundle\Entity\Account;


/**
 * @ORM\Table(name="experiment_url")
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class ExperimentUrl
{
    /**
     * @var integer
     *
     * @Expose()
     * @Groups({"static", "details", "complete"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint")
     */
    private $type = self::TYPE_PREG;

    /**
     * @var string
     *
     * @Expose()
     * @Groups({"static", "details", "complete"})
     * @ORM\Column(name="pattern", type="string", length=255, nullable=true)
     */
    private $pattern;

    /**
     * @var Experiment
     *
     * @ORM\ManyToOne(targetEntity="Experiment", inversedBy="urls")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $experiment;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Base\UsersBundle\Entity\Account")
     * @ORM\JoinColumn(nullable=false)
     */
    private $account;

    /**
     * Goal types constants
     */
    const TYPE_EXACT = 1;
    const TYPE_PREG = 2;
    const TYPE_POSIX = 4;
    const TYPE_SQL = 8;

    private static $typeMap = [
        self::TYPE_EXACT => 'exact',
        self::TYPE_PREG => 'preg'
    ];


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return ExperimentUrl
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Maps type constant value to convenience type name
     *
     * @param integer $type
     * @return string|null
     */
    public static function mapType($type)
    {
        return isset(static::$typeMap[$type])
            ? static::$typeMap[$type]
            : null;
    }

    /**
     * Set pattern
     *
     * @param string $pattern
     * @return ExperimentUrl
     */
    public function setPattern($pattern)
    {
        $this->pattern = $pattern;

        return $this;
    }

    /**
     * Get pattern
     *
     * @return string
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * @return Experiment
     */
    public function getExperiment()
    {
        return $this->experiment;
    }

    /**
     * @param Experiment $experiment
     * @return ExperimentUrl
     */
    public function setExperiment($experiment)
    {
        $this->experiment = $experiment;

        return $this;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     * @return ExperimentUrl
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }
}
