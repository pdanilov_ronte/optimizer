<?php

namespace Base\ExperimentsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

use Base\UsersBundle\Entity\Account;


/**
 * @ORM\Table(name="goal")
 * @ORM\Entity()
 * @ExclusionPolicy("all")
 */
class Goal
{
    /**
     * @var integer
     *
     * @Expose()
     * @Groups({"static", "list", "details"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Expose()
     * @Groups({"static", "list", "details", "complete"})
     * @ORM\Column(name="title", type="string", length=64, nullable=true)
     */
    private $title;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="smallint")
     */
    private $type;

    /**
     * @var GoalDestination
     *
     * @Expose()
     * @Groups({"static", "details", "complete"})
     * @ORM\OneToOne(targetEntity="GoalDestination", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $destination;

    /**
     * @var Experiment
     *
     * @ORM\ManyToOne(targetEntity="Experiment", inversedBy="goals")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $experiment;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Base\UsersBundle\Entity\Account")
     */
    private $account;


    /**
     * Goal types constants
     */
    const TYPE_DESTINATION = 1;
    const TYPE_DURATION = 2;
    const TYPE_EVENT = 3;
    const TYPE_PPS = 4;
    const TYPE_ENGAGE = 5;
    const TYPE_SUBMIT = 6;

    private static $typeMap = [
        Goal::TYPE_DESTINATION => 'destination',
        Goal::TYPE_DURATION => 'duration'
    ];


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return Goal
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Maps type constant value to convenience type name
     *
     * @param int $type
     * @return string|null
     */
    public static function mapType($type)
    {
        return isset(static::$typeMap[ $type ])
            ? static::$typeMap[ $type ]
            : null;
    }

    /**
     * Maps convenience type name to integer constant value
     *
     * @param string $name
     * @return int|null
     */
    public static function reverseType($name)
    {
        $reverse = array_flip(static::$typeMap);

        return isset($reverse[ $name ])
            ? $reverse[ $name ]
            : null;
    }

    /**
     * @return GoalDestination
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param GoalDestination $destination
     * @return Goal
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * @return Experiment
     */
    public function getExperiment()
    {
        return $this->experiment;
    }

    /**
     * @param Experiment $experiment
     * @return Goal
     */
    public function setExperiment($experiment)
    {
        $this->experiment = $experiment;

        return $this;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     * @return Goal
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }
}
