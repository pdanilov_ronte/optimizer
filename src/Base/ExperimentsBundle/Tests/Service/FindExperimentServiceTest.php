<?php


namespace Base\ExperimentsBundle\Tests\Service;

use Base\ExperimentsBundle\Service\CreateExperiment\CreateExperimentProvider;
use Base\ExperimentsBundle\Service\CreateExperiment\CreateExperimentRequest;
use Base\ExperimentsBundle\Service\FindExperiment\FindExperimentService;
use Shared\SoaBundle\Service\RequestData;
use Shared\TestBundle\TestSuite\ServiceTestCase;


class FindExperimentTest extends ServiceTestCase
{
    /** @type int */
    protected static $experiment_id;

    /** @type array */
    protected static $fixture = [
        'title' => 'Tested Found Experiment',
        'type'  => 'ab'
    ];


    public static function setUpBeforeClass()
    {
        /** @type CreateExperimentProvider $provider */
        $provider = static::buildContainer()->get('opt.base.experiment.create.provider');

        $data = new RequestData(static::$fixture);
        $request = new CreateExperimentRequest($data);

        $provider->createExperiment($request);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        } else {
            $experiment = $request->getAttachment()->getExperiment();
            static::$experiment_id = $experiment->getId();
        }
    }

    /**
     * Try to find experiment by stored id
     */
    public function testFindExperiment()
    {
        $this->rebuildContainer();

        /** @type FindExperimentService $service */
        $service = $this->container->get('opt.base.experiment.find');

        $request = $service->findExperiment([
            'id' => static::$experiment_id
        ]);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        }

        /** Test that experiment is found */
        $this->assertTrue($request->isFound());

        $experiment = $request->getAttachment()->getExperiment();

        /** Test that experiment is not empty */
        $this->assertNotEmpty($experiment);
    }
}
