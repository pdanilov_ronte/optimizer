<?php


namespace Base\ExperimentsBundle\Tests\Service;

use Base\ExperimentsBundle\Service\CreateBase\CreateBaseProvider;
use Base\ExperimentsBundle\Service\CreateBase\CreateBaseRequest;
use Base\ExperimentsBundle\Service\CreateExperiment\CreateExperimentProvider;
use Base\ExperimentsBundle\Service\CreateExperiment\CreateExperimentRequest;
use Base\ExperimentsBundle\Service\FindBase\FindBaseProvider;
use Base\ExperimentsBundle\Service\FindBase\FindBaseRequest;
use Shared\SoaBundle\Service\RequestData;
use Shared\TestBundle\TestSuite\ServiceTestCase;


class FindBaseTest extends ServiceTestCase
{
    /** @type int */
    protected static $base_id;

    /** @type array */
    protected static $fixture = [
        'experiment' => [
            'title' => 'Tested Create Base Experiment',
            'type'  => 'ab'
        ],
        'base'       => [
            'title'    => 'Tested Create Base',
            'selector' => '#variant-base-id',
            'code'     => <<<CODE
<div>
    <form method="post">
        <input type="text" value="" placeholder="text value">
    </form>
</div>
CODE

        ]
    ];


    public static function setUpBeforeClass()
    {
        $container = static::buildContainer();

        /** @type CreateExperimentProvider $provider */
        $provider = $container->get('opt.base.experiment.create.provider');

        $data = new RequestData(static::$fixture['experiment']);
        $request = new CreateExperimentRequest($data);

        $provider->createExperiment($request);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        } else {
            $experiment = $request->getAttachment()->getExperiment();
        }

        /** @type CreateBaseProvider $provider */
        $provider = $container->get('opt.base.variant_base.create.provider');

        static::$fixture['base']['experiment_id'] = $experiment->getId();
        $data = new RequestData(static::$fixture['base']);
        $request = new CreateBaseRequest($data);

        $provider->createBase($request);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        } else {
            $base = $request->getAttachment()->getBase();
            static::$base_id = $base->getId();
        }
    }

    public function testFindBase()
    {
        $this->rebuildContainer();

        /** @type FindBaseProvider $provider */
        $provider = $this->container->get('opt.base.variant_base.find.provider');

        $data = new RequestData([
            'id' => static::$base_id
        ]);
        $request = new FindBaseRequest($data);

        $provider->findBase($request);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        }

        /** Test that variant base is found */
        $this->assertTrue($request->isFound());

        $base = $request->getAttachment()->getBase();

        /** Test that the base is not empty */
        $this->assertNotEmpty($base);
    }
}
