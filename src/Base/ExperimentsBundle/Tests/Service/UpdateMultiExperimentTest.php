<?php


namespace Base\ExperimentsBundle\Tests\Service;

use Base\ExperimentsBundle\Entity\Experiment;
use Base\ExperimentsBundle\Service\FindExperiment\FindExperimentProvider;
use Base\ExperimentsBundle\Service\FindExperiment\FindExperimentRequest;
use Shared\SoaBundle\Service\RequestData;
use Shared\TestBundle\TestSuite\ServiceTestCase;
use Base\ExperimentsBundle\Service\CreateExperiment\CreateExperimentProvider;
use Base\ExperimentsBundle\Service\CreateExperiment\CreateExperimentRequest;


class UpdateMultiExperimentTest extends ServiceTestCase
{
    /** @type int */
    protected static $experiment_id;

    /** @type Experiment */
    protected static $experiment;

    /** @type array */
    protected static $fixture = [
        'title' => 'Tested Update Multivariant Experiment',
        'type' => 'multi'
    ];


    public static function setUpBeforeClass()
    {
        /** @type CreateExperimentProvider $provider */
        $provider = static::buildContainer()->get('opt.base.experiment.create.provider');

        $data = new RequestData(static::$fixture);
        $request = new CreateExperimentRequest($data);

        $provider->createExperiment($request);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        } else {
            $experiment = $request->getAttachment()->getExperiment();
            static::$experiment_id = $experiment->getId();
        }
    }

    /**
     * Try to find experiment using FindExperiment service.
     */
    public function testExperimentUpdated()
    {
        $this->rebuildContainer();

        /** @type FindExperimentProvider $provider */
        $provider = $this->container->get('opt.base.experiment.find.provider');

        $data = new RequestData([
            'id' => static::$experiment_id
        ]);
        $request = new FindExperimentRequest($data);

        $provider->findExperiment($request);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        }

        /** Test that experiment is found */
        $this->assertTrue($request->isFound());

        $experiment = $request->getAttachment()->getExperiment();

        /** Test that experiment is not empty */
        $this->assertNotEmpty($experiment);

        static::$experiment = $experiment;
    }

    /**
     * Test that all scalar fields correspond to fixture values.
     *
     * @depends testExperimentCreated
     */
    public function testFieldsCorrect()
    {
        $this->assertEquals(static::$fixture['title'], static::$experiment->getTitle());
    }

    /**
     * Test that state field is set to STATE_NEW
     *
     * @depends testExperimentCreated
     */
    public function testStateCorrect()
    {
        $this->assertEquals(Experiment::STATE_NEW, static::$experiment->getState());
    }

    /**
     * Test that type field is set to TYPE_MULTI
     *
     * @depends testExperimentCreated
     */
    public function testTypeCorrect()
    {
        $this->assertEquals(Experiment::TYPE_MULTI, static::$experiment->getType());
    }
}
