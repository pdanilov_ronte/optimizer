<?php


namespace Base\ExperimentsBundle\Tests\Service;

use Base\ExperimentsBundle\Entity\Variant;
use Base\ExperimentsBundle\Service\CreateBase\CreateBaseProvider;
use Base\ExperimentsBundle\Service\CreateBase\CreateBaseRequest;
use Base\ExperimentsBundle\Service\CreateVariant\CreateVariantProvider;
use Base\ExperimentsBundle\Service\CreateVariant\CreateVariantRequest;
use Base\ExperimentsBundle\Service\FindVariant\FindVariantProvider;
use Base\ExperimentsBundle\Service\FindVariant\FindVariantRequest;
use Shared\SoaBundle\Service\RequestData;
use Shared\TestBundle\TestSuite\ServiceTestCase;
use Base\ExperimentsBundle\Service\CreateExperiment\CreateExperimentProvider;
use Base\ExperimentsBundle\Service\CreateExperiment\CreateExperimentRequest;


class CreateVariantTest extends ServiceTestCase
{
    /** @type int */
    protected static $base_id;

    /** @type int */
    protected static $variant_id;

    /** @type Variant */
    protected static $variant;

    /** @type array */
    protected static $fixture = [
        'experiment' => [
            'title' => 'Tested Create Variant Experiment',
            'type' => 'ab'
        ],
        'base' => [
            'title' => 'Tested Create Variant Base',
            'selector' => '#variant-id',
            'code' => <<<CODE
<div>
    <form method="post">
        <input type="text" value="" placeholder="text value">
    </form>
</div>
CODE

        ],
        'variant' => [
            'title' => 'Tested Create Variant',
            'code' => <<<CODE
<div>
    <form method="post">
        <input type="text" value="" placeholder="optimized text value">
    </form>
</div>
CODE

        ]
    ];


    public static function setUpBeforeClass()
    {
        $container = static::buildContainer();

        /** @type CreateExperimentProvider $provider */
        $provider = $container->get('opt.base.experiment.create.provider');

        $data = new RequestData(static::$fixture['experiment']);
        $request = new CreateExperimentRequest($data);

        $provider->createExperiment($request);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        } else {
            $experiment = $request->getAttachment()->getExperiment();
        }

        /** @type CreateBaseProvider $provider */
        $provider = $container->get('opt.base.variant_base.create.provider');

        static::$fixture['base']['experiment_id'] = $experiment->getId();
        $data = new RequestData(static::$fixture['base']);
        $request = new CreateBaseRequest($data);

        $provider->createBase($request);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        } else {
            $base = $request->getAttachment()->getBase();
            static::$base_id = $base->getId();
        }

        /** @type CreateVariantProvider $provider */
        $provider = $container->get('opt.base.variant.create');

        static::$fixture['variant']['base_id'] = static::$base_id;
        $data = new RequestData(static::$fixture['variant']);
        $request = new CreateVariantRequest($data);

        $provider->createVariant($request);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        } else {
            $variant = $request->getAttachment()->getVariant();
            static::$variant_id = $variant->getId();
        }
    }

    /**
     * Try to find variant using FindVariant service.
     */
    public function testVariantCreated()
    {
        $this->rebuildContainer();

        /** @type FindVariantProvider $provider */
        $provider = $this->container->get('opt.base.variant.find');

        $data = new RequestData([
            'id' => static::$variant_id
        ]);
        $request = new FindVariantRequest($data);

        $provider->findVariant($request);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        }

        /** Test that variant is found */
        $this->assertTrue($request->isFound());

        $variant = $request->getAttachment()->getVariant();

        /** Test that the variant is not empty */
        $this->assertNotEmpty($variant);

        static::$variant = $variant;
    }

    /**
     * Test that all scalar fields correspond to fixture values.
     */
    public function testFieldsCorrect()
    {
        $this->assertEquals(static::$fixture['variant']['title'], static::$variant->getTitle());
        $this->assertEquals(static::$fixture['variant']['code'], static::$variant->getCode()->getValue());
    }

    /**
     * Test that owning variant base field is correct
     */
    public function testBaseCorrect()
    {
        $this->assertEquals(static::$base_id, static::$variant->getBase()->getId());
    }
}
