<?php


namespace Base\ExperimentsBundle\Tests\Service;

use Base\ExperimentsBundle\Service\RunExperiment\RunExperimentProvider;
use Base\ExperimentsBundle\Service\RunExperiment\RunExperimentRequest;
use Shared\SoaBundle\Service\RequestData;
use Shared\TestBundle\TestSuite\ServiceTestCase;
use Base\ExperimentsBundle\Service\CreateExperiment\CreateExperimentProvider;
use Base\ExperimentsBundle\Service\CreateExperiment\CreateExperimentRequest;


class RunExperimentTest extends ServiceTestCase
{
    /** @type int */
    protected static $experiment_id;

    /** @type array */
    protected static $fixture = [
        'title' => 'Tested Run Experiment',
        'type' => 'ab'
    ];


    public static function setUpBeforeClass()
    {
        /** @type CreateExperimentProvider $provider */
        $provider = static::buildContainer()->get('opt.base.experiment.create.provider');

        $data = new RequestData(static::$fixture);
        $request = new CreateExperimentRequest($data);

        $provider->createExperiment($request);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        } else {
            $experiment = $request->getAttachment()->getExperiment();
            static::$experiment_id = $experiment->getId();
        }
    }

    public function testRunExperiment()
    {
        $this->rebuildContainer();

        $data = new RequestData([
            'id' => static::$experiment_id
        ]);

        /** @type RunExperimentProvider $provider */
        $provider = $this->container->get('opt.base.experiment.run');

        $request = new RunExperimentRequest($data);
        $provider->runExperiment($request);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        }

        $this->assertTrue($request->isSuccessful());
        $this->assertFalse($request->wasRunning());
    }

    public function testAlreadyRunningExperiment()
    {
        $this->rebuildContainer();

        $data = new RequestData([
            'id' => static::$experiment_id
        ]);

        /** @type RunExperimentProvider $provider */
        $provider = $this->container->get('opt.base.experiment.run');

        $request = new RunExperimentRequest($data);
        $provider->runExperiment($request);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        }

        $this->assertFalse($request->isSuccessful());
        $this->assertTrue($request->wasRunning());
    }
}
