<?php


namespace Base\ExperimentsBundle\Tests\Service;

use Base\ExperimentsBundle\Service\CreateExperiment\CreateExperimentProvider;
use Base\ExperimentsBundle\Service\CreateExperiment\CreateExperimentRequest;
use Base\ExperimentsBundle\Service\DeleteExperiment\DeleteExperimentProvider;
use Base\ExperimentsBundle\Service\DeleteExperiment\DeleteExperimentRequest;
use Base\ExperimentsBundle\Service\FindExperiment\FindExperimentProvider;
use Base\ExperimentsBundle\Service\FindExperiment\FindExperimentRequest;
use Shared\SoaBundle\Service\RequestData;
use Shared\TestBundle\TestSuite\ServiceTestCase;


class DeleteExperimentTest extends ServiceTestCase
{
    /** @type int */
    protected static $experiment_id;

    /** @type array */
    protected static $fixture = [
        'title' => 'Tested Deleted Experiment',
        'type'  => 'ab'
    ];


    public static function setUpBeforeClass()
    {
        /** @type CreateExperimentProvider $provider */
        $provider = static::buildContainer()->get('opt.base.experiment.create.provider');

        $data = new RequestData(static::$fixture);
        $request = new CreateExperimentRequest($data);

        $provider->createExperiment($request);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        } else {
            $experiment = $request->getAttachment()->getExperiment();
            static::$experiment_id = $experiment->getId();
        }
    }

    public function testDeleteExperimentTest()
    {
        $this->rebuildContainer();

        /** @type DeleteExperimentProvider $provider */
        $provider = $this->container->get('opt.base.experiment.delete');

        $data = new RequestData([
            'id' => static::$experiment_id
        ]);
        $request = new DeleteExperimentRequest($data);

        $provider->deleteExperiment($request);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        }

        $this->assertTrue($request->isDeleted());
    }

    public function testExperimentIsDeleted()
    {
        $this->rebuildContainer();

        /** @type FindExperimentProvider $provider */
        $provider = $this->container->get('opt.base.experiment.find.provider');

        $data = new RequestData([
            'id' => static::$experiment_id
        ]);
        $request = new FindExperimentRequest($data);

        $provider->findExperiment($request);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        }

        /** Test that experiment can not be found */
        $this->assertFalse($request->isFound());
    }
}
