<?php


namespace Base\ExperimentsBundle\Tests\Service;

use Base\ExperimentsBundle\Entity\Experiment;
use Base\ExperimentsBundle\Entity\VariantBase;
use Base\ExperimentsBundle\Service\CreateBase\CreateBaseProvider;
use Base\ExperimentsBundle\Service\CreateBase\CreateBaseRequest;
use Base\ExperimentsBundle\Service\FindBase\FindBaseProvider;
use Base\ExperimentsBundle\Service\FindBase\FindBaseRequest;
use Shared\SoaBundle\Service\RequestData;
use Shared\TestBundle\TestSuite\ServiceTestCase;
use Base\ExperimentsBundle\Service\CreateExperiment\CreateExperimentProvider;
use Base\ExperimentsBundle\Service\CreateExperiment\CreateExperimentRequest;


class CreateBaseTest extends ServiceTestCase
{
    /** @type Experiment */
    protected static $experiment;

    /** @type int */
    protected static $base_id;

    /** @type VariantBase */
    protected static $base;

    /** @type array */
    protected static $fixture = [
        'experiment' => [
            'title' => 'Tested Create Base Experiment',
            'type' => 'ab'
        ],
        'base' => [
            'title' => 'Tested Create Base',
            'selector' => '#variant-base-id',
            'code' => <<<CODE
<div>
    <form method="post">
        <input type="text" value="" placeholder="text value">
    </form>
</div>
CODE

        ]
    ];


    public static function setUpBeforeClass()
    {
        $container = static::buildContainer();

        /** @type CreateExperimentProvider $provider */
        $provider = $container->get('opt.base.experiment.create.provider');

        $data = new RequestData(static::$fixture['experiment']);
        $request = new CreateExperimentRequest($data);

        $provider->createExperiment($request);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        } else {
            static::$experiment = $request->getAttachment()->getExperiment();
        }

        /** @type CreateBaseProvider $provider */
        $provider = $container->get('opt.base.variant_base.create.provider');

        static::$fixture['base']['experiment_id'] = static::$experiment->getId();
        $data = new RequestData(static::$fixture['base']);
        $request = new CreateBaseRequest($data);

        $provider->createBase($request);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        } else {
            $base = $request->getAttachment()->getBase();
            static::$base_id = $base->getId();
        }
    }

    /**
     * Try to find variant base using FindBase service.
     */
    public function testBaseCreated()
    {
        $this->rebuildContainer();

        /** @type FindBaseProvider $provider */
        $provider = $this->container->get('opt.base.variant_base.find.provider');

        $data = new RequestData([
            'id' => static::$base_id
        ]);
        $request = new FindBaseRequest($data);

        $provider->findBase($request);

        if ($request->isFailed()) {
            throw new \Exception($request->getException()->getMessage());
        }

        /** Test that variant base is found */
        $this->assertTrue($request->isFound());

        $base = $request->getAttachment()->getBase();

        /** Test that the base is not empty */
        $this->assertNotEmpty($base);

        static::$base = $base;
    }

    /**
     * Test that all scalar fields correspond to fixture values.
     *
     * @depends testBaseCreated
     */
    public function testFieldsCorrect()
    {
        $this->assertEquals(static::$fixture['base']['title'], static::$base->getTitle());
        $this->assertEquals(static::$fixture['base']['selector'], static::$base->getSelector());
        $this->assertEquals(static::$fixture['base']['code'], static::$base->getCode()->getValue());
    }

    /**
     * Test that the base is connected to correct experiment.
     *
     * @depends testBaseCreated
     */
    public function testExperimentCorrect()
    {
        $this->assertEquals(static::$experiment->getBase()->getId(), static::$base->getId());
    }
}
