<?php

namespace Base\ExperimentsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class VariationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id')
            ->add('title')
            ->add('number')
            ->add('weight')
            ->add('state')
            ->add('enabled');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Base\ExperimentsBundle\Entity\Variation',
        ]);
    }

    public function getName()
    {
        return '';
    }
}
