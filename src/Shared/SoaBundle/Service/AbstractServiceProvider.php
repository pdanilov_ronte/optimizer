<?php


namespace Shared\SoaBundle\Service;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;


abstract class AbstractServiceProvider
{
    use ContainerAwareTrait;
}
