<?php

namespace Shared\SoaBundle\Service;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;


abstract class AbstractServiceRequester
{
    use ContainerAwareTrait;

    /**
     * @param AbstractServiceRequest $request
     * @param array $data
     * @return AbstractServiceRequest
     */
    protected function prepareRequest(AbstractServiceRequest $request, $data)
    {
        $requestData = new RequestData($data);
        $request->setRequestData($requestData);

        return $request;
    }

    /**
     * @param AbstractServiceProvider $provider
     * @return AbstractServiceProvider
     */
    protected function prepareProvider(AbstractServiceProvider $provider)
    {
        $provider->setContainer($this->container);

        return $provider;
    }

    /**
     * @param array $data
     * @return AbstractServiceRequest
     */
    abstract protected function createRequest(array $data);

    /**
     * @return AbstractServiceProvider
     */
    abstract protected function createProvider();
}
