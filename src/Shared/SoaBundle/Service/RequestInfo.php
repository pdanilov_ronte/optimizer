<?php

namespace Shared\SoaBundle\Service;


class RequestInfo
{
    /**
     * @var string
     */
    private $remoteAddr;


    /**
     * @return string
     */
    public function getRemoteAddr()
    {
        return $this->remoteAddr;
    }

    /**
     * @param string $remoteAddr
     */
    public function setRemoteAddr($remoteAddr)
    {
        $this->remoteAddr = $remoteAddr;
    }
}
