<?php

namespace Shared\SoaBundle\Service;


abstract class AbstractServiceRequest
{
    /**
     * @var ServiceRequestAttachment
     */
    protected $attachment;

    /**
     * @var ServiceException
     */
    protected $exception;

    /**
     * @var RequestData
     */
    protected $data;

    /**
     * @var RequestInfo
     */
    protected $info;


    /**
     * @param RequestData|null $data optional Request data
     */
    public function __construct(RequestData $data = null)
    {
        if (isset($data)) {
            $this->setRequestData($data);
        }
    }

    /**
     * @return ServiceRequestAttachment|null
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * @param ServiceRequestAttachment $attachment
     */
    public function setAttachment(ServiceRequestAttachment $attachment)
    {
        $this->attachment = $attachment;
    }

    /**
     * @return ServiceException
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * @param ServiceException $exception
     */
    public function setException(ServiceException $exception)
    {
        $this->exception = $exception;
    }

    /**
     * Convenience method to create exception and set it for the request.
     *
     * @param string $error
     * @return void
     */
    public function setError($error)
    {
        $this->setException(
            new ServiceException($error)
        );

        return;
    }

    /**
     * Returns true if there was an exception
     *
     * @return bool
     */
    public function isFailed()
    {
        return !empty($this->exception);
    }

    /**
     * @return RequestData
     */
    public function getRequestData()
    {
        return $this->data;
    }

    /**
     * @param RequestData $data
     */
    public function setRequestData(RequestData $data)
    {
        $this->data = $data;
    }

    /**
     * @return RequestInfo
     */
    public function getRequestInfo()
    {
        return $this->info;
    }

    /**
     * @param RequestInfo $data
     */
    public function setRequestInfo(RequestInfo $info)
    {
        $this->info = $info;
    }
}
