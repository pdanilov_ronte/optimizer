<?php

namespace Shared\SoaBundle\Service;


class RequestData
{
    /**
     * @type []
     */
    private $data = [];


    /**
     * @param array|null $data optional
     */
    public function __construct($data = null)
    {
        if (isset($data)) {
            $this->setData($data);
        }
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return RequestData
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }
}
