<?php


namespace Shared\TestBundle\TestSuite;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Base class for testing services or for test cases requiring service container to run.
 *
 * @class   ServiceTestCase
 * @package Shared\TestBundle\TestSuite
 * @author  PM:/ <pm@spiral.ninja>
 */
abstract class ServiceTestCase extends KernelTestCase implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;


    /**
     * @throws \Exception
     */
    protected function rebuildContainer()
    {
        $this->setContainer(static::buildContainer());
    }

    /**
     * @return ContainerInterface
     * @throws \Exception
     */
    protected static function buildContainer()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        $container = $kernel->getContainer();

        if (empty($container)) {
            throw new \Exception('Failed to get service container from the kernel');
        }

        return $container;
    }

    /**
     * @inheritdoc
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}