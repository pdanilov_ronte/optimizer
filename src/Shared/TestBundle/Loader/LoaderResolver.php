<?php

namespace Shared\TestBundle\Loader;


/**
 * Interface LoaderResolverInterface
 * @package Shared\TestBundle\Loader
 * @author PM:/ <pm@spiral.ninja>
 */
interface LoaderResolverInterface
{
    /**
     * Returns a loader able to load the container.
     *
     * @param Loadable $container Container instance which needs to be loaded
     *
     * @return LoaderInterface|bool Appropriate loader instance or false if none is able to load the resource
     */
    public function resolve(Loadable $container);

    /**
     * Adds loader to resolver.
     *
     * @param LoaderInterface $loader Loader instance
     *
     * @return void
     */
    public function addLoader(LoaderInterface $loader);
}


/**
 * Class which keeps loader contractors and returns appropriate loaders for different loadable containers.
 *
 * Class LoaderResolver
 * @package Shared\TestBundle\Loader
 * @author PM:/ <pm@spiral.ninja>
 */
class LoaderResolver implements LoaderResolverInterface
{
    /**
     * @var LoaderContainer
     */
    private $loaders;


    /**
     * @param LoaderContainer $loaders
     */
    public function __construct(LoaderContainer $loaders)
    {
        $this->loaders = $loaders;
    }

    /**
     * @inheritdoc
     */
    public function resolve(Loadable $container)
    {
        foreach ($this->loaders as $loader) {
            if ($loader->supports($container)) {
                return $loader;
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function addLoader(LoaderInterface $loader)
    {
        $this->loaders->add($loader);
    }
}
