<?php

namespace Shared\TestBundle\Loader;


/**
 * Collection container for loadable objects.
 *
 * Class LoadableCollection
 * @package Shared\TestBundle\Loader
 * @author PM:/ <pm@spiral.ninja>
 */
class LoadableCollection implements \Iterator
{
    /**
     * @var Loadable[]
     */
    private $elements = [];


    public function add(Loadable $element)
    {
        $this->elements[] = $element;
    }

    /**
     * @return Loadable
     */
    public function current()
    {
        return current($this->elements);
    }

    /**
     * @inheritdoc
     */
    public function next()
    {
        next($this->elements);
    }

    /**
     * @inheritdoc
     */
    public function key()
    {
        return key($this->elements);
    }

    /**
     * @inheritdoc
     */
    public function valid()
    {
        return false !== $this->current();
    }

    /**
     * @inheritdoc
     */
    public function rewind()
    {
        reset($this->elements);
    }
}
