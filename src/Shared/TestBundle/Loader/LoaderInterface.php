<?php

namespace Shared\TestBundle\Loader;


/**
 * Interface LoaderInterface
 * @package Shared\TestBundle\Loader
 * @author PM:/ <pm@spiral.ninja>
 */
interface LoaderInterface
{
    /**
     * Loads container into the loader.
     *
     * @param Loadable $container The container to be loaded
     *
     * @return mixed
     * @throws \Exception
     */
    public function load(Loadable $container);

    /**
     * Checks whether this class supports the given container.
     *
     * @param Loadable $container The container to be checked
     *
     * @return bool
     */
    public function supports(Loadable $container);
}
