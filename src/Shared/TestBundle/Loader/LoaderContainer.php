<?php

namespace Shared\TestBundle\Loader;


/**
 * Interface LoaderContainerInterface
 * @package Shared\TestBundle\Loader
 * @author PM:/ <pm@spiral.ninja>
 */
interface LoaderContainerInterface
{
    /**
     * Adds loader to container.
     *
     * @param LoaderInterface $loader The loader
     * @return void
     */
    public function add(LoaderInterface $loader);
}


/**
 * Interface LoaderContainerIterator
 * @package Shared\TestBundle\Loader
 * @author PM:/ <pm@spiral.ninja>
 */
interface LoaderContainerIterator extends \Iterator
{
    /**
     * @return LoaderInterface
     */
    public function current();
}


/**
 * Container used to keep loaders and iterate through them.
 *
 * Class LoaderContainer
 * @package Shared\TestBundle\Loader
 * @author PM:/ <pm@spiral.ninja>
 */
class LoaderContainer implements LoaderContainerInterface, LoaderContainerIterator
{
    /**
     * @var LoaderInterface[]
     */
    private $loaders = [];


    /**
     * @param LoaderInterface[] $loaders
     */
    public function __construct($loaders = [])
    {
        foreach ($loaders as $loader)
        {
            $this->add($loader);
        }
    }

    /**
     * Adds loader to container.
     *
     * @param LoaderInterface $loader The loader
     *
     * @return void
     */
    public function add(LoaderInterface $loader)
    {
        $this->loaders[] = $loader;
    }

    /**
     * @inheritdoc
     */
    public function current()
    {
        return current($this->loaders);
    }

    /**
     * @inheritdoc
     */
    public function next()
    {
        next($this->loaders);
    }

    /**
     * @inheritdoc
     */
    public function key()
    {
        return key($this->loaders);
    }

    /**
     * @inheritdoc
     */
    public function valid()
    {
        return false !== $this->current();
    }

    /**
     * @inheritdoc
     */
    public function rewind()
    {
        reset($this->loaders);
    }
}
