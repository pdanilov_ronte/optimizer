<?php

namespace Shared\TestBundle\Loader\Resource;

use Shared\TestBundle\Loader\Loadable;


/**
 * Interface ResourceInterface
 * @package Shared\TestBundle\Loader\Resource
 * @author PM:/ <pm@spiral.ninja>
 */
interface ResourceInterface extends Loadable
{
    /**
     * Returns type of the resource.
     *
     * @return int
     */
    public function getResourceType();

    /**
     * Returns type of content provided by the resource
     *
     * @return int
     */
    public function getContentType();

    public function __toString();
}


/**
 * Base container class for loadable resources.
 *
 * Class Resource
 * @package Shared\TestBundle\Loader\Resource
 * @author PM:/ <pm@spiral.ninja>
 */
abstract class Resource implements ResourceInterface
{
    const RESOURCE_TYPE_FILE = 1;

    const CONTENT_TYPE_YAML = 1;

    /**
     * @var int
     */
    protected $resourceType;

    /**
     * @var int
     */
    protected $contentType;


    /**
     * @param int $resourceType
     * @param int $contentType
     */
    public function __construct($resourceType, $contentType)
    {
        if (!in_array($resourceType, [
            static::RESOURCE_TYPE_FILE
        ])) {
            throw new \InvalidArgumentException(sprintf('Invalid resource type %s passed', $resourceType));
        }

        if (!in_array($contentType, [
            static::CONTENT_TYPE_YAML
        ])) {
            throw new \InvalidArgumentException(sprintf('Invalid content type %s passed', $resourceType));
        }

        $this->resourceType = $resourceType;
        $this->contentType = $contentType;
    }

    /**
     * @inheritdoc
     */
    public function getResourceType()
    {
        return $this->resourceType;
    }

    /**
     * @inheritdoc
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        return $this->getContentType();
    }
}
