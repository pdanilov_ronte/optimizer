<?php

namespace Shared\TestBundle\Loader\Resource;

use SplFileInfo;


/**
 * Loadable resource container providing SplFileInfo aggregation.
 *
 * Class FileResource
 * @package Shared\TestBundle\Loader\Resource
 * @author PM:/ <pm@spiral.ninja>
 */
class FileResource extends Resource
{
    /**
     * @var SplFileInfo
     */
    private $fileInfo;

    static $typeExtensions = [
        self::CONTENT_TYPE_YAML => 'yml'
    ];

    /**
     * @var string
     */
    private $content;


    /**
     * @param SplFileInfo $fileInfo
     */
    public function __construct(SplFileInfo $fileInfo)
    {
        if (!$fileInfo->isFile() || !$fileInfo->isReadable()) {
            throw new \InvalidArgumentException(sprintf('Resource %s is not a file type or is not readable', (string) $fileInfo));
        }

        $contentType = $this->guessContentType($fileInfo);
        if ($contentType === false) {
            throw new \RuntimeException('Cannot guess content type for file %s', (string) $fileInfo);
        }

        $this->fileInfo = $fileInfo;

        parent::__construct(static::RESOURCE_TYPE_FILE, $contentType);
    }


    /**
     * @inheritdoc
     */
    public function load()
    {
        $file = $this->fileInfo->openFile();

        $this->content = '';

        while (!$file->eof()) {
            $this->content .= $file->fgets();
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Try to guess content type by file extension.
     *
     * @param SplFileInfo $fileInfo File info object
     *
     * @return int|false Guessed content type or false of no appropriate type found
     */
    private function guessContentType(SplFileInfo $fileInfo)
    {
        $fileExtension = $fileInfo->getExtension();

        foreach (self::$typeExtensions as $type => $ext) {
            if ($ext == $fileExtension) {
                return $type;
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return $this->fileInfo->getFilename();
    }
}
