<?php

namespace Shared\TestBundle\Loader\Resource;

use Symfony\Component\Finder\Finder;
use Shared\TestBundle\Loader\LoadableCollection;


/**
 * Loadable collection adapter class for Finder integration.
 *
 * Class FileCollection
 * @package Shared\TestBundle\ResourceLoader
 * @author PM:/ <pm@spiral.ninja>
 */
class FileCollection extends LoadableCollection
{
    /**
     * Factory method that builds a collection of loadable file resources from the Finder instance.
     *
     * @param Finder $finder
     *
     * @return FileCollection
     */
    public static function createFromFinder(Finder $finder)
    {
        $collection = new FileCollection();

        foreach ($finder as $fileInfo) {
            $container = new FileResource($fileInfo);
            $collection->add($container);
        }

        return $collection;
    }
}
