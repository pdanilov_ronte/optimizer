<?php

namespace Shared\TestBundle\Loader;


/**
 * Interface for loadable containers.
 *
 * Interface Loadable
 * @package Shared\TestBundle\Loader
 * @author PM:/ <pm@spiral.ninja>
 */
interface Loadable
{
    /**
     * Loads container. Returns itself for convenience.
     *
     * @return Loadable
     */
    public function load();

    /**
     * Returns content of container.
     *
     * @return mixed
     */
    public function getContent();

    /**
     * Returns container type.
     *
     * @return int
     */
    public function getType();
}
