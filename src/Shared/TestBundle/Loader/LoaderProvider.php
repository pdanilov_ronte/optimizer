<?php

namespace Shared\TestBundle\Loader;


/**
 * Interface ProviderInterface
 * @package Shared\TestBundle\Loader
 * @author PM:/ <pm@spiral.ninja>
 */
interface ProviderInterface extends LoaderInterface
{
    /**
     * Loads collection of loadable containers.
     *
     * @param LoadableCollection $collection
     * @return void
     */
    public function loadCollection(LoadableCollection $collection);
}


/**
 * Base class which delegates loading tasks to its contractors.
 *
 * Class LoaderProvider
 * @package Shared\TestBundle\Loader
 * @author PM:/ <pm@spiral.ninja>
 */
abstract class LoaderProvider implements ProviderInterface
{
    /**
     * @var LoaderContainer
     */
    private $loaders;

    /**
     * @var LoaderResolver
     */
    private $resolver;


    /**
     * @param LoaderInterface[] $contractors
     */
    public function __construct($contractors = [])
    {
        $this->loaders = new LoaderContainer($contractors);
        $this->resolver = new LoaderResolver($this->loaders);
    }

    /**
     * @inheritdoc
     */
    public function load(Loadable $container)
    {
        $loader = $this->resolver->resolve($container);

        if ($loader === false) {
            throw new \Exception(sprintf('Cannot load container %s - failed to get valid loader.', $container));
        }

        return $loader->load($container);
    }

    /**
     * @inheritdoc
     */
    public function supports(Loadable $container)
    {
        return false !== $this->resolver->resolve($container);
    }
}
