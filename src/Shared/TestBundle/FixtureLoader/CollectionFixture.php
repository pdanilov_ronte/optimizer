<?php


namespace Shared\TestBundle\FixtureLoader;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Exception, InvalidArgumentException;


/**
 * Interface CollectionFixtureInterface
 * @package Shared\TestBundle\FixtureLoader
 * @author PM:/ <pm@spiral.ninja>
 */
interface CollectionFixtureInterface extends FixtureInterface
{
    /**
     * Get class metadata for entities used by this fixture
     *
     * @return ClassMetadata
     */
    function getClassMetadata();

    /**
     * Set array of source data entries
     *
     * @param array $data
     *
     * @return void
     */
    function setData(array $data);
}


/**
 * Parametrized collection fixture class.
 * Builds a list of fixture entities of specific class from plain array of data entries.
 *
 * Class CollectionFixture
 * @package Shared\TestBundle\FixtureLoader
 * @author PM:/ <pm@spiral.ninja>
 */
class CollectionFixture implements CollectionFixtureInterface
{
    /**
     * @var array
     */
    private $data = [];

    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var ClassMetadata
     */
    protected $classMetadata;


    /**
     * @param ClassMetadata $metadata
     * @param array $data
     */
    public function __construct(ClassMetadata $metadata, array $data = null)
    {
        $this->collection = new ArrayCollection();
        $this->classMetadata = $metadata;

        if (isset($data)) {
            $this->data = $data;
        }
    }

    /**
     * Loads data fixtures into database using passed ObjectManager.
     *
     * @param ObjectManager $manager
     * @throws Exception
     */
    public function load(ObjectManager $manager)
    {
        $this->prepare($manager);

        foreach ($this->collection as $entity) {
            $manager->persist($entity);
        }

        $manager->flush();
    }

    /**
     * Builds collection of fixture entities to be be inserted into database.
     *
     * @param ObjectManager $manager
     */
    protected function prepare(ObjectManager $manager)
    {
        foreach ($this->data as $entry) {
            $entity = $this->buildEntity($manager, $entry);
            $this->collection->add($entity);
        }
    }

    /**
     * Builds an entity from passed data entry
     *
     * @param ObjectManager $manager
     * @param $data array
     *
     * @return mixed
     * @throws InvalidArgumentException
     */
    protected function buildEntity(ObjectManager $manager, $data)
    {
        $class = $this->classMetadata->getReflectionClass();
        $entity = new $class->name;

        foreach ($data as $field => $value) {

            if ($this->classMetadata->isIdentifier($field)
                || $this->classMetadata->hasField($field)) {

                $method = 'set'.ucfirst($field);
                $entity->$method($value);

            } elseif ($this->classMetadata->isSingleValuedAssociation($field)) {

                $targetClass = $this->classMetadata->getAssociationTargetClass($field);
                $targetEntity = $manager->getRepository($targetClass)->find($value);

                if (empty($targetEntity)) {
                    throw new InvalidArgumentException(sprintf('Target entity %s(%s) not found', $targetClass, $value));
                }

                $method = 'set'.ucfirst($field);
                $entity->$method($targetEntity);

            } else {

                $type = $this->classMetadata->getTypeOfField($field);
                throw new InvalidArgumentException(sprintf('Field type %s not supported', $type));
            }
        }

        return $entity;
    }

    /**
     * @inheritdoc
     */
    function getClassMetadata()
    {
        return $this->classMetadata;
    }

    /**
     * @inheritdoc
     */
    function setData(array $data)
    {
        $this->data = $data;
    }
}
