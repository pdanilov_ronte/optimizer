<?php

namespace Shared\TestBundle\FixtureLoader;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Yaml\Yaml;
use Shared\TestBundle\Loader\Loadable;
use Shared\TestBundle\Loader\LoaderInterface;
use Shared\TestBundle\Loader\Resource\Resource;


/**
 * YAML fixture loader contractor.
 *
 * Does not actually load fixture into database, only prepares them to be loaded from the outside provider.
 *
 * Class YamlFixtureLoader
 * @package Shared\TestBundle\FixtureLoader
 * @author PM:/ <pm@spiral.ninja>
 */
class YamlFixtureLoader implements LoaderInterface
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var string
     */
    private $mappings;


    public function __construct(EntityManager $em, $mappings)
    {
        $this->em = $em;
        $this->mappings = $mappings;
    }

    /**
     * @inheritdoc
     */
    public function load(Loadable $container)
    {
        $content = $container->load()->getContent();
        $parsed = Yaml::parse($content);

        $factory = $this->em->getMetadataFactory();
        $fixtures = [];

        foreach ($parsed as $entity => $data) {
            $meta = $factory->getMetadataFor($this->mappings.':'.$entity);
            $fixture = new CollectionFixture($meta, $data);
            $fixtures[] = $fixture;
        }

        return $fixtures;
    }

    /**
     * @inheritdoc
     */
    public function supports(Loadable $container)
    {
        return $container->getType() === Resource::CONTENT_TYPE_YAML;
    }
}
