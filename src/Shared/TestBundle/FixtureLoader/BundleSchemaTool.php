<?php

namespace Shared\TestBundle\FixtureLoader;

use Doctrine\Bundle\DoctrineBundle\Mapping\ClassMetadataCollection;
use Doctrine\Bundle\DoctrineBundle\Mapping\DisconnectedMetadataFactory;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;


/**
 * Helper class that can drop existing schema and build new one from a given bundle metadata
 *
 * Class BundleSchemaTool
 * @package Shared\TestBundle\FixtureLoader
 * @author PM:/ <pm@spiral.ninja>
 */
class BundleSchemaTool
{
    /**
     * Loads metadata from a bundle and creates database schema.
     *
     * @param BundleInterface $bundle Bundle to load metadata from
     * @param ManagerRegistry $registry Doctrine manager registry
     * @param EntityManagerInterface $em Entity manager for creating schema
     */
    public function create(BundleInterface $bundle, ManagerRegistry $registry, EntityManagerInterface $em)
    {
        $metadata = $this->loadBundleMetadata($bundle, $registry);
        $this->createSchema($em, $metadata);
    }

    /**
     * @param BundleInterface $bundle
     * @param ManagerRegistry $registry
     * @return ClassMetadataCollection
     */
    protected function loadBundleMetadata(BundleInterface $bundle, ManagerRegistry $registry)
    {
        $factory = new DisconnectedMetadataFactory($registry);
        $metadata = $factory->getBundleMetadata($bundle);

        return $metadata;
    }

    /**
     * @param EntityManagerInterface $em
     * @param ClassMetadataCollection $metadata
     */
    protected function createSchema(EntityManagerInterface $em, ClassMetadataCollection $metadata)
    {
        $st = new SchemaTool($em);
        $st->dropDatabase();
        $st->createSchema($metadata->getMetadata());
    }
}
