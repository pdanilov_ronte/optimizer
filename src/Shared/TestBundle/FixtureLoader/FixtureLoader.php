<?php

namespace Shared\TestBundle\FixtureLoader;

use Doctrine\Common\DataFixtures\Executor\AbstractExecutor;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use Shared\TestBundle\Loader\Loadable;
use Shared\TestBundle\Loader\LoadableCollection;
use Shared\TestBundle\Loader\LoaderInterface;
use Shared\TestBundle\Loader\LoaderProvider;


/**
 * Delegating provider class for loading fixtures.
 *
 * Class FixtureLoader
 * @package Shared\TestBundle\FixtureLoader
 * @author PM:/ <pm@spiral.ninja>
 */
class FixtureLoader extends LoaderProvider
{
    /**
     * @var AbstractExecutor
     */
    protected $executor;

    /**
     * @var bool
     */
    protected $append;


    /**
     * @param LoaderInterface[] $contractors
     * @param AbstractExecutor $executor
     * @param bool $append
     */
    public function __construct($contractors, AbstractExecutor $executor, $append = false)
    {
        parent::__construct($contractors);

        $this->executor = $executor;
        $this->append = $append;
    }

    /**
     * Factory method to build loader provider.
     *
     * @param EntityManager $em Entity manager instance
     * @param string $mappings Name of the bundle which will be used to load mappings from
     * @param bool $append Whether to purge existing data before loading fixtures
     *
     * @return FixtureLoader
     */
    public static function create(EntityManager $em, $mappings, $append = false)
    {
        $contractors = [
            new YamlFixtureLoader($em, $mappings)
        ];

        $purger = new ORMPurger($em);

        // It's necessary to truncate auto_increment fields on purge.
        $purger->setPurgeMode(ORMPurger::PURGE_MODE_TRUNCATE);

        $executor = new ORMExecutor($em, $purger);

        return new static($contractors, $executor, $append);
    }

    /**
     * @param Loadable $container
     * @return void
     */
    public function load(Loadable $container)
    {
        $fixtures = parent::load($container);

        /**
         * Purge data manually to disable foreign checks, then pass append=true to executor.
         */
        if ($this->append === false) {
            /**
             * @var ORMPurger $purger
             */
            $purger = $this->executor->getPurger();
            $purger->getObjectManager()->getConnection()->exec('SET FOREIGN_KEY_CHECKS = 0');
            $purger->purge();
            $purger->getObjectManager()->getConnection()->exec('SET FOREIGN_KEY_CHECKS = 1');
        }

        $this->executor->execute($fixtures, true);
    }

    public function loadCollection(LoadableCollection $collection)
    {
        foreach ($collection as $container) {
            $this->load($container);
        }
    }
}
