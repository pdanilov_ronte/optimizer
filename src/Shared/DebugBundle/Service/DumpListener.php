<?php

namespace Shared\DebugBundle\Service;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\VarDumper\Cloner\ClonerInterface;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;
use Symfony\Component\VarDumper\VarDumper;

/**
 * Configures dump() handler.
 */
class DumpListener implements EventSubscriberInterface
{
    private $cloner;

    /**
     * @param ClonerInterface $cloner Cloner service.
     */
    public function __construct(ClonerInterface $cloner)
    {
        $this->cloner = $cloner;
    }

    public function configure()
    {
        $cloner = $this->cloner;

        $dumper = new HtmlDumper();
        $dumper->setOutput('file:///dev/null');

        VarDumper::setHandler(function ($var) use ($cloner, $dumper) {
            $dumper->dump($cloner->cloneVar($var));
        });
    }

    public static function getSubscribedEvents()
    {
        return array(KernelEvents::REQUEST => array('configure', 512));
    }
}
