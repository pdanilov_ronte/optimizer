#! /bin/sh

### BEGIN INIT INFO
# Provides:          stats
# Required-Start:    $local_fs $remote_fs
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Collects online analytics data for strongoptimizer project
# Description: Collects online analytics data for strongoptimizer project
### END INIT INFO

. /lib/lsb/init-functions

N=/etc/init.d/stats

set -e

case "$1" in
  start)
        if [ -d /var/www/production/stats/receiver ]
        then
        /var/www/production/stats/receiver/bin/receiver -D /var/db/so/stats.db
        fi
        ;;
  stop)
    if [ -f /var/run/so/stats.pid ]
    then
      kill `cat /var/run/so/stats.pid`
    fi
    ;;
  reload)
    if [ -f /var/run/so/stats.pid ]
    then
      kill -HUP `cat /var/run/so/stats.pid`
    fi
    ;;
  sync)
    if [ -f /var/run/so/stats.pid ]
    then
      kill -USR1 `cat /var/run/so/stats.pid`
    fi
    ;;
  restart|force-reload|status)
        ;;
  *)
        echo "Usage: $N {start|stop|reload|sync|restart|force-reload|status}" >&2
        exit 1
        ;;
esac

exit 0
