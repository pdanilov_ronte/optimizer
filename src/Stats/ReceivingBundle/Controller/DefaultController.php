<?php

namespace Stats\ReceivingBundle\Controller;

use Doctrine\ORM\EntityManager;
use Stats\ReceivingBundle\Entity\Hit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use /** @noinspection PhpUnusedAliasInspection */
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * Stats collecting endpoint prototype
 *
 * @package Stats\ReceivingBundle\Controller
 * @author  PM:/ <pm@spiral.ninja>
 */
class DefaultController extends Controller
{
    /**
     * Receive and store page hit data.
     *
     * @Route("/hit")
     * @param Request $request
     * @return JsonResponse
     */
    public function hitAction(Request $request)
    {
        $hit = new Hit();

        $hit->setExperiment($request->get('experiment'));
        $hit->setVariation($request->get('variation'));
        $hit->setUrl($request->get('url'));
        $hit->setReferer($request->get('referer'));
        $hit->setAcceptLanguage($_SERVER['HTTP_ACCEPT_LANGUAGE']);
        $hit->setIp($_SERVER['REMOTE_ADDR']);
        $hit->setCookie(serialize($_COOKIE));

        /** @type EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $em->persist($hit);

        $em->flush();

        return new JsonResponse();
    }
}
