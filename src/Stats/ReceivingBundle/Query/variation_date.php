<?php

ini_set('display_errors', 1);
ini_set('log_errors', 1);

$experiment_id_ = @$_GET['experiment_id'];

if (empty($experiment_id_)) {
    print "Parameter `experiment_id` required";
    exit;
}

if (!preg_match('|^\d+$|', $experiment_id_)) {
    print "Invalid experiment id";
    exit;
}

function exit500()
{
    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
    exit;
}

$mysqli = new mysqli('localhost', 'olap', 'D$B$XAP8$@PVBSYzF9eSEt2MDP#XK#@^', 'warehouse');
if ($mysqli->connect_errno) {
    trigger_error('Failed to connect to MySQL: (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error, E_USER_ERROR);
    exit500();
}


$query = <<<Q
SELECT
    dv.variation, dd.date_value, f.hits, f.visitors, f.xviews, f.uxviews, f.goals
FROM
    dim_date dd,
    dim_variation dv,
    fact_visitor f
WHERE
    dv.experiment_id = ?
        AND dv.id = f.dim_variation_id
        AND dd.id = f.dim_date_id;
Q;

if (!($stmt = $mysqli->prepare($query))) {
    trigger_error('Prepare failed: (' . $mysqli->errno . ') ' . $mysqli->error, E_USER_ERROR);
    exit500();
}

$stmt->bind_param('i', $experiment_id_);

$result = [];

$stmt->execute();
$stmt->bind_result($variation, $date_value, $hits, $visitors, $xviews, $uxviews, $goals);
while ($stmt->fetch()) {
    $result[] = [
        'variation'     => $variation,
        'date_value'    => $date_value,
        'hits'          => $hits,
        'visitors'      => $visitors,
        'xviews'        => $xviews,
        'uxviews'       => $uxviews,
        'goals'         => $goals
    ];
}
$stmt->close();
$mysqli->close();

header('Content-type: application/json; charset=utf-8');
print json_encode($result);
