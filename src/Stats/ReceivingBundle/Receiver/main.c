#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <string.h>

#include "log.h"
#include "process.h"

#define CWD_DIR        "/tmp"
#define LOCK_FILE    "/var/run/so/stats.pid"

int sock_out;


void terminate(int code) {
    log_message("Terminating");
    close(sock_out);
    if (dbp != NULL)
        dbp->close(dbp, 0);
    close_log();
    exit(code);
}

void signal_handler(int sig) {

    int ret = 0;

    switch (sig) {

        case SIGUSR1:
            if ((ret = dbp->sync(dbp, 0)) != 0) {
                dbenv->err(dbenv, ret, "signal_handler:DB->sync");
                terminate(ret);
            }
            if ((ret = dbp->truncate(dbp, NULL, NULL, 0)) != 0) {
                dbenv->err(dbenv, ret, "signal_handler:DB->truncate");
                terminate(ret);
            }
            log_message("Database synchronized");
            break;

        case SIGHUP:
            reopen_log();
            break;

        case SIGINT:
        case SIGTERM:
            log_message("Interrupt signal received");
            terminate(0);
            break;

        default:
            log_message("Unknown signal received");
    }
}

void write_lockfile() {

    int lfp = open(LOCK_FILE, O_RDWR | O_CREAT, 0640);
    char str[10];
    FILE *log = get_log();

    if (lfp < 0) {
        fprintf(log, "Failed to open lock file %s (errno %i)\n", LOCK_FILE,
                errno);
        exit(EXIT_FAILURE);
    }
    if (lockf(lfp, F_TLOCK, 0) < 0) {
        fprintf(log, "Failed to get lock on %s (errno %i)\n", LOCK_FILE, errno);
        exit(EXIT_FAILURE);
    }
    sprintf(str, "%d\n", getpid());
    if (write(lfp, str, strlen(str)) == 0) {
        fprintf(log, "Failed to write process lock file (errno %i)\n", errno);
        exit(EXIT_FAILURE);
    }
}

void set_handlers() {
    signal(SIGCHLD, SIG_IGN);
    signal(SIGTSTP, SIG_IGN);
    signal(SIGTTOU, SIG_IGN);
    signal(SIGTTIN, SIG_IGN);
    signal(SIGHUP, signal_handler);
    signal(SIGUSR1, signal_handler);
    signal(SIGINT, signal_handler);
    signal(SIGTERM, signal_handler);
}

void detach_process() {

    int i;
    FILE *log;

    log = get_log();
    if (log == NULL) {
        printf("Failed to obtain log file pointer\n");
        exit(EXIT_FAILURE);
    }

    if (getppid() == 1) {
        return;
    }

    i = fork();
    if (i < 0) {
        fprintf(log, "Failed to fork process (errno %i)", errno);
        exit(1);
    }
    if (i > 0) {
        exit(EXIT_SUCCESS);
    }

    setsid();

    umask(027);
    if (chdir(CWD_DIR) != 0) {
        fprintf(log, "Failed to change working directory (errno %i)", errno);
        exit(EXIT_FAILURE);
    }

    log_message("Process detached");
}

int usage() {
    printf("usage: receiver [-D] <db2filename>\n");
    return (-1);
}

int main(int argc, char **argv) {

    int ch, detach = 0;
    char *dbfilename = 0;

    while ((ch = getopt(argc, argv, "Dh")) != EOF) {
        switch (ch) {
            case 'D':
                detach = 1;
                break;
            case 'h':
            default:
                usage();
        }
    }

    if (optind == argc || optind < argc - 1) {
        return usage();
    }

    dbfilename = argv[optind];

    if (detach) {
        detach_process();
    }

    write_lockfile();
    set_handlers();

    run_process(&sock_out, dbfilename, log_message);

    return (EXIT_SUCCESS);
}
