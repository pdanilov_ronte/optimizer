#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX_SIZE 4096
#define LISTEN_PORT 5000
#define LISTEN_BACKLOG 50

int start_server (sock_out, log_cb, receive_cb)
  int *sock_out;void
  (*log_cb) (char *data);int
  (*receive_cb) (char *data, void
  (*log_cb) (char *data));
{
  int sock_desc, conn_desc;
  struct sockaddr_in serv_addr, client_addr;
  char buf[MAX_SIZE], *msg;

  char *response =
      "HTTP/1.0 200 OK\r\n\
Server: StrongOptimizer\r\n\
Content-Type: text/plain\r\n\
\r\n";
  size_t response_len = strlen (response);

  sock_desc = socket (AF_INET, SOCK_STREAM, 0);
  if (sock_desc < 0)
    {
      log_cb ("Failed to create socket");
      return (EXIT_FAILURE);
    }
  sock_out = &sock_desc;

  memset ((char *) &serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons (LISTEN_PORT);

  if (bind (sock_desc, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
    {
      log_cb ("Failed to bind");
      return (EXIT_FAILURE);
    }

  listen (sock_desc, LISTEN_BACKLOG);
  log_cb ("Ready to accept connections");

  socklen_t size = sizeof(client_addr);
  msg = malloc (0);

  while (1)
    {

      conn_desc = accept (sock_desc, (struct sockaddr *) &client_addr, &size);
      if (conn_desc == -1)
	{
	  log_cb ("Failed to accept connection");
	  return (EXIT_FAILURE);
	}
      else
	{
	  log_cb ("Connected");
	}

      memset (buf, 0, MAX_SIZE);

      if (read (conn_desc, buf, MAX_SIZE - 1) > 0)
	{
	  msg = realloc (msg, strlen (buf) + 9);
	  memset (msg, 0, strlen (buf) + 9);
	  snprintf (msg, strlen (buf) + 9, "Received %s", buf);
	  log_cb (msg);
	  receive_cb (buf, log_cb);
	}
      else
	{
	  log_cb ("Failed to receive data");
	}

      write (conn_desc, response, response_len);

      close (conn_desc);
    }

  return (EXIT_SUCCESS);
}
