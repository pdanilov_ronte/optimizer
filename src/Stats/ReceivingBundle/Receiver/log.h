#include <stdio.h>

#define LOG_FILE    "/var/log/stats/receiver.log"

static FILE *logfile_desc;

FILE *get_log();

void log_message(char *message);

int open_log(char *filename) {
    logfile_desc = fopen(filename, "a");
    return (logfile_desc ? EXIT_SUCCESS : EXIT_FAILURE);
}

void close_log() {
    fclose(logfile_desc);
}

FILE *get_log() {

    if (logfile_desc == NULL) {
        if (open_log(LOG_FILE) != EXIT_SUCCESS) {
            printf("Failed to open log file %s\n", LOG_FILE);
            return NULL;
        }
    }
    return logfile_desc;
}

int reopen_log() {
    close_log();
    return open_log(LOG_FILE);
}

void log_message(char *message) {

    FILE *fp = get_log();

    if (fp == NULL) {
        exit(EXIT_FAILURE);
    }

    fprintf(fp, "%s\n", message);
    fflush(fp);
}
