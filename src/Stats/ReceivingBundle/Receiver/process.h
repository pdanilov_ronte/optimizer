#include <sys/time.h>
#include <sys/types.h>

#include <bdb/db.h>

#include "server.h"

#define DB_TYPE        DB_BTREE

static DB_ENV *dbenv;
static DB *dbp;


int open_db(DB **, DB_ENV *, char *, DBTYPE, u_int32_t, u_int32_t, u_int32_t);

int open_env(DB_ENV **, char *, u_int32_t);

int insert_btree(DB *dbp, DB_ENV *dbenv, char *val);

int compare_int(DB *, const DBT *, const DBT *, size_t *);


int receive_hit(char *request, void (*log_cb)(char *)) {

    int ret = insert_btree(dbp, dbenv, request);

    if (ret != 0) {
        log_cb("Failed to put record to database");
        dbp->close(dbp, 0);
        return (EXIT_FAILURE);
    }

    log_cb("Hit record stored");
    return (EXIT_SUCCESS);
}

void run_process(int *sock_out, char *dbfilename, void (*log_cb)(char *)) {

    FILE *logfile;
    u_int32_t hpsize;
    int ret = 0;

    logfile = get_log();

    if ((ret = open_env(&dbenv, NULL, 0)) != 0) {
        fprintf(logfile, "open_env: %s", db_strerror(ret));
        return;
    }

    hpsize = 1024 * 15000;

    if ((ret = open_db(&dbp, dbenv, dbfilename, DB_TYPE, 0, hpsize, 0)) != 0) {
        dbenv->err(dbenv, ret, "Failed to open database.");
        return;
    }

    start_server(sock_out, log_cb, receive_hit);
}

int open_env(DB_ENV **dbenvp, char *home, u_int32_t cachesize) {

    FILE *logfile;
    int ret = 0;

    logfile = get_log();

    if ((ret = db_env_create(&dbenv, 0)) != 0) {
        fprintf(logfile, "db_env_create: %s\n", db_strerror(ret));
        return (ret);
    }

    *dbenvp = dbenv;

    dbenv->set_errfile(dbenv, logfile);
    dbenv->set_errpfx(dbenv, "stats");

    if ((cachesize > 0)
        && (ret = dbenv->set_cachesize(dbenv, (u_int32_t) 0, cachesize, 1)) != 0) {
        dbenv->err(dbenv, ret, "DB_ENV->set_cachesize");
        return (ret);
    }

    if ((ret = dbenv->open(dbenv, home, DB_CREATE | DB_INIT_MPOOL, 0)) != 0) {
        dbenv->err(dbenv, ret, "DB_ENV->open");
        return (ret);
    }

    return (ret);
}

int open_db(DB **dbpp, DB_ENV *dbenv, char *dbfilename, DBTYPE dbtype, u_int32_t ghpsize, u_int32_t hpsize,
            u_int32_t pgsize) {

    int ret = 0;

    if ((ret = db_create(dbpp, dbenv, 0)) != 0) {
        dbenv->err(dbenv, ret, "db_create : %s", dbfilename);
        return (ret);
    }

    if ((dbtype == DB_BTREE)
        && (ret = dbp->set_bt_compare(dbp, compare_int)) != 0) {
        dbp->err(dbp, ret, "DB->set_bt_compare");
        return (ret);
    }

    if ((dbtype == DB_BTREE) && (ret = dbp->set_flags(dbp, DB_DUP)) != 0) {
        dbenv->err(dbenv, ret, "DB->set_flags: DB_DUP");
        return (ret);
    }

    if ((dbtype == DB_HEAP) && (ghpsize > 0 || hpsize > 0)
        && (ret = dbp->set_heapsize(dbp, ghpsize, hpsize, 0)) != 0) {
        dbenv->err(dbenv, ret, "DB->set_heapsize");
        return (ret);
    }

    if ((pgsize > 0) && (ret = dbp->set_pagesize(dbp, pgsize)) != 0) {
        dbenv->err(dbenv, ret, "DB->set_pagesize");
        return (ret);
    }

    if ((ret = dbp->open(dbp, NULL, dbfilename, NULL, dbtype, DB_CREATE, 0)) != 0) {
        dbenv->err(dbenv, ret, "DB->open");
    }

    return (ret);
}

int insert_btree(DB *dbp, DB_ENV *dbenv, char *val) {

    DBT key, data;
    struct timeval tv;
    __time_t *utime = malloc(sizeof(__time_t));
    int ret = 0;

    memset(&key, 0, sizeof(DBT));
    memset(&data, 0, sizeof(DBT));

    gettimeofday(&tv, NULL);
    *utime = tv.tv_sec * 1000 + tv.tv_usec / 1000;

    key.data = utime;
    key.size = key.ulen = sizeof(u_int64_t);
    key.flags = DB_DBT_USERMEM;
    data.data = val;
    data.flags = DB_DBT_USERMEM;

    data.size = data.ulen = (u_int32_t) strlen(val) + 1;

    if ((ret = dbp->put(dbp, NULL, &key, &data, 0)) != 0) {
        dbenv->err(dbenv, ret, "insert_btree:DB->put");
        return (EXIT_FAILURE);
    }

    return (ret);
}

int compare_int(DB *dbp, const DBT *a, const DBT *b, size_t *locp) {

    int ai, bi;

    memcpy(&ai, a->data, sizeof(int));
    memcpy(&bi, b->data, sizeof(int));

    return (ai - bi);
}
