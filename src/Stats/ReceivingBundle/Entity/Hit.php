<?php

namespace Stats\ReceivingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hit
 *
 * @ORM\Table(name="page_hit")
 * @ORM\Entity(repositoryClass="Stats\ReceivingBundle\Entity\HitRepository")
 */
class Hit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="experiment", type="string", length=32)
     */
    private $experiment;

    /**
     * @var integer
     *
     * @ORM\Column(name="variation", type="smallint", nullable=true)
     */
    private $variation;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text")
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="referer", type="text")
     */
    private $referer;

    /**
     * @var string
     *
     * @ORM\Column(name="acceptLanguage", type="string", length=255)
     */
    private $acceptLanguage;

    /**
     * @var string
     *
     * @ORM\Column(name="cookie", type="text")
     */
    private $cookie;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=15)
     */
    private $ip;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set experiment
     *
     * @param string $experiment
     * @return Hit
     */
    public function setExperiment($experiment)
    {
        $this->experiment = $experiment;

        return $this;
    }

    /**
     * Get experiment
     *
     * @return string 
     */
    public function getExperiment()
    {
        return $this->experiment;
    }

    /**
     * Set variation
     *
     * @param integer $variation
     * @return Hit
     */
    public function setVariation($variation)
    {
        $this->variation = $variation;

        return $this;
    }

    /**
     * Get variation
     *
     * @return integer 
     */
    public function getVariation()
    {
        return $this->variation;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Hit
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set referer
     *
     * @param string $referer
     * @return Hit
     */
    public function setReferer($referer)
    {
        $this->referer = $referer;

        return $this;
    }

    /**
     * Get referer
     *
     * @return string 
     */
    public function getReferer()
    {
        return $this->referer;
    }

    /**
     * Set acceptLanguage
     *
     * @param string $acceptLanguage
     * @return Hit
     */
    public function setAcceptLanguage($acceptLanguage)
    {
        $this->acceptLanguage = $acceptLanguage;

        return $this;
    }

    /**
     * Get acceptLanguage
     *
     * @return string 
     */
    public function getAcceptLanguage()
    {
        return $this->acceptLanguage;
    }

    /**
     * Set cookie
     *
     * @param string $cookie
     * @return Hit
     */
    public function setCookie($cookie)
    {
        $this->cookie = $cookie;

        return $this;
    }

    /**
     * Get cookie
     *
     * @return string 
     */
    public function getCookie()
    {
        return $this->cookie;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return Hit
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }
}
