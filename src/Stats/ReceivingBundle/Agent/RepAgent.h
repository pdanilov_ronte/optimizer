/*
 * RepAgent.h
 *
 * Author: PM:/ <pm@spiral.ninja>
 */

#ifndef REPAGENT_H_
#define REPAGENT_H_

#include <bdb/db.h>
#include <bdb/db_cxx.h>

class RepAgent
{
public:

  RepAgent (std::string &dbpath, std::string &dbname, std::string &lockfile);

  int
  run ();

  int
  sync ();

  void
  dump (FILE *);

  int
  fetch ();

  ~RepAgent ()
  {
    close ();
  }

  inline Db &
  getDb ()
  {
    return db_;
  }

private:

  Db db_;
  std::string dbfilename_, lockfile_;
  u_int32_t dbflags_;
  pid_t collpid_;

  void
  close ();

  pid_t
  readpid (std::string &lockfile);
};

#endif /* REPAGENT_H_ */
