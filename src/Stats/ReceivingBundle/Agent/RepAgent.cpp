/*
 * RepAgent.cpp
 *
 * Author: PM:/ <pm@spiral.ninja>
 */

#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <stdexcept>
#include <signal.h>
#include <errno.h>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;

#include "RepAgent.h"

RepAgent::RepAgent (std::string &dbpath, std::string &dbname,
		    std::string &lockfile) :
    db_ (NULL, 0), dbfilename_ (dbpath + dbname), lockfile_ (lockfile), dbflags_ (
	0), collpid_ (0)
{
  int ret;

  try
    {
      db_.set_error_stream (&std::cerr);
      ret = db_.open (NULL, dbfilename_.c_str (), NULL, DB_BTREE, dbflags_, 0);
      if (ret == 0)
	{
	  std::cout << "Database opened" << std::endl;
	}
    }
  catch (DbException &e)
    {
      std::cerr << "Error opening database: " << dbfilename_ << std::endl;
      std::cerr << e.what () << std::endl;
    }
  catch (std::exception &e)
    {
      std::cerr << "Error opening database: " << dbfilename_ << std::endl;
      std::cerr << e.what () << std::endl;
    }
}

int
__db_pr_callback (void *handle, const void *str_arg)
{
  char *str;
  FILE *f;

  str = (char *) str_arg;
  f = (FILE *) handle;

  if (fprintf (f, "%s", str) != (int) strlen (str))
    return (EIO);

  return (0);
}

int
RepAgent::run ()
{
  collpid_ = readpid (lockfile_);

  boost::asio::io_service service;
  service.run ();

  tcp::resolver resolver (service);
  tcp::resolver::query query ("127.0.0.1", "5001");

  tcp::socket socket (service);
  boost::asio::connect (socket, resolver.resolve (query));

  tcp::socket::native_handle_type socketp = socket.native_handle ();

  dump ((FILE *) &socketp);

  return (EXIT_SUCCESS);
}

int
RepAgent::sync ()
{
  int ret;

  ret = kill (collpid_, 0);

  if (-1 == ret && ESRCH == errno)
    {
      std::cout << "Failed syncing database: no such process" << std::endl;
      throw new std::exception;
    }
  else if (-1 == ret)
    {
      std::cout << "Failed syncing database: unknown error" << std::endl;
      throw new std::exception;
    }

  return kill (collpid_, SIGHUP);
}

void
RepAgent::dump (FILE * stream)
{
  DB *dbp = db_.get_DB ();
  dbp->dump (dbp, NULL, __db_pr_callback, stream, 0, 0);
}

int
RepAgent::fetch ()
{
  Dbc *dbcp;
  Dbt key, data;

  try
    {
      db_.cursor (NULL, &dbcp, 0);

      while ((dbcp->get (&key, &data, DB_NEXT)) == 0)
	{
	  // @todo: store record data to collection
	}
    }
  catch (DbException &e)
    {
      db_.err (e.get_errno (), "Error fetching database records");
      dbcp->close ();
      throw e;
    }
  catch (std::exception &e)
    {
      dbcp->close ();
      throw e;
    }

  dbcp->close ();

  return (0);
}

pid_t
RepAgent::readpid (std::string &lockfile)
{
  std::string line;
  pid_t pid;

  std::ifstream lockfilep (lockfile.c_str ());
  if (!lockfilep.is_open ())
    {
      std::cerr << "Failed opening lockfile " << lockfile << std::endl;
      throw new std::exception;
    }

  getline (lockfilep, line);

  pid = atoi (line.c_str ());

  return pid;
}

void
RepAgent::close ()
{
  try
    {
      db_.close (0);
      std::cout << "Database " << dbfilename_ << " is closed." << std::endl;
    }
  catch (DbException &e)
    {
      std::cerr << "Error closing database: " << dbfilename_ << std::endl;
      std::cerr << e.what () << std::endl;
    }
  catch (std::exception &e)
    {
      std::cerr << "Error closing database: " << dbfilename_ << std::endl;
      std::cerr << e.what () << std::endl;
    }
}
