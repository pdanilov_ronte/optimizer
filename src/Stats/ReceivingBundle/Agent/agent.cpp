//============================================================================
// Name        : agent.cpp
// Author      : PM:/ <pm@spiral.ninja>
// Version     :
// Copyright   :
// Description : Hits data replication agent
//============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <string>

#include "RepAgent.h"

#define LOCK_FILE "/var/run/lock/opt/stats.lock"
#define DB_PATH "/var/db/opt/stats/"

int
usage ()
{
  std::cout << "./agent [-d <database directory>] <database name>" << std::endl;
  return (-1);
}

int
main (int argc, char *argv[])
{
  int ch;
  std::string dbpath (DB_PATH), dbname, lockfile (LOCK_FILE);

  while ((ch = getopt (argc, argv, "hd:")) != EOF)
    switch (ch)
      {
      case 'd':
	dbpath = optarg;
	break;
      case 'h':
      default:
	return (usage ());
	break;
      }

  if (optind == argc || optind < argc - 1)
    {
      return usage ();
    }

  dbname = argv[optind];

  RepAgent agent (dbpath, dbname, lockfile);

  try
    {

      agent.run ();

    }
  catch (std::exception &e)
    {
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}
