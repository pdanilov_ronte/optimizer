DROP TABLE IF EXISTS dim_browser;
CREATE TABLE dim_browser
(
  id           INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  browser_name VARCHAR(45)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


DROP TABLE IF EXISTS dim_country;
CREATE TABLE dim_country
(
  id           INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  country_name VARCHAR(45)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


DROP TABLE IF EXISTS dim_date;
CREATE TABLE dim_date
(
  id         INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  date_value DATE                NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


DROP TABLE IF EXISTS dim_device;
CREATE TABLE dim_device
(
  id                 INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  dim_device_type_id INT(11)             NOT NULL,
  device_name        VARCHAR(45)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


DROP TABLE IF EXISTS dim_device_type;
CREATE TABLE dim_device_type
(
  id          INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  device_type VARCHAR(45)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


DROP TABLE IF EXISTS dim_os;
CREATE TABLE dim_os
(
  id      INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  os_name VARCHAR(45)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


DROP TABLE IF EXISTS dim_source;
CREATE TABLE dim_source
(
  id          INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  source_type VARCHAR(45)         NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


DROP TABLE IF EXISTS dim_variation;
CREATE TABLE dim_variation
(
  id            INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  experiment_id INT(11)             NOT NULL,
  variation     SMALLINT(6)         NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


DROP TABLE IF EXISTS fact_visitor;
CREATE TABLE fact_visitor
(
  dim_variation_id INT(11) NOT NULL,
  dim_source_id    INT(11),
  dim_browser_id   INT(11),
  dim_os_id        INT(11),
  dim_device_id    INT(11),
  dim_country_id   INT(11),
  dim_date_id      INT(11),
  hits             INT(11) NOT NULL,
  visitors         INT(11) NOT NULL,
  xviews           INT(11) NOT NULL,
  uxviews          INT(11) NOT NULL,
  goals            INT(11) NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


CREATE INDEX browser_name ON dim_browser (browser_name);
CREATE INDEX country_name ON dim_country (country_name);
CREATE UNIQUE INDEX date_value ON dim_date (date_value);
CREATE INDEX device_name ON dim_device (device_name);
CREATE INDEX fk_dim_device_dim_device_type1_idx ON dim_device (dim_device_type_id);
CREATE INDEX device_type ON dim_device_type (device_type);
CREATE INDEX os_name ON dim_os (os_name);
CREATE INDEX source_type ON dim_source (source_type);
CREATE UNIQUE INDEX experiment_variation_UNIQUE ON dim_variation (experiment_id, variation);
CREATE INDEX fk_fact_visitor_dim_browser1_idx ON fact_visitor (dim_browser_id);
CREATE INDEX fk_fact_visitor_dim_country1_idx ON fact_visitor (dim_country_id);
CREATE INDEX fk_fact_visitor_dim_date1_idx ON fact_visitor (dim_date_id);
CREATE INDEX fk_fact_visitor_dim_device1_idx ON fact_visitor (dim_device_id);
CREATE INDEX fk_fact_visitor_dim_os1_idx ON fact_visitor (dim_os_id);
CREATE INDEX fk_fact_visitor_dim_source1_idx ON fact_visitor (dim_source_id);
CREATE INDEX fk_fact_visitor_dim_variation1_idx ON fact_visitor (dim_variation_id);
CREATE UNIQUE INDEX variation_per_date_UNIQUE ON fact_visitor (dim_variation_id, dim_date_id);
