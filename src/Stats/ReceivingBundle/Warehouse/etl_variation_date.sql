DROP PROCEDURE IF EXISTS etl_variation_date;

CREATE DEFINER =`dba`@`strongoptimizer.com` PROCEDURE `etl_variation_date`()

  SQL SECURITY INVOKER

  BEGIN

    DECLARE done INT DEFAULT FALSE;

    DECLARE experiment_id_, variation_, hits_, visitors_, xviews_, uxviews_, goals_ INT;
    DECLARE date_value_ DATE;
    DECLARE dim_variation_id_, dim_date_id_ INT;

    DECLARE load_cursor CURSOR FOR
      SELECT
        e.id           AS experiment_id,
        v.number       AS variation,
        DATE(h.dt)     AS date_value,
        COUNT(*)       AS hits,
        SUM(is_unique) AS visitors,
        SUM(is_xview)  AS xviews,
        SUM(is_uxview) AS uxviews,
        SUM(is_goal)   AS goals
      FROM
        stats.hit h,
        stats.variation v,
        stats.experiment e
      WHERE
        h.variation_id = v.id
        AND v.experiment_id = e.id
        AND dt BETWEEN UTC_TIMESTAMP() - INTERVAL 6 SECOND AND UTC_TIMESTAMP() - INTERVAL 1 SECOND
      GROUP BY e.id, v.number, date_value;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN load_cursor;

    fetch_loop: LOOP

      FETCH load_cursor
      INTO experiment_id_, variation_, date_value_, hits_, visitors_, xviews_, uxviews_, goals_;
      IF done
      THEN
        LEAVE fetch_loop;
      END IF;

      INSERT INTO warehouse.dim_variation (experiment_id, variation)
      VALUES (experiment_id_, variation_)
      ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id);

      SELECT LAST_INSERT_ID()
      INTO dim_variation_id_;

      INSERT INTO warehouse.dim_date (date_value)
      VALUES (date_value_)
      ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id);

      SELECT LAST_INSERT_ID()
      INTO dim_date_id_;

      INSERT INTO warehouse.fact_visitor (dim_variation_id, dim_date_id, hits, visitors, xviews, uxviews, goals)
      VALUES (dim_variation_id_, dim_date_id_, hits_, visitors_, xviews_, uxviews_, goals_)
      ON DUPLICATE KEY UPDATE
        hits     = hits + hits_,
        visitors = visitors + visitors_,
        xviews   = xviews + xviews_,
        uxviews  = uxviews + uxviews_,
        goals    = goals + goals_;

    END LOOP;

  END;
