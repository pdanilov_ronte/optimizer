DROP TABLE IF EXISTS client;
CREATE TABLE client
(
  id  INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  cid CHAR(25)            NOT NULL
)
  ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


DROP TABLE IF EXISTS experiment;
CREATE TABLE experiment
(
  id INT(11) PRIMARY KEY NOT NULL
)
  ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


DROP TABLE IF EXISTS hit;
CREATE TABLE hit
(
  id           INT(11) PRIMARY KEY                   NOT NULL AUTO_INCREMENT,
  client_id    INT(11)                               NOT NULL,
  variation_id INT(11)                               NOT NULL,
  landing_id   INT(11)                               NOT NULL,
  useragent_id INT(11)                               NOT NULL,
  language_id  INT(11)                               NOT NULL,
  referer_id   INT(11),
  dt           TIMESTAMP DEFAULT CURRENT_TIMESTAMP   NOT NULL,
  ip           VARCHAR(15)                           NOT NULL,
  st           BIGINT(20) UNSIGNED DEFAULT 0         NOT NULL,
  is_unique    TINYINT(4)                            NOT NULL,
  is_xview     TINYINT(4)                            NOT NULL,
  is_uxview    TINYINT(4)                            NOT NULL,
  is_gpview    TINYINT(4)                            NOT NULL,
  is_goal      TINYINT(4)                            NOT NULL
)
  ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


DROP TABLE IF EXISTS landing;
CREATE TABLE landing
(
  id   INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  url  BLOB                NOT NULL,
  hash BIGINT(20) UNSIGNED NOT NULL
)
  ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


DROP TABLE IF EXISTS language;
CREATE TABLE language
(
  id         INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  descriptor VARCHAR(255)        NOT NULL
)
  ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


DROP TABLE IF EXISTS referer;
CREATE TABLE referer
(
  id   INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  url  BLOB                NOT NULL,
  hash BIGINT(20) UNSIGNED NOT NULL
)
  ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


DROP TABLE IF EXISTS useragent;
CREATE TABLE useragent
(
  id         INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  descriptor VARCHAR(255)        NOT NULL
)
  ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


DROP TABLE IF EXISTS variation;
CREATE TABLE variation
(
  id            INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  experiment_id INT(11)             NOT NULL,
  number        SMALLINT(6)         NOT NULL
)
  ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


CREATE UNIQUE INDEX cid ON client (cid);
CREATE INDEX client_id ON hit (client_id);
CREATE INDEX dt ON hit (dt);
CREATE INDEX ip ON hit (ip);
CREATE INDEX landing_id ON hit (landing_id);
CREATE INDEX language_id ON hit (language_id);
CREATE INDEX referer_id ON hit (referer_id);
CREATE INDEX st ON hit (st);
CREATE INDEX useragent_id ON hit (useragent_id);
CREATE INDEX variation_id ON hit (variation_id);
CREATE UNIQUE INDEX hash ON landing (hash);
CREATE UNIQUE INDEX descriptor ON language (descriptor);
CREATE UNIQUE INDEX hash ON referer (hash);
CREATE UNIQUE INDEX descriptor ON useragent (descriptor);
CREATE UNIQUE INDEX variation_identity ON variation (experiment_id, number);
