#include "HitStorage.h"
#include "Hit.h"


HitStorage::HitStorage(const std::string &username, const std::string &password, const std::string &database) {

    driver_ = get_driver_instance();
    conn_ = driver_->connect(HIT_STORAGE_CONN, username, password);
    conn_->setSchema(database);

    std::string sql;

    /**
     * Experiment
     */
    sql = std::string("INSERT IGNORE INTO `experiment` (`id`) VALUES (?)");
    statements_.insert(prep_statement_pair_t(oltp_insert_experiment, conn_->prepareStatement(sql)));

    /**
     * Variation
     */
    sql = std::string("INSERT IGNORE INTO `variation` (`experiment_id`, `number`) VALUES (?, ?)");
    statements_.insert(prep_statement_pair_t(oltp_insert_variation, conn_->prepareStatement(sql)));

    sql = std::string("SELECT id FROM `variation` WHERE experiment_id = ? AND number = ?");
    statements_.insert(prep_statement_pair_t(oltp_select_variation, conn_->prepareStatement(sql)));

    /**
     * Client
     */
    sql = std::string("INSERT IGNORE INTO `client` (`cid`) VALUES (?)");
    statements_.insert(prep_statement_pair_t(oltp_insert_client, conn_->prepareStatement(sql)));

    sql = std::string("SELECT id FROM `client` WHERE `cid` = ?");
    statements_.insert(prep_statement_pair_t(oltp_select_client, conn_->prepareStatement(sql)));

    /**
     * Landing
     */
    sql = std::string("INSERT IGNORE INTO `landing` (`url`, `hash`) VALUES (?, ?)");
    statements_.insert(prep_statement_pair_t(oltp_insert_landing, conn_->prepareStatement(sql)));

    sql = std::string("SELECT id FROM `landing` WHERE hash = ?");
    statements_.insert(prep_statement_pair_t(oltp_select_landing, conn_->prepareStatement(sql)));

    /**
     * User-Agent
     */
    sql = std::string("INSERT IGNORE INTO `useragent` (`descriptor`) VALUES (?)");
    statements_.insert(prep_statement_pair_t(oltp_insert_useragent, conn_->prepareStatement(sql)));

    sql = std::string("SELECT id FROM `useragent` WHERE descriptor = ?");
    statements_.insert(prep_statement_pair_t(oltp_select_useragent, conn_->prepareStatement(sql)));

    /**
     * Accept-Language
     */
    sql = std::string("INSERT IGNORE INTO `language` (`descriptor`) VALUES (?)");
    statements_.insert(prep_statement_pair_t(oltp_insert_language, conn_->prepareStatement(sql)));

    sql = std::string("SELECT id FROM `language` WHERE descriptor = ?");
    statements_.insert(prep_statement_pair_t(oltp_select_language, conn_->prepareStatement(sql)));

    /**
     * Referer
     */
    sql = std::string("INSERT IGNORE INTO `referer` (`url`, `hash`) VALUES (?, ?)");
    statements_.insert(prep_statement_pair_t(oltp_insert_referer, conn_->prepareStatement(sql)));

    sql = std::string("SELECT id FROM `referer` WHERE hash = ?");
    statements_.insert(prep_statement_pair_t(oltp_select_referer, conn_->prepareStatement(sql)));

    /**
     * Hit
     */
    sql = std::string("INSERT INTO `hit`"
                              " (variation_id, client_id, landing_id, useragent_id, language_id, referer_id, ip, dt, st, is_unique, is_xview, is_uxview, is_gpview, is_goal)"
                              " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    statements_.insert(prep_statement_pair_t(oltp_insert_hit, conn_->prepareStatement(sql)));
}

void
HitStorage::store(const HIT *hit) {
    sql::PreparedStatement *insert, *fetchback;
    std::hash<std::string> hash_fn;
    uint64_t hash;
    char dt_cstr[20];
    std::string error;

    int variation_id, client_id, landing_id, useragent_id, language_id, referer_id;


    /**
     * Insert experiment
     */
    insert = statements_.find(oltp_insert_experiment)->second;
    insert->setInt(1, hit->experiment_id);
    insert->execute();

    /**
     * Insert variation
     */
    insert = statements_.find(oltp_insert_variation)->second;
    insert->setInt(1, hit->experiment_id);
    insert->setInt(2, hit->variation);

    fetchback = statements_.find(oltp_select_variation)->second;
    fetchback->setInt(1, hit->experiment_id);
    fetchback->setInt(2, hit->variation);

    try {
        variation_id = insert_(*insert, *fetchback);
    } catch (std::runtime_error &e) {
        std::cout << e.what() << std::endl;
        throw std::runtime_error("Failed to insert variation");
    }

    /**
     * Insert client
     */
    insert = statements_.find(oltp_insert_client)->second;
    insert->setString(1, hit->client_id);

    fetchback = statements_.find(oltp_select_client)->second;
    fetchback->setString(1, hit->client_id);

    try {
        client_id = insert_(*insert, *fetchback);
    } catch (std::runtime_error &e) {
        std::cout << e.what() << std::endl;
        throw std::runtime_error("Failed to insert client");
    }

    /**
     * Insert landing
     */
    insert = statements_.find(oltp_insert_landing)->second;
    hash = hash_fn(hit->landing);
    insert->setString(1, hit->landing);
    insert->setUInt64(2, hash);

    fetchback = statements_.find(oltp_select_landing)->second;
    fetchback->setUInt64(1, hash);

    try {
        landing_id = insert_(*insert, *fetchback);
    } catch (std::runtime_error &e) {
        std::cout << e.what() << std::endl;
        throw std::runtime_error("Failed to insert landing");
    }

    /**
     * Insert user-agent
     */
    insert = statements_.find(oltp_insert_useragent)->second;
    insert->setString(1, hit->user_agent);

    fetchback = statements_.find(oltp_select_useragent)->second;
    fetchback->setString(1, hit->user_agent);

    try {
        useragent_id = insert_(*insert, *fetchback);
    } catch (std::runtime_error &e) {
        std::cout << e.what() << std::endl;
        throw std::runtime_error("Failed to insert useragent");
    }

    /**
     * Insert language
     */
    insert = statements_.find(oltp_insert_language)->second;
    insert->setString(1, hit->language);

    fetchback = statements_.find(oltp_select_language)->second;
    fetchback->setString(1, hit->language);

    try {
        language_id = insert_(*insert, *fetchback);
    } catch (std::runtime_error &e) {
        std::cout << e.what() << std::endl;
        throw std::runtime_error("Failed to insert language");
    }

    /**
     * Insert referer
     */
    insert = statements_.find(oltp_insert_referer)->second;
    hash = hash_fn(hit->referer);
    insert->setString(1, hit->referer);
    insert->setUInt64(2, hash);

    fetchback = statements_.find(oltp_select_referer)->second;
    fetchback->setUInt64(1, hash);

    try {
        referer_id = insert_(*insert, *fetchback);
    } catch (std::runtime_error &e) {
        std::cout << e.what() << std::endl;
        throw std::runtime_error("Failed to insert referer");
    }

    /**
     * Insert hit
     */
    insert = statements_.find(oltp_insert_hit)->second;
    insert->setInt(1, variation_id);
    insert->setInt(2, client_id);
    insert->setInt(3, landing_id);
    insert->setInt(4, useragent_id);
    insert->setInt(5, language_id);
    insert->setInt(6, referer_id);
    insert->setString(7, hit->ip);
    std::strftime(dt_cstr, 20, "%Y-%m-%d %H-%M-%S",
                  std::gmtime((const time_t *) &hit->dt));
    insert->setDateTime(8, dt_cstr);
    insert->setInt(9, hit->session_start);
    insert->setInt(10, hit->is_unique);
    insert->setInt(11, hit->is_xview);
    insert->setInt(12, hit->is_uxview);
    insert->setInt(13, hit->is_gpview);
    insert->setInt(14, hit->is_goal);
    insert->execute();
}

int
HitStorage::insert_(sql::PreparedStatement &insert,
                    sql::PreparedStatement &fetchback) {
    sql::ResultSet *res;
    int record_id;

    insert.execute();

    res = fetchback.executeQuery();

    if (res->rowsCount() == 0) {
        delete res;
        throw std::runtime_error(
                "Empty result on fetching back the inserted record");
    }
    else if (res->rowsCount() > 1) {
        delete res;
        throw std::runtime_error(
                "Multiple result on fetching back the inserted record");
    }

    res->first();
    record_id = res->getInt("id");

    delete res;

    return record_id;
}

HitStorage::~HitStorage() {
    for (prep_statement_map_t::iterator it = statements_.begin();
         it != statements_.end(); it++) {
        delete it->second;
        statements_.erase(it);
    }

    delete conn_;
}
