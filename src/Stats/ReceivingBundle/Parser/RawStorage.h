/*
 * RawStorage.h
 *
 * Author: PM:/ <pm@spiral.ninja>
 */

#ifndef RAWSTORAGE_H_
#define RAWSTORAGE_H_

#include <bdb/db.h>
#include <bdb/db_cxx.h>

#include "Hit.h"
#include "HitParser.h"
#include "HitStorage.h"

class RawStorage
{

public:

  RawStorage (const std::string &dbfilename);

  void
  walk (IHitParser&, IHitStorage&);

  ~RawStorage ()
  {
    close ();
  }

private:

  Db db_;
  std::string dbfilename_;
  u_int32_t dbflags_;

  void
  close ();
};

#endif /* RAWSTORAGE_H_ */
