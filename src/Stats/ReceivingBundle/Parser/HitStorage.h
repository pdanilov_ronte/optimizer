#ifndef HITSTORAGE_H_
#define HITSTORAGE_H_

#include <map>
#include <cppconn/driver.h>
#include <cppconn/connection.h>
#include <cppconn/prepared_statement.h>

#include "Hit.h"

#define HIT_STORAGE_CONN "tcp://127.0.0.1:3306"


enum {
    oltp_insert_experiment,
    oltp_insert_variation,
    oltp_select_variation,
    oltp_insert_client,
    oltp_select_client,
    oltp_insert_landing,
    oltp_select_landing,
    oltp_insert_useragent,
    oltp_select_useragent,
    oltp_insert_language,
    oltp_select_language,
    oltp_insert_referer,
    oltp_select_referer,
    oltp_insert_hit
};

typedef std::pair<int, sql::PreparedStatement *> prep_statement_pair_t;
typedef std::map<int, sql::PreparedStatement *> prep_statement_map_t;

class IHitStorage {
public:

    virtual ~IHitStorage() { }

    virtual void store(const HIT *) = 0;
};

class HitStorage : public IHitStorage {

public:

    HitStorage(const std::string &username, const std::string &password, const std::string &database);

    virtual void store(const HIT *);

    virtual ~HitStorage();

private:

    sql::Driver *driver_;
    sql::Connection *conn_;

    prep_statement_map_t statements_;

    int insert_(sql::PreparedStatement &insert, sql::PreparedStatement &fetchback);
};

#endif /* HITSTORAGE_H_ */
