#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>

#include "RawStorage.h"

int
usage() {
    std::cout << "usage: parser -u <username> -p <password> -d <database> <db2filename>"
    << std::endl;
    return (-1);
}

int
main(int argc, char *argv[]) {
    int ch;
    std::string username, password, database, db2filename;

    while ((ch = getopt(argc, argv, "u:p:d:h")) != EOF)
        switch (ch) {
            case 'u':
                username = optarg;
                break;
            case 'p':
                password = optarg;
                break;
            case 'd':
                database = optarg;
                break;
            case 'h':
            default:
                return (usage());
        }

    if (optind == argc || optind < argc - 1) {
        return usage();
    }

    db2filename = argv[optind];

    try {
        RawStorage raw(db2filename);
        HitParser parser;
        HitStorage storage(username, password, database);
        raw.walk(parser, storage);
    } catch (std::runtime_error &e) {
        std::cerr << "Runtime error: " << e.what() << " ..aborting." << std::endl;
        return EXIT_FAILURE;
    } catch (std::exception &e) {
        std::cerr << "Unknown error: ..aborting." << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
