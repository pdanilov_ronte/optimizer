/*
 * HitParser.h
 *
 * Author: PM:/ <pm@spiral.ninja>
 */
#ifndef HITPARSER_H_
#define HITPARSER_H_

#include <string>
#include <vector>
#include <map>

#include "Hit.h"
#include "http_parser.h"

class HitParser;

enum header_field {
    header_field_unknown,
    header_field_xrealip,
    header_field_useragent,
    header_field_language,
    header_field_referer
};

enum query_field {
    query_field_unknown,
    query_field_event_type,
    query_field_client_id,
    query_field_session_start,
    query_field_experiment,
    query_field_variation,
    query_field_landing,
    query_field_referer,
    query_field_is_unique,
    query_field_is_xview,
    query_field_is_uxview,
    query_field_is_gpview,
    query_field_is_goal
};

enum event_type {
    event_type_unknown,
    event_type_pageview
};

typedef struct {
    std::string h_field;
    std::string h_value;
} hit_parser_header_t;

typedef struct {
    std::string q_field;
    std::string q_value;
} hit_parser_query_t;

struct hit_parser_data_t {
    std::vector<hit_parser_header_t> *headers;
    std::vector<hit_parser_query_t> *query;
    HitParser *delegate;
    HIT *hit = 0;

    hit_parser_header_t h_process;
    std::string url;

    hit_parser_data_t(HitParser *);
};

class IHitParser {
public:

    virtual
    ~IHitParser() {
    }

    virtual int
            parse(const std::string *request, HIT *) = 0;
};

class HitParser : public IHitParser {
public:
    HitParser();

    virtual int
            parse(const std::string *request, HIT *);

    int
            http_data_cb(http_parser *, const char *at, size_t length);

    virtual
    ~HitParser();

    static std::map<std::string, int> header_field_map;
    static std::map<std::string, int> query_field_map;
    static std::map<std::string, int> event_type_map;

private:

    http_parser parser_;
    http_parser_settings settings_;
    hit_parser_data_t data_;

    void
            parse_query(const std::string *url, std::vector<hit_parser_query_t> *);
};

#endif /* HITPARSER_H_ */
