#include "RawStorage.h"

RawStorage::RawStorage(const std::string &dbfilename) :
        db_(NULL, 0), dbfilename_(dbfilename), dbflags_(0) {
    int ret;

    try {
        db_.set_error_stream(&std::cerr);
        ret = db_.open(NULL, dbfilename_.c_str(), NULL, DB_BTREE, dbflags_, 0);
        if (ret == 0) {
            std::cout << "Database opened" << std::endl;
        }
    }
    catch (std::exception &e) {
        std::cerr << "Error opening database: " << dbfilename_ << std::endl;
        std::cerr << e.what() << std::endl;

        throw e;
    }
}

void
RawStorage::walk(IHitParser &parser, IHitStorage &storage) {
    Dbc *dbcp;
    Dbt key, data;

    try {
        db_.cursor(NULL, &dbcp, 0);

        while (dbcp->get(&key, &data, DB_NEXT) == 0) {
            char *data_string = (char *) data.get_data();
            std::string request(data_string);

            HIT *hit = new HIT
                    {};

            // Record key is an Epoch time in milliseconds
            hit->dt = *((uint64_t *) key.get_data()) / 1000;

            try {
                parser.parse(&request, hit);
                storage.store(hit);
                dbcp->del(0);
            }
            catch (DbException &e) {
                delete hit;
                throw e;
            }
            catch (std::runtime_error &e) {
                delete hit;
                throw e;
            }

            delete hit;
        }

        db_.sync(0);

        dbcp->close();
    }
    catch (DbException &e) {
        db_.err(e.get_errno(), "Error reading database record");
        dbcp->close();
        throw e;
    }
    catch (std::runtime_error &e) {
        dbcp->close();
        throw e;
    }
    catch (std::exception &e) {
        dbcp->close();
        throw e;
    }
}

void
RawStorage::close() {
    try {
        db_.close(0);
        std::cout << "Database " << dbfilename_ << " is closed." << std::endl;
    }
    catch (DbException &e) {
        std::cerr << "Error closing database: " << dbfilename_ << std::endl;
        std::cerr << e.what() << std::endl;
    }
    catch (std::exception &e) {
        std::cerr << "Error closing database: " << dbfilename_ << std::endl;
        std::cerr << e.what() << std::endl;
    }
}
