/*
 * Hit.h
 *
 * Author: PM:/ <pm@spiral.ninja>
 */

#ifndef HIT_H_
#define HIT_H_

#include <sys/types.h>
#include <string>
#include <ctime>

typedef struct
{
  u_int8_t event_type;
  std::string client_id;
  u_int64_t session_start;
  u_int16_t experiment_id;
  u_int8_t variation;
  std::string landing;
  std::string user_agent;
  std::string language;
  std::string referer;
  std::string ip;
  std::time_t dt;
  bool is_unique;
  bool is_xview;
  bool is_uxview;
  bool is_goal;
  bool is_gpview;
} HIT;

#endif /* HIT_H_ */
