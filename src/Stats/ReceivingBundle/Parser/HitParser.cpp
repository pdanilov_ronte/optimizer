#include <iostream>
#include <string.h>

#include "HitParser.h"

#define MAX_HEADER_LINES 25
#define MAX_QUERY_NUM 25
#define MAX_QUERY_LEN 128

struct http_data_fn {
    HitParser *delegate;

    int
    operator()(http_parser *parser, const char *at, size_t length) {
        return delegate->http_data_cb(parser, at, length);
    }
};

hit_parser_data_t::hit_parser_data_t(HitParser *delegate_p) :
        delegate(delegate_p), h_process
        {} {
}

extern "C" int
http_data_cb_wrap(http_parser *parser, const char *at, size_t length) {
    hit_parser_data_t *data = (hit_parser_data_t *) parser->data;
    http_data_fn *fn = (http_data_fn *) data->delegate;
    return (*fn)(parser, at, length);
}

std::map<std::string, int> HitParser::header_field_map;
std::map<std::string, int> HitParser::query_field_map;
std::map<std::string, int> HitParser::event_type_map;

HitParser::HitParser() :
        parser_(), settings_(), data_(this) {
    header_field_map[std::string("X-Real-IP")] = header_field_xrealip;
    header_field_map[std::string("User-Agent")] = header_field_useragent;
    header_field_map[std::string("Accept-Language")] = header_field_language;
    header_field_map[std::string("X-Real-Referer")] = header_field_referer;

    query_field_map[std::string("_evt")] = query_field_event_type;
    query_field_map[std::string("_cid")] = query_field_client_id;
    query_field_map[std::string("_st")] = query_field_session_start;
    query_field_map[std::string("experiment")] = query_field_experiment;
    query_field_map[std::string("variation")] = query_field_variation;
    query_field_map[std::string("landing")] = query_field_landing;
    query_field_map[std::string("referer")] = query_field_referer;
    query_field_map[std::string("is_unique")] = query_field_is_unique;
    query_field_map[std::string("is_xview")] = query_field_is_xview;
    query_field_map[std::string("is_uxview")] = query_field_is_uxview;
    query_field_map[std::string("is_gpview")] = query_field_is_gpview;
    query_field_map[std::string("is_goal")] = query_field_is_goal;

    event_type_map[std::string("pageview")] = event_type_pageview;

    settings_.on_header_field = &http_data_cb_wrap;
    settings_.on_header_value = &http_data_cb_wrap;
    settings_.on_url = &http_data_cb_wrap;
}

int HitParser::parse(const std::string *request, HIT *hit) {

    http_parser_init(&parser_, HTTP_REQUEST);

    data_.hit = hit;

    data_.headers = new std::vector<hit_parser_header_t>();
    data_.headers->clear();
    data_.headers->reserve(MAX_HEADER_LINES);

    data_.query = new std::vector<hit_parser_query_t>();
    data_.query->clear();
    data_.query->reserve(MAX_QUERY_NUM);

    parser_.data = &data_;

    http_parser_execute(&parser_, &settings_, request->c_str(), request->length());

    for (size_t i = 0; i < data_.headers->size(); i++) {
        switch (header_field_map.find(data_.headers->at(i).h_field)->second) {
            case header_field_xrealip:
                data_.hit->ip = data_.headers->at(i).h_value;
                break;
            case header_field_useragent:
                data_.hit->user_agent = data_.headers->at(i).h_value;
                break;
            case header_field_language:
                data_.hit->language = data_.headers->at(i).h_value;
                break;
            case header_field_referer:
                data_.hit->referer = data_.headers->at(i).h_value;
                break;
            case header_field_unknown:
            default:
                break;
        }
    }

    parse_query(&data_.url, data_.query);

    for (size_t i = 0; i < data_.query->size(); i++) {

        switch (query_field_map.find(data_.query->at(i).q_field)->second) {

            case query_field_event_type:
                hit->event_type = (u_int8_t) event_type_map.find(data_.query->at(i).q_field)->second;
                break;
            case query_field_client_id:
                hit->client_id = data_.query->at(i).q_value.c_str();
                break;
            case query_field_session_start:
                hit->session_start = atoi(data_.query->at(i).q_value.c_str());
                break;
            case query_field_experiment:
                hit->experiment_id = (u_int16_t) atoi(
                        data_.query->at(i).q_value.c_str());
                break;
            case query_field_variation:
                hit->variation = (u_int8_t) atoi(
                        data_.query->at(i).q_value.c_str());
                break;
            case query_field_landing:
                hit->landing = data_.query->at(i).q_value;
                break;
            case query_field_referer:
                // Referer field passed with query will override one from the header
                hit->referer = data_.query->at(i).q_value;
                break;
            case query_field_is_unique:
                hit->is_unique = (bool) atoi(data_.query->at(i).q_value.c_str());
                break;
            case query_field_is_xview:
                hit->is_xview = (bool) atoi(data_.query->at(i).q_value.c_str());
                break;
            case query_field_is_uxview:
                hit->is_uxview = (bool) atoi(data_.query->at(i).q_value.c_str());
                break;
            case query_field_is_gpview:
                hit->is_gpview = (bool) atoi(data_.query->at(i).q_value.c_str());
                break;
            case query_field_is_goal:
                hit->is_goal = (bool) atoi(data_.query->at(i).q_value.c_str());
                break;
            case query_field_unknown:
            default:
                std::cerr << "Unknown field " << data_.query->at(i).q_field
                << std::endl;
                break;
        }
    }

    return 0;
}

int
HitParser::http_data_cb(http_parser *parser, const char *at, size_t length) {
    hit_parser_data_t *data = (hit_parser_data_t *) parser->data;
    switch (parser->state) {
        case 44:
            // Prevent out of range exception
            if (data->headers->size() == MAX_HEADER_LINES) {
                std::cerr
                << "Amount of header lines in request exceeds maximum allowed value"
                << std::endl;
                throw new std::exception;
            }
            data->h_process.h_field = std::string(at, length);
            break;
        case 50:
            data->h_process.h_value = std::string(at, length);
            // Store current pair to the headers vector
            data->headers->push_back(data->h_process);
            break;
        case 32:
            data->url = std::string(at, length);
            break;
        default:
            std::cerr << "Invalid parser state intercepted: " << parser->state
            << std::endl;
            throw new std::exception;
    }
    return 0;
}

void HitParser::parse_query(const std::string *url_str, std::vector<hit_parser_query_t> *query_v) {

    http_parser_url *url = new http_parser_url;

    http_parser_parse_url(url_str->c_str(), url_str->length(), 0, url);

    std::string query_string = std::string(
            (char *) url_str->c_str() + sizeof(char) * url->field_data[4].off,
            url->field_data[4].len);


    std::string field, value;
    std::string::size_type off = 0;

    for (std::string::size_type i = 0; i < query_string.size(); ++i) {

        if (query_string.compare(i, 1, "=") == 0) {

            field = query_string.substr(off, i - off);
            off = i + 1;

        } else if (query_string.compare(i, 1, "&") == 0) {

            // Prevent out of range exception
            if (query_v->size() == MAX_QUERY_NUM) {
                std::cerr << "Amount of query parameters in request exceeds maximum allowed value" << std::endl;
                throw new std::exception;
            }

            value = query_string.substr(off, i - off);
            off = i + 1;

            query_v->push_back(hit_parser_query_t {field, value});
        }
        else if (i == (query_string.size() - 1)) {

            value = query_string.substr(off, i - off + 1);

            query_v->push_back(hit_parser_query_t {field, value});
        }
    }
}

HitParser::~HitParser() {
}

