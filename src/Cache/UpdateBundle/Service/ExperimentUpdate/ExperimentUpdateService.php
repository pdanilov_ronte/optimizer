<?php

namespace Cache\UpdateBundle\Service\ExperimentUpdate;

use Shared\SoaBundle\Service\RequestData;
use Shared\SoaBundle\Service\AbstractServiceRequester;


/**
 * @class   ExperimentUpdateService
 * @package Cache\UpdateBundle\Service\ExperimentUpdate
 * @author  PM:/ <pm@spiral.ninja>
 */
class ExperimentUpdateService extends AbstractServiceRequester
{
    /**
     * @param $data
     * @return ExperimentUpdateRequest
     */
    public function update(array $data)
    {
        $request = $this->createRequest($data);
        $provider = $this->createProvider();

        $provider->update($request);

        return $request;
    }

    /**
     * @param array $data
     * @return ExperimentUpdateRequest
     */
    protected function createRequest(array $data)
    {
        $request = new ExperimentUpdateRequest(new RequestData($data));
        parent::prepareRequest($request, $data);

        return $request;
    }

    /**
     * @return ExperimentUpdateProvider
     */
    protected function createProvider()
    {
        $provider = new ExperimentUpdateProvider();
        parent::prepareProvider($provider);

        return $provider;
    }
}
