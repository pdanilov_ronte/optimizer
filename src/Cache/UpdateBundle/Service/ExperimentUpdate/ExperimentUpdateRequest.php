<?php

namespace Cache\UpdateBundle\Service\ExperimentUpdate;

use Shared\SoaBundle\Service\AbstractServiceRequest;


/**
 * @class   ExperimentUpdateRequest
 * @package Cache\UpdateBundle\Service\ExperimentUpdate
 * @author  PM:/ <pm@spiral.ninja>
 */
class ExperimentUpdateRequest extends AbstractServiceRequest
{
}
