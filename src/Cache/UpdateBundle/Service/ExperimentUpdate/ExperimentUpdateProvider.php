<?php

namespace Cache\UpdateBundle\Service\ExperimentUpdate;

use Base\ExperimentsBundle\Entity\Experiment;
use Base\ExperimentsBundle\Entity\ExperimentUrl;
use Base\ExperimentsBundle\Entity\Goal;
use Base\ExperimentsBundle\Entity\Variation;
use Base\UsersBundle\Entity\Account;
use Client\CacheBundle\Entity\VariationCache;
use Client\CacheBundle\Service\JsStorage\JsStorageService;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Filter\SQLFilter;
use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceProvider;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Process\Exception\InvalidArgumentException;


class ExperimentUpdateProvider extends AbstractServiceProvider
{
    /**
     * @param ExperimentUpdateRequest $request
     * @return void
     */
    public function update(ExperimentUpdateRequest $request)
    {
        $input = $request->getRequestData()->getData();

        // Validate request data
        if (!isset($input['id'])) {
            $request->setException(
                new ServiceException('Parameter `id` must be set in request data')
            );

            return;
        }
        if (!is_numeric($input['id'])) {
            $request->setException(
                new ServiceException(sprintf('Invalid value for `id` parameter (%s)', $input['id']))
            );

            return;
        }

        $account_id = isset($input['account_id']) ? $input['account_id'] : 0;
        $experiment_id = $input['id'];

        /** @type Registry $doctrine */
        $doctrine = $this->container->get('doctrine');
        /** @type EntityManager $em */
        $em = $doctrine->getManager();

        $filters = $em->getFilters()->getEnabledFilters();

        /** @type SQLFilter $filter */
        foreach ($filters as $filter) {
            $filter->setParameter('account_id', $account_id);
        }

        /** @type Experiment $experiment */
        $experiment = null;

        // Fetch experiment data directly in order to avoid HTTP Auth
        try {

            /** @type Account $account */
            $account = $em->getRepository('BaseUsersBundle:Account')
                ->find($account_id);

            $uid = $account->getUid();

            if (empty($uid)) {
                throw new \InvalidArgumentException("Property `uid` is not defined for the account");
            }

            $experiment = $em->getRepository('BaseExperimentsBundle:Experiment')
                ->find($experiment_id);

            $em->beginTransaction();

            // Wipe existing experiment cache data
            $em->getRepository('ClientCacheBundle:VariationCache')
                ->createQueryBuilder('c')
                ->delete()
                ->where('c.experimentId = :experiment_id')
                ->setParameter('experiment_id', $experiment)
                ->getQuery()
                ->execute();

            /** @type Variation $variation */
            foreach ($experiment->getVariations() as $variation) {
                $cache = new VariationCache();
                $cache->setNumber($variation->getNumber());
                $cache->setWeight($variation->getWeight());
                $cache->setExperimentId($input['id']);
                $cache->setVariationId($variation->getId());

                $em->persist($cache);
            }

            $em->flush();
            $em->commit();

        } catch (Exception $e) {

            $em->rollback();

            $request->setException(
                new ServiceException($e->getMessage())
            );

            return;
        }

        $goals = [];
        foreach ($experiment->getGoals() as $goal) {
            $goals[] = [
                'id' => $goal->getId(),
                'type' => $goal->getType(),
                'destination' => $goal->getDestination()->getValue()
            ];
        }

        $variations = [];
        foreach ($experiment->getVariations() as $variation) {
            $_variation = [
                'number' => $variation->getNumber(),
                'weight' => $variation->getWeight(),
                'snippetValues' => []
            ];
            // iterate over snippets
            foreach ($variation->getSnippetValues() as $snippetValue) {
                $_variation['snippetValues'][$snippetValue->getSnippet()->getNumber()] = $snippetValue->getValue();
            }
            $variations[] = $_variation;
        }

        /** @type JsStorageService $storage */
        $storage = $this->container->get('opt.client.cache.js');

        $patterns = $storage->loadPatterns(['uid' => $uid])->getRequestData()->getData()['patterns'];
        $patterns[$experiment_id] = [];
        if ($experiment->getState() == Experiment::STATE_RUNNING) {
            foreach ($experiment->getUrls() as $url) {
                $patterns[$experiment_id][] = $url->getPattern();
            }
            foreach ($experiment->getGoals() as $goal) {
                if ($goal->getType() == Goal::TYPE_DESTINATION) {
                }
                $patterns[$experiment_id][] = $goal->getDestination()->getValue();
            }
        }
        $storage->savePatterns([
            'uid' => $uid,
            'patterns' => $patterns
        ]);

        /** @type TwigEngine $twig */
        $twig = $this->container->get('templating');

        $contents = $twig->render('ClientJSBundle:JS:async.js.twig', [
            'experiment_id' => $experiment_id,
            'variations' => json_encode($variations),
            'goals' => json_encode($goals),
            'variation_mode' => Experiment::mapMode($experiment->getMode()),
            'hit_url' => $this->container->getParameter('collect.url')
        ]);
        $result = $storage->store([
            'uid' => $uid,
            'experiment_id' => $experiment_id,
            'contents' => $contents
        ]);
        if ($result->isFailed()) {
            $request->setException($result->getException());

            return;
        }
    }
}
