<?php

namespace Cache\NotifyBundle\Service\ExperimentNotify;

use Shared\SoaBundle\Service\AbstractServiceProvider;
use Shared\SoaBundle\Service\ServiceException;
use Cache\UpdateBundle\Service\ExperimentUpdate\ExperimentUpdateService;


/**
 * @class   ExperimentNotifyProvider
 * @package Cache\NotifyBundle\Service\ExperimentNotify
 * @author  PM:/ <pm@spiral.ninja>
 */
class ExperimentNotifyProvider extends AbstractServiceProvider
{
    /**
     * @param ExperimentNotifyRequest $request
     * @return void
     */
    public function notify(ExperimentNotifyRequest $request)
    {
        // Validate request
        $data = $request->getRequestData()->getData();
        if (!isset($data['id']) || !is_numeric($data['id'])) {
            $request->setException(
                new ServiceException('Parameter `id` must be set in request data')
            );

            return;
        }

        /**
         * Pull `cache.experiment.update` service from container
         * @var ExperimentUpdateService $update
         */
        $update = $this->container->get('opt.cache.update.experiment');

        // Callback update method
        $result = $update->update($data);

        if ($result->isFailed()) {
            $request->setException(
                new ServiceException($result->getException()->getMessage())
            );

            return;
        }
    }
}
