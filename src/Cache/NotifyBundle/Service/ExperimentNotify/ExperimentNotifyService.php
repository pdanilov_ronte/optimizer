<?php

namespace Cache\NotifyBundle\Service\ExperimentNotify;

use Shared\SoaBundle\Service\RequestData;
use Shared\SoaBundle\Service\AbstractServiceRequester;


/**
 * @class   ExperimentNotifyService
 * @package Cache\NotifyBundle\Service\ExperimentNotify
 * @author  PM:/ <pm@spiral.ninja>
 */
class ExperimentNotifyService extends AbstractServiceRequester
{
    /**
     * @param array $data
     * @return ExperimentNotifyRequest
     */
    public function notify(array $data)
    {
        $request = $this->createRequest($data);
        $provider = $this->createProvider();

        $provider->notify($request);

        return $request;
    }

    /**
     * @param array $data
     * @return ExperimentNotifyRequest
     */
    protected function createRequest(array $data)
    {
        $request = new ExperimentNotifyRequest(new RequestData($data));
        parent::prepareRequest($request, $data);

        return $request;
    }

    /**
     * @return ExperimentNotifyProvider
     */
    protected function createProvider()
    {
        $provider = new ExperimentNotifyProvider();
        parent::prepareProvider($provider);

        return $provider;
    }
}
