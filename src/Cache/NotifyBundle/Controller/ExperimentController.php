<?php

namespace Cache\NotifyBundle\Controller;

use Base\UsersBundle\Entity\Account;
use Cache\NotifyBundle\Service\ExperimentNotify\ExperimentNotifyService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use /** @noinspection PhpUnusedAliasInspection */
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use /** @noinspection PhpUnusedAliasInspection */
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


/**
 * @package Cache\NotifyBundle\Controller
 * @author  PM:/ <pm@spiral.ninja>
 *
 * @Route("/experiment")
 */
class ExperimentController extends Controller
{
    /**
     * @Route("/publish/{id}")
     * @Template()
     *
     * @param $id
     * @return array
     */
    public function publishAction($id)
    {
        /**
         * Pull `cache.notify.experiment` service from container
         * @var ExperimentNotifyService $notify
         */
        $notify = $this->get('opt.cache.notify.experiment');

        /** @type Account $account */
        $account = $this->container->get('security.context')->getToken()->getUser();

        // Call notify method
        $notify->notify([
            'id' => $id,
            'account_id' => $account->getId()
        ]);

        return [
            'id' => $id
        ];
    }

}
