<?php

namespace Client\TrackBundle\Service\TrackEvent;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Shared\SoaBundle\Service\AbstractServiceRequester;
use Shared\SoaBundle\Service\RequestInfo;
use Base\UsersBundle\DependencyInjection\AccountAwareTrait;


class TrackEventService extends AbstractServiceRequester
{
    use AccountAwareTrait;

    /**
     * @param array $data
     * @param array $info
     * @return TrackEventRequest
     *
     * @Route("/track")
     */
    public function track(array $data, array $info)
    {
        $request = $this->createRequest($data);

        if (!isset($info['remoteAddr'])) {
            throw new \InvalidArgumentException('Remote address must be defined');
        }

        $requestInfo = new RequestInfo();
        $requestInfo->setRemoteAddr($info['remoteAddr']);

        $request->setRequestInfo($requestInfo);

        $request->setAccount($this->getAccount());

        return $this->createProvider()
            ->track($request);
    }

    /**
     * @param array $data
     * @return TrackEventRequest
     */
    protected function createRequest(array $data)
    {
        return parent::prepareRequest(new TrackEventRequest(), $data);
    }

    /**
     * @return TrackEventProvider
     */
    protected function createProvider()
    {
        return parent::prepareProvider(new TrackEventProvider());
    }
}
