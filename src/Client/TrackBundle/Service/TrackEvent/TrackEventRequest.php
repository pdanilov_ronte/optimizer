<?php

namespace Client\TrackBundle\Service\TrackEvent;

use Shared\SoaBundle\Service\AbstractServiceRequest;
use Base\UsersBundle\Entity\Account;


class TrackEventRequest extends AbstractServiceRequest
{
    /**
     * @type array
     */
    private $response = [];

    /**
     * @type Account
     */
    private $account;

    public function setResponse(array $response)
    {
        $this->response = $response;
    }

    /**
     * @return array
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }
}
