<?php

namespace Client\TrackBundle\Service\TrackEvent;

use Shared\SoaBundle\Service\AbstractServiceProvider;
use Client\TrackBundle\Entity\TrackedEvent;


class TrackEventProvider extends AbstractServiceProvider
{
    /**
     * @param TrackEventRequest $request
     * @return TrackEventRequest
     */
    public function track(TrackEventRequest $request)
    {
        $account = $request->getAccount();
        if (empty($account)) {
            throw new \InvalidArgumentException('Account must be defined');
        }

        $data = $request->getRequestData()->getData();
        $info = $request->getRequestInfo();

        $event = new TrackedEvent();

        $event->setAccount($account);
        
        $event->setUid($data['session']['uid']);
        $event->setSid($data['session']['sid']);
        $event->setPage($data['session']['page']);
        $event->setTimestamp($data['timestamp']);
        $event->setType($data['type']);

        $data = isset($data['data']) ? $data['data'] : [];
        if ($event->getType() == 11) {
            $data['remoteAddr'] = $info->getRemoteAddr();
        }
        $event->setData(json_encode($data));

        $em = $this->container->get('doctrine')
            ->getManager();

        try {
            $em->persist($event);
            $em->flush();
        } catch (\Exception $e) {
            $request->setError($e->getMessage());
            return $request;
        }

        $request->setResponse([]);

        return $request;
    }
}
