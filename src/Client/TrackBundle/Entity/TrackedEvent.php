<?php

namespace Client\TrackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Base\UsersBundle\Entity\Account;


/**
 * TrackedEvent
 *
 * @ORM\Table("event_tracker", indexes={
 *     @Index(name="uid_idx", columns={"uid"}),
 *     @Index(name="sid_idx", columns={"sid"}),
 *     @Index(name="page_idx", columns={"page"}),
 *     @Index(name="type_idx", columns={"type"})
 *     })
 * @ORM\Entity(repositoryClass="Client\TrackBundle\Entity\TrackedEventRepository")
 */
class TrackedEvent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uid", type="string", length=32)
     */
    private $uid;

    /**
     * @var string
     *
     * @ORM\Column(name="sid", type="string", length=16)
     */
    private $sid;

    /**
     * @var string
     *
     * @ORM\Column(name="page", type="string", length=16)
     */
    private $page;

    /**
     * @var int
     *
     * @ORM\Column(name="timestamp", type="bigint")
     */
    private $timestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="text")
     */
    private $data;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Base\UsersBundle\Entity\Account")
     */
    private $account;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uid
     *
     * @param string $uid
     * @return TrackedEvent
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid
     *
     * @return string 
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set sid
     *
     * @param string $sid
     * @return TrackedEvent
     */
    public function setSid($sid)
    {
        $this->sid = $sid;

        return $this;
    }

    /**
     * Get sid
     *
     * @return string 
     */
    public function getSid()
    {
        return $this->sid;
    }

    /**
     * Set page
     *
     * @param string $page
     * @return TrackedEvent
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return string 
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set timestamp
     *
     * @param int $timestamp
     * @return TrackedEvent
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return int
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return TrackedEvent
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return TrackedEvent
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }
}
