<?php

namespace Client\TrackBundle\Controller;

use Client\TrackBundle\Service\TrackEvent\TrackEventService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    /**
     * @Route("/initialize")
     */
    public function initializeAction()
    {
        return new JsonResponse([]);
    }

    /**
     * @Route("/track")
     */
    public function indexAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data)) {
            return new JsonResponse([], 400);
        }

        $info = [
            'remoteAddr' => $request->server->get('REMOTE_ADDR')
        ];

        /** @var TrackEventService $tracker */
        $tracker = $this->get('so_tracker.track_event');
        $result = $tracker->track($data, $info);

        if ($result->isFailed()) {
            return new JsonResponse([
                'error' => $result->getException()->getMessage()
            ], 400);
        }

        return new JsonResponse([]);
    }
}
