<?php

namespace Client\CacheBundle\Service\JsStorage;

use Shared\SoaBundle\Service\AbstractServiceRequest;
use Shared\SoaBundle\Service\ServiceRequestAttachment;


/**
 * @class   JsStorageRequest
 * @package Client\CacheBundle\Service\JsStorage
 * @author  PM:/ <pm@spiral.ninja>
 */
class JsStorageRequest extends AbstractServiceRequest
{
    public function getAttachment()
    {
        return parent::getAttachment();
    }

    public function setAttachment(ServiceRequestAttachment $attachment)
    {
        parent::setAttachment($attachment);
    }
}
