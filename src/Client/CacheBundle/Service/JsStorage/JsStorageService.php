<?php

namespace Client\CacheBundle\Service\JsStorage;

use Shared\SoaBundle\Service\RequestData;
use Shared\SoaBundle\Service\AbstractServiceRequester;


/**
 * @class   JsStorageService
 * @package Client\CacheBundle\Service\JsStorage
 * @author  PM:/ <pm@spiral.ninja>
 */
class JsStorageService extends AbstractServiceRequester
{
    /**
     * @param array $data
     * @return JsStorageRequest
     */
    public function store(array $data)
    {
        $request = $this->createRequest($data);
        $provider = $this->createProvider();

        $provider->store($request);

        return $request;
    }

    /**
     * @param array $data
     * @return JsStorageRequest
     */
    public function savePatterns(array $data)
    {
        $request = $this->createRequest($data);
        $provider = $this->createProvider();

        $provider->savePatterns($request);

        return $request;
    }

    /**
     * @param array $data
     * @return JsStorageRequest
     */
    public function loadPatterns(array $data)
    {
        $request = $this->createRequest($data);
        $provider = $this->createProvider();

        $provider->loadPatterns($request);

        return $request;
    }

    /**
     * @param array $data
     * @return JsStorageRequest
     */
    protected function createRequest(array $data)
    {
        $request = new JsStorageRequest(new RequestData($data));
        parent::prepareRequest($request, $data);

        return $request;
    }

    /**
     * @return JsStorageProvider
     */
    protected function createProvider()
    {
        $provider = new JsStorageProvider();
        parent::prepareProvider($provider);

        return $provider;
    }
}
