<?php

namespace Client\CacheBundle\Service\JsStorage;

use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceProvider;
use Symfony\Component\Filesystem\Filesystem;


/**
 * @class   JsStorageProvider
 * @package Client\CacheBundle\Service\JsStorage
 * @author  PM:/ <pm@spiral.ninja>
 */
class JsStorageProvider extends AbstractServiceProvider
{
    public function store(JsStorageRequest $request)
    {
        $data = $request->getRequestData()->getData();

        // Validate input data
        if (!isset($data['experiment_id'])) {
            $request->setException(
                new ServiceException('Parameter `experiment_id` must be set in request data')
            );

            return;
        }

        if (!is_numeric($data['experiment_id'])) {
            $request->setException(
                new ServiceException(sprintf('Invalid value for parameter `experiment_id` (%s)', $data['experiment_id']))
            );

            return;
        }

        if (!isset($data['uid']) || empty($data['uid'])) {
            $request->setException(
                new ServiceException('Parameter `uid` must be set in request data')
            );

            return;
        }

        if (!isset($data['contents'])) {
            $request->setException(
                new ServiceException('Parameter `contents` must be set in request data')
            );

            return;
        }

        // Open target directory, create if necessary
        /** @noinspection RealpathOnRelativePathsInspection */
        $dirname = sprintf(realpath(__DIR__ . '/../../Standalone/c/') . '/%s', $data['uid']);

        $filesystem = new Filesystem();

        if (!$filesystem->exists($dirname)) {
            $filesystem->mkdir($dirname);
            //$filesystem->chmod($dirname, 0777, 0000);
        }

        // Store target js file
        $filename = sprintf('%s/%s.js', $dirname, $data['experiment_id']);
        file_put_contents($filename, $data['contents']);

        //$filesystem->chmod($filename, 0666, 0000);
    }

    public function savePatterns(JsStorageRequest $request)
    {
        $data = $request->getRequestData()->getData();

        if (!isset($data['uid'])) {
            $request->setException(
                new ServiceException('Parameter `uid` must be set in request data')
            );

            return;
        }

        /** @noinspection RealpathOnRelativePathsInspection */
        $dirname = realpath(__DIR__ . '/../../Standalone/x/');

        $filesystem = new Filesystem();

        if (!$filesystem->exists($dirname)) {
            $filesystem->mkdir($dirname);
        }

        $filename = sprintf('%s/%s.php', $dirname, $data['uid']);
        $patterns = $data['patterns'];

        /** @noinspection ForgottenDebugOutputInspection */
        $contents = sprintf(<<<C
<?php

\$patterns = %s;

C
        , var_export($patterns, true));

        file_put_contents($filename, $contents);
    }

    public function loadPatterns(JsStorageRequest $request)
    {
        $data = $request->getRequestData()->getData();

        if (!isset($data['uid'])) {
            $request->setException(
                new ServiceException('Parameter `uid` must be set in request data')
            );

            return;
        }

        /** @noinspection RealpathOnRelativePathsInspection */
        $dirname = realpath(__DIR__ . '/../../Standalone/x/');
        $filename = sprintf('%s/%s.php', $dirname, $data['uid']);

        $patterns = [];

        if (file_exists($filename)) {
            /** @noinspection PhpIncludeInspection */
            include $filename;
        }

        $data['patterns'] = $patterns;
        $request->getRequestData()->setData($data);
    }
}
