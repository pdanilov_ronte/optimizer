<?php

namespace Client\CacheBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VariationCache
 *
 * @ORM\Table(name="variation_cache")
 * @ORM\Entity(repositoryClass="Client\CacheBundle\Entity\VariationCacheRepository")
 */
class VariationCache
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="experiment_id", type="integer")
     */
    private $experimentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="variation_id", type="integer")
     */
    private $variationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="smallint")
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="weight", type="decimal", precision=4, scale=2, nullable=true)
     */
    private $weight;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set experimentId
     *
     * @param integer $experimentId
     * @return VariationCache
     */
    public function setExperimentId($experimentId)
    {
        $this->experimentId = $experimentId;

        return $this;
    }

    /**
     * Get experimentId
     *
     * @return integer 
     */
    public function getExperimentId()
    {
        return $this->experimentId;
    }

    /**
     * Set variationId
     *
     * @param integer $variationId
     * @return VariationCache
     */
    public function setVariationId($variationId)
    {
        $this->variationId = $variationId;

        return $this;
    }

    /**
     * Get variationId
     *
     * @return integer 
     */
    public function getVariationId()
    {
        return $this->variationId;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return VariationCache
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set weight
     *
     * @param string $weight
     * @return VariationCache
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return string 
     */
    public function getWeight()
    {
        return $this->weight;
    }
}
