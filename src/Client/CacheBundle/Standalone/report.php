<?php

require __DIR__ . '/config.php';

define('DATA_FILE', __DIR__ . '/r/report.log');
define('TRACK_FILE', __DIR__ . '/r/track.log');

if (!file_exists(DATA_FILE) || !is_readable(DATA_FILE)) {
    throw new \Exception('Report file does not exist or is not readable');
}

if (!is_writable(DATA_FILE)) {
    throw new \Exception('Report file is not writable');
}

$fp = fopen(DATA_FILE, 'r+');
if ($fp == false) {
    throw new \Exception('Cannot open report file for reading/writing');
}

if (!flock($fp, LOCK_EX)) {
    throw new \Exception('Cannot get the lock for report file');
}

$data = file(DATA_FILE);

ftruncate($fp, 0);
flock($fp, LOCK_UN);
fclose($fp);

$time = time();

$report = [
    'broken' => 0,
    'invalid' => 0,
    'malformed' => 0,
    'empty' => 0,
    'active' => 0,
    'total' => 0
];

$track = [];

foreach ($data as $k => $line) {
    $parts = explode('] [', substr($line, 1, -1));
    switch ($parts[1]) {
        case 'BROKEN':
            ++$report['broken'];
            file_put_contents(TRACK_FILE, $line, FILE_APPEND);
            break;
        case 'INVALID':
            ++$report['invalid'];
            file_put_contents(TRACK_FILE, $line, FILE_APPEND);
            break;
        case 'MALFORMED':
            ++$report['malformed'];
            file_put_contents(TRACK_FILE, $line, FILE_APPEND);
            break;
        case 'EMPTY':
            ++$report['empty'];
            if (!isset($track[$parts['2']])) {
                $track[$parts['2']] = ['empty' => 0, 'active' => 0];
            }
            ++$track[$parts['2']]['empty'];
            break;
        case 'ACTIVE':
            ++$report['active'];
            if (!isset($track[$parts['2']])) {
                $track[$parts['2']] = ['empty' => 0, 'active' => 0];
            }
            ++$track[$parts['2']]['active'];
            break;
        default:
            throw new \Exception(sprintf('Invalid record type "%s"', $parts[1]));
    }
    ++$report['total'];
}

$db = new mysqli('localhost', MYSQL_USER, MYSQL_PASS, MYSQL_DB);
if ($db->connect_errno) {
    throw new \Exception(sprintf('Failed to connect to db: %s', $db->connect_error));
}

$st = $db->prepare('INSERT `visitor_activity_report` VALUES (?, ?, ?, ?, ?, ?, ?)');
if (!$st->bind_param('iiiiiii', $time, $report['broken'], $report['invalid'], $report['malformed'],
    $report['empty'], $report['active'], $report['total'])
) {
    throw new \Exception('Failed to bind query params');
}
if (!$st->execute()) {
    throw new \Exception($st->error);
}

$sq = <<<SQ
INSERT visitor_activity_tracking
    (`id`, `account_id`, `date`, `empty`, `active`)
    VALUES (NULL, (SELECT id FROM account WHERE uid = ?), ?, ?, ?)
    ON DUPLICATE KEY UPDATE empty = empty + ?, active = active + ?
SQ;
$st = $db->prepare($sq);
if (!$st) {
    throw new \Exception($db->error);
}
$date = date('Y-m-d', $time);

foreach ($track as $uid => $values) {
    if (!$st->bind_param('ssiiii', $uid, $date, $values['empty'],
        $values['active'], $values['empty'], $values['active'])
    ) {
        throw new \Exception('Failed to bind query params');
    }
    if (!$st->execute()) {
        throw new \Exception($st->error);
    }
}
