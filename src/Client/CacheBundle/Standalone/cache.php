<?php

define('REPORT_FILE', __DIR__ . '/r/report.log');


/**
 * Get stored mysqli connection (persistent)
 *
 * @return mysqli
 */
function getConnection()
{
    static $conn;

    if (!isset($conn)) {
        $conn = mysqli_connect(
            'p:localhost',
            'pm',
            '1q2w3e',
            'opt_master'
        );
    }

    return $conn;
}


/**
 * Get the list of variations for a specific experiment stored in caching table
 *
 * @param $id
 * @return array
 */
function getCache($id)
{
    $conn = getConnection();

    // @todo Passed id value should be validated (regex is an option but also the slowest solution)
    $id = mysqli_escape_string($conn, $id);

    $r = mysqli_query(
        $conn,
        sprintf('-- noinspection SqlDialectInspection
SELECT * FROM `variation_cache` WHERE `experiment_id` = "%s"', $id)
    );

    $result = [];

    while ($e = $r->fetch_assoc()) {
        $result[] = $e;
    }

    return $result;
}


/**
 * Choose variation for an experiment of `weight` type
 *
 * @param array $cache Array of cached variations for the experiment
 * @return int
 */
function chooseVariationWeight($cache)
{
    $implicit = [];
    $range = [];

    foreach ($cache as $n => $entry) {
        if (null !== $entry['weight']) {
            $c = (int)$entry['weight'];
            if ($c === 0) {
                throw new InvalidArgumentException(
                    sprintf('Variation weight can\'t be zero (number %s)', $n));
            }
            /** @noinspection AdditionOperationOnArraysInspection */
            $range += array_fill(count($range), (int)$entry['weight'], $n);
        } else {
            $implicit[] = $n;
        }
    }

    $share = (0 !== count($implicit)) ?
        0 : floor((100 - count($range)) / count($implicit));

    foreach ($implicit as $n) {
        /** @noinspection AdditionOperationOnArraysInspection */
        $range += array_fill(count($range), $share, $n);
    }

    /**
     * In case if shares approximation eaten some cells
     */
    if (count($range) < 100) {
        $last = $cache[count($cache) - 1];
        /** @noinspection AdditionOperationOnArraysInspection */
        $range += array_fill(count($range), 100 - count($range), $last['number']);
    }

    return $range[mt_rand(0, 99)];
}

function hash_u_int32($str)
{
    $hash = 5381;
    $i = strlen($str);

    while ($i) {
        $hash = (($hash * 33) & 0xffffffff) ^ ord($str[--$i]);
    }

    return $hash >> 0;
}

function permute_int32_reverse($x)
{
    // Maximum prime number less than unsigned 32 integer
    static $prime = 4294967291;

    if ($x > $prime) {
        throw new \Exception('Maximum allowed number for the permutation is 4294967291');
    }

    $residue = ($x * $x) % $prime;

    return ($x <= ($prime / 2)) ? $residue : ($prime - $residue);
}


function generate_uid_number($a)
{
    $b = bin2hex(openssl_random_pseudo_bytes(1));
    $d = intval($b, 16);
    $r = $d / 255;
    $mx = 98 / 255;
    $mn = 1 / 255;
    $v = round(($r * $mx + $mn) * 255);
    $s = sprintf('%s%06s', dechex($v), dechex($a));

    $x = intval($s, 16);
    $y = permute_int32_reverse($x);

    return $y;
}

function generate_uid($a)
{
    $n = dechex(generate_uid_number($a));

    $bl = bin2hex(openssl_random_pseudo_bytes(2));
    $br = bin2hex(openssl_random_pseudo_bytes(2));

    return sprintf('%04s-%04s-%08s', $bl, $br, $n);
}


/**
 * Run the script
 */
header('Access-Control-Allow-Origin', '*');
header('Content-Type: text/plain; charset=utf-8');


/**
 * Generate cached javascript file and return its' name
 *
 * @return string
 */
function run()
{
    $timestamp = time();
    $uri = $_REQUEST['uri'];

    if (!preg_match('|^/a/\w{4}-\w{4}-\w{8}\.js$|', $uri)) {
        file_put_contents(REPORT_FILE, sprintf("[%s] [BROKEN] [%s] [%s]\n", $timestamp, $uri, $_SERVER['HTTP_REFERER']), FILE_APPEND);
        print '';
        return;
    }

    $scope = substr($uri, 1, 1);
    $uid = substr($uri, 3, 18);

    $patterns_file = __DIR__ . '/x/' . $uid . '.php';
    if (!is_file($patterns_file)) {
        file_put_contents(REPORT_FILE, sprintf("[%s] [INVALID] [%s] [%s]\n", $timestamp, $uid, $_SERVER['HTTP_REFERER']), FILE_APPEND);
        print '';
        return;
    }

    if (!isset($_SERVER['HTTP_REFERER']) || empty($_SERVER['HTTP_REFERER'])) {
        file_put_contents(REPORT_FILE, sprintf("[%s] [MALFORMED] [%s] []\n", $timestamp, $uid), FILE_APPEND);
        print '';
        return;
    }

    $parts = parse_url($_SERVER['HTTP_REFERER']);
//    $search = $parts['scheme'] . '://' . $parts['host'] . $parts['path'];
    $search = $parts['host'] . $parts['path'];

    // Filename to save JS copy for explicit queries
    /*$explicit = false;
    if ($scope === 's' && strlen($uri) >= 25) {
        $hash = sprintf('%s-%s-%s',
            dechex(hash_u_int32($parts['host'] . $parts['path'])),
            dechex(hash_u_int32($parts['host'])),
            dechex(hash_u_int32($parts['path']))
        );
        $explicit = __DIR__ . '/s/' . $hash . '.js';
    }*/

    $patterns = [];
    $xid = [];

    /** @noinspection PhpIncludeInspection */
    require $patterns_file;

    foreach ($patterns as $experiment_id => $experiment_patterns) {
        foreach ($experiment_patterns as $pattern) {
            $pattern = preg_replace('|^https?://|', '', $pattern);
            $pattern = preg_quote($pattern, '|');
            $pattern = str_replace(['\?', '\*'], ['?', '.*'], $pattern);
            if (preg_match('|^' . $pattern . '$|', $search)) {
                $xid[] = $experiment_id;
            }
        }
    }

    $xid = array_unique($xid);

    $contents = '';
    foreach ($xid as $x) {
        $contents .= file_get_contents(__DIR__ . '/c/' . $uid . '/' . $x . '.js') . "\n";
    }

    if (!empty($xid)) {
        file_put_contents(REPORT_FILE, sprintf("[%s] [ACTIVE] [%s] [%s] [%s]\n",
            $timestamp, $uid, join(',', $xid), $_SERVER['HTTP_REFERER']), FILE_APPEND);
    } else {
        file_put_contents(REPORT_FILE, sprintf("[%s] [EMPTY] [%s] [%s]\n",
            $timestamp, $uid, $_SERVER['HTTP_REFERER']), FILE_APPEND);
    }

    /*if ($explicit) {
        file_put_contents($explicit, $contents);
    }*/

    print $contents;
}

run();
