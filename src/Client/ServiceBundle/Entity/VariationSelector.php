<?php

namespace Client\ServiceBundle\Entity;

use Base\ExperimentsBundle\Entity\Experiment;
use Doctrine\ORM\Mapping as ORM;


/**
 * VariationSelector
 *
 * @ORM\Table(name="variation_selector")
 * @ORM\Entity
 */
class VariationSelector
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="smallint")
     */
    private $number;

    /**
     * @var integer
     *
     * @ORM\Column(name="occurrence", type="integer")
     */
    private $occurrence;

    /**
     * @var string
     *
     * @ORM\Column(name="session", type="string", length=32)
     */
    private $session;

    /**
     * @var Experiment
     *
     * @ORM\ManyToOne(targetEntity="Base\ExperimentsBundle\Entity\Experiment")
     */
    private $experiment;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return VariationSelector
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set occurrence
     *
     * @param integer $occurrence
     * @return VariationSelector
     */
    public function setOccurrence($occurrence)
    {
        $this->occurrence = $occurrence;

        return $this;
    }

    /**
     * Get occurrence
     *
     * @return integer 
     */
    public function getOccurrence()
    {
        return $this->occurrence;
    }

    /**
     * Set session
     *
     * @param string $session
     * @return VariationSelector
     */
    public function setSession($session)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session
     *
     * @return string 
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @return Experiment
     */
    public function getExperiment()
    {
        return $this->experiment;
    }

    /**
     * @param Experiment $experiment
     * @return VariationSelector
     */
    public function setExperiment(Experiment $experiment)
    {
        $this->experiment = $experiment;

        return $this;
    }
}
