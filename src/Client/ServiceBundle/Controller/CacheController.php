<?php

namespace Client\ServiceBundle\Controller;

use Client\ServiceBundle\Service\RebuildCache\RebuildCacheProvider;
use Client\ServiceBundle\Service\RebuildCache\RebuildCacheRequest;
use Shared\SoaBundle\Service\RequestData;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use /** @noinspection PhpUnusedAliasInspection */
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use /** @noinspection PhpUnusedAliasInspection */
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/cache")
 */
class CacheController extends Controller
{
    /**
     * @Route("/rebuild")
     * @Template()
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function rebuildCacheAction(Request $request)
    {
        $experiment_id = $request->get('experiment_id');

        if (empty($experiment_id)) {
            return new JsonResponse([
                'status' => 'error',
                'message' => 'Parameter `experiment_id` is required'
            ], 500);
        }

        $data = new RequestData([
            'experiment_id' => $experiment_id
        ]);
        $serviceRequest = new RebuildCacheRequest($data);

        /** @type RebuildCacheProvider $provider */
        $provider = $this->container->get('opt.client.variant.rebuild_cache');

        $provider->rebuildCache($serviceRequest);

        if ($serviceRequest->isSuccess()) {
            return new JsonResponse([
                'status' => 'success'
            ]);
        } else {
            $response = new JsonResponse([
                'status' => 'error',
                'message' => json_encode($serviceRequest->getException()->getMessage())
            ], 500);
            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
            return $response;
        }
    }
}
