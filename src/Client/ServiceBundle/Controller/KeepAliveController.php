<?php

namespace Client\ServiceBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Client\ServiceBundle\Service\KeepAlive\KeepAliveService;


/**
 * @Route("/keep-alive")
 */
class KeepAliveController extends Controller
{
    /**
     * @Route("/initialize")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function initialize(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw $this->createNotFoundException();
        }

        /** @type KeepAliveService $service */
        $service = $this->get('so_service.keep_alive');

        $data = $service->initialize([])->getResponse();

        return new JsonResponse([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    /**
     * @Route("/refresh")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function refresh(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw $this->createNotFoundException();
        }

        /** @type KeepAliveService $service */
        $service = $this->get('so_service.keep_alive');

        $data = $service->refresh([])->getResponse();

        return new JsonResponse([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    /**
     * @Route("/close")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function close(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw $this->createNotFoundException();
        }

        /** @type KeepAliveService $service */
        $service = $this->get('so_service.keep_alive');

        $data = $service->close([])->getResponse();

        return new JsonResponse([
            'status' => 'success',
            'data'   => $data
        ]);
    }
}
