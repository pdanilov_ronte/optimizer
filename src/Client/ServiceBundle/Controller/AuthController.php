<?php

namespace Client\ServiceBundle\Controller;

use Base\UsersBundle\DependencyInjection\AccountAwareTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("auth")
 */
class AuthController extends Controller
{
    use AccountAwareTrait;

    /**
     * @Route("/account")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function accountAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw $this->createNotFoundException();
        }

        $account = $this->getAccount();
        $result = [
            'name' => $account->getName(),
            'email' => $account->getEmailCanonical(),
            'username' => $account->getUsername(),
            'uid' => $account->getUid()
        ];

        return new JsonResponse([
            'account' => $result
        ]);
    }
}
