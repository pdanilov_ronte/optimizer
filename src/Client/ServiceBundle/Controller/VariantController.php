<?php

namespace Client\ServiceBundle\Controller;

use Base\ExperimentsBundle\Service\FindVariant\FindVariantProvider;
use Base\ExperimentsBundle\Service\FindVariant\FindVariantRequest;
use Client\ServiceBundle\Service\ChooseVariant\ChooseVariantProvider;
use Client\ServiceBundle\Service\ChooseVariant\ChooseVariantRequest;
use Shared\SoaBundle\Service\RequestData;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use /** @noinspection PhpUnusedAliasInspection */
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use /** @noinspection PhpUnusedAliasInspection */
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/variant")
 */
class VariantController extends Controller
{
    /**
     * @Route("/choose")
     * @Template()
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function chooseVariantAction(Request $request)
    {
        $experiment_id = $request->get('experiment_id');

        if (empty($experiment_id)) {
            return new JsonResponse([
                'status'  => 'error',
                'message' => 'Parameter `experiment_id` is required'
            ], 500);
        }

        $data = new RequestData([
            'experiment_id' => $experiment_id
        ]);
        $serviceRequest = new ChooseVariantRequest($data);

        /** @type ChooseVariantProvider $provider */
        $provider = $this->container->get('opt.client.variant.choose_variant');

        $provider->chooseVariant($serviceRequest);

        if ($serviceRequest->isNotRunning()) {
            return new JsonResponse([
                'status' => 'error',
                'code' => 'NOT_RUNNING', // @todo: implement error codes and messages classes
                'message' => json_encode('Experiment is not running')
            ], 200);
        } elseif (!$serviceRequest->isFailed()) {
            return new JsonResponse([
                'status'  => 'success',
                'variant' => $serviceRequest->getVariant()
            ]);
        } else {
            $response = new JsonResponse([
                'status'  => 'error',
                'message' => json_encode($serviceRequest->getException()->getMessage())
            ], 500);
            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

            return $response;
        }
    }

    /**
     * @Route("/get")
     * @Template()
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getVariantAction(Request $request)
    {
        $experiment_id = $request->get('experiment_id');
        $number = $request->get('variant');

        if (empty($experiment_id)) {
            return new JsonResponse([
                'status'  => 'error',
                'message' => 'Parameter `experiment_id` is required'
            ], 500);
        }

        if (!isset($number)) {
            return new JsonResponse([
                'status'  => 'error',
                'message' => 'Parameter `variant` is required'
            ], 500);
        }

        $data = new RequestData([
            'experiment_id' => $experiment_id,
            'number' => $number
        ]);
        $serviceRequest = new FindVariantRequest($data);

        /** @type FindVariantProvider $provider */
        $provider = $this->container->get('opt.base.variant.find');

        $provider->findVariantByNumber($serviceRequest);

        if (!$serviceRequest->isFailed()) {
            $variant = $serviceRequest->getAttachment()->getVariation();
            return new JsonResponse([
                'status'  => 'success',
                'variant' => $variant->getCode()->getValue()
            ]);
        } else {
            $response = new JsonResponse([
                'status'  => 'error',
                'message' => json_encode($serviceRequest->getException()->getMessage())
            ], 500);
            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

            return $response;
        }
    }
}
