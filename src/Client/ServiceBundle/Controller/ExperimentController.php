<?php

namespace Client\ServiceBundle\Controller;

use Base\ExperimentsBundle\Service\FindExperiment\FindExperimentService;
use Base\ExperimentsBundle\Service\FindVariant\FindVariantProvider;
use Base\ExperimentsBundle\Service\FindVariant\FindVariantRequest;
use Client\ServiceBundle\Service\ChooseVariant\ChooseVariantProvider;
use Client\ServiceBundle\Service\ChooseVariant\ChooseVariantRequest;
use Shared\SoaBundle\Service\RequestData;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use /** @noinspection PhpUnusedAliasInspection */
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use /** @noinspection PhpUnusedAliasInspection */
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/experiment")
 */
class ExperimentController extends Controller
{
    /**
     * @Route("/{id}")
     *
     * @param $id
     * @return JsonResponse
     */
    public function getExperimentAction($id)
    {
        /** @type FindExperimentService $service */
        $service = $this->get('opt.base.experiment.find');

        $request = $service->findExperiment([
            'id' => $id
        ]);

        if (!$request->isFound()) {
            return new JsonResponse([
                'status'  => 'error',
                'message' => sprintf('Experiment %s not found', $id)
            ], 404);
        }

        $experiment = $request->getAttachment()->getExperiment();

        $result = [
            'variations' => [
            ]
        ];

        foreach ($experiment->getVariations() as $variation) {
            $result['variations'][] = [
                'id' => $variation->getId(),
                'weight' => $variation->getWeight()
            ];
        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/variation/{id}/js")
     * @Template()
     *
     * @param $id
     * @return JsonResponse
     */
    public function getVariationJsAction($id)
    {
        if (empty($experiment_id)) {
            return new JsonResponse([
                'status'  => 'error',
                'message' => 'Parameter `experiment_id` is required'
            ], 500);
        }

        if (!isset($number)) {
            return new JsonResponse([
                'status'  => 'error',
                'message' => 'Parameter `variant` is required'
            ], 500);
        }

        $data = new RequestData([
            'experiment_id' => $experiment_id,
            'number'        => $number
        ]);
        $serviceRequest = new FindVariantRequest($data);

        /** @type FindVariantProvider $provider */
        $provider = $this->container->get('opt.base.variant.find');

        $provider->findVariantByNumber($serviceRequest);

        if (!$serviceRequest->isFailed()) {
            $variant = $serviceRequest->getAttachment()->getVariation();

            return new JsonResponse([
                'status'  => 'success',
                'variant' => $variant->getCode()->getValue()
            ]);
        } else {
            $response = new JsonResponse([
                'status'  => 'error',
                'message' => json_encode($serviceRequest->getException()->getMessage())
            ], 500);
            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

            return $response;
        }
    }
}
