<?php

namespace Client\ServiceBundle\Service\ChooseVariant;

use Base\ExperimentsBundle\Entity\Experiment;
use Client\ServiceBundle\Entity\VariationCache;
use Client\ServiceBundle\Entity\VariationSelector;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceProvider;


/**
 * @class   ChooseVariantProvider
 * @package Client\ServiceBundle\Service\ChooseVariant
 * @author  PM:/ <pm@spiral.ninja>
 */
class ChooseVariantProvider extends AbstractServiceProvider
{
    /**
     * @param ChooseVariantRequest $request
     */
    public function chooseVariant(ChooseVariantRequest $request)
    {
        $data = $request->getRequestData()->getData();

        if (!isset($data['experiment_id']) || empty($data['experiment_id'])) {
            $request->operationFailed(
                new ServiceException('Parameter "experiment_id" must be set in request data')
            );
        }

        /** @type Registry $doctrine */
        $doctrine = $this->container->get('doctrine');
        /** @type EntityManager $em */
        $em = $doctrine->getManager();

        try {
            /** @type Experiment $experiment */
            $experiment = $em->getRepository('BaseExperimentsBundle:Experiment')
                ->find($data['experiment_id']);
        } catch (\Exception $e) {
            $request->operationFailed(
                new ServiceException($e->getMessage())
            );
            return;
        }

        if (empty($experiment)) {
            $request->operationFailed(
                new ServiceException(sprintf('Experiment id %s not found', $data['experiment_id']))
            );
            return;
        }

        if ($experiment->getState() != Experiment::STATE_RUNNING) {
            $request->setNotRunning();
            return;
        }

        try {
            $cache = $em->getRepository('ClientServiceBundle:VariationCache')
                ->findBy(['experiment' => $experiment]);
        } catch (\Exception $e) {
            $request->operationFailed(
                new ServiceException($e->getMessage())
            );
            return;
        }

        if (empty($cache)) {
            $request->operationFailed(
                new ServiceException(sprintf('Variants cache for experiment %s is empty', $experiment->getId()))
            );
            return;
        }

        $session = isset($data['session']) ? $data['session'] : '';

        try {
            $selectors = $em->getRepository('ClientServiceBundle:VariationSelector')
                ->findBy([
                    'experiment' => $experiment->getId(),
                    'session' => $session
                ]);
        } catch (\Exception $e) {
            $request->operationFailed(
                new ServiceException($e->getMessage())
            );
            return;
        }

        switch ($experiment->getMethod()) {
            case Experiment::METHOD_SEQ:
                $number = $this->chooseVariantSeq($experiment, $session, $cache, $selectors);
                break;
            case Experiment::METHOD_WEIGHT:
                $number = $this->chooseVariantWeight($cache);
                break;
            case Experiment::METHOD_RTO:
                $request->operationFailed(
                    new ServiceException('Decision method RTO is not yet implemented')
                );
                return;
            default:
                $request->operationFailed(
                    new ServiceException(sprintf('Unknown decision method: %s', $experiment->getMethod()))
                );
                return;
        }

        try {
            foreach ($selectors as $selector) {
                $em->persist($selector);
            }
            $em->flush();
        } catch (\Exception $e) {
            $request->operationFailed(
                new ServiceException($e->getMessage())
            );
            return;
        }

        $request->variantChosen($number);
    }

    private function chooseVariantSeq(Experiment $experiment, $session, $cache, &$selectors)
    {
        if (empty($selectors)) {
            $selector = new VariationSelector();
            $selector->setExperiment($experiment);
            $selector->setSession($session);
            $selector->setNumber(0);
            $selector->setOccurrence(1);
            $selectors[] = $selector;
            return 0;
        }

        /** @type VariationSelector $selector */
        $selector = $selectors[0];
        $number = $selector->getNumber() < (count($cache) - 1)
            ? $selector->getNumber() + 1
            : 0;
        $selector->setNumber($number);

        return $number;
    }

    private function chooseVariantWeight($cache)
    {
        $implicit = [];
        $range = [];

        /** @type VariationCache $entry */
        foreach ($cache as $n => $entry) {
            $weight = $entry->getWeight();
            if (!is_null($weight)) {
                $range += array_fill(count($range), $weight, $n);
            } else {
                $implicit[] = $n;
            }
        }

        $share = floor((100 - count($range))/count($implicit));

        foreach ($implicit as $n) {
            $range += array_fill(count($range), $share, $n);
        }

        /**
         * In case shares approximation eaten some cells.
         * Not the best way to mitigate that but it will do for prototyping.
         */
        if (count($range) < 100) {
            /** @type VariationCache $last */
            $last = $cache[count($cache) - 1];
            $range += array_fill(count($range), 100 - count($range), $last->getNumber());
        }

        return $range[ mt_rand(0, 99) ];
    }
}
