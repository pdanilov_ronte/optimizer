<?php

namespace Client\ServiceBundle\Service\ChooseVariant;

use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceRequest;


/**
 * @class   ChooseVariantRequest
 * @package Client\ServiceBundle\Service\ChooseVariant
 * @author  PM:/ <pm@spiral.ninja>
 */
class ChooseVariantRequest extends AbstractServiceRequest
{
    /**
     * @type int
     */
    protected $number;

    /**
     * @type bool
     */
    protected $notRunning;


    /**
     * @param int $number
     */
    public function variantChosen($number)
    {
        $this->number = $number;
    }

    /**
     * @return void
     */
    public function setNotRunning()
    {
        $this->notRunning = true;
    }

    /**
     * @return bool
     */
    public function isNotRunning()
    {
        return $this->notRunning;
    }

    /**
     * @param ServiceException $error
     */
    public function operationFailed(ServiceException $error)
    {
        $this->setException($error);
    }

    /**
     * @return int
     */
    public function getVariant()
    {
        return $this->number;
    }
}
