<?php

namespace Client\ServiceBundle\Service\RebuildCache;

use Base\ExperimentsBundle\Entity\Experiment;
use Client\ServiceBundle\Entity\VariationCache;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceProvider;


class RebuildCacheProvider extends AbstractServiceProvider
{
    /**
     * @param RebuildCacheRequest $request
     * @return void
     */
    public function rebuildCache(RebuildCacheRequest $request)
    {
        $data = $request->getRequestData()->getData();

        if (!isset($data['experiment_id']) || empty($data['experiment_id'])) {
            $request->operationFailed(
                new ServiceException('Parameter "experiment_id" must be set in request data')
            );
            return;
        }

        /** @type Registry $doctrine */
        $doctrine = $this->container->get('doctrine');
        /** @type EntityManager $em */
        $em = $doctrine->getManager();

        /**
         * Try to fetch experiment for which the cache should be rebuilt.
         */
        try {
            /** @type Experiment $experiment */
            $experiment = $em->getRepository('BaseExperimentsBundle:Experiment')
                ->find($data['experiment_id']);
        } catch (\Exception $e) {
            $request->operationFailed(
                new ServiceException($e->getMessage())
            );
            return;
        }

        if (empty($experiment)) {
            $request->operationFailed(
                new ServiceException(sprintf('Experiment %s not found', $data['experiment_id']))
            );
            return;
        }

        /**
         * Clear the cache for the experiment.
         */
        try {
            $em->getRepository('ClientServiceBundle:VariationCache')
                ->createQueryBuilder('c')
                ->delete()
                ->where('c.experiment = :experiment')
                ->setParameter('experiment', $experiment);
        } catch (\Exception $e) {
            $request->operationFailed(
                new ServiceException($e->getMessage())
            );
            return;
        }

        /**
         * Get variant base for the experiment.
         */
        /*$control = $experiment->getControlVariation();

        if (empty($control)) {
            $request->operationFailed(
                new ServiceException(
                    sprintf('Experiment %s does not have a control variation', $experiment->getId())
                )
            );
            return;
        }*/

        try {
            /**
             * Store variant base as cache entry with 0 index number.
             */
            /*$cache = new VariationCache();
            $cache->setNumber(0);
            $cache->setWeight($control->getWeight());
            $cache->setExperiment($experiment);
            $cache->setBase($control);
            $em->persist($cache);

            foreach ($base->getVariants() as $variant) {
                $cache = new VariantCache();
                $cache->setNumber($variant->getNumber());
                $cache->setWeight($cache->getWeight());
                $cache->setExperiment($experiment);
                $cache->setVariant($variant);
                $em->persist($cache);
            }

            $em->flush();*/

            foreach ($experiment->getVariations() as $variation) {
                $cache = new VariationCache();
                $cache->setNumber($variation->getNumber());
                $cache->setWeight($cache->getWeight());
                $cache->setExperiment($experiment);
                $cache->setVariation($variation);
                $em->persist($cache);
            }

            $em->flush();

        } catch (\Exception $e) {
            $request->operationFailed(
                new ServiceException($e->getMessage())
            );
            return;
        }

        $request->cacheRebuilt();
    }
}
