<?php

namespace Client\ServiceBundle\Service\RebuildCache;

use Shared\SoaBundle\Service\ServiceException;
use Shared\SoaBundle\Service\AbstractServiceRequest;


class RebuildCacheRequest extends AbstractServiceRequest
{
    /**
     * @type bool
     */
    protected $success;


    /**
     * @return void
     */
    public function cacheRebuilt()
    {
        $this->success = true;
    }

    /**
     * @param ServiceException $error
     * @return void
     */
    public function operationFailed(ServiceException $error)
    {
        $this->setException($error);
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success;
    }
}
