<?php

namespace Client\ServiceBundle\Service\KeepAlive;

use Shared\SoaBundle\Service\AbstractServiceRequest;


class KeepAliveRequest extends AbstractServiceRequest
{
    /**
     * @type array
     */
    private $response = [];

    public function setResponse(array $response)
    {
        $this->response = $response;
    }

    /**
     * @return array
     */
    public function getResponse()
    {
        return $this->response;
    }
}
