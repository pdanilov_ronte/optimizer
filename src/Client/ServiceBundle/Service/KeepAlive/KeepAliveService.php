<?php

namespace Client\ServiceBundle\Service\KeepAlive;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Shared\SoaBundle\Service\AbstractServiceRequester;


class KeepAliveService extends AbstractServiceRequester
{
    /**
     * @param array $data
     * @return KeepAliveRequest
     *
     * @Route("/initialize")
     */
    public function initialize(array $data)
    {
        return $this->createProvider()
            ->initialize($this->createRequest($data));
    }

    /**
     * @param array $data
     * @return KeepAliveRequest
     */
    public function refresh(array $data)
    {
        return $this->createProvider()
            ->refresh($this->createRequest($data));
    }

    /**
     * @param array $data
     * @return KeepAliveRequest
     */
    public function close(array $data)
    {
        return $this->createProvider()
            ->close($this->createRequest($data));
    }

    /**
     * @param array $data
     * @return KeepAliveRequest
     */
    protected function createRequest(array $data)
    {
        return parent::prepareRequest(new KeepAliveRequest(), $data);
    }

    /**
     * @return KeepAliveProvider
     */
    protected function createProvider()
    {
        return parent::prepareProvider(new KeepAliveProvider());
    }
}
