<?php

namespace Client\ServiceBundle\Service\KeepAlive;

use Shared\SoaBundle\Service\AbstractServiceProvider;


class KeepAliveProvider extends AbstractServiceProvider
{
    /**
     * @param KeepAliveRequest $request
     * @return KeepAliveRequest
     */
    public function initialize(KeepAliveRequest $request)
    {
        $request->setResponse([
            'timeout' => 60,
            'max'     => 3600
        ]);

        return $request;
    }

    /**
     * @param KeepAliveRequest $request
     * @return KeepAliveRequest
     */
    public function refresh(KeepAliveRequest $request)
    {
        return $request;
    }

    /**
     * @param KeepAliveRequest $request
     * @return KeepAliveRequest
     */
    public function close(KeepAliveRequest $request)
    {
        return $request;
    }
}
