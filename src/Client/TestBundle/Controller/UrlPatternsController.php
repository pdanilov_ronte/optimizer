<?php

namespace Client\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/url-patterns")
 */
class UrlPatternsController extends Controller
{
    /**
     * @Route("", name="test_url_patterns_index")
     * @Template()
     */
    public function indexAction()
    {
        return [];
    }

    /**
     * @Route("/page-one", name="test_url_patterns_pageOne")
     * @Template("ClientTestBundle:UrlPatterns:page.html.twig")
     */
    public function pageOneAction()
    {
        return [
            'page' => 'one'
        ];
    }

    /**
     * @Route("/page-two", name="test_url_patterns_pageTwo")
     * @Template("ClientTestBundle:UrlPatterns:page.html.twig")
     */
    public function pageTwoAction()
    {
        return [
            'page' => 'two'
        ];
    }

    /**
     * @Route("/goal-one", name="test_url_patterns_goalOne")
     * @Template("ClientTestBundle:UrlPatterns:goal.html.twig")
     */
    public function goalOneAction()
    {
        return [
            'goal' => 'one'
        ];
    }

    /**
     * @Route("/goal-two", name="test_url_patterns_goalTwo")
     * @Template("ClientTestBundle:UrlPatterns:goal.html.twig")
     */
    public function goalTwoAction()
    {
        return [
            'goal' => 'two'
        ];
    }
}
