<?php

namespace Client\RestBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Base\ExperimentsBundle\Entity\Experiment;
use Base\ExperimentsBundle\Form\Type\ExperimentType;
use Symfony\Component\Validator\Constraints\DateTime;


class ExperimentsController extends AbstractRestController
{
    /**
     * @return Response
     */
    public function getExperimentsAction()
    {
        $em = $this->getManager();

        $data = $em->getRepository('BaseExperimentsBundle:Experiment')->findAll();

        $view = $this->view($data);
        $view->getContext()->addGroup('static');

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function getExperimentAction(Request $request, $id)
    {
        $em = $this->getManager();

        $group = $request->query->get('group');

        $data = $em->getRepository('BaseExperimentsBundle:Experiment')->find($id);

        $view = $this->view($data);
        $view->getContext()
            ->addGroup(empty($group) ? 'complete' : $group);

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function postExperimentsAction(Request $request)
    {
        $account = $this->getAccount();
        $em = $this->getManager();

        $experiment = new Experiment();
        $experiment->setType(Experiment::TYPE_AB);
        $experiment->setAccount($account);

        $form = $this->createForm(new ExperimentType(), $experiment, [
            'method' => 'POST',
            'csrf_protection' => false
        ]);

        $form->submit($request->request->all(), false);

        if (!$form->isValid()) {
            return $this->handleView(
                $this->view($form->getErrors(), 400)
            );
        }

        $em->persist($experiment);
        $em->flush();

        $view = $this->view($experiment);
        $view->getContext()->addGroup('list');

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function putExperimentAction(Request $request, $id)
    {
        $em = $this->getManager();

        /** @var Experiment $data */
        $data = $em->getRepository('BaseExperimentsBundle:Experiment')->find($id);

        $form = $this->createForm(new ExperimentType(), $data, [
            'method' => 'PUT',
            'csrf_protection' => false
        ]);

        $params = $request->request->all();

        if (isset($params['startedAt'])) {
            if (!$data->getStartedAt()) {
                $data->setStartedAt(new \DateTime($params['startedAt']));
            }
            unset($params['startedAt']);
        }

        $form->submit($params, false);

        if (!$form->isValid()) {
            return $this->handleView(
                $this->view($form->getErrors(), 400)
            );
        }

        $em->persist($data);
        $em->flush();

        $view = $this->view($data);
        $view->getContext()->addGroup('list');

        return $this->handleView($view);
    }

    /**
     * @param $id
     * @return Response
     */
    public function deleteExperimentAction($id)
    {
        $em = $this->getManager();

        /** @var Experiment $data */
        $data = $em->getRepository('BaseExperimentsBundle:Experiment')->find($id);

        if (empty($data)) {
            return $this->handleView($this->view([], 404));
        }

        $em->remove($data);
        $em->flush();

        $view = $this->view($data);
        $view->getContext()->addGroup('list');
        return $this->handleView($view);
    }
}
