<?php

namespace Client\RestBundle\Controller;

use Base\UsersBundle\DependencyInjection\AccountAwareTrait;
use FOS\RestBundle\Controller\FOSRestController;

use Doctrine\ORM\EntityManager;


abstract class AbstractRestController extends FOSRestController
{
    use AccountAwareTrait;


    /**
     * @return EntityManager
     */
    protected function getManager()
    {
        /** @type EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $account = $this->getAccount();

        $em->getFilters()->getFilter('account_scope')
            ->setParameter('account_id', $account->getId());

        return $em;
    }
}
