<?php

namespace Client\RestBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Base\ExperimentsBundle\Entity\Goal;
use Base\ExperimentsBundle\Entity\GoalDestination;
use Base\ExperimentsBundle\Form\Type\GoalDestinationType;


class GoalDestinationsController extends AbstractRestController
{
    /**
     * @param int $experiment_id
     * @param int $goal_id
     * @return Response
     */
    public function getDestinationsAction(/** @noinspection PhpUnusedParameterInspection */
        $experiment_id, $goal_id)
    {
        $em = $this->getManager();

        /** @type Goal $goal */
        $goal = $em->getRepository('BaseExperimentsBundle:Goal')->find($goal_id);

        if (empty($goal)) {
            return $this->handleView($this->view([], 404));
        }

        $data = [$goal->getDestination()];

        $view = $this->view($data);
        $view->getContext()->addGroup('details');

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param int $experiment_id
     * @param int $goal_id
     * @return Response
     */
    public function postDestinationsAction(Request $request, /** @noinspection PhpUnusedParameterInspection */
                                    $experiment_id, $goal_id)
    {
        $account = $this->getAccount();
        $em = $this->getManager();

        /** @type Goal $goal */
        $goal = $em->getRepository('BaseExperimentsBundle:Goal')->find($goal_id);

        if (empty($goal)) {
            return $this->handleView($this->view([], 404));
        }

        $destination = new GoalDestination();
        $destination->setAccount($account);

        $form = $this->createForm(new GoalDestinationType(), $destination, [
            'method' => 'POST',
            'csrf_protection' => false
        ]);

        $form->submit($request->request->all(), false);

        if (!$form->isValid()) {
            return $this->handleView(
                $this->view($form->getErrors(), 400)
            );
        }

        $em->persist($destination);
        $goal->setDestination($destination);
        $em->persist($goal);
        $em->flush();

        $view = $this->view($destination);
        $view->getContext()->addGroup('details');

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param int $experiment_id
     * @param int $goal_id
     * @param int $id
     * @return Response
     */
    public function putDestinationAction(Request $request, /** @noinspection PhpUnusedParameterInspection */
                                         $experiment_id, /** @noinspection PhpUnusedParameterInspection */
                                         $goal_id, $id)
    {
        $em = $this->getManager();

        /** @var GoalDestination $data */
        $data = $em->getRepository('BaseExperimentsBundle:GoalDestination')->find($id);

        $form = $this->createForm(new GoalDestinationType(), $data, [
            'method' => 'PUT',
            'csrf_protection' => false
        ]);

        $form->submit($request->request->all(), false);

        if (!$form->isValid()) {
            return $this->handleView(
                $this->view($form->getErrors(), 400)
            );
        }

        $em->persist($data);
        $em->flush();

        $view = $this->view($data);
        $view->getContext()->addGroup('details');

        return $this->handleView($view);
    }
}
