<?php

namespace Client\RestBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Base\ExperimentsBundle\Entity\Experiment;
use Base\ExperimentsBundle\Entity\Goal;
use Base\ExperimentsBundle\Form\Type\GoalType;


class GoalsController extends AbstractRestController
{
    /**
     * @param int $experiment_id
     * @return Response
     */
    public function getGoalsAction($experiment_id)
    {
        $em = $this->getManager();

        /** @type Experiment $experiment */
        $experiment = $em->getRepository('BaseExperimentsBundle:Experiment')->find($experiment_id);

        if (empty($experiment)) {
            return $this->handleView($this->view([], 404));
        }

        $data = $em->getRepository('BaseExperimentsBundle:Goal')->findBy(
            ['experiment' => $experiment]
        );

        $view = $this->view($data);
        $view->getContext()->addGroup('details');

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param int $experiment_id
     * @return Response
     */
    public function postGoalsAction(Request $request, /** @noinspection PhpUnusedParameterInspection */
                                    $experiment_id)
    {
        $account = $this->getAccount();
        $em = $this->getManager();

        /** @type Experiment $experiment */
        $experiment = $em->getRepository('BaseExperimentsBundle:Experiment')->find($experiment_id);

        if (empty($experiment)) {
            return $this->handleView($this->view([], 404));
        }

        $goal = new Goal();
        $goal->setExperiment($experiment);
        $goal->setAccount($account);

        $form = $this->createForm(new GoalType(), $goal, [
            'method' => 'POST',
            'csrf_protection' => false
        ]);

        $params = $request->request->all();
        unset($params['pattern']);
        $form->submit($params, false);

        if (!$form->isValid()) {
            return $this->handleView(
                $this->view($form->getErrors(), 400)
            );
        }

        $goal->setType(Goal::TYPE_DESTINATION);

        $em->persist($goal);
        $em->flush();

        $view = $this->view($goal);
        $view->getContext()->addGroup('details');

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param $experiment_id
     * @param int $id
     * @return Response
     */
    public function putGoalAction(Request $request, /** @noinspection PhpUnusedParameterInspection */
                                  $experiment_id, $id)
    {
        $em = $this->getManager();

        /** @var Goal $data */
        $data = $em->getRepository('BaseExperimentsBundle:Goal')->find($id);

        $form = $this->createForm(new GoalType(), $data, [
            'method' => 'PUT',
            'csrf_protection' => false
        ]);

        $form->submit($request->request->all(), false);

        if (!$form->isValid()) {
            return $this->handleView(
                $this->view($form->getErrors(), 400)
            );
        }

        $em->persist($data);
        $em->flush();

        $view = $this->view($data);
        $view->getContext()->addGroup('details');

        return $this->handleView($view);
    }

    /**
     * @param int $experiment_id
     * @param int $id
     * @return Response
     */
    public function deleteGoalAction(/** @noinspection PhpUnusedParameterInspection */
        $experiment_id, $id)
    {
        $em = $this->getManager();

        /** @var Goal $data */
        $data = $em->getRepository('BaseExperimentsBundle:Goal')->find($id);

        if (empty($data)) {
            return $this->handleView($this->view([], 404));
        }

        $em->remove($data);
        $em->flush();

        $view = $this->view($data);
        $view->getContext()->addGroup('list');
        return $this->handleView($view);
    }
}
