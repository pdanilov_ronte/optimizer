<?php

namespace Client\RestBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Base\ExperimentsBundle\Entity\Experiment;
use Base\ExperimentsBundle\Entity\Variation;
use Base\ExperimentsBundle\Form\Type\VariationType;


class VariationsController extends AbstractRestController
{
    /**
     * @param int $experiment_id
     * @return Response
     */
    public function getVariationsAction($experiment_id)
    {
        $em = $this->getManager();

        /** @type Experiment $experiment */
        $experiment = $em->getRepository('BaseExperimentsBundle:Experiment')->find($experiment_id);

        if (empty($experiment)) {
            return $this->handleView($this->view([], 404));
        }

        $data = $em->getRepository('BaseExperimentsBundle:Variation')->findBy(
            ['experiment' => $experiment]
        );

        $view = $this->view($data);
        $view->getContext()->addGroup('details');

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param $experiment_id
     * @return Response
     */
    public function postVariationsAction(Request $request, /** @noinspection PhpUnusedParameterInspection */
                                         $experiment_id)
    {
        $account = $this->getAccount();
        $em = $this->getManager();

        /** @type Experiment $experiment */
        $experiment = $em->getRepository('BaseExperimentsBundle:Experiment')->find($experiment_id);

        if (empty($experiment)) {
            return $this->handleView($this->view([], 404));
        }

        $variation = new Variation();
        $variation->setExperiment($experiment);
        $variation->setAccount($account);

        $form = $this->createForm(new VariationType(), $variation, [
            'method' => 'POST',
            'csrf_protection' => false
        ]);

        $form->submit($request->request->all(), false);

        if (!$form->isValid()) {
            return $this->handleView(
                $this->view($form->getErrors(), 400)
            );
        }

        $variation->setNumber(
            count($experiment->getVariations()));

        $em->persist($variation);
        $em->flush();

        $view = $this->view($variation);
        $view->getContext()->addGroup('details');

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param $experiment_id
     * @param int $id
     * @return Response
     */
    public function putVariationAction(Request $request, /** @noinspection PhpUnusedParameterInspection */
                                       $experiment_id, $id)
    {
        $em = $this->getManager();

        /** @var Variation $data */
        $data = $em->getRepository('BaseExperimentsBundle:Variation')->find($id);

        $form = $this->createForm(new VariationType(), $data, [
            'method' => 'PUT',
            'csrf_protection' => false
        ]);

        $form->handleRequest($request);

        if (!$form->isValid()) {
            return $this->handleView(
                $this->view($form->getErrors(), 400)
            );
        }

        $em->persist($data);
        $em->flush();

        $view = $this->view($data);
        $view->getContext()->addGroup('details');

        return $this->handleView($view);
    }

    /**
     * @param $experiment_id
     * @param $id
     * @return Response
     */
    public function deleteVariationAction(/** @noinspection PhpUnusedParameterInspection */
        $experiment_id, $id)
    {
        $em = $this->getManager();

        /** @var Variation $data */
        $data = $em->getRepository('BaseExperimentsBundle:Variation')->find($id);

        if (empty($data)) {
            return $this->handleView($this->view([], 404));
        }

        $em->remove($data);
        $em->flush();

        $view = $this->view($data);
        $view->getContext()->addGroup('list');
        return $this->handleView($view);
    }
}
