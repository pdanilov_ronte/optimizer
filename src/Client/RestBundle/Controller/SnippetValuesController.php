<?php

namespace Client\RestBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Base\ExperimentsBundle\Entity\Variation;
use Base\ExperimentsBundle\Entity\VariationSnippet;
use Base\ExperimentsBundle\Entity\SnippetValue;
use Base\ExperimentsBundle\Form\Type\SnippetValueType;


class SnippetValuesController extends AbstractRestController
{
    /**
     * @param $experiment_id
     * @param $snippet_id
     * @return Response
     */
    public function getValuesAction(/** @noinspection PhpUnusedParameterInspection */
        $experiment_id, $snippet_id)
    {
        $em = $this->getManager();

        /** @type VariationSnippet $snippet */
        $snippet = $em->getRepository('BaseExperimentsBundle:VariationSnippet')->find($snippet_id);

        if (empty($snippet)) {
            return $this->handleView($this->view([], 404));
        }

        $data = $em->getRepository('BaseExperimentsBundle:SnippetValue')->findBy(
            ['snippet' => $snippet]
        );

        $view = $this->view($data);
        $view->getContext()->addGroup('details');

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param int $experiment_id
     * @param int $snippet_id
     * @return Response
     */
    public function postValuesAction(Request $request, /** @noinspection PhpUnusedParameterInspection */
                                     $experiment_id, /** @noinspection PhpUnusedParameterInspection */
                                     $snippet_id)
    {
        $account = $this->getAccount();
        $em = $this->getManager();

        /** @type VariationSnippet $snippet */
        $snippet = $em->getRepository('BaseExperimentsBundle:VariationSnippet')->find($snippet_id);

        if (empty($snippet)) {
            return $this->handleView($this->view([], 404));
        }

        $number = $request->get('variation');

        /** @var Variation $variation */
        $variation = $em->createQueryBuilder()
            ->select('v')
            ->from('BaseExperimentsBundle:Variation', 'v')
            ->where('v.experiment = :experiment')
            ->andWhere('v.number = :number')
            ->setParameters([
                'experiment' => $snippet->getExperiment(),
                'number' => isset($number) ? $number : 0
            ])
            ->getQuery()
            ->getSingleResult();

        if (empty($variation)) {
            return $this->handleView($this->view([], 400));
        }

        $value = new SnippetValue();
        $value->setSnippet($snippet);
        $value->setVariation($variation);
        $value->setAccount($account);

        $form = $this->createForm(new SnippetValueType(), $value, [
            'method' => 'POST',
            'csrf_protection' => false
        ]);

        // @todo -
        $params = $request->request->all();
        unset($params['variation']);

        $form->submit($params, false);

        if (!$form->isValid()) {
            return $this->handleView(
                $this->view($form->getErrors(), 400)
            );
        }

        $em->persist($value);
        $em->flush();

        $view = $this->view($value);
        $view->getContext()->addGroup('details');

        return $this->handleView($view);
    }


    /**
     * @param Request $request
     * @param int $experiment_id
     * @param int $snippet_id
     * @param int $id
     * @return Response
     */
    public function putValueAction(Request $request, /** @noinspection PhpUnusedParameterInspection */
                                   $experiment_id, /** @noinspection PhpUnusedParameterInspection */
                                   $snippet_id, $id)
    {
        $em = $this->getManager();

        /** @var SnippetValue $data */
        $data = $em->getRepository('BaseExperimentsBundle:SnippetValue')->find($id);

        $form = $this->createForm(new SnippetValueType(), $data, [
            'method' => 'PUT',
            'csrf_protection' => false
        ]);

        // @todo -
        $params = $request->request->all();
        unset($params['variation']);

        $form->submit($params, false);

        if (!$form->isValid()) {
            return $this->handleView(
                $this->view($form->getErrors(), 400)
            );
        }

        $em->persist($data);
        $em->flush();

        $view = $this->view($data);
        $view->getContext()->addGroup('details');

        return $this->handleView($view);
    }

    /**
     * @param $experiment_id
     * @param int $snippet_id
     * @param int $id
     * @return Response
     */
    public function deleteValueAction(/** @noinspection PhpUnusedParameterInspection */
        $experiment_id, $snippet_id, $id)
    {
        $em = $this->getManager();

        /** @var SnippetValue $data */
        $data = $em->getRepository('BaseExperimentsBundle:SnippetValue')->find($id);

        if (empty($data)) {
            return $this->handleView($this->view([], 404));
        }

        $em->remove($data);
        $em->flush();

        $view = $this->view($data);
        $view->getContext()->addGroup('list');
        return $this->handleView($view);
    }
}
