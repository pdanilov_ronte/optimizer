<?php

namespace Client\RestBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Base\ExperimentsBundle\Entity\Experiment;
use Base\ExperimentsBundle\Entity\ExperimentUrl;
use Base\ExperimentsBundle\Form\Type\ExperimentUrlType;


class ExperimentUrlsController extends AbstractRestController
{
    /**
     * @param $experiment_id
     * @return array
     */
    public function getUrlsAction($experiment_id)
    {
        $em = $this->getManager();

        /** @type Experiment $experiment */
        $experiment = $em->getRepository('BaseExperimentsBundle:Experiment')->find($experiment_id);

        if (empty($experiment)) {
            return $this->handleView($this->view([], 404));
        }

        /** @var ExperimentUrl $data */
        $data = $em->getRepository('BaseExperimentsBundle:ExperimentUrl')->findBy(
            ['experiment' => $experiment]
        );

        $view = $this->view($data);
        $view->getContext()->addGroup('details');

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param int $experiment_id
     * @return Response
     */
    public function postUrlsAction(Request $request, /** @noinspection PhpUnusedParameterInspection */
                                   $experiment_id)
    {
        $account = $this->getAccount();
        $em = $this->getManager();

        /** @type Experiment $experiment */
        $experiment = $em->getRepository('BaseExperimentsBundle:Experiment')->find($experiment_id);

        if (empty($experiment)) {
            return $this->handleView($this->view([], 404));
        }

        $url = new ExperimentUrl();
        $url->setExperiment($experiment);
        $url->setAccount($account);

        $form = $this->createForm(new ExperimentUrlType(), $url, [
            'method' => 'POST',
            'csrf_protection' => false
        ]);

        $form->handleRequest($request);

        if (!$form->isValid()) {
            return $this->handleView(
                $this->view($form->getErrors(), 400)
            );
        }

        $em->persist($url);
        $em->flush();

        $view = $this->view($url);
        $view->getContext()->addGroup('details');

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param int $experiment_id
     * @param int $id
     * @return Response
     */
    public function putUrlAction(Request $request, /** @noinspection PhpUnusedParameterInspection */
                                 $experiment_id, $id)
    {
        $em = $this->getManager();

        /** @var ExperimentUrl $data */
        $data = $em->getRepository('BaseExperimentsBundle:ExperimentUrl')->find($id);

        $form = $this->createForm(new ExperimentUrlType(), $data, [
            'method' => 'PUT',
            'csrf_protection' => false
        ]);

        $form->handleRequest($request);

        if (!$form->isValid()) {
            return $this->handleView(
                $this->view($form->getErrors(), 400)
            );
        }

        $em->persist($data);
        $em->flush();

        $view = $this->view($data);
        $view->getContext()->addGroup('details');

        return $this->handleView($view);
    }

    /**
     * @param int $experiment_id
     * @param int $id
     * @return Response
     */
    public function deleteUrlAction(/** @noinspection PhpUnusedParameterInspection */
        $experiment_id, $id)
    {
        $em = $this->getManager();

        /** @var ExperimentUrl $data */
        $data = $em->getRepository('BaseExperimentsBundle:ExperimentUrl')->find($id);

        if (empty($data)) {
            return $this->handleView($this->view([], 404));
        }

        $em->remove($data);
        $em->flush();

        $view = $this->view($data);
        $view->getContext()->addGroup('list');
        return $this->handleView($view);
    }
}
