<?php

namespace Client\RestBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Base\ExperimentsBundle\Entity\Experiment;
use Base\ExperimentsBundle\Entity\VariationSnippet;
use Base\ExperimentsBundle\Form\Type\VariationSnippetType;


class VariationSnippetsController extends AbstractRestController
{
    /**
     * @param int $experiment_id
     * @return Response
     */
    public function getSnippetsAction($experiment_id)
    {
        $em = $this->getManager();

        /** @type Experiment $experiment */
        $experiment = $em->getRepository('BaseExperimentsBundle:Experiment')->find($experiment_id);

        if (empty($experiment)) {
            return $this->handleView($this->view([], 404));
        }

        /** @var VariationSnippet $data */
        $data = $em->getRepository('BaseExperimentsBundle:VariationSnippet')->findBy(
            ['experiment' => $experiment]
        );

        $view = $this->view($data);
        $view->getContext()->addGroup('details');

        return $this->handleView($view);
    }

    /**
     * @param int $experiment_id
     * @param int $id
     * @return Response
     */
    public function getSnippetAction(/** @noinspection PhpUnusedParameterInspection */
        $experiment_id, $id)
    {
        $em = $this->getManager();

        /** @var VariationSnippet $data */
        $data = $em->getRepository('BaseExperimentsBundle:VariationSnippet')->find($id);

        $view = $this->view($data);
        $view->getContext()
            ->addGroups(['details']);

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param $experiment_id
     * @return Response
     */
    public function postSnippetsAction(Request $request, /** @noinspection PhpUnusedParameterInspection */
                                       $experiment_id)
    {
        $account = $this->getAccount();
        $em = $this->getManager();

        /** @type Experiment $experiment */
        $experiment = $em->getRepository('BaseExperimentsBundle:Experiment')->find($experiment_id);

        if (empty($experiment)) {
            return $this->handleView($this->view([], 404));
        }

        $snippet = new VariationSnippet();
        $snippet->setExperiment($experiment);
        $snippet->setAccount($account);

        $form = $this->createForm(new VariationSnippetType(), $snippet, [
            'method' => 'POST',
            'csrf_protection' => false
        ]);

        $form->handleRequest($request);

        if (!$form->isValid()) {
            return $this->handleView(
                $this->view($form->getErrors(), 400)
            );
        }

        $em->persist($snippet);
        $em->flush();

        $view = $this->view($snippet);
        $view->getContext()->addGroup('details');

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param $experiment_id
     * @param int $id
     * @return Response
     */
    public function putSnippetAction(Request $request, /** @noinspection PhpUnusedParameterInspection */
                                       $experiment_id, $id)
    {
        $em = $this->getManager();

        /** @var VariationSnippet $data */
        $data = $em->getRepository('BaseExperimentsBundle:VariationSnippet')->find($id);

        $form = $this->createForm(new VariationSnippetType(), $data, [
            'method' => 'PUT',
            'csrf_protection' => false
        ]);

        $form->handleRequest($request);

        if (!$form->isValid()) {
            return $this->handleView(
                $this->view($form->getErrors(), 400)
            );
        }

        $em->persist($data);
        $em->flush();

        $view = $this->view($data);
        $view->getContext()->addGroup('details');

        return $this->handleView($view);
    }

    /**
     * @param $experiment_id
     * @param $id
     * @return Response
     */
    public function deleteSnippetAction(/** @noinspection PhpUnusedParameterInspection */
        $experiment_id, $id)
    {
        $em = $this->getManager();

        /** @var VariationSnippet $data */
        $data = $em->getRepository('BaseExperimentsBundle:VariationSnippet')->find($id);

        if (empty($data)) {
            return $this->handleView($this->view([], 404));
        }

        $em->remove($data);
        $em->flush();

        $view = $this->view($data);
        $view->getContext()->addGroup('list');
        return $this->handleView($view);
    }
}
