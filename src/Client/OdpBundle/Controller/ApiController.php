<?php

namespace Client\OdpBundle\Controller;

use Adrotec\BreezeJs\Framework\Application;
use Base\UsersBundle\Entity\Account;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Filter\SQLFilter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class ApiController extends Controller
{
    public function apiAction(Request $request)
    {
        /** @type Application $api */$api = $this->container->get('adrotec_webapi');

        $api->addResources([
            'Experiment'       => 'Base\ExperimentsBundle\Entity\Experiment',
            'ExperimentUrl'    => 'Base\ExperimentsBundle\Entity\ExperimentUrl',
            'Variation'        => 'Base\ExperimentsBundle\Entity\Variation',
            'VariationSnippet' => 'Base\ExperimentsBundle\Entity\VariationSnippet',
            'SnippetValue'     => 'Base\ExperimentsBundle\Entity\SnippetValue',
            'VariationCode'    => 'Base\ExperimentsBundle\Entity\VariationCode',
            'Goal'             => 'Base\ExperimentsBundle\Entity\Goal',
            'GoalDestination'  => 'Base\ExperimentsBundle\Entity\GoalDestination',
            'Account'          => 'Base\UsersBundle\Entity\Account'
        ]);

        /** @type Registry $doctrine */
        $doctrine = $this->get('doctrine');
        /** @type EntityManager $em */
        $em = $doctrine->getManager();

        $filters = $em->getFilters()->getEnabledFilters();

        /** @type Account $account */
        $account = $this->container->get('security.context')->getToken()->getUser();

        /** @type SQLFilter $filter */
        foreach ($filters as $filter) {
            $filter->setParameter('account_id', $account->getId());
        }

        $response = $api->handle($request);

        return $response;
    }
}
