<?php

namespace Client\JSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("", name="client_integration_test_page")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/success/", name="client_integration_test_success")
     * @Template()
     */
    public function successAction()
    {
        return array();
    }
}
