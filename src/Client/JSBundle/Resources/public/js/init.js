var mathObj = Math,
    toStringFn = 'toString',
    documentObj = document,
    localStorageObj = localStorage;

window["StrongOptimizerObject"] = so;

window[so] = window[so] || function () {
    (window[so].q = window[so].q || []).push(arguments)
};

window[so].l = 1 * new Date();

window[so].g = function () {
    return "so-ui-" +
    (window[so].l)[toStringFn](16) + "-" +
    mathObj.floor((mathObj.random()*0.9+0.1) * mathObj.pow(16, 9))[toStringFn](16)
};

window[so].u = localStorageObj['getItem'](so + '.u') ||
(function () {
    return u = window[so].g(),
        localStorageObj['setItem'](so + '.u', u),
        u;
})();

async = documentObj.createElement(tag),
    node = documentObj.getElementsByTagName(tag)[0];
async.async = 1;
async.src = src + window[so].u + '.js';
node.parentNode.insertBefore(async, node);

