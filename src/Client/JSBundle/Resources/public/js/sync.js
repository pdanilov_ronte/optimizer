/**
 * Prototype of synchronous static JS for using on experiment pages.
 *
 * Performs as experiment initializer and asynchronous data pre-loader;
 * should be loaded synchronously before page rendering, so is better to be inserted
 * into the head tag as a snippet, or as an external script source.
 */
(function () {

    function getOptimizerObj() {
        return window.optimizer;
    }

    function getCacheBaseUrl() {
        return __parameters["cache_base_url"];
    }

    function getWindowLocationSearch() {
        return window.location.search;
    }

    if (getOptimizerObj() == undefined) {
        window.optimizer = {};
    }

    function getScript(url, success) {
        var script = document.createElement('script');
        script.src = url;
        var head = document.getElementsByTagName('head')[0], done = false;
        script.onload = script.onreadystatechange = function () {
            if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                done = true;
                success();
                script.onload = script.onreadystatechange = null;
                head.removeChild(script);
            }
        };
        head.appendChild(script);
    }

    function getScriptUrl() {
        var url;
        if (getOptimizerObj().mode == 'transparent') {
            var variation = getOptimizerObj().variation != undefined ? getOptimizerObj().variation : 0;
            url = getCacheBaseUrl() + 'compiled/' + getOptimizerObj().experiment_id + '/' + variation + '.js';
        } else {
            url = getCacheBaseUrl() + 'session/' + getUID() + '.js?id=' + getOptimizerObj().experiment_id
        }
        return url;
    }

    function showBase() {
        revealElement(getOptimizerObj().base_container_element);
    }

    function hideElement(element) {
        var style = element.getAttribute("style");
        if (style) {
            style += ";";
        } else {
            style = "";
        }
        style += "opacity:0;";
        element.setAttribute('style', style);
    }

    function revealElement(element) {
        var style = element.getAttribute("style");
        style = style.replace("opacity:0;", "opacity:1;transition:opacity 0.15s ease;");
        element.setAttribute('style', style);
    }

    function hideById(id) {
        var element = document.getElementById(id);
        if (element != undefined) {
            hideElement(element);
        }
        return element;
    }

    function hideBySelector(selector) {
        var element = $(selector)[0];
        if (element != undefined) {
            hideElement(element);
        }
        return element;
    }

    function generateUID() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }

    function getUID() {
        var uid = localStorage.getItem('uid');
        if (uid == null) {
            uid = generateUID();
            localStorage.setItem('uid', uid);
        }
        return uid;
    }

    if (getWindowLocationSearch().match(/\b__xmode=(\w+)\b/)) {
        var mode = getWindowLocationSearch().replace(/^.*\b__xmode=(\w+)\b.*$/, '$1');
        if (mode == 'transparent') {
            getOptimizerObj().mode = 'transparent';
            if (getWindowLocationSearch().match(/\bexperiment_id=(\d+)\b/)) {
                getOptimizerObj().experiment_id = getWindowLocationSearch().replace(/^.*\bexperiment_id=(\d+)\b.*$/, '$1');
            }
            if (getWindowLocationSearch().match(/\bvariation=(\d+)\b/)) {
                getOptimizerObj().variation = getWindowLocationSearch().replace(/^.*\bvariation=(\d+)\b.*$/, '$1');
            }
        }
    }

    /** Load asynchronous script part */
    getOptimizerObj().async_loaded = false;
    setTimeout(function () {
        // Reveal base if async part was not loaded successfully
        if (!getOptimizerObj().async_loaded) {
            console.log('Timed out, revealing base content');
            showBase();
        }
    }, 200);
    // Get asynchronous cached javascript
    getScript(getScriptUrl(), function () {
        // success
    });

    document.onreadystatechange = function () {
        if (getOptimizerObj().initialized) {
            return;
        }
        var element;
        if (getOptimizerObj().base_id != undefined) {
            element = hideById(getOptimizerObj().base_id);
        } else if (getOptimizerObj().base_selector != undefined) {
            element = hideBySelector(getOptimizerObj().base_selector);
        }
        getOptimizerObj().base_container_element = element;
        getOptimizerObj().initialized = true;
        if (getOptimizerObj().ready) {
            getOptimizerObj().ready();
        }
    };

})();
