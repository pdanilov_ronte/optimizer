module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                mangle: true,
                compress: {}
            },
            sync_js: {
                options: {
                    banner: '/*! <%= pkg.description %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
                },
                files: {
                    'sync.min.js': ['params.js', 'sync.js']
                }
            },
            snippet_js: {
                options: {
                    enclose: {
                        window: "window",
                        '\'script\'': "tag",
                        '\'so\'': ["so", "src", "async", "node"]
                    },
                    mangle: {
                        toplevel: true
                    },
                    beautify: {
                        beautify: true,
                        width: 60,
                        quote_style: 1,
                        space_colon: false,
                        semicolons: false
                    }
                },
                files: {
                    'snippet.min.js': ['params.js', 'init.js']
                }
            },
            async_js: {
                options: {
                    enclose: {
                        window: "window",
                        document: "document"
                    }
                },
                files: {
                    'async.min.js': ['async.js']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.registerTask('default', ['uglify']);
};
