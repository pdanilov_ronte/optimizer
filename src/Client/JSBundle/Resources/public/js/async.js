(function () {

    var so = window.StrongOptimizerObject || 'so';

    window[so] = window[so] || function () {
        (window[so].q = window[so].q || []).push(arguments)
    };

    window[so].l = 1 * new Date();

    window[so].g = function () {
        return "sx-ui-" +
            (window[so].l)['toString'](16) + "-" +
            Math.floor((Math.random() * 0.9 + 0.1) * Math.pow(16, 9))['toString'](16)
    };

    window[so].u = localStorage['getItem'](so + '.u') ||
    (function () {
        return u = window[so].g(),
            localStorage['setItem'](so + '.u', u),
            u;
    })();

    window[so].p = window[so].p || {};

    window[so].async_loaded = true;
    window[so].initialized = true;

    window[so].xid = parseInt('{{ experiment_id }}');

    if (window[so].p['_xa'] && (window[so].p['_xa'] == window[so].xid)) {
        throw Error("Experiment " + window[so].xid + " already processed, aborting");
    }

    window[so].p['_xa'] = window[so].xid;

    window[so].goals = JSON.parse('{{ goals }}');

    window[so].variations = JSON.parse('{{ variations }}');

    // persistent | transient | hybrid | manual
    window[so].mode = '{{ variation_mode }}';

    function isGoalPage() {
        var loc = document.location.protocol + '//' + document.location.hostname + document.location.pathname;
        for (var i = 0; i < window[so].goals.length; i++) {
            var goal = window[so].goals[i];
            if (goal.destination === loc) {
                // NOTE: implicit definition of goal id
                window[so].goal_id = goal.id;
                return 1;
            }
            var re = new RegExp(goal.destination);
            if (loc.match(re)) {
                window[so].goal_id = goal.id;
                return 1;
            }
        }
        return 0;
    }

    function isRealGoal() {
        var v = getCachedVariationNumber(window[so].xid)
        if (null === v) {
            return 0;
        }
        if (null !== localStorage.getItem(so + '.x' + window[so].xid + '.v' + v + '.g')) {
            return 0;
        }
        localStorage.setItem(so + '.x' + window[so].xid + '.v' + v + '.g', window[so].goal_id);
        return 1;
    }

    function isVariationViewUnique() {
        //localStorage.setItem(so + '.x' + window[so].xid + '.v' + v
    }

    function revealElement(element) {
        var style = element.getAttribute("style");
        style = style.replace("opacity:0;", "opacity:1;transition:opacity 0.15s ease;");
        element.setAttribute('style', style);
    }

    function chooseVariation() {
        return chooseVariationWeight();
    }

    function chooseVariationWeight() {

        var implicit = [], range = [], i, n, l;

        var variations = window[so].variations;

        for (i = 0; i < variations.length; i++) {
            var variation = variations[i];
            if (null !== variation['weight']) {
                var c = parseInt(variation['weight']);
                if (c === 0) {
                    throw "Variation weight can't be zero (variation number " + i + ")";
                }
                l = range.length + c;
                for (n = range.length; n < l; n++) {
                    range[n] = i;
                }
            } else {
                implicit.push(i);
            }
        }

        var share = (0 !== implicit.length) ?
            0 : Math.floor(100 - range.length) / implicit.length;

        for (i = 0; i < implicit.length; i++) {
            l = range.length + share;
            for (n = range.length; n < l; n++) {
                range[n] = i;
            }
        }

        if (range.length < 100) {
            var last = variations.length - 1;
            for (n = range.length; n < 100; n++) {
                range[n] = last;
            }
        }

        return range[Math.round(Math.random() * 0.99 * 100)];
    }

    function getCachedVariationNumber(xid) {
        return localStorage.getItem(so + '.x' + xid + '.v');
    }

    function saveVariationNumber(xid, number) {
        localStorage.setItem(so + '.x' + xid + '.v', number);
    }

    function getVariationNumber() {

        var number;

        switch (window[so].mode) {
            case 'persistent':
                number = getCachedVariationNumber(window[so].xid);
                if (number == undefined) {
                    number = chooseVariation();
                    saveVariationNumber(window[so].xid, number);
                }
                break;
            case 'transient':
                number = chooseVariation();
                saveVariationNumber(window[so].xid, number);
                break;
            default:
                throw 'Invalid variation mode "' + window[so].mode + '"';
        }

        window[so].variation = window[so].p['_xn'] = number;

        return number;
    }

    function showVariationCode() {
        if (window[so].variation !== 0) {
            var control = getControlCode();
            var number = window[so].p['_xn'];
            if (/<[a-z0-9]*(\s[^>]*)?>/i.test(control)) {
                var source = createElement(getControlCode());
                var equals = findEqualNodes(source);
                var html = getVariationCode(number);
                for (var i = 0; i < equals.length; i++) {
                    equals[i].outerHTML = html;
                }
            } else {
                var all = document.body.getElementsByTagName("*");
                var replace = getVariationCode(number);
                for (var i = 0, max = all.length; i < max; i++) {
                    var e = all[i];
                    e.innerHTML = e.innerHTML.replace(
                        new RegExp(control, 'gi'),
                        replace
                    );
                }
            }
        }
        //revealElement(window[so].base_container_element);
    }

    function showVariationSnippet() {
        if (window[so].variation !== 0) {

            var number = window[so].p['_xn'];

            for (var n in window[so].variations[number].snippetValues) {

                var control = getControlSnippetCode(n);

                if (/<[a-z]*(\s[^>]*)?>/i.test(control)) {
                    var source = createElement(control);
                    var equals = findEqualNodes(source);
                    var html = getVariationSnippetCode(number, n);
                    for (var i = 0; i < equals.length; i++) {
                        equals[i].outerHTML = html;
                    }
                } else {
                    var all = document.body.getElementsByTagName("*");
                    var replace = getVariationSnippetCode(number, n);
                    for (var m = 0, max = all.length; m < max; m++) {
                        var e = all[m];
                        e.innerHTML = e.innerHTML.replace(
                            new RegExp(control, 'gi'),
                            replace
                        );
                    }
                }
            }
        }
    }

    function getControlCode() {
        return window[so].variations[0].code;
    }

    function getControlSnippetCode(number) {
        return window[so].variations[0].snippetValues[number];
    }

    function getVariationCode(number) {
        return window[so].variations[number].code;
    }

    function getVariationSnippetCode(number, snippetNumber) {
        return window[so].variations[number].snippetValues[snippetNumber];
    }

    /**
     * Create DOM node from html string.
     *
     * @param html {String}
     * @return {Element}
     */
    function createElement(html) {
        var div = document.createElement('div');
        div.innerHTML = html;
        return div.firstElementChild;
    }

    /**
     * Search the document DOM tree for the node equal to the source.
     *
     * @param source {Element}
     * @return {Array}
     */
    function findEqualNodes(source) {
        var equals = [];
        var tagName = source.tagName;
        var candidates = document.getElementsByTagName(tagName);
        for (var i = 0; i < candidates.length; i++) {
            var candidate = candidates[i];
            if (candidate.isEqualNode(source)) {
                equals.push(candidate);
            }
        }
        return equals;
    }

    function getHash32(str) {
        var hash = 0;
        if (str.length == 0) return hash;
        for (i = 0; i < str.length; i++) {
            c = str.charCodeAt(i);
            hash = ((hash << 5) - hash) + c;
            hash = hash & hash;
        }
        return hash;

    }

    function getNumHash(str) {
        var res = 0,
            len = str.length;
        for (var i = 0; i < len; i++) {
            res = res * 31 + str.charCodeAt(i);
        }
        return res;
    }

    function sendHit() {

        if (window[so].p['_xn'] == undefined) {
            throw Error("Variation is undefined, can't send hit");
        }

        var ct = window[so].p['ct'] = parseInt(window[so].u.substring(6, 17), 16);
        window[so].p['cs'] = window[so].l;
        window[so].p['ci'] = parseInt(window[so].u.substring(18), 16) + '.' + ct;
        window[so].p['cu'] = 1 * (ct == window[so].l);

        var script = document.createElement('script');
        var url = '{{ hit_url|raw }}?';
        url += '_evt=pageview';
        url += '&_cid=' + window[so].p['ci'];
        url += '&_st=' + window[so].l;
        url += '&experiment=' + window[so].p['_xa'];
        url += '&variation=' + window[so].p['_xn'];
        url += '&landing=' + encodeURIComponent(document.location.toString());
        url += '&referer=' + encodeURIComponent(document.referrer);
        url += '&is_unique=' + window[so].p['cu'];
        url += '&is_xview=' + window[so].p['xv'];
        url += '&is_uxview=' + window[so].p['xvu'];
        url += '&is_gpview=' + window[so].p['gp'];
        url += '&is_goal=' + window[so].p['g'];
        script.src = url;
        var body = document.getElementsByTagName('body')[0];
        body.appendChild(script);
    }

    function ready() {

        window[so].p['gp'] = isGoalPage();
        window[so].p['g'] = 1 * (window[so].p['gp'] && isRealGoal());
        window[so].p['xv'] = 1 * !window[so].p['gp'];

        if (window[so].p['xv']) {
            var xn = getVariationNumber();
            if (null == localStorage.getItem(so + '.x' + window[so].xid + '.v' + xn)) {
                window[so].p['xvu'] = 1;
                localStorage.setItem(so + '.x' + window[so].xid + '.v' + xn, 1);
            } else {
                window[so].p['xvu'] = 0;
            }
        } else {
            window[so].p['_xn'] = getCachedVariationNumber(window[so].xid);
            window[so].p['xvu'] = 0;
        }

        if (0 == window[so].p['gp']) {
            showVariationSnippet();
            sendHit();
        } else {
            if (window[so].p['_xn']) {
                sendHit();
            }
        }
    }

    if (window[so].initialized) {
        ready();
    } else {
        window[so].ready = ready;
    }

})();
