const gulp = require('gulp');
const uglify = require('gulp-uglify');
const rename = require("gulp-rename");


gulp.task('compress', function () {
    return gulp.src('dist/tracker.js')
        .pipe(uglify({
            mangle: true,
            compress: true,
            wrap: true
        }))
        .pipe(rename('tracker.min.js'))
        .pipe(gulp.dest('dist'));
});


gulp.task('build', ['compress']);
gulp.task('default', ['build']);
