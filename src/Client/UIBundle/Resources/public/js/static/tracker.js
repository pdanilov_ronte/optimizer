var Helper = (function () {
    function Helper() {
    }
    /**
     * Hash string
     *
     * @param {string} str
     * @returns {number}
     */
    Helper.hs = function (str) {
        var hash = 0, i, chr, len;
        if (str.length === 0)
            return hash;
        for (i = 0, len = str.length; i < len; i++) {
            chr = str.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0;
        }
        return hash;
    };
    /**
     * Get hex representation of 32-bit integer
     *
     * @param {number} int
     * @returns {string}
     */
    Helper.hi = function (int) {
        return (int < 0 ? int >>> 0 : int).toString(16);
    };
    /**
     * Hash string and return its hex representation
     *
     * @param {string} str
     * @returns {string}
     */
    Helper.hh = function (str) {
        return Helper.hi(Helper.hs(str));
    };
    return Helper;
}());
var SingletonConsole = (function () {
    function SingletonConsole() {
    }
    SingletonConsole.warn = function (message) {
        if (SingletonConsole._w.indexOf(message) == -1) {
            SingletonConsole._w.push(message);
            console.warn(message);
        }
    };
    SingletonConsole.error = function (message) {
        if (SingletonConsole._e.indexOf(message) == -1) {
            SingletonConsole._e.push(message);
            console.error(message);
        }
    };
    SingletonConsole._w = [];
    SingletonConsole._e = [];
    return SingletonConsole;
}());
var XhrService = (function () {
    function XhrService(endpoint) {
        this.endpoint = endpoint;
    }
    XhrService.initialize = function (endpoint, excluded) {
        XhrService._e = excluded;
        XhrService._i = new XhrService(endpoint);
    };
    Object.defineProperty(XhrService, "instance", {
        get: function () {
            if (XhrService._i == undefined) {
                throw Error('Service not initialized');
            }
            return XhrService._i;
        },
        enumerable: true,
        configurable: true
    });
    XhrService.excluded = function (url) {
        if (url == XhrService.instance.endpoint) {
            return true;
        }
        var parser = document.createElement('a');
        parser.href = url;
        for (var _a = 0, _b = XhrService._e; _a < _b.length; _a++) {
            var entry = _b[_a];
            var re = new RegExp(entry);
            if (parser.pathname.match(re)) {
                return true;
            }
        }
        return false;
    };
    XhrService.prototype.send = function (data) {
        var client = new XMLHttpRequest();
        client.open("POST", this.endpoint, true);
        client.setRequestHeader("Content-Type", "text/plain;charset=UTF-8");
        client.send(JSON.stringify(data));
    };
    return XhrService;
}());
var EventType = (function () {
    function EventType(_type) {
        this._type = _type;
    }
    Object.defineProperty(EventType.prototype, "type", {
        get: function () {
            return this._type;
        },
        enumerable: true,
        configurable: true
    });
    /** Session created **/
    EventType.SC = 11;
    /** Session restored **/
    EventType.SR = 12;
    /** Session navigation state changed **/
    EventType.SF = 15;
    /** Session navigation state change requested **/
    EventType.SN = 16;
    /** Open XHR connection **/
    EventType.XO = 21;
    /** XHR connection loaded **/
    EventType.XL = 25;
    /** XHR connection error **/
    EventType.XE = 28;
    //noinspection JSUnusedGlobalSymbols
    /** General error **/
    EventType.EG = 41;
    /** Unhandled exception **/
    EventType.EU = 42;
    //noinspection JSUnusedGlobalSymbols
    /** Triggered error **/
    EventType.ET = 43;
    /** General click event **/
    EventType.CG = 71;
    /** Button clicked **/
    EventType.CB = 75;
    return EventType;
}());
/**
 * Tracker configuration
 */
var TRACKER_VERSION = '0.0.4';
var TRACKER_VERSION_HASH = Helper.hh(TRACKER_VERSION);
var SESSION_TIMEOUT = 3600;
/**
 * Event object
 */
var TrackedEvent = (function () {
    function TrackedEvent(context) {
        this.context = context;
    }
    TrackedEvent.prototype.track = function () {
        try {
            XhrService.instance.send(this.context);
        }
        catch (e) {
            SingletonConsole.warn('JSTRACKER: ' + e.message);
        }
    };
    return TrackedEvent;
}());
/**
 * Tracker session
 */
var TrackerSession = (function () {
    function TrackerSession(data) {
        this.data = data;
    }
    TrackerSession.build = function () {
        var host = Helper.hh(window.location.hostname);
        var sid = Helper.hi((new Date).getTime());
        var uid = host + '-' + sid + '-' + Helper.hi(Math.random() * 1000000).substr(0, 4);
        var data = {
            version: TRACKER_VERSION_HASH,
            uid: uid,
            sid: sid,
            page: sid
        };
        return new TrackerSession(data);
    };
    TrackerSession.restore = function () {
        var json = localStorage.getItem('so.tracker.session');
        if (json == undefined) {
            return null;
        }
        var data;
        try {
            data = JSON.parse(json);
        }
        catch (e) {
            localStorage.removeItem('so.tracker.session');
            return null;
        }
        var session = new TrackerSession(data);
        if (session.data.version != TRACKER_VERSION_HASH) {
        }
        TrackerSession._i = session;
        return session;
    };
    TrackerSession.create = function () {
        var session = TrackerSession.build();
        session.save();
        TrackerSession._i = session;
        return session;
    };
    Object.defineProperty(TrackerSession, "instance", {
        get: function () {
            if (TrackerSession._i == undefined) {
                throw Error('Session not initialized');
            }
            return TrackerSession._i;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TrackerSession.prototype, "expired", {
        get: function () {
            var time = (new Date).getTime();
            var sid = parseInt(this.data.sid, 16);
            /*if (time - sid > SESSION_TIMEOUT * 1000) {
                this.data.sid = this.data.page;
            }*/
            return time - sid > SESSION_TIMEOUT * 1000;
        },
        enumerable: true,
        configurable: true
    });
    TrackerSession.prototype.migrate = function () {
        /*let sid = Helper.hi((new Date).getTime());
        let uid: string = Helper.hh(window.location.hostname) + '-' + sid + '-' + Helper.hi(Math.random() * 1000000).substr(0, 4);
        this.data = {
            version: TRACKER_VERSION_HASH,
            uid: uid,
            sid: sid,
            page: sid
        };*/
    };
    TrackerSession.prototype.refresh = function () {
        var time = (new Date).getTime();
        this.data.page = Helper.hi(time);
        return this;
    };
    TrackerSession.prototype.save = function () {
        var json = JSON.stringify(this.data);
        localStorage.setItem('so.tracker.session', json);
    };
    TrackerSession.prototype.destroy = function () {
        localStorage.removeItem('so.tracker.session');
    };
    return TrackerSession;
}());
/**
 * Tracker controller
 */
var Tracker = (function () {
    function Tracker() {
    }
    Tracker.prototype.initialize = function (window, endpoint, exclude) {
        if (exclude === void 0) { exclude = []; }
        /**
         * Initialize services and misc data
         */
        XhrService.initialize(endpoint, exclude);
        this.navigator = {
            userAgent: navigator.userAgent
        };
        /**
         * Initialize session and track initial event
         */
        this.session = TrackerSession.restore();
        if (this.session && !this.session.expired) {
            this.session.refresh().save();
            this.create(EventType.SR).track();
            this.session.refresh().save();
        }
        else {
            this.session = TrackerSession.create();
            this.create(EventType.SC).track();
        }
        var $tracker = this;
        /**
         * Listen for XHR events
         */
        (function (open) {
            XMLHttpRequest.prototype.open = function (method, url, async, user, pass) {
                if (!XhrService.excluded(url)) {
                    // $tracker.create(EventType.XO).track();
                    this.addEventListener("loadstart", function (event) {
                        event.currentTarget['loadStart'] = event.timeStamp;
                        event.currentTarget['requestURL'] = url;
                    });
                    this.addEventListener("load", $tracker.intercept.bind($tracker, EventType.XL));
                    this.addEventListener("error", $tracker.intercept.bind($tracker, EventType.XE));
                }
                open.call(this, method, url, true, user, pass);
            };
        })(XMLHttpRequest.prototype.open);
        /**
         * Intercept all window errors
         */
        window.onerror = function (error, url, line) {
            var event = $tracker.create(EventType.EU);
            event.context.data = {
                error: error,
                file: url,
                line: line
            };
            event.track();
        };
        /**
         * Track click events on anchors and buttons
         */
        document.addEventListener('click', function (event) {
            var element = event.target || event.srcElement;
            (function () {
                do {
                    switch (element.nodeName) {
                        case 'A':
                        case 'BUTTON':
                            return;
                        // break;
                        default:
                    }
                } while (element = element.parentElement);
            })();
            if (element) {
                var tracked = $tracker.create(EventType.CB);
                var elementName = element.nodeName.toLowerCase();
                var target = event.target || event.srcElement;
                if (element.id) {
                    elementName += '#' + element.id;
                }
                else {
                    elementName += '.' + element.classList.toString();
                }
                var elementContent = (target['innerText'] != undefined)
                    ? target['innerText'] : target.textContent;
                var elementHtml = element.parentElement.innerHTML;
                tracked.context.data = {
                    element: elementName,
                    text: elementContent.trim(),
                    html: elementHtml
                };
                tracked.track();
            }
        }, true);
        /**
         * Track html5 history state changes
         */
        (function (pushState) {
            window.history.pushState = function (stateObj, title, url) {
                var tracked = $tracker.create(EventType.SN);
                tracked.context.data = {
                    location: url
                };
                tracked.track();
                $tracker.session.refresh()
                    .save();
                pushState.call(this, stateObj, title, url);
                tracked = $tracker.create(EventType.SF);
                tracked.context.data = {
                    location: url
                };
                tracked.track();
            };
        })(window.history.pushState);
        window.onpopstate = function (event) {
            var tracked = $tracker.create(EventType.SN);
            tracked.context.data = {
                location: document.location.href
            };
            tracked.track();
            $tracker.session.refresh()
                .save();
            tracked = $tracker.create(EventType.SF);
            tracked.context.data = {
                location: document.location.href
            };
            tracked.track();
        };
    };
    Tracker.prototype.intercept = function (type, event) {
        this.wrap(type, event).track();
    };
    Tracker.prototype.create = function (type) {
        var context = {
            type: type,
            // navigator: this.navigator,
            session: this.session.data,
            // location: window.location.href,
            timestamp: Date.now()
        };
        switch (type) {
            case EventType.SC:
            case EventType.SR:
                context.data = {
                    navigator: this.navigator,
                    location: window.location.href
                };
                break;
            default:
        }
        return new TrackedEvent(context);
    };
    Tracker.prototype.wrap = function (type, event) {
        var tracked = this.create(type);
        switch (type) {
            case EventType.XL:
                // @todo monotonic timestamps
                var loadTime = event.timeStamp - event.currentTarget['loadStart'];
                tracked.context.data = {
                    url: event.currentTarget['requestURL'],
                    loadTime: loadTime
                };
                break;
            default:
        }
        return tracked;
    };
    return Tracker;
}());
(new Tracker()).initialize(window, '/tracker/-/track', ['^/service/-/keep-alive/', '^/-/apps-static/ui/templates/', '^/_wdt/']);
//# sourceMappingURL=tracker.js.map