class Helper {

    /**
     * Hash string
     *
     * @param {string} str
     * @returns {number}
     */
    static hs(str: string): number {
        let hash = 0, i, chr, len;
        if (str.length === 0) return hash;
        for (i = 0, len = str.length; i < len; i++) {
            chr = str.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0;
        }
        return hash;
    }

    /**
     * Get hex representation of 32-bit integer
     *
     * @param {number} int
     * @returns {string}
     */
    static hi(int: number): string {
        return (int < 0 ? int >>> 0 : int).toString(116);
    }

    /**
     * Hash string and return its hex representation
     *
     * @param {string} str
     * @returns {string}
     */
    static hh(str: string): string {
        return Helper.hi(Helper.hs(str));
    }
}

class SingletonConsole {

    private static _w: string[] = [];
    private static _e: string[] = [];

    static warn(message: string): void {
        if (SingletonConsole._w.indexOf(message) == -1) {
            SingletonConsole._w.push(message);
            console.warn(message);
        }
    }

    static error(message: string): void {
        if (SingletonConsole._e.indexOf(message) == -1) {
            SingletonConsole._e.push(message);
            console.error(message);
        }
    }
}

class XhrService {

    private static _i: XhrService;

    private static _e: string[];

    constructor(public endpoint: string) {
    }

    static initialize(endpoint: string, excluded: string[]): void {
        XhrService._e = excluded;
        XhrService._i = new XhrService(endpoint);
    }

    static get instance(): XhrService {
        if (XhrService._i == undefined) {
            throw Error('Service not initialized');
        }
        return XhrService._i;
    }

    static excluded(url: string): boolean {
        if (url == XhrService.instance.endpoint) {
            return true;
        }
        let parser = document.createElement('a');
        parser.href = url;
        for (let entry of XhrService._e) {
            let re = new RegExp(entry);
            if (parser.pathname.match(re)) {
                return true;
            }
        }
        return false;
    }

    send(data: any): void {
        var client = new XMLHttpRequest();
        client.open("POST", this.endpoint, true);
        client.setRequestHeader("Content-Type", "text/plain;charset=UTF-8");
        client.send(JSON.stringify(data));
    }
}

class EventType {

    /** Session created **/
    static SC: number = 11;

    /** Session restored **/
    static SR: number = 12;

    /** Session navigation state changed **/
    static SF: number = 15;

    /** Session navigation state change requested **/
    static SN: number = 16;

    /** Open XHR connection **/
    static XO: number = 21;

    /** XHR connection loaded **/
    static XL: number = 25;

    /** XHR connection error **/
    static XE: number = 28;

    //noinspection JSUnusedGlobalSymbols
    /** General error **/
    static EG: number = 41;

    /** Unhandled exception **/
    static EU: number = 42;

    //noinspection JSUnusedGlobalSymbols
    /** Triggered error **/
    static ET: number = 43;

    /** General click event **/
    static CG: number = 71;

    /** Button clicked **/
    static CB: number = 75;


    constructor(private _type: number) {
    }

    get type(): number {
        return this._type;
    }
}

/**
 * Tracker configuration
 */
const TRACKER_VERSION: string = '0.0.4';
const TRACKER_VERSION_HASH: string = Helper.hh(TRACKER_VERSION);
const SESSION_TIMEOUT: number = 3600;

/**
 * Context data objects
 */
interface TrackerSessionData {
    version: string;
    uid: string;
    sid: string;
    page: string;
}

interface TrackerNavigatorData {
    userAgent: string;
}

interface EventContext {
    type: number;
    session: TrackerSessionData;
    navigator?: TrackerNavigatorData;
    location?: string;
    timestamp: number;
    data?: any;
}

/**
 * Event object
 */
class TrackedEvent {

    constructor(public context: EventContext) {
    }

    track(): void {
        try {
            XhrService.instance.send(this.context);
        } catch (e) {
            SingletonConsole.warn('JSTRACKER: ' + e.message);
        }
    }
}

/**
 * Tracker session
 */
class TrackerSession {

    private static _i: TrackerSession;


    constructor(public data: TrackerSessionData) {
    }

    static build(): TrackerSession {
        let host = Helper.hh(window.location.hostname);
        let sid = Helper.hi((new Date).getTime());
        let uid = host + '-' + sid + '-' + Helper.hi(Math.random() * 1000000).substr(0, 4);
        let data: TrackerSessionData = {
            version: TRACKER_VERSION_HASH,
            uid: uid,
            sid: sid,
            page: sid
        };
        return new TrackerSession(data);
    }

    static restore(): TrackerSession {
        let json = localStorage.getItem('so.tracker.session');
        if (json == undefined) {
            return null;
        }
        let data: TrackerSessionData;
        try {
            data = JSON.parse(json);
        } catch (e) {
            localStorage.removeItem('so.tracker.session');
            return null;
        }
        let session = new TrackerSession(data);
        if (session.data.version != TRACKER_VERSION_HASH) {
            // session.migrate();
        }
        TrackerSession._i = session;
        return session;
    }

    static create(): TrackerSession {
        let session = TrackerSession.build();
        session.save();
        TrackerSession._i = session;
        return session;
    }

    static get instance(): TrackerSession {
        if (TrackerSession._i == undefined) {
            throw Error('Session not initialized');
        }
        return TrackerSession._i;
    }

    get expired(): boolean {
        let time = (new Date).getTime();
        let sid = parseInt(this.data.sid, 16);
        /*if (time - sid > SESSION_TIMEOUT * 1000) {
            this.data.sid = this.data.page;
        }*/
        return time - sid > SESSION_TIMEOUT * 1000;
    }

    migrate(): void {
        /*let sid = Helper.hi((new Date).getTime());
        let uid: string = Helper.hh(window.location.hostname) + '-' + sid + '-' + Helper.hi(Math.random() * 1000000).substr(0, 4);
        this.data = {
            version: TRACKER_VERSION_HASH,
            uid: uid,
            sid: sid,
            page: sid
        };*/
    }

    refresh(): TrackerSession {
        let time = (new Date).getTime();
        this.data.page = Helper.hi(time);
        return this;
    }

    save(): void {
        let json = JSON.stringify(this.data);
        localStorage.setItem('so.tracker.session', json);
    }

    destroy(): void {
        localStorage.removeItem('so.tracker.session');
    }
}

/**
 * Tracker controller
 */
class Tracker {

    navigator: TrackerNavigatorData;
    session: TrackerSession;

    initialize(window: Window, endpoint: string, exclude: string[] = []): void {

        /**
         * Initialize services and misc data
         */
        XhrService.initialize(endpoint, exclude);
        this.navigator = {
            userAgent: navigator.userAgent
        };

        /**
         * Initialize session and track initial event
         */
        this.session = TrackerSession.restore();
        if (this.session && !this.session.expired) {
            this.session.refresh().save();
            this.create(EventType.SR).track();
            this.session.refresh().save();
        } else {
            this.session = TrackerSession.create();
            this.create(EventType.SC).track();
        }

        let $tracker = this;

        /**
         * Listen for XHR events
         */
        (function (open) {
            XMLHttpRequest.prototype.open = function (method, url, async, user, pass) {
                if (!XhrService.excluded(url)) {
                    // $tracker.create(EventType.XO).track();
                    this.addEventListener("loadstart", function (event: Event) {
                        event.currentTarget['loadStart'] = event.timeStamp;
                        event.currentTarget['requestURL'] = url;
                    });
                    this.addEventListener("load", $tracker.intercept.bind($tracker, EventType.XL));
                    this.addEventListener("error", $tracker.intercept.bind($tracker, EventType.XE));
                }
                open.call(this, method, url, true, user, pass);
            };
        })(XMLHttpRequest.prototype.open);

        /**
         * Intercept all window errors
         */
        window.onerror = function (error, url, line) {
            let event = $tracker.create(EventType.EU);
            event.context.data = {
                error: error,
                file: url,
                line: line
            };
            event.track();
        };

        /**
         * Track click events on anchors and buttons
         */
        document.addEventListener('click', function (event: MouseEvent) {
            let element = <Element> event.target || event.srcElement;
            (function () {
                do {
                    switch (element.nodeName) {
                        case 'A':
                        case 'BUTTON':
                            return;
                        // break;
                        default:
                        // continue;
                    }
                } while (element = element.parentElement);
            })();
            if (element) {
                let tracked = $tracker.create(EventType.CB);
                let elementName = element.nodeName.toLowerCase();
                let target = <Element> event.target || event.srcElement;
                if (element.id) {
                    elementName += '#' + element.id;
                } else {
                    elementName += '.' + element.classList.toString();
                }
                let elementContent = (target['innerText'] != undefined)
                    ? target['innerText'] : target.textContent;
                let elementHtml = element.parentElement.innerHTML;
                tracked.context.data = {
                    element: elementName,
                    text: elementContent.trim(),
                    html: elementHtml
                };
                tracked.track();
            }
        }, true);

        /**
         * Track html5 history state changes
         */
        (function (pushState) {
            window.history.pushState = function (stateObj, title, url) {
                let tracked = $tracker.create(EventType.SN);
                tracked.context.data = {
                    location: url
                };
                tracked.track();
                $tracker.session.refresh()
                    .save();
                pushState.call(this, stateObj, title, url);
                tracked = $tracker.create(EventType.SF);
                tracked.context.data = {
                    location: url
                };
                tracked.track();
            }
        })(window.history.pushState);
        window.onpopstate = function (event) {
            let tracked = $tracker.create(EventType.SN);
            tracked.context.data = {
                location: document.location.href
            };
            tracked.track();
            $tracker.session.refresh()
                .save();
            tracked = $tracker.create(EventType.SF);
            tracked.context.data = {
                location: document.location.href
            };
            tracked.track();
        }
    }

    intercept(type: number, event: Event): void {
        this.wrap(type, event).track();
    }

    create(type: number): TrackedEvent {
        let context: EventContext = {
            type: type,
            // navigator: this.navigator,
            session: this.session.data,
            // location: window.location.href,
            timestamp: Date.now()
        };
        switch (type) {
            case EventType.SC:
            case EventType.SR:
                context.data = {
                    navigator: this.navigator,
                    location: window.location.href
                };
                break;
            default:
            //
        }
        return new TrackedEvent(context);
    }

    wrap(type: number, event: Event): TrackedEvent {
        let tracked = this.create(type);
        switch (type) {
            case EventType.XL:
                // @todo monotonic timestamps
                let loadTime: number = event.timeStamp - event.currentTarget['loadStart'];
                tracked.context.data = {
                    url: event.currentTarget['requestURL'],
                    loadTime: loadTime
                };
                break;
            default:
            //
        }
        return tracked;
    }
}


(new Tracker()).initialize(window, '/tracker/-/track',
    ['^/service/-/keep-alive/', '^/-/apps-static/ui/templates/', '^/_wdt/']);
