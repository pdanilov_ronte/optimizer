var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
System.register("app/config.service", ['angular2/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var core_1;
    var ConfigService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            ConfigService = (function () {
                function ConfigService() {
                    this._config = {};
                    if (CONFIG == undefined) {
                        throw new Error('Config file is not loaded');
                    }
                    this._config = CONFIG;
                    this._config.app = (typeof CONFIG_EXT != 'undefined') ? CONFIG_EXT.app : {};
                }
                Object.defineProperty(ConfigService.prototype, "_", {
                    get: function () {
                        return this._config;
                    },
                    enumerable: true,
                    configurable: true
                });
                ConfigService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [])
                ], ConfigService);
                return ConfigService;
            }());
            exports_1("ConfigService", ConfigService);
        }
    }
});
System.register("app/xhr.service", ['angular2/core', 'angular2/http'], function(exports_2, context_2) {
    "use strict";
    var __moduleName = context_2 && context_2.id;
    var core_2, http_1;
    var XhrService;
    return {
        setters:[
            function (core_2_1) {
                core_2 = core_2_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            }],
        execute: function() {
            XhrService = (function () {
                function XhrService(http) {
                    this.http = http;
                }
                XhrService.prototype.get = function (url, search) {
                    var args = {};
                    args.headers = new http_1.Headers({
                        'X-Requested-With': 'XMLHttpRequest',
                        'Content-Type': 'application/json'
                    });
                    if (typeof search == 'object') {
                        var plain = '';
                        for (var prop in search) {
                            if (search.hasOwnProperty(prop)) {
                                plain += (plain.length ? '&' : '') + prop + '=' + search[prop];
                            }
                        }
                        args.search = plain;
                    }
                    var request = this.http.get(url, args);
                    return new Promise(function (resolve, reject) {
                        request.subscribe(function (response) {
                            resolve(response);
                        }, function (error) {
                            reject(error.json());
                        });
                    });
                };
                XhrService.prototype.post = function (url, data) {
                    var headers = new http_1.Headers({
                        'X-Requested-With': 'XMLHttpRequest',
                        'Content-Type': 'application/json'
                    });
                    var json = JSON.stringify(data);
                    var options = {
                        headers: headers
                    };
                    var request = this.http.post(url, json, options);
                    return new Promise(function (resolve, reject) {
                        request.subscribe(function (response) {
                            resolve(response);
                        }, function (error) {
                            reject(error.json());
                        });
                    });
                };
                XhrService = __decorate([
                    core_2.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], XhrService);
                return XhrService;
            }());
            exports_2("XhrService", XhrService);
        }
    }
});
System.register("app/auth", ['rxjs/add/operator/map', 'angular2/core', 'angular2/http', "app/config.service", "app/xhr.service"], function(exports_3, context_3) {
    "use strict";
    var __moduleName = context_3 && context_3.id;
    var core_3, http_2, config_service_1, xhr_service_1;
    var AuthService;
    return {
        setters:[
            function (_1) {},
            function (core_3_1) {
                core_3 = core_3_1;
            },
            function (http_2_1) {
                http_2 = http_2_1;
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            },
            function (xhr_service_1_1) {
                xhr_service_1 = xhr_service_1_1;
            }],
        execute: function() {
            AuthService = (function () {
                function AuthService(http, config, xhr) {
                    var _this = this;
                    this.http = http;
                    this.config = config;
                    this.xhr = xhr;
                    this._account = {};
                    this.xhr.get('/service/-/auth/account')
                        .then(function (result) {
                        var data = result.json();
                        _this._account = data['account'];
                    })
                        .catch(function (error) { return console.error(error); });
                }
                Object.defineProperty(AuthService.prototype, "account", {
                    get: function () {
                        return this._account;
                    },
                    enumerable: true,
                    configurable: true
                });
                AuthService.prototype.isLoggedIn = function () {
                    return Boolean(parseInt(localStorage.getItem('so.auth.is_logged_in')));
                };
                AuthService.prototype.login = function (username, password) {
                    var headers = new http_2.Headers({
                        'X-Requested-With': 'XMLHttpRequest',
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    });
                    var body = '_username=' + username + '&_password=' + password;
                    var json = JSON.stringify({
                        _username: username,
                        _password: password
                    });
                    var options = {
                        method: http_2.RequestMethod.Get,
                        headers: headers,
                        search: '_username=' + username + '&_password=' + password
                    };
                    var request = new http_2.Request({
                        method: http_2.RequestMethod.Get,
                        url: this.config._.auth.login_check,
                        headers: headers,
                        search: '_username=' + username + '&_password=' + password
                    });
                    var post = this.http.get(this.config._.auth.login_check, { search: '_username=' + username + '&_password=' + password });
                    var $this = this;
                    return new Promise(function (resolve, reject) {
                        post
                            .subscribe(function (res) {
                            var json = res.json();
                            $this._account = json.user;
                            localStorage.setItem('so.auth.logged', '1');
                            localStorage.setItem('so.auth.account', JSON.stringify(json.user));
                            resolve(res);
                            window.location.href = '/';
                        }, function (err) {
                            var json = err.json();
                            reject(json.error);
                        }, function () {
                        });
                    });
                };
                AuthService.prototype.logout = function () {
                    localStorage.clear();
                    window.location.href = '/logout';
                };
                AuthService.prototype.updateAccount = function (data) {
                    var _this = this;
                    var account = {
                        name: '',
                        email: ''
                    };
                    var error;
                    for (var prop in account) {
                        if (!this._account.hasOwnProperty(prop)) {
                            error = sprintf('Account has no property %s', prop);
                            console.error(error);
                            return new Promise(function (resolve, reject) {
                                reject(error);
                            });
                        }
                        account[prop] = data[prop];
                    }
                    //account['_token'] = this.config._.app.csrf.fos_user_profile_form;
                    var headers = new http_2.Headers({
                        'X-Requested-With': 'XMLHttpRequest',
                        'Content-Type': 'application/json'
                    });
                    var json = JSON.stringify(account);
                    var options = {
                        headers: headers
                    };
                    var request = this.http.post(this.config._.auth.account_update, json, options);
                    return new Promise(function (resolve, reject) {
                        request
                            .subscribe(function (res) {
                            var json = res.json();
                            _this._account = json.account;
                            localStorage.setItem('so.auth.account', JSON.stringify(json.account));
                            resolve(json);
                        }, function (err) {
                            reject(err.json().error);
                        }, function () {
                        });
                    });
                };
                AuthService.prototype.changePassword = function (data) {
                    var password = {
                        current: '',
                        new: '',
                        confirm: ''
                    };
                    var error;
                    for (var prop in password) {
                        if (!data.hasOwnProperty(prop)) {
                            error = sprintf('Data has no property %s', prop);
                            console.error(error);
                            return new Promise(function (resolve, reject) {
                                reject(error);
                            });
                        }
                        password[prop] = data[prop];
                    }
                    var headers = new http_2.Headers({
                        'X-Requested-With': 'XMLHttpRequest',
                        'Content-Type': 'application/json'
                    });
                    var json = JSON.stringify(password);
                    var options = {
                        headers: headers
                    };
                    var request = this.http.post(this.config._.auth.change_password, json, options);
                    return new Promise(function (resolve, reject) {
                        request
                            .subscribe(function (res) {
                            var json = res.json();
                            //this._account = json.account;
                            //localStorage.setItem('so.auth.account', JSON.stringify(json.account));
                            resolve(json);
                        }, function (err) {
                            reject(err.json());
                        }, function () {
                        });
                    });
                };
                AuthService = __decorate([
                    core_3.Injectable(), 
                    __metadata('design:paramtypes', [http_2.Http, config_service_1.ConfigService, xhr_service_1.XhrService])
                ], AuthService);
                return AuthService;
            }());
            exports_3("AuthService", AuthService);
        }
    }
});
System.register("app/em/entity-repository", [], function(exports_4, context_4) {
    "use strict";
    var __moduleName = context_4 && context_4.id;
    var EntityRepository;
    return {
        setters:[],
        execute: function() {
            EntityRepository = (function () {
                function EntityRepository() {
                }
                return EntityRepository;
            }());
            exports_4("EntityRepository", EntityRepository);
        }
    }
});
System.register("app/em/restangular.service", ['angular2/core'], function(exports_5, context_5) {
    "use strict";
    var __moduleName = context_5 && context_5.id;
    var core_4;
    var RestangularService, IPostElement, IPostCollection;
    return {
        setters:[
            function (core_4_1) {
                core_4 = core_4_1;
            }],
        execute: function() {
            RestangularService = (function () {
                function RestangularService() {
                    var $http = window['legacyProviders']['http'];
                    this.service = window['restangularConstructor']($http, Q);
                    this.service.setBaseUrl('/api/-/rest/v1');
                }
                RestangularService.prototype.all = function (route, base) {
                    //let path = name + 's';
                    var element = base ? base.all(route) : this.service.all(route);
                    return new Promise(function (resolve, reject) {
                        element.getList()
                            .then(function (result) {
                            resolve(result);
                        })
                            .catch(function (error) { return reject(error); });
                    });
                };
                //noinspection JSMethodCanBeStatic
                RestangularService.prototype.save = function (entity) {
                    return new Promise(function (resolve, reject) {
                        entity.save()
                            .then(function (element) { return resolve(element); })
                            .catch(function (error) { return reject(error); });
                    });
                };
                //noinspection JSMethodCanBeStatic
                RestangularService.prototype.remove = function (entity) {
                    return new Promise(function (resolve, reject) {
                        entity.remove()
                            .then(function (element) { return resolve(element); })
                            .catch(function (error) { return reject(error); });
                    });
                };
                RestangularService = __decorate([
                    core_4.Injectable(), 
                    __metadata('design:paramtypes', [])
                ], RestangularService);
                return RestangularService;
            }());
            exports_5("RestangularService", RestangularService);
            IPostElement = (function () {
                function IPostElement(element) {
                    this.element = element;
                }
                IPostElement.prototype.post = function (entity) {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        _this.element.post(entity.route, entity.plain())
                            .then(function (element) {
                            element['parentResource'] = _this.element;
                            entity.ielement = element;
                            resolve(entity);
                        })
                            .catch(function (error) { return reject(error); });
                    });
                };
                return IPostElement;
            }());
            exports_5("IPostElement", IPostElement);
            IPostCollection = (function () {
                function IPostCollection(collection) {
                    this.collection = collection;
                }
                IPostCollection.prototype.post = function (entity) {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        return _this.collection.post(entity.plain())
                            .then(function (element) {
                            entity.ielement = element;
                            resolve(entity);
                        })
                            .catch(function (error) { return reject(error); });
                    });
                };
                return IPostCollection;
            }());
            exports_5("IPostCollection", IPostCollection);
        }
    }
});
System.register("app/em/entity-factory", [], function(exports_6, context_6) {
    "use strict";
    var __moduleName = context_6 && context_6.id;
    var AbstractEntityFactory, FactoryRegistry, EntityFactoryMetadata, EntityFactoryDecorator, EntityFactory;
    return {
        setters:[],
        execute: function() {
            AbstractEntityFactory = (function () {
                function AbstractEntityFactory(_em) {
                    this._em = _em;
                }
                AbstractEntityFactory.prototype.create = function (complete, params) {
                    if (complete === void 0) { complete = false; }
                    throw new Error('You must implement `create` method in ' + this.constructor['name'] + ' class');
                };
                AbstractEntityFactory.prototype.complete = function (entity) {
                    throw new Error('You must implement `complete` method in ' + this.constructor['name'] + ' class');
                };
                AbstractEntityFactory.prototype.fromIElement = function (element) {
                    throw new Error('You must implement `fromIElement` method in ' + this.constructor['name'] + ' class');
                };
                Object.defineProperty(AbstractEntityFactory.prototype, "em", {
                    get: function () {
                        return this._em;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(AbstractEntityFactory, "ENTITY_NAME", {
                    get: function () {
                        return undefined;
                    },
                    enumerable: true,
                    configurable: true
                });
                ;
                return AbstractEntityFactory;
            }());
            exports_6("AbstractEntityFactory", AbstractEntityFactory);
            FactoryRegistry = (function () {
                function FactoryRegistry() {
                }
                FactoryRegistry.register = function (type, factoryClass) {
                    if (FactoryRegistry.factoryClasses.has(type)) {
                        throw new Error('Factory for type ' + type['name'] + ' already registered');
                    }
                    FactoryRegistry.factoryClasses.set(type, factoryClass);
                };
                FactoryRegistry.getClass = function (type) {
                    if (!FactoryRegistry.factoryClasses.has(type)) {
                        throw new Error('Factory for type ' + type['name'] + ' is not registered');
                    }
                    return FactoryRegistry.factoryClasses.get(type);
                };
                FactoryRegistry.factoryClasses = new Map();
                return FactoryRegistry;
            }());
            exports_6("FactoryRegistry", FactoryRegistry);
            EntityFactoryMetadata = (function () {
                function EntityFactoryMetadata() {
                }
                return EntityFactoryMetadata;
            }());
            EntityFactoryDecorator = (function () {
                function EntityFactoryDecorator(metadata) {
                    this.metadata = metadata;
                }
                EntityFactoryDecorator.prototype.decorate = function () {
                };
                return EntityFactoryDecorator;
            }());
            exports_6("EntityFactory", EntityFactory = function () {
                return function (target) {
                    var metadata = new EntityFactoryMetadata();
                    var decorator = new EntityFactoryDecorator(metadata);
                    decorator.decorate();
                };
            });
        }
    }
});
System.register("app/em/index", [], function(exports_7, context_7) {
    "use strict";
    var __moduleName = context_7 && context_7.id;
    var Index;
    return {
        setters:[],
        execute: function() {
            Index = (function () {
                function Index(propertyName, items) {
                    if (items === void 0) { items = []; }
                    this.propertyName = propertyName;
                    this._map = new Map();
                    this.build(items);
                }
                Index.prototype.push = function (item) {
                    this.get(item[this.propertyName]).push(item);
                };
                Index.prototype.remove = function (item) {
                    this._map.forEach(function (items, key) {
                        var i = items.indexOf(item);
                        if (i !== -1) {
                            items.splice(i, 1);
                        }
                    });
                };
                Index.prototype.get = function (key) {
                    if (!this._map.has(key)) {
                        this._map.set(key, []);
                    }
                    return this._map.get(key);
                };
                //noinspection ReservedWordAsName
                Index.prototype.in = function (keys) {
                    var _this = this;
                    var result = [];
                    keys.forEach(function (key) {
                        result = result.concat(_this.get(key));
                    });
                    return result;
                };
                Index.prototype.rebuild = function (items) {
                    this.clear();
                    this.build(items);
                };
                Index.prototype.build = function (items) {
                    var _this = this;
                    items.forEach(function (item) { return _this.push(item); });
                };
                Index.prototype.clear = function () {
                    this._map.clear();
                };
                return Index;
            }());
            exports_7("Index", Index);
        }
    }
});
System.register("app/em/indexed-collection", ["app/em/index"], function(exports_8, context_8) {
    "use strict";
    var __moduleName = context_8 && context_8.id;
    var index_1;
    var IndexedCollection;
    return {
        setters:[
            function (index_1_1) {
                index_1 = index_1_1;
            }],
        execute: function() {
            IndexedCollection = (function () {
                function IndexedCollection(items) {
                    this._items = [];
                    this._cursor = -1;
                    this._indexes = new Map();
                    if (items) {
                        this._items = items;
                    }
                }
                IndexedCollection.prototype.push = function (item) {
                    var length = this._items.push(item);
                    this._indexes.forEach(function (index) {
                        index.push(item);
                    });
                    return length;
                };
                IndexedCollection.prototype.splice = function (start, deleteCount) {
                    return this._items.splice(start, deleteCount);
                };
                IndexedCollection.prototype.remove = function (item) {
                    this._indexes.forEach(function (index) {
                        index.remove(item);
                    });
                    var i = this._items.indexOf(item);
                    this._items.splice(i, 1);
                };
                IndexedCollection.prototype.indexOf = function (searchElement) {
                    return this._items.indexOf(searchElement);
                };
                IndexedCollection.prototype.forEach = function (callbackfn, thisArg) {
                    this._items.forEach(callbackfn, this);
                };
                Object.defineProperty(IndexedCollection.prototype, "items", {
                    get: function () {
                        return this._items;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(IndexedCollection.prototype, "length", {
                    get: function () {
                        return this._items.length;
                    },
                    enumerable: true,
                    configurable: true
                });
                IndexedCollection.prototype.next = function (value) {
                    return this._cursor < this._items.length
                        ? { done: false, value: this._items[this._cursor++] }
                        : { done: true };
                };
                IndexedCollection.prototype.index = function (property) {
                    if (!this._indexes.has(property)) {
                        this._indexes.set(property, new index_1.Index(property, this._items));
                    }
                    return this._indexes.get(property);
                };
                IndexedCollection.prototype.reindex = function (property) {
                    var _this = this;
                    if (property) {
                        this.index(property).rebuild(this._items);
                    }
                    else {
                        this._indexes.forEach(function (index) {
                            index.rebuild(_this._items);
                        });
                    }
                };
                return IndexedCollection;
            }());
            exports_8("IndexedCollection", IndexedCollection);
        }
    }
});
System.register("app/decorator", [], function(exports_9, context_9) {
    "use strict";
    var __moduleName = context_9 && context_9.id;
    var DecoratorMetadata, TypeDecoratorMetadata, AbstractDecorator, TypeDecorator, DecoratorFactory, TypeDecoratorFactory;
    function makeTypeDecorator(annotationCls) {
        function TypeDecoratorFactory(objOrType) {
            var annotationInstance = new annotationCls(objOrType);
            if (this instanceof annotationCls) {
                return annotationInstance;
            }
            else {
                var chainAnnotation = this.annotations instanceof Array ? this.annotations : [];
                chainAnnotation.push(annotationInstance);
                var TypeDecorator = function TypeDecorator(cls) {
                    var annotations = Reflect['getOwnMetadata']('annotations', cls);
                    annotations = annotations || [];
                    annotations.push(annotationInstance);
                    Reflect['defineMetadata']('annotations', annotations, cls);
                    annotationInstance.decorateTarget(cls, null);
                    return cls;
                };
                TypeDecorator['annotations'] = chainAnnotation;
                return TypeDecorator;
            }
        }
        TypeDecoratorFactory.prototype = Object.create(annotationCls.prototype);
        return TypeDecoratorFactory;
    }
    exports_9("makeTypeDecorator", makeTypeDecorator);
    function makePropDecorator(decoratorCls) {
        function PropDecoratorFactory() {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i - 0] = arguments[_i];
            }
            var decoratorInstance = Object.create(decoratorCls.prototype);
            if (this instanceof decoratorCls) {
                return decoratorInstance;
            }
            else {
                return function PropDecorator(target, name) {
                    var meta = Reflect['getOwnMetadata']('propMetadata', target.constructor);
                    meta = meta || {};
                    meta[name] = meta[name] || [];
                    meta[name].unshift(decoratorInstance);
                    Reflect['defineMetadata']('propMetadata', meta, target.constructor);
                    decoratorInstance.decorateTarget(target.constructor, name, args);
                };
            }
        }
        PropDecoratorFactory.prototype = Object.create(decoratorCls.prototype);
        return PropDecoratorFactory;
    }
    exports_9("makePropDecorator", makePropDecorator);
    return {
        setters:[],
        execute: function() {
            DecoratorMetadata = (function () {
                function DecoratorMetadata() {
                }
                return DecoratorMetadata;
            }());
            exports_9("DecoratorMetadata", DecoratorMetadata);
            TypeDecoratorMetadata = (function (_super) {
                __extends(TypeDecoratorMetadata, _super);
                function TypeDecoratorMetadata() {
                    _super.apply(this, arguments);
                }
                TypeDecoratorMetadata.prototype.decorateTarget = function (target) {
                };
                return TypeDecoratorMetadata;
            }(DecoratorMetadata));
            exports_9("TypeDecoratorMetadata", TypeDecoratorMetadata);
            AbstractDecorator = (function () {
                function AbstractDecorator() {
                }
                return AbstractDecorator;
            }());
            exports_9("AbstractDecorator", AbstractDecorator);
            TypeDecorator = (function (_super) {
                __extends(TypeDecorator, _super);
                function TypeDecorator() {
                    _super.apply(this, arguments);
                }
                TypeDecorator.prototype.decorate = function (target) {
                };
                return TypeDecorator;
            }(AbstractDecorator));
            exports_9("TypeDecorator", TypeDecorator);
            /**
             * Factory
             */
            DecoratorFactory = (function () {
                function DecoratorFactory() {
                }
                DecoratorFactory.build = function (type) {
                    var factory = new TypeDecoratorFactory();
                    var metadata = new TypeDecoratorMetadata();
                    var o_metadata = Object.create(type);
                    return factory.build(metadata);
                };
                return DecoratorFactory;
            }());
            exports_9("DecoratorFactory", DecoratorFactory);
            TypeDecoratorFactory = (function () {
                function TypeDecoratorFactory() {
                }
                TypeDecoratorFactory.prototype.build = function (metadata) {
                    return new TypeDecorator();
                };
                return TypeDecoratorFactory;
            }());
            exports_9("TypeDecoratorFactory", TypeDecoratorFactory);
        }
    }
});
System.register("app/em/contract.decorator", ["app/em/entity", "app/decorator"], function(exports_10, context_10) {
    "use strict";
    var __moduleName = context_10 && context_10.id;
    var entity_1, decorator_1;
    var PropertyContract, ContractDelegate, ContractFactory, ContractMetadata, Contract;
    return {
        setters:[
            function (entity_1_1) {
                entity_1 = entity_1_1;
            },
            function (decorator_1_1) {
                decorator_1 = decorator_1_1;
            }],
        execute: function() {
            PropertyContract = (function () {
                function PropertyContract(entityType, propertyName) {
                    this.entityType = entityType;
                    this.propertyName = propertyName;
                    this._fulfilled = false;
                }
                PropertyContract.prototype.bind = function (entity, em) {
                    return new ContractDelegate(this, entity, em);
                };
                PropertyContract.prototype.fulfill = function (data, delegate) {
                    delegate.deliver(this.property.bind(this), data);
                    this._fulfilled = true;
                };
                Object.defineProperty(PropertyContract.prototype, "fulfilled", {
                    get: function () {
                        return this._fulfilled;
                    },
                    enumerable: true,
                    configurable: true
                });
                PropertyContract.prototype.property = function (entity) {
                    return entity[this.propertyName];
                };
                return PropertyContract;
            }());
            exports_10("PropertyContract", PropertyContract);
            ContractDelegate = (function () {
                function ContractDelegate(contract, entity, em) {
                    this.contract = contract;
                    this.entity = entity;
                    this.em = em;
                }
                ContractDelegate.prototype.transfer = function (method) {
                    return method(this.contract.entityType, this.entity);
                };
                ContractDelegate.prototype.deliver = function (property, data) {
                    var destination = property(this.entity);
                    if (destination.length) {
                        destination.items.splice(0);
                    }
                    for (var _i = 0, _a = data.items; _i < _a.length; _i++) {
                        var item = _a[_i];
                        destination.push(item);
                    }
                };
                ContractDelegate.prototype.expand = function (method, data) {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        if (!_this.contract.fulfilled) {
                            return reject('Attempt to expand unfulfilled contract');
                        }
                        if (!data.length) {
                            // @todo empty promise results
                            return resolve();
                        }
                        if (!entity_1.ModelEntity.isExpandable(data.items[0])) {
                            return resolve();
                        }
                        var expanded = 0;
                        data.forEach(function (entity) {
                            entity.expand()
                                .then(function (result) {
                                if (++expanded == data.length) {
                                    resolve(_this.entity);
                                }
                            })
                                .catch(function (error) {
                                reject(error);
                            });
                        });
                    });
                };
                return ContractDelegate;
            }());
            exports_10("ContractDelegate", ContractDelegate);
            ContractFactory = (function () {
                function ContractFactory() {
                }
                ContractFactory.create = function (meta) {
                    return new PropertyContract(meta.entityType, meta.propertyName);
                };
                return ContractFactory;
            }());
            ContractMetadata = (function () {
                function ContractMetadata() {
                }
                ContractMetadata.prototype.decorateTarget = function (targetClass, propertyName, args) {
                    this.targetClass = targetClass;
                    this.propertyName = propertyName;
                    this.entityType = args[0];
                    this.contract = ContractFactory.create(this);
                };
                return ContractMetadata;
            }());
            exports_10("ContractMetadata", ContractMetadata);
            exports_10("Contract", Contract = decorator_1.makePropDecorator(ContractMetadata));
        }
    }
});
System.register("app/em/entity-type.decorator", ["app/decorator"], function(exports_11, context_11) {
    "use strict";
    var __moduleName = context_11 && context_11.id;
    var decorator_2;
    var EntityTypeMetadata, EntityTypeRegistry, EntityType;
    return {
        setters:[
            function (decorator_2_1) {
                decorator_2 = decorator_2_1;
            }],
        execute: function() {
            EntityTypeMetadata = (function () {
                function EntityTypeMetadata() {
                }
                EntityTypeMetadata.prototype.decorateTarget = function (target, name) {
                    this.type = target;
                    EntityTypeRegistry.register(target);
                };
                return EntityTypeMetadata;
            }());
            exports_11("EntityTypeMetadata", EntityTypeMetadata);
            EntityTypeRegistry = (function () {
                function EntityTypeRegistry() {
                }
                EntityTypeRegistry.register = function (type) {
                    if (EntityTypeRegistry.types.indexOf(type) !== undefined) {
                        EntityTypeRegistry.types.push(type);
                    }
                };
                EntityTypeRegistry.types = [];
                return EntityTypeRegistry;
            }());
            exports_11("EntityTypeRegistry", EntityTypeRegistry);
            /**
             * @deprecated
             */
            exports_11("EntityType", EntityType = decorator_2.makeTypeDecorator(EntityTypeMetadata));
        }
    }
});
System.register("app/em/change-tracker", [], function(exports_12, context_12) {
    "use strict";
    var __moduleName = context_12 && context_12.id;
    var ChangeTracker;
    return {
        setters:[],
        execute: function() {
            ChangeTracker = (function () {
                function ChangeTracker() {
                    // @todo
                    this._map = new Map();
                    this._queue = [];
                }
                Object.defineProperty(ChangeTracker.prototype, "queue", {
                    get: function () {
                        return this._queue;
                    },
                    enumerable: true,
                    configurable: true
                });
                ChangeTracker.prototype.track = function (entity) {
                    // @todo track
                    var index = this._queue.indexOf(entity);
                    if (index !== -1) {
                        // @todo necessary to update?
                        this._queue[index] = entity;
                        return index;
                    }
                    return this._queue.push(entity);
                };
                ChangeTracker.prototype.shift = function () {
                    return this._queue.shift();
                };
                ChangeTracker.prototype.contains = function (entity) {
                    return this._queue.indexOf(entity) !== -1;
                };
                return ChangeTracker;
            }());
            exports_12("ChangeTracker", ChangeTracker);
        }
    }
});
System.register("app/em/entity-manager", ['angular2/core', "app/em/entity", "app/em/entity-repository", "app/em/restangular.service", "app/em/indexed-collection", "app/em/entity-type.decorator", "app/em/change-tracker", "app/em/entity-factory"], function(exports_13, context_13) {
    "use strict";
    var __moduleName = context_13 && context_13.id;
    var core_5, entity_2, entity_repository_1, restangular_service_1, indexed_collection_1, entity_type_decorator_1, change_tracker_1, restangular_service_2, entity_factory_1;
    var EntityManager;
    return {
        setters:[
            function (core_5_1) {
                core_5 = core_5_1;
            },
            function (entity_2_1) {
                entity_2 = entity_2_1;
            },
            function (entity_repository_1_1) {
                entity_repository_1 = entity_repository_1_1;
            },
            function (restangular_service_1_1) {
                restangular_service_1 = restangular_service_1_1;
                restangular_service_2 = restangular_service_1_1;
            },
            function (indexed_collection_1_1) {
                indexed_collection_1 = indexed_collection_1_1;
            },
            function (entity_type_decorator_1_1) {
                entity_type_decorator_1 = entity_type_decorator_1_1;
            },
            function (change_tracker_1_1) {
                change_tracker_1 = change_tracker_1_1;
            },
            function (entity_factory_1_1) {
                entity_factory_1 = entity_factory_1_1;
            }],
        execute: function() {
            EntityManager = (function () {
                function EntityManager(rest) {
                    this.rest = rest;
                    this._repositories = new Map();
                    this._factories = new Map();
                    this._types = new Map();
                    this._services = new Map();
                    this._tracked = [];
                    this._tracker = new change_tracker_1.ChangeTracker();
                    for (var _i = 0, _a = entity_type_decorator_1.EntityTypeRegistry.types; _i < _a.length; _i++) {
                        var type = _a[_i];
                        this._types.set(type['name'], type);
                    }
                }
                EntityManager.prototype.getRepository = function (type) {
                    var repository = this._repositories.get(type);
                    if (!repository) {
                        repository = new entity_repository_1.EntityRepository();
                        this._repositories.set(type, repository);
                    }
                    return repository;
                };
                EntityManager.prototype.getFactory = function (type) {
                    var factory = this._factories.get(type);
                    if (!factory) {
                        var factoryClass = entity_factory_1.FactoryRegistry.getClass(type);
                        factory = new factoryClass(this);
                        this._factories.set(type, factory);
                    }
                    return factory;
                };
                EntityManager.prototype.getType = function (name) {
                    return this._types.get(name);
                };
                // @todo -?complete flag
                // @todo -?parent
                EntityManager.prototype.create = function (type, parent, params, complete) {
                    var _this = this;
                    if (complete === void 0) { complete = true; }
                    return new Promise(function (resolve, reject) {
                        if (!parent && !_this._services.has(type)) {
                            return reject(sprintf('Attempt to create `%s` as a root entity prior to service initialization', type['name']));
                        }
                        var service = parent
                            ? parent.service
                            : _this._services.get(type);
                        var factory = _this.getFactory(type);
                        factory.create(complete, params)
                            .then(function (entity) {
                            _this.attach(entity, service)
                                .then(function (entity) {
                                entity.id = entity.ielement['id'];
                                resolve(entity);
                            })
                                .catch(function (error) { return reject(error); });
                        })
                            .catch(function (error) { return reject(error); });
                    });
                };
                EntityManager.prototype.all = function (type, entity) {
                    var _this = this;
                    var route = type.ENTITY_ROUTE;
                    //noinspection TypeScriptValidateTypes
                    return new Promise(function (resolve, reject) {
                        _this.rest.all(route, entity ? entity.ielement : null)
                            .then(function (result) {
                            // @todo -direct services access
                            // @todo +move to rest service
                            if (!_this._services.has(type)) {
                                _this._services.set(type, new restangular_service_2.IPostCollection(result));
                            }
                            // @todo +repository cache
                            var collection = new indexed_collection_1.IndexedCollection();
                            var factory = _this.getFactory(type);
                            result.forEach(function (entry) {
                                var entity = factory.fromIElement(entry);
                                collection.push(entity);
                                entity.observe(_this.changed.bind(_this));
                            });
                            resolve(collection);
                        })
                            .catch(function (error) { return reject(error); });
                    });
                };
                EntityManager.prototype.persist = function () {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        if (_this._tracker.queue.length == 0) {
                            resolve([]);
                        }
                        var entity;
                        while (entity = _this._tracker.shift()) {
                            entity.serialize();
                            _this.rest.save(entity.ielement)
                                .then(function (result) {
                                // @todo
                                resolve(result);
                            })
                                .catch(function (error) {
                                console.error(error);
                                // @todo return reject(error);
                                reject(error);
                            });
                        }
                        // resolve();
                    });
                };
                EntityManager.prototype.expand = function (type, target) {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        var contracts = entity_2.ModelEntity.getContracts(type);
                        var fulfilled = 0;
                        contracts.forEach(function (contract) {
                            var delegate = contract.bind(target, _this);
                            delegate.transfer(_this.all.bind(_this))
                                .then(function (result) {
                                contract.fulfill(result, delegate);
                                delegate.expand(_this.expand.bind(_this), result)
                                    .then(function (result) {
                                    if (++fulfilled == contracts.length) {
                                        resolve(target);
                                    }
                                })
                                    .catch(function (error) {
                                    console.error(error);
                                    reject(error);
                                });
                            })
                                .catch(function (error) {
                                console.error(error);
                                reject(error);
                            });
                        });
                    });
                };
                EntityManager.prototype.remove = function (entity) {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        _this.rest.remove(entity.ielement)
                            .then(function () { return resolve(entity); })
                            .catch(function (error) { return reject(error); });
                    });
                };
                // @todo no post/save assumed
                EntityManager.prototype.attach = function (entity, service) {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        service.post(entity)
                            .then(function (element) {
                            entity.traverse(_this.attach.bind(_this))
                                .then(function () {
                                entity.observe(_this.changed.bind(_this));
                                resolve(entity);
                            })
                                .catch(function (error) { return reject(error); });
                        })
                            .catch(function (error) { return reject(error); });
                    });
                };
                EntityManager.prototype.changed = function (changes) {
                    var _this = this;
                    changes.forEach(function (entry) {
                        if (entity_2.ModelEntity.isObserved(entry.object, entry.name)) {
                            //this._tracked.push(entry.object);
                            _this._tracker.track(entry.object);
                        }
                    });
                };
                EntityManager = __decorate([
                    core_5.Injectable(), 
                    __metadata('design:paramtypes', [restangular_service_1.RestangularService])
                ], EntityManager);
                return EntityManager;
            }());
            exports_13("EntityManager", EntityManager);
        }
    }
});
System.register("app/em/expandable.decorator", ["app/decorator"], function(exports_14, context_14) {
    "use strict";
    var __moduleName = context_14 && context_14.id;
    var decorator_3;
    var EntityExpandable, ExpandableFactory, ExpandableMetadata, Expandable;
    return {
        setters:[
            function (decorator_3_1) {
                decorator_3 = decorator_3_1;
            }],
        execute: function() {
            EntityExpandable = (function () {
                function EntityExpandable() {
                }
                return EntityExpandable;
            }());
            exports_14("EntityExpandable", EntityExpandable);
            ExpandableFactory = (function () {
                function ExpandableFactory() {
                }
                ExpandableFactory.create = function (meta) {
                    return new EntityExpandable();
                };
                return ExpandableFactory;
            }());
            ExpandableMetadata = (function () {
                function ExpandableMetadata() {
                }
                ExpandableMetadata.prototype.decorateTarget = function (targetClass) {
                    this.targetClass = targetClass;
                    this.expandable = ExpandableFactory.create(this);
                };
                return ExpandableMetadata;
            }());
            exports_14("ExpandableMetadata", ExpandableMetadata);
            /**
             * @deprecated
             */
            exports_14("Expandable", Expandable = decorator_3.makeTypeDecorator(ExpandableMetadata));
        }
    }
});
System.register("app/em/entity", ["app/em/entity-factory", "app/em/restangular.service"], function(exports_15, context_15) {
    "use strict";
    var __moduleName = context_15 && context_15.id;
    var entity_factory_2, restangular_service_3, entity_factory_3;
    var ModelEntity, EntityMetadata, EntityDecorator, Entity;
    return {
        setters:[
            function (entity_factory_2_1) {
                entity_factory_2 = entity_factory_2_1;
                entity_factory_3 = entity_factory_2_1;
            },
            function (restangular_service_3_1) {
                restangular_service_3 = restangular_service_3_1;
            }],
        execute: function() {
            ModelEntity = (function () {
                function ModelEntity() {
                    // @todo smart?
                    this.expanded = false;
                    this._locked = false;
                }
                Object.defineProperty(ModelEntity.prototype, "em", {
                    set: function (em) {
                        this._em = em;
                    },
                    enumerable: true,
                    configurable: true
                });
                ModelEntity.getContracts = function (type) {
                    var reflect = Reflect['getMetadata']('propMetadata', type);
                    var result = [];
                    for (var key in reflect) {
                        //noinspection JSUnfilteredForInLoop
                        var list = reflect[key];
                        for (var _i = 0, list_1 = list; _i < list_1.length; _i++) {
                            var meta = list_1[_i];
                            // @todo smart
                            if (meta.contract) {
                                result.push(meta.contract);
                            }
                        }
                    }
                    return result;
                };
                ModelEntity.isObserved = function (entity, name) {
                    var reflect = Reflect['getMetadata']('propMetadata', entity.constructor);
                    if (!reflect || !reflect[name]) {
                        return false;
                    }
                    for (var _i = 0, _a = reflect[name]; _i < _a.length; _i++) {
                        var meta = _a[_i];
                        if (meta.persist) {
                            return true;
                        }
                    }
                    return false;
                };
                ModelEntity.isExpandable = function (entity) {
                    var reflect = Reflect['getMetadata']('annotations', entity.constructor);
                    if (!reflect) {
                        return false;
                    }
                    for (var _i = 0, reflect_1 = reflect; _i < reflect_1.length; _i++) {
                        var meta = reflect_1[_i];
                        if (meta.constructor['name'] == 'ExpandableMetadata') {
                            return true;
                        }
                    }
                    return false;
                };
                ModelEntity.prototype.lock = function () {
                    this._locked = true;
                };
                ModelEntity.prototype.unlock = function () {
                    this._locked = false;
                };
                Object.defineProperty(ModelEntity.prototype, "locked", {
                    get: function () {
                        return this._locked;
                    },
                    enumerable: true,
                    configurable: true
                });
                // @todo replace with `is expandable` check (descendant's decorator?)
                ModelEntity.prototype.expand = function () {
                    var _this = this;
                    return new Promise(function (resolve) {
                        resolve(_this);
                    });
                };
                ModelEntity.prototype.observe = function (observer) {
                    this._shrinkIElement();
                    // @todo replace with defineProperty
                    Object['observe'](this, observer);
                };
                ModelEntity.prototype.serialize = function () {
                    for (var prop in this.plain()) {
                        if (prop != 'id') {
                            //noinspection JSUnfilteredForInLoop
                            this._ielement[prop] = this[prop];
                        }
                    }
                };
                ModelEntity.prototype.plain = function () {
                    var plain = {};
                    for (var prop in this) {
                        if (!this.hasOwnProperty(prop)) {
                            continue;
                        }
                        if (ModelEntity.isObserved(this, prop)) {
                            plain[prop] = this[prop];
                        }
                    }
                    return plain;
                };
                // @todo -callback / +promise interface
                // @todo +contracts to generalize traversal
                ModelEntity.prototype.traverse = function (callback) {
                    var _this = this;
                    return new Promise(function (resolve) {
                        resolve(_this);
                    });
                };
                // @todo
                ModelEntity.prototype._shrinkIElement = function () {
                    for (var prop in this._ielement.plain()) {
                        //noinspection JSUnfilteredForInLoop
                        if (prop != 'id' && !ModelEntity.isObserved(this, prop)) {
                            //noinspection JSUnfilteredForInLoop
                            delete this._ielement[prop];
                        }
                    }
                };
                Object.defineProperty(ModelEntity.prototype, "ielement", {
                    get: function () {
                        return this._ielement;
                    },
                    // @todo hide / encapsulate
                    set: function (ielement) {
                        this._ielement = ielement;
                        this._iservice = new restangular_service_3.IPostElement(this._ielement);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ModelEntity.prototype, "service", {
                    get: function () {
                        return this._iservice;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ModelEntity.prototype, "base", {
                    get: function () {
                        return this._ielement['getParentList']
                            ? this._ielement['getParentList']()
                            : undefined;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ModelEntity.prototype, "route", {
                    get: function () {
                        return undefined;
                    },
                    enumerable: true,
                    configurable: true
                });
                ModelEntity.prototype.doExpand = function (type, target) {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        if (_this.expanded) {
                            return reject('Entity ' + type['name'] + ' already expanded');
                        }
                        if (_this.locked) {
                            return reject('Entity ' + type['name'] + ' is locked');
                        }
                        _this.lock();
                        _this._em.expand(type, target)
                            .then(function (result) {
                            _this.expanded = true;
                            _this.unlock();
                            resolve(result);
                        })
                            .catch(function (error) { return reject(error); });
                    });
                };
                ModelEntity.prototype.doTraverse = function (tasks, callback) {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        var done = 0;
                        for (var _i = 0, tasks_1 = tasks; _i < tasks_1.length; _i++) {
                            var collection = tasks_1[_i];
                            if (collection.length == 0) {
                                if (++done == tasks.length)
                                    resolve(_this);
                                continue;
                            }
                            _this._doTraverseCollection(collection, callback)
                                .then(function () {
                                if (++done == tasks.length)
                                    resolve(_this);
                            })
                                .catch(function (error) { return reject(error); });
                        }
                    });
                };
                ModelEntity.prototype._doTraverseCollection = function (collection, callback) {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        var done = 0;
                        collection.forEach(function (entity) {
                            callback(entity, _this.service)
                                .then(function () {
                                if (++done == collection.length)
                                    resolve();
                            })
                                .catch(function (error) { return reject(error); });
                        });
                    });
                };
                Object.defineProperty(ModelEntity, "FACTORY_CLASS", {
                    /**
                     * @deprecated
                     */
                    get: function () {
                        return entity_factory_2.AbstractEntityFactory;
                    },
                    enumerable: true,
                    configurable: true
                });
                ;
                Object.defineProperty(ModelEntity, "ENTITY_ROUTE", {
                    get: function () {
                        return undefined;
                    },
                    enumerable: true,
                    configurable: true
                });
                ;
                return ModelEntity;
            }());
            exports_15("ModelEntity", ModelEntity);
            EntityMetadata = (function () {
                function EntityMetadata(type, factoryClass) {
                    this.type = type;
                    this.factoryClass = factoryClass;
                }
                return EntityMetadata;
            }());
            EntityDecorator = (function () {
                function EntityDecorator(metadata) {
                    this.metadata = metadata;
                }
                EntityDecorator.prototype.decorate = function () {
                    // register entity type and its factory class
                    entity_factory_3.FactoryRegistry.register(this.metadata.type, this.metadata.factoryClass);
                };
                return EntityDecorator;
            }());
            exports_15("Entity", Entity = function (factoryClass) {
                return function (target) {
                    var metadata = new EntityMetadata(target, factoryClass);
                    var decorator = new EntityDecorator(metadata);
                    decorator.decorate();
                };
            });
        }
    }
});
System.register("app/model/variation.factory", ["app/em/entity-factory", "app/model/variation"], function(exports_16, context_16) {
    "use strict";
    var __moduleName = context_16 && context_16.id;
    var entity_factory_4, variation_1;
    var VariationFactory;
    return {
        setters:[
            function (entity_factory_4_1) {
                entity_factory_4 = entity_factory_4_1;
            },
            function (variation_1_1) {
                variation_1 = variation_1_1;
            }],
        execute: function() {
            //noinspection JSUnusedGlobalSymbols
            VariationFactory = (function (_super) {
                __extends(VariationFactory, _super);
                function VariationFactory() {
                    _super.apply(this, arguments);
                }
                VariationFactory.prototype.create = function (complete, params) {
                    var _this = this;
                    if (complete === void 0) { complete = false; }
                    var variation = new variation_1.Variation();
                    for (var prop in params) {
                        if (params.hasOwnProperty(prop)) {
                            variation[prop] = params[prop];
                        }
                    }
                    return new Promise(function (resolve, reject) {
                        _this.em.persist()
                            .then(function () {
                            resolve(variation);
                        })
                            .catch(function (error) {
                            reject(error);
                        });
                    });
                };
                VariationFactory.prototype.fromIElement = function (element) {
                    var variation = new variation_1.Variation();
                    variation.number = element['number'];
                    variation.title = element['title'];
                    variation.weight = element['weight'];
                    variation.state = element['state'];
                    variation.weightLocked = (variation.state & variation_1.Variation.states.WEIGHT_LOCKED) != 0;
                    variation.em = this.em;
                    variation.ielement = element;
                    return variation;
                };
                return VariationFactory;
            }(entity_factory_4.AbstractEntityFactory));
            exports_16("VariationFactory", VariationFactory);
        }
    }
});
System.register("app/model/snippet-value.factory", ["app/em/entity-factory", "app/model/snippet-value"], function(exports_17, context_17) {
    "use strict";
    var __moduleName = context_17 && context_17.id;
    var entity_factory_5, snippet_value_1;
    var SnippetValueFactory;
    return {
        setters:[
            function (entity_factory_5_1) {
                entity_factory_5 = entity_factory_5_1;
            },
            function (snippet_value_1_1) {
                snippet_value_1 = snippet_value_1_1;
            }],
        execute: function() {
            //noinspection JSUnusedGlobalSymbols
            SnippetValueFactory = (function (_super) {
                __extends(SnippetValueFactory, _super);
                function SnippetValueFactory() {
                    _super.apply(this, arguments);
                }
                SnippetValueFactory.prototype.create = function (complete, params) {
                    var _this = this;
                    if (complete === void 0) { complete = false; }
                    var value = new snippet_value_1.SnippetValue();
                    // @todo -
                    if (params && (params['variation'] !== undefined)) {
                        value.variation = params['variation'];
                    }
                    return new Promise(function (resolve, reject) {
                        _this.em.persist()
                            .then(function () {
                            resolve(value);
                        })
                            .catch(function (error) {
                            reject(error);
                        });
                    });
                };
                SnippetValueFactory.prototype.fromIElement = function (element) {
                    var value = new snippet_value_1.SnippetValue();
                    value.value = element['value'];
                    value.em = this.em;
                    value.ielement = element;
                    return value;
                };
                return SnippetValueFactory;
            }(entity_factory_5.AbstractEntityFactory));
            exports_17("SnippetValueFactory", SnippetValueFactory);
        }
    }
});
System.register("app/em/persist.decorator", ["app/decorator"], function(exports_18, context_18) {
    "use strict";
    var __moduleName = context_18 && context_18.id;
    var decorator_4;
    var PropertyPersist, PersistFactory, PersistMetadata, Persist;
    return {
        setters:[
            function (decorator_4_1) {
                decorator_4 = decorator_4_1;
            }],
        execute: function() {
            PropertyPersist = (function () {
                function PropertyPersist(propertyName) {
                    this.propertyName = propertyName;
                }
                return PropertyPersist;
            }());
            exports_18("PropertyPersist", PropertyPersist);
            PersistFactory = (function () {
                function PersistFactory() {
                }
                PersistFactory.create = function (meta) {
                    return new PropertyPersist(meta.propertyName);
                };
                return PersistFactory;
            }());
            PersistMetadata = (function () {
                function PersistMetadata() {
                }
                PersistMetadata.prototype.decorateTarget = function (targetClass, propertyName) {
                    this.targetClass = targetClass;
                    this.propertyName = propertyName;
                    this.persist = PersistFactory.create(this);
                };
                return PersistMetadata;
            }());
            exports_18("PersistMetadata", PersistMetadata);
            exports_18("Persist", Persist = decorator_4.makePropDecorator(PersistMetadata));
        }
    }
});
System.register("app/model/snippet-value", ["app/em/entity", "app/model/snippet-value.factory", "app/em/persist.decorator"], function(exports_19, context_19) {
    "use strict";
    var __moduleName = context_19 && context_19.id;
    var entity_3, snippet_value_factory_1, persist_decorator_1;
    var SnippetValue;
    return {
        setters:[
            function (entity_3_1) {
                entity_3 = entity_3_1;
            },
            function (snippet_value_factory_1_1) {
                snippet_value_factory_1 = snippet_value_factory_1_1;
            },
            function (persist_decorator_1_1) {
                persist_decorator_1 = persist_decorator_1_1;
            }],
        execute: function() {
            SnippetValue = (function (_super) {
                __extends(SnippetValue, _super);
                function SnippetValue() {
                    _super.call(this);
                }
                Object.defineProperty(SnippetValue.prototype, "route", {
                    get: function () {
                        return SnippetValue.ENTITY_ROUTE;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(SnippetValue, "ENTITY_ROUTE", {
                    get: function () {
                        return 'values';
                    },
                    enumerable: true,
                    configurable: true
                });
                __decorate([
                    persist_decorator_1.Persist(), 
                    __metadata('design:type', String)
                ], SnippetValue.prototype, "value", void 0);
                __decorate([
                    persist_decorator_1.Persist(), 
                    __metadata('design:type', Number)
                ], SnippetValue.prototype, "variation", void 0);
                SnippetValue = __decorate([
                    entity_3.Entity(snippet_value_factory_1.SnippetValueFactory), 
                    __metadata('design:paramtypes', [])
                ], SnippetValue);
                return SnippetValue;
            }(entity_3.ModelEntity));
            exports_19("SnippetValue", SnippetValue);
        }
    }
});
System.register("app/model/variation", ["app/em/entity", "app/model/variation.factory", "app/em/indexed-collection", "app/em/persist.decorator"], function(exports_20, context_20) {
    "use strict";
    var __moduleName = context_20 && context_20.id;
    var entity_4, variation_factory_1, indexed_collection_2, persist_decorator_2;
    var Variation;
    return {
        setters:[
            function (entity_4_1) {
                entity_4 = entity_4_1;
            },
            function (variation_factory_1_1) {
                variation_factory_1 = variation_factory_1_1;
            },
            function (indexed_collection_2_1) {
                indexed_collection_2 = indexed_collection_2_1;
            },
            function (persist_decorator_2_1) {
                persist_decorator_2 = persist_decorator_2_1;
            }],
        execute: function() {
            Variation = (function (_super) {
                __extends(Variation, _super);
                function Variation() {
                    _super.call(this);
                    this.number = 0;
                    this.title = '';
                    this.weight = 0;
                    this.state = 0;
                    this.weightDisabled = false;
                    this.snippetValues = new indexed_collection_2.IndexedCollection();
                }
                Object.defineProperty(Variation.prototype, "weightLocked", {
                    get: function () {
                        return this._weightLocked;
                    },
                    set: function (value) {
                        if (value == this._weightLocked) {
                            return;
                        }
                        if (value == true) {
                            this.state |= Variation.states.WEIGHT_LOCKED;
                        }
                        else {
                            this.state = this.state & ~Variation.states.WEIGHT_LOCKED;
                        }
                        this._weightLocked = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Variation.prototype, "route", {
                    get: function () {
                        return Variation.ENTITY_ROUTE;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Variation, "ENTITY_ROUTE", {
                    get: function () {
                        return 'variations';
                    },
                    enumerable: true,
                    configurable: true
                });
                Variation.states = {
                    WEIGHT_LOCKED: 0x1,
                    WEIGHT_DISABLED: 0x2
                };
                __decorate([
                    persist_decorator_2.Persist(), 
                    __metadata('design:type', Number)
                ], Variation.prototype, "number", void 0);
                __decorate([
                    persist_decorator_2.Persist(), 
                    __metadata('design:type', String)
                ], Variation.prototype, "title", void 0);
                __decorate([
                    persist_decorator_2.Persist(), 
                    __metadata('design:type', Number)
                ], Variation.prototype, "weight", void 0);
                __decorate([
                    persist_decorator_2.Persist(), 
                    __metadata('design:type', Number)
                ], Variation.prototype, "state", void 0);
                Variation = __decorate([
                    entity_4.Entity(variation_factory_1.VariationFactory), 
                    __metadata('design:paramtypes', [])
                ], Variation);
                return Variation;
            }(entity_4.ModelEntity));
            exports_20("Variation", Variation);
        }
    }
});
System.register("app/model/experiment-url.factory", ["app/em/entity-factory", "app/model/experiment-url"], function(exports_21, context_21) {
    "use strict";
    var __moduleName = context_21 && context_21.id;
    var entity_factory_6, experiment_url_1;
    var ExperimentUrlFactory;
    return {
        setters:[
            function (entity_factory_6_1) {
                entity_factory_6 = entity_factory_6_1;
            },
            function (experiment_url_1_1) {
                experiment_url_1 = experiment_url_1_1;
            }],
        execute: function() {
            //noinspection JSUnusedGlobalSymbols
            ExperimentUrlFactory = (function (_super) {
                __extends(ExperimentUrlFactory, _super);
                function ExperimentUrlFactory() {
                    _super.apply(this, arguments);
                }
                ExperimentUrlFactory.prototype.create = function (complete, params) {
                    var _this = this;
                    if (complete === void 0) { complete = false; }
                    var url = new experiment_url_1.ExperimentUrl();
                    url.pattern = '';
                    return new Promise(function (resolve, reject) {
                        _this.em.persist()
                            .then(function () {
                            resolve(url);
                        })
                            .catch(function (error) {
                            reject(error);
                        });
                    });
                };
                ExperimentUrlFactory.prototype.fromIElement = function (element) {
                    var url = new experiment_url_1.ExperimentUrl();
                    url.pattern = element['pattern'];
                    url.em = this.em;
                    url.ielement = element;
                    return url;
                };
                return ExperimentUrlFactory;
            }(entity_factory_6.AbstractEntityFactory));
            exports_21("ExperimentUrlFactory", ExperimentUrlFactory);
        }
    }
});
System.register("app/model/experiment-url", ["app/em/entity", "app/model/experiment-url.factory", "app/em/persist.decorator"], function(exports_22, context_22) {
    "use strict";
    var __moduleName = context_22 && context_22.id;
    var entity_5, experiment_url_factory_1, persist_decorator_3;
    var ExperimentUrl;
    return {
        setters:[
            function (entity_5_1) {
                entity_5 = entity_5_1;
            },
            function (experiment_url_factory_1_1) {
                experiment_url_factory_1 = experiment_url_factory_1_1;
            },
            function (persist_decorator_3_1) {
                persist_decorator_3 = persist_decorator_3_1;
            }],
        execute: function() {
            ExperimentUrl = (function (_super) {
                __extends(ExperimentUrl, _super);
                function ExperimentUrl() {
                    _super.call(this);
                }
                Object.defineProperty(ExperimentUrl.prototype, "route", {
                    get: function () {
                        return ExperimentUrl.ENTITY_ROUTE;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ExperimentUrl, "ENTITY_ROUTE", {
                    get: function () {
                        return 'urls';
                    },
                    enumerable: true,
                    configurable: true
                });
                __decorate([
                    persist_decorator_3.Persist(), 
                    __metadata('design:type', String)
                ], ExperimentUrl.prototype, "pattern", void 0);
                ExperimentUrl = __decorate([
                    entity_5.Entity(experiment_url_factory_1.ExperimentUrlFactory), 
                    __metadata('design:paramtypes', [])
                ], ExperimentUrl);
                return ExperimentUrl;
            }(entity_5.ModelEntity));
            exports_22("ExperimentUrl", ExperimentUrl);
        }
    }
});
System.register("app/model/variation-snippet.factory", ["app/em/entity-factory", "app/model/variation-snippet", "app/model/snippet-value"], function(exports_23, context_23) {
    "use strict";
    var __moduleName = context_23 && context_23.id;
    var entity_factory_7, variation_snippet_1, snippet_value_2;
    var VariationSnippetFactory;
    return {
        setters:[
            function (entity_factory_7_1) {
                entity_factory_7 = entity_factory_7_1;
            },
            function (variation_snippet_1_1) {
                variation_snippet_1 = variation_snippet_1_1;
            },
            function (snippet_value_2_1) {
                snippet_value_2 = snippet_value_2_1;
            }],
        execute: function() {
            //noinspection JSUnusedGlobalSymbols
            VariationSnippetFactory = (function (_super) {
                __extends(VariationSnippetFactory, _super);
                function VariationSnippetFactory() {
                    _super.apply(this, arguments);
                }
                VariationSnippetFactory.prototype.create = function (complete, params) {
                    var _this = this;
                    if (complete === void 0) { complete = false; }
                    var snippet = new variation_snippet_1.VariationSnippet();
                    snippet.title = 'New Snippet';
                    if (params && (params['number'] !== undefined)) {
                        snippet.number = params['number'];
                    }
                    return new Promise(function (resolve, reject) {
                        if (complete) {
                            _this.complete(snippet)
                                .then(function () {
                                _this.em.persist()
                                    .then(function () {
                                    resolve(snippet);
                                })
                                    .catch(function (error) {
                                    reject(error);
                                });
                            })
                                .catch(function (error) { return reject(error); });
                        }
                        else {
                            _this.em.persist()
                                .then(function () {
                                resolve(snippet);
                            })
                                .catch(function (error) {
                                reject(error);
                            });
                        }
                    });
                };
                VariationSnippetFactory.prototype.complete = function (entity) {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        _this.em.getFactory(snippet_value_2.SnippetValue)
                            .create(true)
                            .then(function (value) {
                            entity.values.push(value);
                            resolve(entity);
                        })
                            .catch(function (error) {
                            reject(error);
                        });
                    });
                };
                VariationSnippetFactory.prototype.fromIElement = function (element) {
                    var snippet = new variation_snippet_1.VariationSnippet();
                    snippet.number = element['number'];
                    snippet.title = element['title'];
                    snippet.em = this.em;
                    snippet.ielement = element;
                    return snippet;
                };
                return VariationSnippetFactory;
            }(entity_factory_7.AbstractEntityFactory));
            exports_23("VariationSnippetFactory", VariationSnippetFactory);
        }
    }
});
System.register("app/model/variation-snippet", ["app/em/entity", "app/em/indexed-collection", "app/model/variation-snippet.factory", "app/model/snippet-value", "app/em/contract.decorator", "app/em/expandable.decorator", "app/em/persist.decorator"], function(exports_24, context_24) {
    "use strict";
    var __moduleName = context_24 && context_24.id;
    var entity_6, indexed_collection_3, variation_snippet_factory_1, snippet_value_3, contract_decorator_1, expandable_decorator_1, persist_decorator_4;
    var VariationSnippet;
    return {
        setters:[
            function (entity_6_1) {
                entity_6 = entity_6_1;
            },
            function (indexed_collection_3_1) {
                indexed_collection_3 = indexed_collection_3_1;
            },
            function (variation_snippet_factory_1_1) {
                variation_snippet_factory_1 = variation_snippet_factory_1_1;
            },
            function (snippet_value_3_1) {
                snippet_value_3 = snippet_value_3_1;
            },
            function (contract_decorator_1_1) {
                contract_decorator_1 = contract_decorator_1_1;
            },
            function (expandable_decorator_1_1) {
                expandable_decorator_1 = expandable_decorator_1_1;
            },
            function (persist_decorator_4_1) {
                persist_decorator_4 = persist_decorator_4_1;
            }],
        execute: function() {
            VariationSnippet = (function (_super) {
                __extends(VariationSnippet, _super);
                function VariationSnippet() {
                    _super.call(this);
                    this.number = 0;
                    this.title = '';
                    this.values = new indexed_collection_3.IndexedCollection();
                }
                VariationSnippet.prototype.traverse = function (callback) {
                    return this.doTraverse([this.values], callback);
                };
                Object.defineProperty(VariationSnippet.prototype, "route", {
                    get: function () {
                        return VariationSnippet.ENTITY_ROUTE;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(VariationSnippet, "ENTITY_ROUTE", {
                    get: function () {
                        return 'snippets';
                    },
                    enumerable: true,
                    configurable: true
                });
                VariationSnippet.prototype.expand = function () {
                    return this.doExpand(VariationSnippet, this);
                };
                __decorate([
                    persist_decorator_4.Persist(), 
                    __metadata('design:type', Number)
                ], VariationSnippet.prototype, "number", void 0);
                __decorate([
                    persist_decorator_4.Persist(), 
                    __metadata('design:type', String)
                ], VariationSnippet.prototype, "title", void 0);
                __decorate([
                    contract_decorator_1.Contract(snippet_value_3.SnippetValue), 
                    __metadata('design:type', indexed_collection_3.IndexedCollection)
                ], VariationSnippet.prototype, "values", void 0);
                VariationSnippet = __decorate([
                    expandable_decorator_1.Expandable(),
                    entity_6.Entity(variation_snippet_factory_1.VariationSnippetFactory), 
                    __metadata('design:paramtypes', [])
                ], VariationSnippet);
                return VariationSnippet;
            }(entity_6.ModelEntity));
            exports_24("VariationSnippet", VariationSnippet);
        }
    }
});
System.register("app/model/goal.factory", ["app/em/entity-factory", "app/model/goal"], function(exports_25, context_25) {
    "use strict";
    var __moduleName = context_25 && context_25.id;
    var entity_factory_8, goal_1;
    var GoalFactory;
    return {
        setters:[
            function (entity_factory_8_1) {
                entity_factory_8 = entity_factory_8_1;
            },
            function (goal_1_1) {
                goal_1 = goal_1_1;
            }],
        execute: function() {
            //noinspection JSUnusedGlobalSymbols
            GoalFactory = (function (_super) {
                __extends(GoalFactory, _super);
                function GoalFactory() {
                    _super.apply(this, arguments);
                }
                GoalFactory.prototype.create = function (complete, params) {
                    var _this = this;
                    if (complete === void 0) { complete = false; }
                    var goal = new goal_1.Goal();
                    return new Promise(function (resolve, reject) {
                        _this.em.persist()
                            .then(function () {
                            resolve(goal);
                        })
                            .catch(function (error) {
                            reject(error);
                        });
                    });
                };
                GoalFactory.prototype.fromIElement = function (element) {
                    var goal = new goal_1.Goal();
                    goal.title = element['title'];
                    goal.em = this.em;
                    goal.ielement = element;
                    return goal;
                };
                return GoalFactory;
            }(entity_factory_8.AbstractEntityFactory));
            exports_25("GoalFactory", GoalFactory);
        }
    }
});
System.register("app/model/goal-destination.factory", ["app/em/entity-factory", "app/model/goal-destination"], function(exports_26, context_26) {
    "use strict";
    var __moduleName = context_26 && context_26.id;
    var entity_factory_9, goal_destination_1;
    var GoalDestinationFactory;
    return {
        setters:[
            function (entity_factory_9_1) {
                entity_factory_9 = entity_factory_9_1;
            },
            function (goal_destination_1_1) {
                goal_destination_1 = goal_destination_1_1;
            }],
        execute: function() {
            GoalDestinationFactory = (function (_super) {
                __extends(GoalDestinationFactory, _super);
                function GoalDestinationFactory() {
                    _super.apply(this, arguments);
                }
                GoalDestinationFactory.prototype.create = function (complete, params) {
                    var _this = this;
                    if (complete === void 0) { complete = false; }
                    var destination = new goal_destination_1.GoalDestination();
                    return new Promise(function (resolve, reject) {
                        _this.em.persist()
                            .then(function () {
                            resolve(destination);
                        })
                            .catch(function (error) {
                            reject(error);
                        });
                    });
                };
                GoalDestinationFactory.prototype.fromIElement = function (element) {
                    var destination = new goal_destination_1.GoalDestination();
                    destination.value = element['value'];
                    destination.em = this.em;
                    destination.ielement = element;
                    return destination;
                };
                return GoalDestinationFactory;
            }(entity_factory_9.AbstractEntityFactory));
            exports_26("GoalDestinationFactory", GoalDestinationFactory);
        }
    }
});
System.register("app/model/goal-destination", ["app/em/entity", "app/em/persist.decorator", "app/model/goal-destination.factory"], function(exports_27, context_27) {
    "use strict";
    var __moduleName = context_27 && context_27.id;
    var entity_7, persist_decorator_5, goal_destination_factory_1;
    var GoalDestination;
    return {
        setters:[
            function (entity_7_1) {
                entity_7 = entity_7_1;
            },
            function (persist_decorator_5_1) {
                persist_decorator_5 = persist_decorator_5_1;
            },
            function (goal_destination_factory_1_1) {
                goal_destination_factory_1 = goal_destination_factory_1_1;
            }],
        execute: function() {
            GoalDestination = (function (_super) {
                __extends(GoalDestination, _super);
                function GoalDestination() {
                    _super.call(this);
                }
                Object.defineProperty(GoalDestination.prototype, "route", {
                    get: function () {
                        return GoalDestination.ENTITY_ROUTE;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(GoalDestination, "ENTITY_ROUTE", {
                    get: function () {
                        return 'destinations';
                    },
                    enumerable: true,
                    configurable: true
                });
                __decorate([
                    persist_decorator_5.Persist(), 
                    __metadata('design:type', String)
                ], GoalDestination.prototype, "value", void 0);
                GoalDestination = __decorate([
                    entity_7.Entity(goal_destination_factory_1.GoalDestinationFactory), 
                    __metadata('design:paramtypes', [])
                ], GoalDestination);
                return GoalDestination;
            }(entity_7.ModelEntity));
            exports_27("GoalDestination", GoalDestination);
        }
    }
});
System.register("app/model/goal", ["app/em/entity", "app/em/indexed-collection", "app/model/goal.factory", "app/model/goal-destination", "app/em/expandable.decorator", "app/em/contract.decorator", "app/em/persist.decorator"], function(exports_28, context_28) {
    "use strict";
    var __moduleName = context_28 && context_28.id;
    var entity_8, indexed_collection_4, goal_factory_1, goal_destination_2, expandable_decorator_2, contract_decorator_2, persist_decorator_6;
    var Goal;
    return {
        setters:[
            function (entity_8_1) {
                entity_8 = entity_8_1;
            },
            function (indexed_collection_4_1) {
                indexed_collection_4 = indexed_collection_4_1;
            },
            function (goal_factory_1_1) {
                goal_factory_1 = goal_factory_1_1;
            },
            function (goal_destination_2_1) {
                goal_destination_2 = goal_destination_2_1;
            },
            function (expandable_decorator_2_1) {
                expandable_decorator_2 = expandable_decorator_2_1;
            },
            function (contract_decorator_2_1) {
                contract_decorator_2 = contract_decorator_2_1;
            },
            function (persist_decorator_6_1) {
                persist_decorator_6 = persist_decorator_6_1;
            }],
        execute: function() {
            Goal = (function (_super) {
                __extends(Goal, _super);
                function Goal() {
                    _super.call(this);
                    this.pattern = '';
                    this.destinations = new indexed_collection_4.IndexedCollection();
                }
                Object.defineProperty(Goal.prototype, "route", {
                    get: function () {
                        return Goal.ENTITY_ROUTE;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Goal, "ENTITY_ROUTE", {
                    get: function () {
                        return 'goals';
                    },
                    enumerable: true,
                    configurable: true
                });
                Goal.prototype.expand = function () {
                    return this.doExpand(Goal, this);
                };
                __decorate([
                    persist_decorator_6.Persist(), 
                    __metadata('design:type', String)
                ], Goal.prototype, "title", void 0);
                __decorate([
                    contract_decorator_2.Contract(goal_destination_2.GoalDestination), 
                    __metadata('design:type', Object)
                ], Goal.prototype, "destinations", void 0);
                Goal = __decorate([
                    expandable_decorator_2.Expandable(),
                    entity_8.Entity(goal_factory_1.GoalFactory), 
                    __metadata('design:paramtypes', [])
                ], Goal);
                return Goal;
            }(entity_8.ModelEntity));
            exports_28("Goal", Goal);
        }
    }
});
System.register("app/model/experiment.factory", ["app/em/entity-factory", "app/model/experiment", "app/model/variation", "app/model/experiment-url", "app/model/variation-snippet", "app/model/goal"], function(exports_29, context_29) {
    "use strict";
    var __moduleName = context_29 && context_29.id;
    var entity_factory_10, experiment_1, variation_2, experiment_url_2, variation_snippet_2, goal_2, entity_factory_11;
    var ExperimentFactory;
    return {
        setters:[
            function (entity_factory_10_1) {
                entity_factory_10 = entity_factory_10_1;
                entity_factory_11 = entity_factory_10_1;
            },
            function (experiment_1_1) {
                experiment_1 = experiment_1_1;
            },
            function (variation_2_1) {
                variation_2 = variation_2_1;
            },
            function (experiment_url_2_1) {
                experiment_url_2 = experiment_url_2_1;
            },
            function (variation_snippet_2_1) {
                variation_snippet_2 = variation_snippet_2_1;
            },
            function (goal_2_1) {
                goal_2 = goal_2_1;
            }],
        execute: function() {
            //noinspection JSUnusedGlobalSymbols
            ExperimentFactory = (function (_super) {
                __extends(ExperimentFactory, _super);
                function ExperimentFactory() {
                    _super.apply(this, arguments);
                }
                ExperimentFactory.prototype.create = function (complete, params) {
                    var _this = this;
                    if (complete === void 0) { complete = false; }
                    var experiment = new experiment_1.Experiment();
                    experiment.title = 'New Experiment';
                    experiment.type = 1;
                    experiment.state = 1;
                    experiment.method = 1;
                    experiment.mode = 2;
                    experiment.em = this.em;
                    return new Promise(function (resolve, reject) {
                        if (complete) {
                            _this.complete(experiment)
                                .then(function () {
                                experiment.expanded = true;
                                //let number = 0;
                                experiment.variations.forEach(function (variation) {
                                    variation.experiment = experiment;
                                    experiment.snippets.items.forEach(function (snippet) {
                                        variation.snippetValues.push(snippet.values.items[variation.number]);
                                    });
                                });
                                resolve(experiment);
                            })
                                .catch(function (error) {
                                reject(error);
                            });
                        }
                        else {
                            resolve(experiment);
                        }
                    });
                };
                // @todo simplify
                ExperimentFactory.prototype.complete = function (entity) {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        var tasks = 0;
                        var done = function () {
                            if (++tasks == 3) {
                                resolve(entity);
                            }
                        };
                        // url
                        _this.em.getFactory(experiment_url_2.ExperimentUrl)
                            .create(true)
                            .then(function (url) {
                            entity.urls.push(url);
                            done();
                        })
                            .catch(function (error) {
                            reject(error);
                        });
                        // variation
                        _this.em.getFactory(variation_2.Variation)
                            .create(true, { title: 'Control Variation', number: 0, weight: 100 })
                            .then(function (variation) {
                            entity.variations.push(variation);
                            done();
                        })
                            .catch(function (error) {
                            reject(error);
                        });
                        // snippet
                        _this.em.getFactory(variation_snippet_2.VariationSnippet)
                            .create(true)
                            .then(function (snippet) {
                            entity.snippets.push(snippet);
                            done();
                        })
                            .catch(function (error) {
                            reject(error);
                        });
                    });
                };
                ExperimentFactory.prototype.fromIElement = function (ielement) {
                    var experiment = new experiment_1.Experiment();
                    experiment.id = ielement['id'];
                    experiment.title = ielement['title'];
                    experiment.type = ielement['type'];
                    experiment.state = ielement['state'];
                    experiment.method = ielement['method'];
                    experiment.mode = ielement['mode'];
                    experiment.startedAt = new Date(ielement['started_at']);
                    for (var _i = 0, _a = ielement['variations']; _i < _a.length; _i++) {
                        var entry = _a[_i];
                        var variation = this.em.getFactory(variation_2.Variation).fromIElement(entry);
                        experiment.variations.push(variation);
                    }
                    for (var _b = 0, _c = ielement['goals']; _b < _c.length; _b++) {
                        var entry = _c[_b];
                        var goal = this.em.getFactory(goal_2.Goal).fromIElement(entry);
                        experiment.goals.push(goal);
                    }
                    experiment.em = this.em;
                    experiment.ielement = ielement;
                    return experiment;
                };
                ExperimentFactory = __decorate([
                    entity_factory_11.EntityFactory(), 
                    __metadata('design:paramtypes', [])
                ], ExperimentFactory);
                return ExperimentFactory;
            }(entity_factory_10.AbstractEntityFactory));
            exports_29("ExperimentFactory", ExperimentFactory);
        }
    }
});
System.register("app/model/experiment", ["app/em/entity", "app/model/experiment.factory", "app/model/variation", "app/em/indexed-collection", "app/model/goal", "app/model/experiment-url", "app/model/variation-snippet", "app/em/expandable.decorator", "app/em/contract.decorator", "app/em/persist.decorator", "app/em/entity-type.decorator"], function(exports_30, context_30) {
    "use strict";
    var __moduleName = context_30 && context_30.id;
    var entity_9, experiment_factory_1, variation_3, indexed_collection_5, goal_3, experiment_url_3, variation_snippet_3, expandable_decorator_3, contract_decorator_3, persist_decorator_7, entity_type_decorator_2;
    var Experiment;
    return {
        setters:[
            function (entity_9_1) {
                entity_9 = entity_9_1;
            },
            function (experiment_factory_1_1) {
                experiment_factory_1 = experiment_factory_1_1;
            },
            function (variation_3_1) {
                variation_3 = variation_3_1;
            },
            function (indexed_collection_5_1) {
                indexed_collection_5 = indexed_collection_5_1;
            },
            function (goal_3_1) {
                goal_3 = goal_3_1;
            },
            function (experiment_url_3_1) {
                experiment_url_3 = experiment_url_3_1;
            },
            function (variation_snippet_3_1) {
                variation_snippet_3 = variation_snippet_3_1;
            },
            function (expandable_decorator_3_1) {
                expandable_decorator_3 = expandable_decorator_3_1;
            },
            function (contract_decorator_3_1) {
                contract_decorator_3 = contract_decorator_3_1;
            },
            function (persist_decorator_7_1) {
                persist_decorator_7 = persist_decorator_7_1;
            },
            function (entity_type_decorator_2_1) {
                entity_type_decorator_2 = entity_type_decorator_2_1;
            }],
        execute: function() {
            Experiment = (function (_super) {
                __extends(Experiment, _super);
                function Experiment() {
                    _super.call(this);
                    this.urls = new indexed_collection_5.IndexedCollection();
                    this.goals = new indexed_collection_5.IndexedCollection();
                    this.variations = new indexed_collection_5.IndexedCollection();
                    this.snippets = new indexed_collection_5.IndexedCollection();
                }
                Experiment.prototype.expand = function () {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        _this.doExpand(Experiment, _this)
                            .then(function (experiment) {
                            experiment.variations.forEach(function (variation) {
                                variation.experiment = experiment;
                                experiment.snippets.items.forEach(function (snippet) {
                                    variation.snippetValues.push(snippet.values.items[variation.number]);
                                });
                            });
                            resolve(experiment);
                        })
                            .catch(function (error) { return reject(error); });
                    });
                };
                Experiment.prototype.traverse = function (callback) {
                    var tasks = [
                        this.urls, this.goals, this.variations, this.snippets
                    ];
                    return this.doTraverse(tasks, callback);
                };
                //noinspection JSUnusedGlobalSymbols
                Experiment.prototype.addVariation = function (variation) {
                    return this.variations.push(variation);
                };
                Object.defineProperty(Experiment.prototype, "route", {
                    get: function () {
                        return Experiment.ENTITY_ROUTE;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Experiment, "ENTITY_ROUTE", {
                    // @todo use class decorator
                    //noinspection JSUnusedGlobalSymbols
                    get: function () {
                        return 'experiments';
                    },
                    enumerable: true,
                    configurable: true
                });
                __decorate([
                    persist_decorator_7.Persist(), 
                    __metadata('design:type', String)
                ], Experiment.prototype, "title", void 0);
                __decorate([
                    persist_decorator_7.Persist(), 
                    __metadata('design:type', Date)
                ], Experiment.prototype, "startedAt", void 0);
                __decorate([
                    persist_decorator_7.Persist(), 
                    __metadata('design:type', Number)
                ], Experiment.prototype, "state", void 0);
                __decorate([
                    contract_decorator_3.Contract(experiment_url_3.ExperimentUrl), 
                    __metadata('design:type', indexed_collection_5.IndexedCollection)
                ], Experiment.prototype, "urls", void 0);
                __decorate([
                    contract_decorator_3.Contract(goal_3.Goal), 
                    __metadata('design:type', indexed_collection_5.IndexedCollection)
                ], Experiment.prototype, "goals", void 0);
                __decorate([
                    contract_decorator_3.Contract(variation_3.Variation), 
                    __metadata('design:type', indexed_collection_5.IndexedCollection)
                ], Experiment.prototype, "variations", void 0);
                __decorate([
                    contract_decorator_3.Contract(variation_snippet_3.VariationSnippet), 
                    __metadata('design:type', indexed_collection_5.IndexedCollection)
                ], Experiment.prototype, "snippets", void 0);
                Experiment = __decorate([
                    entity_type_decorator_2.EntityType(),
                    expandable_decorator_3.Expandable(),
                    entity_9.Entity(experiment_factory_1.ExperimentFactory), 
                    __metadata('design:paramtypes', [])
                ], Experiment);
                return Experiment;
            }(entity_9.ModelEntity));
            exports_30("Experiment", Experiment);
        }
    }
});
System.register("app/em/experiment-stats.service", ['angular2/core', 'angular2/http'], function(exports_31, context_31) {
    "use strict";
    var __moduleName = context_31 && context_31.id;
    var core_6, http_3;
    var ExperimentStatsService;
    return {
        setters:[
            function (core_6_1) {
                core_6 = core_6_1;
            },
            function (http_3_1) {
                http_3 = http_3_1;
            }],
        execute: function() {
            ExperimentStatsService = (function () {
                function ExperimentStatsService(http) {
                    this.http = http;
                    this._stats = {};
                }
                ExperimentStatsService.prototype.experiment = function (id) {
                    var _this = this;
                    if (this._stats[id] != undefined) {
                        return new Promise(function (resolve) {
                            resolve(_this._stats[id]);
                        });
                    }
                    return new Promise(function (resolve, reject) {
                        _this.load(id)
                            .then(function (result) {
                            resolve(result);
                        })
                            .catch(function (reason) {
                            reject(reason);
                        });
                    });
                };
                ExperimentStatsService.prototype.load = function (experiment_id) {
                    var url = '/bundles/stats/variation_date.php';
                    var request = this.http.get(url, { search: 'experiment_id=' + experiment_id + '&_qt=' + Date.now() });
                    return new Promise(function (resolve, reject) {
                        request.subscribe(function (res) {
                            var json = res.json();
                            resolve(res.json());
                        }, function (err) {
                            reject(err.json());
                        });
                    });
                };
                ExperimentStatsService = __decorate([
                    core_6.Injectable(), 
                    __metadata('design:paramtypes', [http_3.Http])
                ], ExperimentStatsService);
                return ExperimentStatsService;
            }());
            exports_31("ExperimentStatsService", ExperimentStatsService);
        }
    }
});
System.register("app/dashboard/dashboard-ab-stats-breakdown.component", ['angular2/core', "app/em/experiment-stats.service", "app/model/experiment"], function(exports_32, context_32) {
    "use strict";
    var __moduleName = context_32 && context_32.id;
    var core_7, experiment_stats_service_1, experiment_2;
    var DashboardAbStatsBreakdownComponent;
    return {
        setters:[
            function (core_7_1) {
                core_7 = core_7_1;
            },
            function (experiment_stats_service_1_1) {
                experiment_stats_service_1 = experiment_stats_service_1_1;
            },
            function (experiment_2_1) {
                experiment_2 = experiment_2_1;
            }],
        execute: function() {
            DashboardAbStatsBreakdownComponent = (function () {
                function DashboardAbStatsBreakdownComponent(service) {
                    this.service = service;
                    this.onInit = new core_7.EventEmitter();
                    this.stats = [];
                    this.opened = false;
                }
                DashboardAbStatsBreakdownComponent.prototype.show = function () {
                    this.opened = true;
                };
                DashboardAbStatsBreakdownComponent.prototype.hide = function () {
                    this.opened = false;
                };
                DashboardAbStatsBreakdownComponent.prototype.toggle = function () {
                    this.opened = !this.opened;
                };
                DashboardAbStatsBreakdownComponent.prototype.ngOnInit = function () {
                    this.onInit.emit(this);
                };
                DashboardAbStatsBreakdownComponent.prototype.showStats = function (data) {
                    var stats = [];
                    this.experiment.variations.items.forEach(function (variation) {
                        stats[variation.number] = {
                            title: variation.title,
                            number: variation.number,
                            hits: 0,
                            visitors: 0,
                            xviews: 0,
                            uxviews: 0,
                            goals: 0,
                            conversion: 0
                        };
                    });
                    data.forEach(function (entry) {
                        var number = entry.variation;
                        stats[number].hits += entry.hits;
                        stats[number].visitors += entry.visitors;
                        stats[number].xviews += entry.xviews;
                        stats[number].uxviews += entry.uxviews;
                        stats[number].goals += entry.goals;
                        stats[number].conversion = (stats[number].uxviews > 0)
                            ? Math.round(stats[number].goals / stats[number].uxviews * 100) : 0;
                    });
                    this.stats = stats;
                };
                __decorate([
                    core_7.Input(), 
                    __metadata('design:type', experiment_2.Experiment)
                ], DashboardAbStatsBreakdownComponent.prototype, "experiment", void 0);
                __decorate([
                    core_7.Output('init'), 
                    __metadata('design:type', core_7.EventEmitter)
                ], DashboardAbStatsBreakdownComponent.prototype, "onInit", void 0);
                DashboardAbStatsBreakdownComponent = __decorate([
                    core_7.Component({
                        selector: '[dashboardAbStatsBreakdown]',
                        templateUrl: 'templates/dashboard/dashboard-ab-stats-breakdown.html'
                    }), 
                    __metadata('design:paramtypes', [experiment_stats_service_1.ExperimentStatsService])
                ], DashboardAbStatsBreakdownComponent);
                return DashboardAbStatsBreakdownComponent;
            }());
            exports_32("DashboardAbStatsBreakdownComponent", DashboardAbStatsBreakdownComponent);
        }
    }
});
System.register("app/dashboard/dashboard-ab-stats-row.component", ['angular2/core', 'angular2/router', "app/model/experiment", "app/em/experiment-stats.service"], function(exports_33, context_33) {
    "use strict";
    var __moduleName = context_33 && context_33.id;
    var core_8, router_1, experiment_3, experiment_stats_service_2;
    var DashboardAbStatsRowComponent;
    return {
        setters:[
            function (core_8_1) {
                core_8 = core_8_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (experiment_3_1) {
                experiment_3 = experiment_3_1;
            },
            function (experiment_stats_service_2_1) {
                experiment_stats_service_2 = experiment_stats_service_2_1;
            }],
        execute: function() {
            DashboardAbStatsRowComponent = (function () {
                function DashboardAbStatsRowComponent(service) {
                    this.service = service;
                    this.onInit = new core_8.EventEmitter();
                    this.onReady = new core_8.EventEmitter();
                    this.onDetails = new core_8.EventEmitter();
                    this.stats = {};
                    this.ready = false;
                    this.onInit.emit(this);
                }
                DashboardAbStatsRowComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.onInit.emit(this);
                    this.service.experiment(this.experiment.id)
                        .then(function (result) {
                        _this.showStats(result);
                        _this.ready = true;
                        _this.onReady.emit(result);
                    });
                };
                DashboardAbStatsRowComponent.prototype.setBreakdown = function (component) {
                    this.breakdown = component;
                };
                DashboardAbStatsRowComponent.prototype.toggleBreakdown = function () {
                    this.breakdown.toggle();
                };
                DashboardAbStatsRowComponent.prototype.details = function () {
                    this.onDetails.emit(this);
                };
                DashboardAbStatsRowComponent.prototype.showStats = function (data) {
                    var stats = {
                        hits: 0,
                        visitors: 0,
                        xviews: 0,
                        uxviews: 0,
                        goals: 0,
                        conversion: 0
                    };
                    data.forEach(function (entry) {
                        stats.hits += entry.hits;
                        stats.visitors += entry.visitors;
                        stats.xviews += entry.xviews;
                        stats.uxviews += entry.uxviews;
                        stats.goals += entry.goals;
                    });
                    stats.conversion = stats.uxviews > 0
                        ? Math.round(stats.goals / stats.uxviews * 100) : 0;
                    this.stats = stats;
                };
                __decorate([
                    core_8.Input(), 
                    __metadata('design:type', experiment_3.Experiment)
                ], DashboardAbStatsRowComponent.prototype, "experiment", void 0);
                __decorate([
                    core_8.Output('init'), 
                    __metadata('design:type', core_8.EventEmitter)
                ], DashboardAbStatsRowComponent.prototype, "onInit", void 0);
                __decorate([
                    core_8.Output('ready'), 
                    __metadata('design:type', core_8.EventEmitter)
                ], DashboardAbStatsRowComponent.prototype, "onReady", void 0);
                __decorate([
                    core_8.Output('details'), 
                    __metadata('design:type', core_8.EventEmitter)
                ], DashboardAbStatsRowComponent.prototype, "onDetails", void 0);
                DashboardAbStatsRowComponent = __decorate([
                    core_8.Component({
                        selector: '[dashboardAbStatsRow]',
                        templateUrl: 'templates/dashboard/dashboard-ab-stats-row.html',
                        providers: [],
                        directives: [
                            router_1.ROUTER_DIRECTIVES
                        ]
                    }), 
                    __metadata('design:paramtypes', [experiment_stats_service_2.ExperimentStatsService])
                ], DashboardAbStatsRowComponent);
                return DashboardAbStatsRowComponent;
            }());
            exports_33("DashboardAbStatsRowComponent", DashboardAbStatsRowComponent);
        }
    }
});
System.register("app/app.messages", [], function(exports_34, context_34) {
    "use strict";
    var __moduleName = context_34 && context_34.id;
    var MESSAGES;
    return {
        setters:[],
        execute: function() {
            exports_34("MESSAGES", MESSAGES = {
                'ab_experiments_list.button_new': {
                    title: 'Create new A/B test experiment',
                    content: "\n        New experiment will be created in inactive state.<br>\n        To activate and run your experiment you will need to add at least one variation,\n        define experiment page URL and experiment goal,\n        and then press <b>\"Run\"</b> button.\n        "
                },
                'ab_experiments_list.running_status.running': {
                    content: "Experiment is running"
                },
                'ab_experiments_list.running_status.paused': {
                    content: "Experiment is paused"
                },
                'ab_experiment_details.add_url_button': {
                    title: 'Add experiment URL',
                    content: "\n        Experiment URL is the page(s) where your experiment will run on. This means that experiment code won't be executed on any other pages.\n        You can have as many page URLs as you need.\n        "
                },
                'ab_experiment_details.delete_url_button': {
                    title: 'Delete this URL',
                    content: "\n        Removes page(s) at this url from the experiment.\n        "
                },
                'ab_experiment_details.cant_delete_last_url': {
                    content: "\n        You can't delete this URL because at least one must be present to run an experiment.\n        "
                },
                'ab_experiment_details.url_input_field': {
                    title: 'Experiment URL / URL mask',
                    content: "\n        You can use wildcard symbol \"<samp>*</samp>\" in the URL string, which means \"any character\".<br>\n        For example, <samp>http://example.com/category1/*</samp> <br> will match any page under <samp>http://example.com/category1/</samp>.\n        "
                },
                'ab_experiment_details.variation_list_title': {
                    title: 'List of experiment variations',
                    content: "\n        Variations are different presentations of some elements on your experiment page.\n        Each A/B experiment includes one \"control variation\" which is simply an unmodified page, and one or more variations\n        that present some modifications. Upon finishing experiment you can choose the most effective variation based on the results.\n        "
                },
                'ab_experiment_details.create_variation_button': {
                    title: 'Add new variation',
                    content: "\n        You can add as many variation as you see fit, but it is a good practice in general to keep 3-5 variations per experiment including control variation.\n        "
                },
                'ab_experiment_details.distribute_weights_button': {
                    title: 'Distribute weights evenly',
                    content: "\n        This will evenly distribute weights between all <em>unlocked</em> variations.\n        Locked variations weights remain untouched so if you have, for example, 3 variations with one of them locked at 50%\n        the 2 other variations weights will be set to 25%.\n        "
                },
                'ab_experiment_details.variation_weight_number': {
                    title: 'Variation weight',
                    content: "\n        Variation weight determines number of times this variation will be shown to experiment page visitors as relative to other variations.\n\n        "
                },
                'ab_experiment_details.weight_lock_button': {
                    title: 'Lock variation weight',
                    content: "\n        When variation weight is locked, it cannot be changed by any means, neither explicitly (using weight handler)\n        nor implicitly (e.g. when \"distribute weights\" button is pressed).\n        "
                },
                'ab_experiment_details.change_weight_handler': {
                    content: 'Change variation weight'
                },
                'ab_experiment_details.delete_variation_button': {
                    title: 'Delete variation',
                    content: "\n        Remove this variation and collected data from experiment.<br>\n        NOTICE: You must have at least one variation aside from control variation to be able to publish your changes.\n        "
                },
                'ab_experiment_details.snippet_tab_header': {
                    title: 'Variation snippet',
                    content: "Variation snippet can be either html code or text."
                },
                'ab_experiment_details.snippet_tab_header_control': {
                    content: "\n        For control variation, this snippet defines html code or plain text that will be searched on experiment page\n        and replaced with corresponding snippet from effective variation.\n        "
                },
                'ab_experiment_details.snippet_tab_header_variation': {
                    content: "\n        For any variation except control, this snippet defines html code or plain text that will replace corresponding\n        snippet from control variation.\n        "
                },
                'ab_experiment_details.delete_snippet_button': {
                    title: 'Delete snippet',
                    content: "\n        Delete this variation snippet from all variations including control variation. Note that at least one snippet\n        must be present.\n        "
                },
                'ab_experiment_details.add_snippet_button': {
                    title: 'Add new snippet',
                    content: "\n        A new snippet will be created for all variations including control variation.\n        "
                },
                'ab_experiment_details.cant_delete_last_snippet': {
                    content: "\n        You can't delete this snippet because at least one must be present to run an experiment.\n        "
                },
                'ab_experiment_details.goals_list_title': {
                    title: 'Experiment goals',
                    content: "\n        Experiment goal presents a page that must be reached by a visitor for experiment session to be counted as successful.\n        "
                },
                'ab_experiment_details.add_goal_button': {
                    content: 'Add new experiment goal'
                },
                'ab_experiment_details.delete_goal_button': {
                    title: 'Delete goal',
                    content: "\n        Delete this goal from experiment. Note that at least one goal must be present.\n        "
                },
                'ab_experiment_details.cant_delete_last_goal': {
                    content: "\n        You can't delete this goal because at least one must be present to run an experiment.\n        "
                },
                'ab_experiment_details.run_experiment_button': {
                    title: 'Run experiment',
                    content: "\n        Activate and run the experiment.\n        "
                },
                'ab_experiment_details.run_experiment_button.ready': {
                    content: 'Note that once experiment is activated, any changes made afterwards must be published explicitly to' +
                        ' take effect.'
                },
                'ab_experiment_details.cant_run_experiment.no_variations': {
                    content: "\n        You can't run this experiment because <strong>you have no variations</strong> present.\n        Create at least one variation <em>aside from control variation</em> to be able to activate the experiment.\n        "
                },
                'ab_experiment_details.cant_run_experiment.invalid_url': {
                    content: "\n        You can't run this experiment because <strong>you have no valid experiment URL</strong> present.\n        Define valid experiment URL to be able to activate the experiment.\n        "
                },
                'ab_experiment_details.cant_run_experiment.invalid_goal': {
                    content: "\n        You can't run this experiment because <strong>you have no valid experiment goal</strong> present.\n        Define valid experiment goal to be able to activate the experiment.\n        "
                },
                'ab_experiment_details.delete_experiment_button': {
                    title: 'Delete experiment',
                    content: "\n        Delete this experiment and all its data. Note that once experiment is activated, it cannot be deleted.\n        "
                },
                'ab_experiment_details.toggle_experiment_button': {
                    content: "\n        Pause/Resume experiment\n        "
                },
                'ab_experiment_details.pause_experiment_button': {
                    content: "\n        Pause experiment\n        "
                },
                'ab_experiment_details.resume_experiment_button': {
                    content: "\n        Resume experiment\n        "
                },
                'ab_experiment_details.publish_experiment_button': {
                    title: 'Publish experiment',
                    content: ''
                },
                'ab_experiment_details.publish_experiment_button.ready': {
                    content: "\n        Once you published the experiment, any changes made will take effect on target pages.\n        "
                },
                'ab_experiment_details.cant_publish_experiment.no_variations': {
                    content: "\n        You can't publish this experiment because <strong>you have no variations</strong>.\n        Please, create at least one variation <em>aside from control variation</em>.\n        "
                },
                'ab_experiment_details.cant_publish_experiment.invalid_url': {
                    content: "\n        You can't publish this experiment because <strong>you have no valid experiment URL</strong>.\n        Please, define a valid experiment URL.\n        "
                },
                'ab_experiment_details.cant_publish_experiment.invalid_goal': {
                    content: "\n        You can't publish this experiment because <strong>you have no valid experiment goal</strong>.\n        Please, define a valid experiment goal.\n        "
                },
                'ab_experiment_details.js_code_snippet_title': {
                    content: "\n        Insert this code snippet to all of your experiment pages. You can safely insert it to all pages on your site\n        &nbsp;&mdash;&nbsp;the experiment will only run on pages defined by experiment URL field(s).<br>\n        If you are unsure where to insert this code, a good practice is to put it closer to the top of a page,\n        &nbsp;inside of \"header\" tag or right after opening \"body\" tag.\n        "
                },
                'dashboard.ab_experiments_table.th.views': {
                    title: 'Experiment page views',
                    content: "\n        This value shows the number of times when any of experiment pages has been viewed.<br>\n        The number in brackets shows <b>unique&nbsp;views</b> only.\n        "
                },
                'dashboard.ab_experiments_table.th.goals': {
                    title: 'Experiment goals reached',
                    content: "\n        This value shows the number of times when any of experiment goals has been reached.<br>\n        For destination goal, only first hit is counted.\n        "
                },
                'dashboard.ab_experiments_table.th.conversion': {
                    title: 'Experiment conversion',
                    content: "\n        This value shows <em>conversion ratio</em> for the experiment.<br>\n        Ratio is calculated as <span style=\"white-space:nowrap\">[ <em>(unique views) / (goals reached)</em> ]</span>.\n        "
                },
                'dashboard.ab_experiments_table.th.significance': {
                    title: 'Experiment significance',
                    content: "\n        This value shows <em>statistical&nbsp;significance</em> for the experiment.<br>\n        The higher significance means higher probability to determine a winning variation.\n        "
                }
            });
        }
    }
});
System.register("app/messages.service", ['angular2/core', "app/app.messages"], function(exports_35, context_35) {
    "use strict";
    var __moduleName = context_35 && context_35.id;
    var core_9, app_messages_1;
    var MessagesService;
    return {
        setters:[
            function (core_9_1) {
                core_9 = core_9_1;
            },
            function (app_messages_1_1) {
                app_messages_1 = app_messages_1_1;
            }],
        execute: function() {
            MessagesService = (function () {
                function MessagesService() {
                    this._messages = {};
                    this._messages = app_messages_1.MESSAGES;
                }
                Object.defineProperty(MessagesService.prototype, "_", {
                    get: function () {
                        return this._messages;
                    },
                    enumerable: true,
                    configurable: true
                });
                MessagesService = __decorate([
                    core_9.Injectable(), 
                    __metadata('design:paramtypes', [])
                ], MessagesService);
                return MessagesService;
            }());
            exports_35("MessagesService", MessagesService);
        }
    }
});
System.register("app/popover.service", ['angular2/core', "app/messages.service"], function(exports_36, context_36) {
    "use strict";
    var __moduleName = context_36 && context_36.id;
    var core_10, messages_service_1;
    var PopoverService;
    return {
        setters:[
            function (core_10_1) {
                core_10 = core_10_1;
            },
            function (messages_service_1_1) {
                messages_service_1 = messages_service_1_1;
            }],
        execute: function() {
            PopoverService = (function () {
                function PopoverService(messages) {
                    this.messages = messages;
                }
                PopoverService.prototype.options = function (id) {
                    var message = this.messages._[id];
                    var options = {
                        trigger: 'hover',
                        html: true
                    };
                    if (message['title']) {
                        options['title'] = message['title'];
                    }
                    if (message['content']) {
                        options['content'] = message['content'];
                    }
                    return options;
                };
                PopoverService.prototype.message = function (id) {
                    return this.messages._[id];
                };
                PopoverService = __decorate([
                    core_10.Injectable(), 
                    __metadata('design:paramtypes', [messages_service_1.MessagesService])
                ], PopoverService);
                return PopoverService;
            }());
            exports_36("PopoverService", PopoverService);
        }
    }
});
System.register("app/ui-popover.component", ['angular2/core', "app/popover.service"], function(exports_37, context_37) {
    "use strict";
    var __moduleName = context_37 && context_37.id;
    var core_11, popover_service_1;
    var UiPopoverComponent;
    return {
        setters:[
            function (core_11_1) {
                core_11 = core_11_1;
            },
            function (popover_service_1_1) {
                popover_service_1 = popover_service_1_1;
            }],
        execute: function() {
            UiPopoverComponent = (function () {
                function UiPopoverComponent(elementRef, popovers) {
                    this.elementRef = elementRef;
                    this.popovers = popovers;
                    this.onInit = new core_11.EventEmitter(false);
                    this.template = "\n    <div class=\"popover%s\" style=\"%s\" role=\"tooltip\">\n        <div class=\"arrow\"></div>\n        <h3 class=\"popover-title\"></h3>\n        <div class=\"popover-content\"></div>\n    </div>\n    ";
                    this.defaultStyle = 'min-width:250px';
                }
                UiPopoverComponent.prototype.ngOnInit = function () {
                    this.onInit.emit(this);
                    var options = this.popovers.options(this.uiPopover);
                    options.placement = this.popoverPlacement || 'auto';
                    var popoverClass = this.popoverClass ? ' ' + this.popoverClass : '';
                    var style = this.popoverStyle || this.defaultStyle;
                    options.template = sprintf(this.template, popoverClass, style);
                    if (this.popoverFn) {
                        options.content = this.popoverFn.bind(this.popoverOwner, options, this, this.popovers);
                    }
                    jQuery(this.elementRef.nativeElement).popover(options);
                    jQuery(this.elementRef.nativeElement).click(function (event) {
                        jQuery(event['currentTarget']).popover('hide');
                    });
                };
                UiPopoverComponent.prototype.setOwner = function (owner) {
                    this.popoverOwner = owner;
                };
                __decorate([
                    core_11.Output('popoverInit'), 
                    __metadata('design:type', core_11.EventEmitter)
                ], UiPopoverComponent.prototype, "onInit", void 0);
                __decorate([
                    core_11.Input(), 
                    __metadata('design:type', String)
                ], UiPopoverComponent.prototype, "uiPopover", void 0);
                __decorate([
                    core_11.Input(), 
                    __metadata('design:type', String)
                ], UiPopoverComponent.prototype, "popoverPlacement", void 0);
                __decorate([
                    core_11.Input(), 
                    __metadata('design:type', String)
                ], UiPopoverComponent.prototype, "popoverClass", void 0);
                __decorate([
                    core_11.Input(), 
                    __metadata('design:type', String)
                ], UiPopoverComponent.prototype, "popoverStyle", void 0);
                __decorate([
                    core_11.Input(), 
                    __metadata('design:type', String)
                ], UiPopoverComponent.prototype, "popoverArrowStyle", void 0);
                __decorate([
                    core_11.Input(), 
                    __metadata('design:type', Function)
                ], UiPopoverComponent.prototype, "popoverFn", void 0);
                UiPopoverComponent = __decorate([
                    core_11.Directive({
                        selector: '[uiPopover]',
                        providers: [
                            popover_service_1.PopoverService
                        ]
                    }), 
                    __metadata('design:paramtypes', [core_11.ElementRef, popover_service_1.PopoverService])
                ], UiPopoverComponent);
                return UiPopoverComponent;
            }());
            exports_37("UiPopoverComponent", UiPopoverComponent);
        }
    }
});
System.register("app/em/data.service", ['angular2/core', "app/em/entity-manager"], function(exports_38, context_38) {
    "use strict";
    var __moduleName = context_38 && context_38.id;
    var core_12, entity_manager_1;
    var DataService;
    return {
        setters:[
            function (core_12_1) {
                core_12 = core_12_1;
            },
            function (entity_manager_1_1) {
                entity_manager_1 = entity_manager_1_1;
            }],
        execute: function() {
            DataService = (function () {
                function DataService(_em) {
                    this._em = _em;
                    // @todo make official
                    window['data'] = this;
                }
                DataService.prototype.getType = function (name) {
                    return this._em.getType(name);
                };
                DataService.prototype.getFactory = function (type) {
                    return this._em.getFactory(type);
                };
                /**
                 * @returns {EntityManager}
                 * @deprecated
                 */
                DataService.prototype.getManager = function () {
                    return this._em;
                };
                // @todo -params
                DataService.prototype.create = function (type, parent, params, complete) {
                    if (complete === void 0) { complete = true; }
                    return this._em.create(type, parent, params, complete);
                };
                // @todo create sync decorator version
                DataService.prototype.all = function (type) {
                    return this._em.all(type);
                };
                DataService.prototype.remove = function (entity) {
                    return this._em.remove(entity);
                };
                DataService.prototype.persist = function (delay) {
                    var _this = this;
                    if (delay === void 0) { delay = 0; }
                    if (this._deferred) {
                        window.clearTimeout(this._deferred);
                    }
                    return new Promise(function (resolve, reject) {
                        _this._deferred = window.setTimeout(function () {
                            _this._em.persist()
                                .then(function (result) { return resolve(result); })
                                .catch(function (error) { return reject(error); });
                        }, delay);
                    });
                };
                DataService = __decorate([
                    core_12.Injectable(), 
                    __metadata('design:paramtypes', [entity_manager_1.EntityManager])
                ], DataService);
                return DataService;
            }());
            exports_38("DataService", DataService);
        }
    }
});
System.register("app/notification-message", [], function(exports_39, context_39) {
    "use strict";
    var __moduleName = context_39 && context_39.id;
    var NotificationMessage;
    return {
        setters:[],
        execute: function() {
            NotificationMessage = (function () {
                function NotificationMessage(content, type) {
                    if (type === void 0) { type = NotificationMessage.NOTICE; }
                    this.content = content;
                    this.type = type;
                }
                NotificationMessage.SUCCESS = 0;
                NotificationMessage.INFO = 1;
                NotificationMessage.NOTICE = 2;
                NotificationMessage.WARNING = 3;
                NotificationMessage.ERROR = 4;
                return NotificationMessage;
            }());
            exports_39("NotificationMessage", NotificationMessage);
        }
    }
});
System.register("app/notification-subscriber", [], function(exports_40, context_40) {
    "use strict";
    var __moduleName = context_40 && context_40.id;
    return {
        setters:[],
        execute: function() {
        }
    }
});
System.register("app/notification.service", ['angular2/core', "app/notification-message"], function(exports_41, context_41) {
    "use strict";
    var __moduleName = context_41 && context_41.id;
    var core_13, notification_message_1;
    var NotificationService;
    return {
        setters:[
            function (core_13_1) {
                core_13 = core_13_1;
            },
            function (notification_message_1_1) {
                notification_message_1 = notification_message_1_1;
            }],
        execute: function() {
            NotificationService = (function () {
                function NotificationService() {
                    this._subscribers = [];
                }
                NotificationService.prototype.subscribe = function (subscriber) {
                    this._subscribers.push(subscriber);
                };
                NotificationService.prototype.message = function (message) {
                    this._subscribers.forEach(function (subscriber) {
                        subscriber.next(message);
                    });
                };
                NotificationService.prototype.success = function (content) {
                    this.message(NotificationService.factory(content, notification_message_1.NotificationMessage.SUCCESS));
                };
                NotificationService.prototype.error = function (content) {
                    this.message(NotificationService.factory(content, notification_message_1.NotificationMessage.ERROR));
                };
                NotificationService.prototype.notice = function (content) {
                    this.message(NotificationService.factory(content, notification_message_1.NotificationMessage.NOTICE));
                };
                NotificationService.prototype.warning = function (content) {
                    this.message(NotificationService.factory(content, notification_message_1.NotificationMessage.WARNING));
                };
                NotificationService.factory = function (content, type) {
                    return new notification_message_1.NotificationMessage(content, type);
                };
                NotificationService = __decorate([
                    core_13.Injectable(), 
                    __metadata('design:paramtypes', [])
                ], NotificationService);
                return NotificationService;
            }());
            exports_41("NotificationService", NotificationService);
        }
    }
});
System.register("app/em/experiments.service", ['angular2/core', 'rxjs/add/operator/share', "app/model/experiment", "app/em/data.service", "app/em/indexed-collection", "app/notification.service"], function(exports_42, context_42) {
    "use strict";
    var __moduleName = context_42 && context_42.id;
    var core_14, experiment_4, data_service_1, indexed_collection_6, notification_service_1;
    var ExperimentsService;
    return {
        setters:[
            function (core_14_1) {
                core_14 = core_14_1;
            },
            function (_2) {},
            function (experiment_4_1) {
                experiment_4 = experiment_4_1;
            },
            function (data_service_1_1) {
                data_service_1 = data_service_1_1;
            },
            function (indexed_collection_6_1) {
                indexed_collection_6 = indexed_collection_6_1;
            },
            function (notification_service_1_1) {
                notification_service_1 = notification_service_1_1;
            }],
        execute: function() {
            ExperimentsService = (function () {
                function ExperimentsService(data, notifications) {
                    var _this = this;
                    this.data = data;
                    this.notifications = notifications;
                    this.experiments = new indexed_collection_6.IndexedCollection();
                    this.ready = false;
                    this.data.all(experiment_4.Experiment)
                        .then(function (collection) {
                        _this.experiments = collection;
                        _this.ready = true;
                    })
                        .catch(function (error) {
                        _this.notifications.error('Failed to load experiments list. Check error console for details.');
                        console.error(error);
                    });
                }
                Object.defineProperty(ExperimentsService.prototype, "all", {
                    get: function () {
                        return this.experiments.items;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ExperimentsService.prototype, "created", {
                    get: function () {
                        return this.experiments.index('state').get(1);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ExperimentsService.prototype, "active", {
                    get: function () {
                        return this.experiments.index('state').in([2, 3]);
                    },
                    enumerable: true,
                    configurable: true
                });
                ExperimentsService.prototype.push = function (item) {
                    this.experiments.push(item);
                };
                ExperimentsService.prototype.remove = function (item) {
                    this.experiments.remove(item);
                };
                // @todo excessively open
                ExperimentsService.prototype.reindex = function (prop) {
                    this.experiments.reindex(prop);
                };
                ExperimentsService = __decorate([
                    core_14.Injectable(), 
                    __metadata('design:paramtypes', [data_service_1.DataService, notification_service_1.NotificationService])
                ], ExperimentsService);
                return ExperimentsService;
            }());
            exports_42("ExperimentsService", ExperimentsService);
        }
    }
});
System.register("app/dashboard/dashboard-ab-stats.component", ['angular2/core', 'angular2/router', "app/dashboard/dashboard-ab-stats-row.component", "app/dashboard/dashboard-ab-stats-breakdown.component", "app/ui-popover.component", "app/em/experiments.service"], function(exports_43, context_43) {
    "use strict";
    var __moduleName = context_43 && context_43.id;
    var core_15, router_2, dashboard_ab_stats_row_component_1, dashboard_ab_stats_breakdown_component_1, ui_popover_component_1, experiments_service_1;
    var DashboardAbStatsComponent;
    return {
        setters:[
            function (core_15_1) {
                core_15 = core_15_1;
            },
            function (router_2_1) {
                router_2 = router_2_1;
            },
            function (dashboard_ab_stats_row_component_1_1) {
                dashboard_ab_stats_row_component_1 = dashboard_ab_stats_row_component_1_1;
            },
            function (dashboard_ab_stats_breakdown_component_1_1) {
                dashboard_ab_stats_breakdown_component_1 = dashboard_ab_stats_breakdown_component_1_1;
            },
            function (ui_popover_component_1_1) {
                ui_popover_component_1 = ui_popover_component_1_1;
            },
            function (experiments_service_1_1) {
                experiments_service_1 = experiments_service_1_1;
            }],
        execute: function() {
            ;
            DashboardAbStatsComponent = (function () {
                function DashboardAbStatsComponent(experiments) {
                    this.experiments = experiments;
                    this.rows = new Map();
                    this.breakdown = new Map();
                }
                DashboardAbStatsComponent.prototype.ngOnInit = function () {
                };
                DashboardAbStatsComponent.prototype.registerRow = function (component, index) {
                    this.rows.set(index, component);
                };
                DashboardAbStatsComponent.prototype.registerBreakdown = function (component, index) {
                    this.breakdown.set(index, component);
                    this.rows.get(index).setBreakdown(component);
                };
                DashboardAbStatsComponent.prototype.showBreakdown = function (data, index) {
                    this.breakdown.get(index).showStats(data);
                };
                DashboardAbStatsComponent.prototype.toggleDetails = function (row) {
                };
                DashboardAbStatsComponent = __decorate([
                    core_15.Component({
                        selector: 'dashboard-ab-stats',
                        templateUrl: 'templates/dashboard/dashboard-ab-stats.html',
                        providers: [],
                        directives: [
                            dashboard_ab_stats_row_component_1.DashboardAbStatsRowComponent,
                            dashboard_ab_stats_breakdown_component_1.DashboardAbStatsBreakdownComponent,
                            ui_popover_component_1.UiPopoverComponent,
                            router_2.ROUTER_DIRECTIVES
                        ]
                    }), 
                    __metadata('design:paramtypes', [experiments_service_1.ExperimentsService])
                ], DashboardAbStatsComponent);
                return DashboardAbStatsComponent;
            }());
            exports_43("DashboardAbStatsComponent", DashboardAbStatsComponent);
        }
    }
});
System.register("app/dashboard.component", ['angular2/core', "app/dashboard/dashboard-ab-stats.component"], function(exports_44, context_44) {
    "use strict";
    var __moduleName = context_44 && context_44.id;
    var core_16, dashboard_ab_stats_component_1;
    var DashboardComponent;
    return {
        setters:[
            function (core_16_1) {
                core_16 = core_16_1;
            },
            function (dashboard_ab_stats_component_1_1) {
                dashboard_ab_stats_component_1 = dashboard_ab_stats_component_1_1;
            }],
        execute: function() {
            DashboardComponent = (function () {
                function DashboardComponent() {
                }
                DashboardComponent = __decorate([
                    core_16.Component({
                        selector: 'dashboard',
                        templateUrl: 'templates/dashboard.html',
                        providers: [],
                        directives: [
                            dashboard_ab_stats_component_1.DashboardAbStatsComponent
                        ]
                    }), 
                    __metadata('design:paramtypes', [])
                ], DashboardComponent);
                return DashboardComponent;
            }());
            exports_44("DashboardComponent", DashboardComponent);
        }
    }
});
System.register("app/login-form.component", ['angular2/core', 'angular2/router', "app/auth"], function(exports_45, context_45) {
    "use strict";
    var __moduleName = context_45 && context_45.id;
    var core_17, router_3, auth_1;
    var LoginFormComponent;
    return {
        setters:[
            function (core_17_1) {
                core_17 = core_17_1;
            },
            function (router_3_1) {
                router_3 = router_3_1;
            },
            function (auth_1_1) {
                auth_1 = auth_1_1;
            }],
        execute: function() {
            LoginFormComponent = (function () {
                function LoginFormComponent(auth, router) {
                    this.auth = auth;
                    this.router = router;
                }
                LoginFormComponent.prototype.login = function () {
                    var _this = this;
                    var success = function () {
                        _this.router.navigate(['Dashboard']);
                    };
                    this.auth.login(this.username, this.password)
                        .then(function (data) {
                        success();
                    })
                        .catch(function (reason) {
                        console.error('Login failed: ' + reason);
                    });
                };
                LoginFormComponent = __decorate([
                    core_17.Component({
                        selector: 'login-form',
                        templateUrl: 'templates/login-form.html',
                        providers: [auth_1.AuthService]
                    }), 
                    __metadata('design:paramtypes', [auth_1.AuthService, router_3.Router])
                ], LoginFormComponent);
                return LoginFormComponent;
            }());
            exports_45("LoginFormComponent", LoginFormComponent);
        }
    }
});
System.register("app/login.component", ['angular2/core', 'angular2/router', "app/auth"], function(exports_46, context_46) {
    "use strict";
    var __moduleName = context_46 && context_46.id;
    var core_18, router_4, auth_2;
    var LoginComponent;
    return {
        setters:[
            function (core_18_1) {
                core_18 = core_18_1;
            },
            function (router_4_1) {
                router_4 = router_4_1;
            },
            function (auth_2_1) {
                auth_2 = auth_2_1;
            }],
        execute: function() {
            LoginComponent = (function () {
                function LoginComponent() {
                }
                LoginComponent = __decorate([
                    core_18.Component({
                        template: "\n    <h1>Login Page</h1>\n    <router-outlet></router-outlet>\n    ",
                        directives: [router_4.RouterOutlet],
                        providers: [auth_2.AuthService]
                    }), 
                    __metadata('design:paramtypes', [])
                ], LoginComponent);
                return LoginComponent;
            }());
            exports_46("LoginComponent", LoginComponent);
        }
    }
});
System.register("app/notification-bar.component", ['angular2/core', "app/notification-message", "app/notification.service"], function(exports_47, context_47) {
    "use strict";
    var __moduleName = context_47 && context_47.id;
    var core_19, notification_message_2, notification_service_2;
    var NotificationBarComponent, NotificationSubscriber, NotificationBarMessage;
    return {
        setters:[
            function (core_19_1) {
                core_19 = core_19_1;
            },
            function (notification_message_2_1) {
                notification_message_2 = notification_message_2_1;
            },
            function (notification_service_2_1) {
                notification_service_2 = notification_service_2_1;
            }],
        execute: function() {
            NotificationBarComponent = (function () {
                function NotificationBarComponent(service) {
                    var _this = this;
                    this.messages = [];
                    this.active = [];
                    this.open = false;
                    this.onOpen = new core_19.EventEmitter();
                    this.onClose = new core_19.EventEmitter();
                    service.subscribe(NotificationSubscriber.create(function (message) {
                        _this.show(message);
                    }));
                }
                NotificationBarComponent.prototype.show = function (message) {
                    var _this = this;
                    this.messages.push(message);
                    this.active.push(message);
                    this.open = true;
                    this.onOpen.emit(null);
                    setTimeout(function () {
                        if (_this.active.length == 1) {
                            _this.open = false;
                            _this.onClose.emit(null);
                        }
                        message.closing = true;
                    }, 4000);
                    setTimeout(function () {
                        _this.active.splice(_this.active.indexOf(message), 1);
                    }, 4500);
                };
                __decorate([
                    core_19.Output('open'), 
                    __metadata('design:type', core_19.EventEmitter)
                ], NotificationBarComponent.prototype, "onOpen", void 0);
                __decorate([
                    core_19.Output('close'), 
                    __metadata('design:type', core_19.EventEmitter)
                ], NotificationBarComponent.prototype, "onClose", void 0);
                NotificationBarComponent = __decorate([
                    core_19.Component({
                        selector: 'notification-bar',
                        templateUrl: 'templates/notification-bar.html'
                    }), 
                    __metadata('design:paramtypes', [notification_service_2.NotificationService])
                ], NotificationBarComponent);
                return NotificationBarComponent;
            }());
            exports_47("NotificationBarComponent", NotificationBarComponent);
            NotificationSubscriber = (function () {
                function NotificationSubscriber(callback) {
                    this.callback = callback;
                }
                NotificationSubscriber.create = function (callback) {
                    return new NotificationSubscriber(callback);
                };
                NotificationSubscriber.prototype.next = function (message) {
                    this.callback(new NotificationBarMessage(message));
                };
                return NotificationSubscriber;
            }());
            NotificationBarMessage = (function (_super) {
                __extends(NotificationBarMessage, _super);
                function NotificationBarMessage(message) {
                    _super.call(this, message.content, message.type);
                    this._classes = ['success', 'info', 'info', 'warning', 'danger'];
                    this._prefixes = ['Success', 'Info', 'Notice', 'Warning', 'Error'];
                    this.closing = false;
                }
                Object.defineProperty(NotificationBarMessage.prototype, "classes", {
                    get: function () {
                        return 'alert-' + this._classes[this.type] + (this.closing ? ' closing' : '');
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(NotificationBarMessage.prototype, "prefix", {
                    get: function () {
                        return this._prefixes[this.type];
                    },
                    enumerable: true,
                    configurable: true
                });
                return NotificationBarMessage;
            }(notification_message_2.NotificationMessage));
            exports_47("NotificationBarMessage", NotificationBarMessage);
        }
    }
});
System.register("app/menu-sidebar.component", ['angular2/core', 'angular2/router', "app/auth"], function(exports_48, context_48) {
    "use strict";
    var __moduleName = context_48 && context_48.id;
    var core_20, router_5, auth_3;
    var MenuSidebarComponent;
    return {
        setters:[
            function (core_20_1) {
                core_20 = core_20_1;
            },
            function (router_5_1) {
                router_5 = router_5_1;
            },
            function (auth_3_1) {
                auth_3 = auth_3_1;
            }],
        execute: function() {
            MenuSidebarComponent = (function () {
                function MenuSidebarComponent(auth) {
                    this.auth = auth;
                }
                MenuSidebarComponent.prototype.logout = function () {
                    this.auth.logout();
                };
                MenuSidebarComponent = __decorate([
                    core_20.Component({
                        selector: 'menu-sidebar',
                        templateUrl: 'templates/menu-sidebar.html',
                        directives: [
                            router_5.ROUTER_DIRECTIVES
                        ]
                    }), 
                    __metadata('design:paramtypes', [auth_3.AuthService])
                ], MenuSidebarComponent);
                return MenuSidebarComponent;
            }());
            exports_48("MenuSidebarComponent", MenuSidebarComponent);
        }
    }
});
System.register("app/ab-experiments.component", ['angular2/core'], function(exports_49, context_49) {
    "use strict";
    var __moduleName = context_49 && context_49.id;
    var core_21;
    var AbExperimentsComponent;
    return {
        setters:[
            function (core_21_1) {
                core_21 = core_21_1;
            }],
        execute: function() {
            AbExperimentsComponent = (function () {
                function AbExperimentsComponent() {
                }
                AbExperimentsComponent = __decorate([
                    core_21.Component({
                        template: '<router-outlet></router-outlet>',
                        providers: []
                    }), 
                    __metadata('design:paramtypes', [])
                ], AbExperimentsComponent);
                return AbExperimentsComponent;
            }());
            exports_49("AbExperimentsComponent", AbExperimentsComponent);
        }
    }
});
System.register("app/dom-disabled.directive", ['angular2/core'], function(exports_50, context_50) {
    "use strict";
    var __moduleName = context_50 && context_50.id;
    var core_22;
    var DomDisabledDirective;
    return {
        setters:[
            function (core_22_1) {
                core_22 = core_22_1;
            }],
        execute: function() {
            DomDisabledDirective = (function () {
                function DomDisabledDirective(el) {
                    this.el = el;
                    this.expression = false;
                }
                DomDisabledDirective.prototype.ngOnInit = function () {
                    this.el.nativeElement.disabled = this.expression;
                };
                __decorate([
                    core_22.Input('domDisabled'), 
                    __metadata('design:type', Boolean)
                ], DomDisabledDirective.prototype, "expression", void 0);
                DomDisabledDirective = __decorate([
                    core_22.Directive({
                        selector: '[domDisabled]'
                    }), 
                    __metadata('design:paramtypes', [core_22.ElementRef])
                ], DomDisabledDirective);
                return DomDisabledDirective;
            }());
            exports_50("DomDisabledDirective", DomDisabledDirective);
        }
    }
});
System.register("app/ui-edit-inline.directive", ['angular2/core'], function(exports_51, context_51) {
    "use strict";
    var __moduleName = context_51 && context_51.id;
    var core_23, core_24;
    var UiEditInlineDirective;
    return {
        setters:[
            function (core_23_1) {
                core_23 = core_23_1;
                core_24 = core_23_1;
            }],
        execute: function() {
            UiEditInlineDirective = (function () {
                function UiEditInlineDirective(elementRef) {
                    this.elementRef = elementRef;
                    this.type = 'string';
                    this.onFocus = new core_24.EventEmitter();
                    this.onBlur = new core_24.EventEmitter();
                    this.onChange = new core_24.EventEmitter();
                    this.onEnter = new core_24.EventEmitter();
                    this.onEscape = new core_24.EventEmitter();
                    this.focused = false;
                    this.highlighted = false;
                }
                UiEditInlineDirective.prototype.ngOnInit = function () {
                    this.elementRef.nativeElement.spellcheck = false;
                    jQuery(this.elementRef.nativeElement).css({
                        color: '#212F40',
                        width: '100%',
                        background: 'transparent',
                        border: '0 solid transparent',
                        borderBottom: '1px solid transparent',
                        outline: '0 solid transparent',
                        position: 'relative',
                        padding: '0 0 2px 0',
                        left: '-1px',
                        top: '-1px',
                        overflow: 'hidden',
                        resize: 'none',
                        transition: 'border-color 0.1s ease'
                    });
                    if (this.type == 'code') {
                        jQuery(this.elementRef.nativeElement).css({
                            borderLeft: '1px solid rgba(0, 0, 0, 0.25)',
                            paddingLeft: '10px'
                        });
                    }
                    if (this.prefix) {
                        this.showPrefix();
                    }
                    this.model = this.value;
                };
                Object.defineProperty(UiEditInlineDirective.prototype, "value", {
                    get: function () {
                        return this.elementRef.nativeElement.value;
                    },
                    enumerable: true,
                    configurable: true
                });
                UiEditInlineDirective.prototype.focus = function (fix) {
                    if (fix === void 0) { fix = false; }
                    if (!this.focused) {
                        if (this.type != 'code') {
                            var color = fix ? 'rgba(0, 0, 0, 0.4)' : 'rgba(0, 0, 0, 0.15)';
                            jQuery(this.elementRef.nativeElement).css({
                                borderBottom: '1px solid ' + color
                            });
                        }
                        if (fix) {
                            this.focused = true;
                            this.highlightPrefix();
                        }
                    }
                };
                UiEditInlineDirective.prototype.blur = function (release) {
                    if (release === void 0) { release = false; }
                    if (!this.focused || release) {
                        jQuery(this.elementRef.nativeElement).css({
                            borderBottom: '1px solid transparent'
                        });
                        if (release) {
                            this.focused = false;
                            //this.onBlur.emit(this.elementRef.nativeElement);
                            if (this.value.length == 0) {
                                this.highlightPrefix(false);
                            }
                        }
                    }
                };
                UiEditInlineDirective.prototype.keyup = function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    switch (event['code']) {
                        case 'Enter':
                            this.onEnter.emit(event);
                            break;
                        case 'Escape':
                            jQuery(this.elementRef.nativeElement).blur();
                            this.onEscape.emit(event);
                            break;
                        default:
                            if (this.model != this.value) {
                                console.log(this.model);
                                console.log(this.value);
                                /*let stripped = this.stripPrefix(this.value);
                                if (stripped != this.value) {
                                    jQuery(this.elementRef.nativeElement).val(stripped);
                                }*/
                                this.onChange.emit(null);
                            }
                    }
                };
                UiEditInlineDirective.prototype.showPrefix = function () {
                    var _this = this;
                    var span = jQuery('<span style="position:absolute;visibility:hidden">' + this.prefix + '</span>')
                        .insertBefore(this.elementRef.nativeElement);
                    var width = span.width();
                    var position = jQuery(this.elementRef.nativeElement).position();
                    jQuery(span).remove();
                    var element = jQuery('<input readonly/>').insertBefore(this.elementRef.nativeElement)
                        .css({
                        color: '#212F40',
                        opacity: '0.25',
                        width: width + 'px',
                        background: 'transparent',
                        border: '0 solid transparent',
                        borderBottom: '1px solid transparent',
                        outline: '0 solid transparent',
                        position: 'absolute',
                        padding: '0 0 2px 0',
                        left: position.left,
                        top: position.top,
                        overflow: 'hidden',
                        resize: 'none'
                    })
                        .val(this.prefix);
                    this.prefixElement = element[0];
                    jQuery(this.elementRef.nativeElement).css({
                        paddingLeft: '' + width + 'px'
                    });
                    jQuery(this.prefixElement).focus(function (event) {
                        jQuery(_this.elementRef.nativeElement).focus();
                    });
                    if (this.value.length) {
                        this.highlightPrefix();
                    }
                };
                UiEditInlineDirective.prototype.highlightPrefix = function (on) {
                    if (on === void 0) { on = true; }
                    if (this.prefixElement && (this.highlighted != on)) {
                        jQuery(this.prefixElement).css({
                            opacity: (on ? '1' : '0.4')
                        });
                        this.highlighted = on;
                    }
                };
                UiEditInlineDirective.prototype.stripPrefix = function (value) {
                    if (!this.prefix) {
                        return value;
                    }
                    return value.substr(0, this.prefix.length) == this.prefix
                        ? value.substr(this.prefix.length)
                        : value;
                };
                __decorate([
                    core_23.Input('edit-inline'), 
                    __metadata('design:type', String)
                ], UiEditInlineDirective.prototype, "model", void 0);
                __decorate([
                    core_23.Input('prefix'), 
                    __metadata('design:type', String)
                ], UiEditInlineDirective.prototype, "prefix", void 0);
                __decorate([
                    core_23.Input('content-type'), 
                    __metadata('design:type', String)
                ], UiEditInlineDirective.prototype, "type", void 0);
                __decorate([
                    core_24.Output('focus'), 
                    __metadata('design:type', core_24.EventEmitter)
                ], UiEditInlineDirective.prototype, "onFocus", void 0);
                __decorate([
                    core_24.Output('blur'), 
                    __metadata('design:type', core_24.EventEmitter)
                ], UiEditInlineDirective.prototype, "onBlur", void 0);
                __decorate([
                    core_24.Output('change'), 
                    __metadata('design:type', core_24.EventEmitter)
                ], UiEditInlineDirective.prototype, "onChange", void 0);
                __decorate([
                    core_24.Output('enter'), 
                    __metadata('design:type', core_24.EventEmitter)
                ], UiEditInlineDirective.prototype, "onEnter", void 0);
                __decorate([
                    core_24.Output('escape'), 
                    __metadata('design:type', core_24.EventEmitter)
                ], UiEditInlineDirective.prototype, "onEscape", void 0);
                UiEditInlineDirective = __decorate([
                    core_23.Directive({
                        selector: '[edit-inline]',
                        host: {
                            '(mouseenter)': 'focus()',
                            '(mouseleave)': 'blur()',
                            '(focus)': 'focus(true)',
                            '(blur)': 'blur(true)',
                            '(keyup)': 'keyup($event)'
                        }
                    }), 
                    __metadata('design:paramtypes', [core_23.ElementRef])
                ], UiEditInlineDirective);
                return UiEditInlineDirective;
            }());
            exports_51("UiEditInlineDirective", UiEditInlineDirective);
        }
    }
});
System.register("app/ab-experiment-details/experiment-url.component", ['angular2/core', "app/model/experiment-url", "app/dom-disabled.directive", "app/ui-popover.component", "app/ui-edit-inline.directive", "app/em/data.service"], function(exports_52, context_52) {
    "use strict";
    var __moduleName = context_52 && context_52.id;
    var core_25, experiment_url_4, dom_disabled_directive_1, ui_popover_component_2, ui_edit_inline_directive_1, data_service_2;
    var ExperimentUrlComponent;
    return {
        setters:[
            function (core_25_1) {
                core_25 = core_25_1;
            },
            function (experiment_url_4_1) {
                experiment_url_4 = experiment_url_4_1;
            },
            function (dom_disabled_directive_1_1) {
                dom_disabled_directive_1 = dom_disabled_directive_1_1;
            },
            function (ui_popover_component_2_1) {
                ui_popover_component_2 = ui_popover_component_2_1;
            },
            function (ui_edit_inline_directive_1_1) {
                ui_edit_inline_directive_1 = ui_edit_inline_directive_1_1;
            },
            function (data_service_2_1) {
                data_service_2 = data_service_2_1;
            }],
        execute: function() {
            ExperimentUrlComponent = (function () {
                function ExperimentUrlComponent(data) {
                    this.data = data;
                    this.list = [];
                    this.onCreate = new core_25.EventEmitter();
                    this.onDelete = new core_25.EventEmitter();
                }
                ExperimentUrlComponent.prototype.create = function () {
                    this.onCreate.emit(null);
                };
                //noinspection ReservedWordAsName
                ExperimentUrlComponent.prototype.delete = function () {
                    this.onDelete.emit(this.url);
                };
                ExperimentUrlComponent.prototype.update = function () {
                    this.data.persist(500);
                };
                ExperimentUrlComponent.prototype.onPopoverInit = function (popover) {
                    popover.setOwner(this);
                };
                ExperimentUrlComponent.prototype.deleteUrlButtonPopover = function (options, popover, service) {
                    if (this.list.length > 1) {
                        return service.message(popover.uiPopover).content;
                    }
                    return service.message(popover.uiPopover).content + '<br>NOTICE: ' +
                        service.message('ab_experiment_details.cant_delete_last_url').content;
                };
                __decorate([
                    core_25.Input(), 
                    __metadata('design:type', experiment_url_4.ExperimentUrl)
                ], ExperimentUrlComponent.prototype, "url", void 0);
                __decorate([
                    core_25.Input(), 
                    __metadata('design:type', Array)
                ], ExperimentUrlComponent.prototype, "list", void 0);
                __decorate([
                    core_25.Input(), 
                    __metadata('design:type', Boolean)
                ], ExperimentUrlComponent.prototype, "last", void 0);
                __decorate([
                    core_25.Output('create'), 
                    __metadata('design:type', core_25.EventEmitter)
                ], ExperimentUrlComponent.prototype, "onCreate", void 0);
                __decorate([
                    core_25.Output('delete'), 
                    __metadata('design:type', core_25.EventEmitter)
                ], ExperimentUrlComponent.prototype, "onDelete", void 0);
                ExperimentUrlComponent = __decorate([
                    core_25.Component({
                        selector: 'experiment-url',
                        templateUrl: 'templates/ab-experiment-details/experiment-url.html',
                        directives: [
                            ui_edit_inline_directive_1.UiEditInlineDirective,
                            dom_disabled_directive_1.DomDisabledDirective,
                            ui_popover_component_2.UiPopoverComponent
                        ]
                    }), 
                    __metadata('design:paramtypes', [data_service_2.DataService])
                ], ExperimentUrlComponent);
                return ExperimentUrlComponent;
            }());
            exports_52("ExperimentUrlComponent", ExperimentUrlComponent);
        }
    }
});
System.register("app/ab-experiment-details/experiment-url-list.component", ['angular2/core', "app/ab-experiment-details/experiment-url.component", "app/auth", "app/model/experiment", "app/model/experiment-url", "app/ui-popover.component", "app/em/data.service", "app/notification.service"], function(exports_53, context_53) {
    "use strict";
    var __moduleName = context_53 && context_53.id;
    var core_26, experiment_url_component_1, auth_4, experiment_5, experiment_url_5, ui_popover_component_3, data_service_3, notification_service_3;
    var ExperimentUrlListComponent;
    return {
        setters:[
            function (core_26_1) {
                core_26 = core_26_1;
            },
            function (experiment_url_component_1_1) {
                experiment_url_component_1 = experiment_url_component_1_1;
            },
            function (auth_4_1) {
                auth_4 = auth_4_1;
            },
            function (experiment_5_1) {
                experiment_5 = experiment_5_1;
            },
            function (experiment_url_5_1) {
                experiment_url_5 = experiment_url_5_1;
            },
            function (ui_popover_component_3_1) {
                ui_popover_component_3 = ui_popover_component_3_1;
            },
            function (data_service_3_1) {
                data_service_3 = data_service_3_1;
            },
            function (notification_service_3_1) {
                notification_service_3 = notification_service_3_1;
            }],
        execute: function() {
            ExperimentUrlListComponent = (function () {
                function ExperimentUrlListComponent(data, auth, notification) {
                    this.data = data;
                    this.auth = auth;
                    this.notification = notification;
                }
                Object.defineProperty(ExperimentUrlListComponent.prototype, "urls", {
                    get: function () {
                        return this.experiment.urls.items;
                    },
                    enumerable: true,
                    configurable: true
                });
                ExperimentUrlListComponent.prototype.create = function () {
                    var _this = this;
                    this.data.create(experiment_url_5.ExperimentUrl, this.experiment)
                        .then(function (url) {
                        _this.experiment.urls.push(url);
                    })
                        .catch(function (error) {
                        _this.notification.error('Failed to add experiment URL. Check error console for details.');
                    });
                };
                //noinspection ReservedWordAsName
                ExperimentUrlListComponent.prototype.delete = function (url) {
                    var _this = this;
                    this.data.remove(url)
                        .then(function () {
                        _this.experiment.urls.remove(url);
                    })
                        .catch(function (error) {
                        _this.notification.error('Error has occurred while removing the experiment URL. Check error console for details.');
                    });
                };
                __decorate([
                    core_26.Input(), 
                    __metadata('design:type', experiment_5.Experiment)
                ], ExperimentUrlListComponent.prototype, "experiment", void 0);
                ExperimentUrlListComponent = __decorate([
                    core_26.Component({
                        selector: 'experiment-url-list',
                        templateUrl: 'templates/ab-experiment-details/experiment-url-list.html',
                        directives: [
                            experiment_url_component_1.ExperimentUrlComponent,
                            ui_popover_component_3.UiPopoverComponent
                        ]
                    }), 
                    __metadata('design:paramtypes', [data_service_3.DataService, auth_4.AuthService, notification_service_3.NotificationService])
                ], ExperimentUrlListComponent);
                return ExperimentUrlListComponent;
            }());
            exports_53("ExperimentUrlListComponent", ExperimentUrlListComponent);
        }
    }
});
System.register("app/ui-slider.directive", ['angular2/core'], function(exports_54, context_54) {
    "use strict";
    var __moduleName = context_54 && context_54.id;
    var core_27;
    var UiSliderDirective;
    return {
        setters:[
            function (core_27_1) {
                core_27 = core_27_1;
            }],
        execute: function() {
            UiSliderDirective = (function () {
                function UiSliderDirective(elementRef) {
                    this.elementRef = elementRef;
                    this.config = {};
                    this.slide = new core_27.EventEmitter(false);
                    this.stop = new core_27.EventEmitter(false);
                }
                UiSliderDirective.prototype.ngOnInit = function () {
                    this.el = jQuery(this.elementRef.nativeElement).children('.ui-slider');
                    this.el.slider(this.config);
                    this.el.slider('value', this._model);
                    this.el.on('slide', this.uiOnSlide.bind(this));
                    this.el.on('slidestop', this.uiOnSlideStop.bind(this));
                };
                UiSliderDirective.prototype.uiOnSlide = function (event, ui) {
                    event.ui = ui;
                    this.slide.emit(event);
                };
                UiSliderDirective.prototype.uiOnSlideStop = function (event, ui) {
                    event.ui = ui;
                    this.stop.emit(event);
                };
                Object.defineProperty(UiSliderDirective.prototype, "model", {
                    get: function () {
                        return this._model;
                    },
                    set: function (value) {
                        this._model = value;
                        if (this.el) {
                            this.el.slider('value', this._model);
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(UiSliderDirective.prototype, "disabled", {
                    set: function (value) {
                        if (this.el) {
                            this.el.slider('option', 'disabled', value);
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                __decorate([
                    core_27.Input(), 
                    __metadata('design:type', Object)
                ], UiSliderDirective.prototype, "config", void 0);
                __decorate([
                    core_27.Output(), 
                    __metadata('design:type', core_27.EventEmitter)
                ], UiSliderDirective.prototype, "slide", void 0);
                __decorate([
                    core_27.Output(), 
                    __metadata('design:type', core_27.EventEmitter)
                ], UiSliderDirective.prototype, "stop", void 0);
                __decorate([
                    core_27.Input(), 
                    __metadata('design:type', Number), 
                    __metadata('design:paramtypes', [Number])
                ], UiSliderDirective.prototype, "model", null);
                __decorate([
                    core_27.Input(), 
                    __metadata('design:type', Boolean), 
                    __metadata('design:paramtypes', [Boolean])
                ], UiSliderDirective.prototype, "disabled", null);
                UiSliderDirective = __decorate([
                    core_27.Component({
                        selector: 'ui-slider',
                        template: '<div class="ui-slider"></div>'
                    }), 
                    __metadata('design:paramtypes', [core_27.ElementRef])
                ], UiSliderDirective);
                return UiSliderDirective;
            }());
            exports_54("UiSliderDirective", UiSliderDirective);
        }
    }
});
System.register("app/ab-experiment-details/variation-weights.service", [], function(exports_55, context_55) {
    "use strict";
    var __moduleName = context_55 && context_55.id;
    var VariationWeightsService;
    return {
        setters:[],
        execute: function() {
            VariationWeightsService = (function () {
                function VariationWeightsService() {
                }
                VariationWeightsService.prototype.applyChange = function (value, variation) {
                    var delta = value - variation.weight, limit = 100, subjects = [], list = variation.experiment.variations;
                    if (delta == 0) {
                        return variation.weight;
                    }
                    list.forEach(function (entry) {
                        if (entry == variation) {
                            return;
                        }
                        if (entry.weightLocked) {
                            limit -= entry.weight;
                        }
                        else if (delta > 0) {
                            limit -= 1;
                            if (entry.weight > 1) {
                                subjects.push(entry);
                            }
                        }
                        else {
                            subjects.push(entry);
                        }
                    });
                    if (subjects.length == 0) {
                        return variation.weight;
                    }
                    if (delta < 0) {
                        limit = variation.number == 0 ? 1 : 0;
                    }
                    var applyValue;
                    if (delta > 0) {
                        applyValue = value > limit ? limit : value;
                    }
                    else {
                        applyValue = value < limit ? limit : value;
                    }
                    // apply value
                    var applyDelta = applyValue - variation.weight;
                    variation.weight = applyValue;
                    // redistribute subject's weights
                    this.applyDelta(applyDelta, subjects);
                    return applyValue;
                };
                VariationWeightsService.prototype.refill = function (list) {
                    var subjects = [], delta = -100, total = 100;
                    list.forEach(function (entry) {
                        delta += entry.weight;
                        if (!entry.weightLocked) {
                            subjects.push(entry);
                        }
                        else {
                            total -= entry.weight;
                        }
                    });
                    if (subjects.length == 0) {
                        subjects = list;
                        total = 100;
                    }
                    this.applyDelta(delta, subjects);
                    this.roundOff(subjects, total);
                };
                VariationWeightsService.prototype.even = function (list) {
                    var subjects = [], amount = 100;
                    for (var i = 0; i < list.length; i++) {
                        var entry = list[i];
                        if (entry.weightLocked) {
                            amount -= entry.weight;
                            continue;
                        }
                        subjects.push(entry);
                    }
                    if (subjects.length == 0) {
                        //return console.error('No subjects to distribute weights among');
                        return;
                    }
                    var share = amount / subjects.length;
                    for (var i = 0; i < subjects.length; i++) {
                        subjects[i].weight = share;
                    }
                    this.roundOff(subjects, amount);
                };
                VariationWeightsService.prototype.normalize = function (list, exclude) {
                    var subjects = [], total = 100;
                    list.forEach(function (entry) {
                        if ((exclude == entry) || entry.weightLocked) {
                            total -= entry.weight;
                        }
                        else {
                            subjects.push(entry);
                        }
                    });
                    if (subjects.length) {
                        this.roundOff(subjects, total);
                    }
                };
                VariationWeightsService.prototype.applyDelta = function (delta, subjects) {
                    var overflow = 0, splice = [], share = delta / subjects.length;
                    for (var i = 0; i < subjects.length; i++) {
                        var entry = subjects[i], value = entry.weight - share;
                        if (delta > 0 && value < 1) {
                            overflow += (-1 * value + 1);
                            value = 1;
                            splice.push(i);
                        }
                        entry.weight = value;
                    }
                    if (overflow > 0) {
                        splice.map(function (n) { return subjects.splice(n, 1); });
                        this.applyDelta(overflow, subjects);
                    }
                };
                VariationWeightsService.prototype.roundOff = function (subjects, total) {
                    if (total === void 0) { total = 100; }
                    var rem = 0;
                    subjects.forEach(function (entry) {
                        if (rem > 0) {
                            entry.weight -= 1 - rem;
                        }
                        rem = entry.weight % 1;
                        entry.weight = Math.ceil(entry.weight);
                        total -= entry.weight;
                        if (entry.weight == -1) {
                            entry.weight = 0;
                            total -= 1;
                        }
                    });
                    if (total < 0) {
                        subjects[subjects.length - 1].weight += total;
                    }
                };
                VariationWeightsService.prototype.saveState = function (variation) {
                    var states = VariationWeightsService.states, state = 0x0;
                    if (variation.weightLocked) {
                        state = state | states.LOCKED;
                    }
                    if (variation.weightDisabled) {
                        state = state | states.DISABLED;
                    }
                    variation.state = state;
                };
                VariationWeightsService.prototype.restoreState = function (variation) {
                    var states = VariationWeightsService.states, state = variation.state;
                    if (state & states.LOCKED) {
                        variation.weightLocked = true;
                    }
                    if (state & states.DISABLED) {
                        variation.weightDisabled = true;
                    }
                };
                VariationWeightsService.prototype.getSubjects = function (value, variation, list) {
                    var subjects = [], delta = variation.weight - value;
                    if (delta == 0) {
                        throw new Error('Variation weight delta cannot be zero');
                    }
                    for (var i = 0; i < list.length; i++) {
                        var entry = list[i];
                        if (entry.id == variation.id
                            || entry.weightLocked
                            || (delta > 1 && entry.weight == 1)) {
                            continue;
                        }
                        subjects.push(entry);
                    }
                    return subjects;
                };
                VariationWeightsService.states = {
                    LOCKED: 0x1,
                    DISABLED: 0x2
                };
                return VariationWeightsService;
            }());
            exports_55("VariationWeightsService", VariationWeightsService);
        }
    }
});
System.register("app/ab-experiment-details/variation-weight.component", ['angular2/core', "app/ui-slider.directive", "app/model/variation", "app/ab-experiment-details/variation-weights.service", "app/ui-popover.component", "app/em/data.service"], function(exports_56, context_56) {
    "use strict";
    var __moduleName = context_56 && context_56.id;
    var core_28, ui_slider_directive_1, variation_4, variation_weights_service_1, ui_popover_component_4, data_service_4;
    var VariationWeightComponent;
    return {
        setters:[
            function (core_28_1) {
                core_28 = core_28_1;
            },
            function (ui_slider_directive_1_1) {
                ui_slider_directive_1 = ui_slider_directive_1_1;
            },
            function (variation_4_1) {
                variation_4 = variation_4_1;
            },
            function (variation_weights_service_1_1) {
                variation_weights_service_1 = variation_weights_service_1_1;
            },
            function (ui_popover_component_4_1) {
                ui_popover_component_4 = ui_popover_component_4_1;
            },
            function (data_service_4_1) {
                data_service_4 = data_service_4_1;
            }],
        execute: function() {
            VariationWeightComponent = (function () {
                function VariationWeightComponent(weights, data) {
                    this.weights = weights;
                    this.data = data;
                    this.changeWeight = new core_28.EventEmitter(false);
                    this.afterWeightChanged = new core_28.EventEmitter(false);
                    this.config = {
                        min: 0,
                        max: 100,
                        step: 1
                    };
                }
                VariationWeightComponent.prototype.ngOnInit = function () {
                };
                VariationWeightComponent.prototype.onSlide = function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    if (this.variation.weightLocked) {
                        return;
                    }
                    this.weights.applyChange(event.ui.value, this.variation);
                };
                VariationWeightComponent.prototype.onSlideStop = function (event) {
                    event.so = {};
                    this.afterWeightChanged.emit(event);
                };
                VariationWeightComponent.prototype.toggleLock = function (event) {
                    event.stopPropagation();
                    this.variation.weightLocked = !this.variation.weightLocked;
                    this.weights.saveState(this.variation);
                    this.data.persist();
                };
                __decorate([
                    core_28.Input(), 
                    __metadata('design:type', variation_4.Variation)
                ], VariationWeightComponent.prototype, "variation", void 0);
                __decorate([
                    core_28.Output(), 
                    __metadata('design:type', core_28.EventEmitter)
                ], VariationWeightComponent.prototype, "changeWeight", void 0);
                __decorate([
                    core_28.Output(), 
                    __metadata('design:type', core_28.EventEmitter)
                ], VariationWeightComponent.prototype, "afterWeightChanged", void 0);
                VariationWeightComponent = __decorate([
                    core_28.Component({
                        selector: 'variation-weight',
                        templateUrl: 'templates/ab-experiment-details/variation-weight.html',
                        directives: [
                            ui_slider_directive_1.UiSliderDirective,
                            ui_popover_component_4.UiPopoverComponent
                        ]
                    }), 
                    __metadata('design:paramtypes', [variation_weights_service_1.VariationWeightsService, data_service_4.DataService])
                ], VariationWeightComponent);
                return VariationWeightComponent;
            }());
            exports_56("VariationWeightComponent", VariationWeightComponent);
        }
    }
});
System.register("app/ab-experiment-details/variation-snippets.service", [], function(exports_57, context_57) {
    "use strict";
    var __moduleName = context_57 && context_57.id;
    var VariationSnippetsService;
    return {
        setters:[],
        execute: function() {
            VariationSnippetsService = (function () {
                function VariationSnippetsService() {
                    this._components = [];
                    this._active = 0;
                }
                VariationSnippetsService.prototype.addComponent = function (component) {
                    this._components.push(component);
                };
                Object.defineProperty(VariationSnippetsService.prototype, "active", {
                    get: function () {
                        return this._active;
                    },
                    enumerable: true,
                    configurable: true
                });
                VariationSnippetsService.prototype.activate = function (index, exclude, include) {
                    for (var i = 0; i < this._components.length; i++) {
                        var entry = this._components[i];
                        if (exclude && entry.variation.number == exclude.number) {
                            continue;
                        }
                        else if (include && entry.variation.number != include.number) {
                            continue;
                        }
                        entry.doActivate(index);
                        this._active = index;
                    }
                };
                return VariationSnippetsService;
            }());
            exports_57("VariationSnippetsService", VariationSnippetsService);
        }
    }
});
System.register("app/ab-experiment-factory.service", ['angular2/core', "app/auth", "app/model/experiment", "app/model/experiment-url", "app/model/variation", "app/model/variation-snippet", "app/model/snippet-value"], function(exports_58, context_58) {
    "use strict";
    var __moduleName = context_58 && context_58.id;
    var core_29, auth_5, experiment_6, experiment_url_6, variation_5, variation_snippet_4, snippet_value_4;
    var AbExperimentFactoryService;
    return {
        setters:[
            function (core_29_1) {
                core_29 = core_29_1;
            },
            function (auth_5_1) {
                auth_5 = auth_5_1;
            },
            function (experiment_6_1) {
                experiment_6 = experiment_6_1;
            },
            function (experiment_url_6_1) {
                experiment_url_6 = experiment_url_6_1;
            },
            function (variation_5_1) {
                variation_5 = variation_5_1;
            },
            function (variation_snippet_4_1) {
                variation_snippet_4 = variation_snippet_4_1;
            },
            function (snippet_value_4_1) {
                snippet_value_4 = snippet_value_4_1;
            }],
        execute: function() {
            AbExperimentFactoryService = (function () {
                function AbExperimentFactoryService(/*private data: ExperimentsService,*/ auth) {
                    this.auth = auth;
                }
                AbExperimentFactoryService.prototype.createExperiment = function () {
                    return new experiment_6.Experiment();
                };
                AbExperimentFactoryService.prototype.createExperimentUrl = function (experiment) {
                    return new experiment_url_6.ExperimentUrl();
                };
                AbExperimentFactoryService.prototype.createVariation = function (experiment) {
                    return new variation_5.Variation();
                };
                AbExperimentFactoryService.prototype.createVariationSnippet = function (experiment) {
                    return new variation_snippet_4.VariationSnippet();
                };
                AbExperimentFactoryService.prototype.createSnippetValue = function (variation, snippet) {
                    return new snippet_value_4.SnippetValue();
                };
                AbExperimentFactoryService = __decorate([
                    core_29.Injectable(), 
                    __metadata('design:paramtypes', [auth_5.AuthService])
                ], AbExperimentFactoryService);
                return AbExperimentFactoryService;
            }());
            exports_58("AbExperimentFactoryService", AbExperimentFactoryService);
        }
    }
});
System.register("app/ab-experiment-details/variation-snippets.component", ['angular2/core', "app/ab-experiment-details/variation-snippets.service", "app/model/variation", "app/model/variation-snippet", "app/auth", "app/model/snippet-value", "app/ui-popover.component", "app/ab-experiment-factory.service", "app/ui-edit-inline.directive", "app/em/data.service", "app/notification.service"], function(exports_59, context_59) {
    "use strict";
    var __moduleName = context_59 && context_59.id;
    var core_30, variation_snippets_service_1, variation_6, variation_snippet_5, auth_6, snippet_value_5, ui_popover_component_5, ab_experiment_factory_service_1, ui_edit_inline_directive_2, data_service_5, notification_service_4;
    var VariationSnippetsComponent;
    return {
        setters:[
            function (core_30_1) {
                core_30 = core_30_1;
            },
            function (variation_snippets_service_1_1) {
                variation_snippets_service_1 = variation_snippets_service_1_1;
            },
            function (variation_6_1) {
                variation_6 = variation_6_1;
            },
            function (variation_snippet_5_1) {
                variation_snippet_5 = variation_snippet_5_1;
            },
            function (auth_6_1) {
                auth_6 = auth_6_1;
            },
            function (snippet_value_5_1) {
                snippet_value_5 = snippet_value_5_1;
            },
            function (ui_popover_component_5_1) {
                ui_popover_component_5 = ui_popover_component_5_1;
            },
            function (ab_experiment_factory_service_1_1) {
                ab_experiment_factory_service_1 = ab_experiment_factory_service_1_1;
            },
            function (ui_edit_inline_directive_2_1) {
                ui_edit_inline_directive_2 = ui_edit_inline_directive_2_1;
            },
            function (data_service_5_1) {
                data_service_5 = data_service_5_1;
            },
            function (notification_service_4_1) {
                notification_service_4 = notification_service_4_1;
            }],
        execute: function() {
            VariationSnippetsComponent = (function () {
                function VariationSnippetsComponent(elementRef, data, factory, snippetsService, auth, notifications) {
                    this.elementRef = elementRef;
                    this.data = data;
                    this.factory = factory;
                    this.snippetsService = snippetsService;
                    this.auth = auth;
                    this.notifications = notifications;
                    this.activated = new core_30.EventEmitter();
                }
                VariationSnippetsComponent.prototype.ngOnInit = function () {
                    if (this.variation.snippetValues.length > 0) {
                        this._active = 0;
                    }
                    this.snippetsService.addComponent(this);
                };
                Object.defineProperty(VariationSnippetsComponent.prototype, "experiment", {
                    get: function () {
                        return this.variation.experiment;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(VariationSnippetsComponent.prototype, "snippetValues", {
                    get: function () {
                        return this.variation.snippetValues.items;
                    },
                    enumerable: true,
                    configurable: true
                });
                VariationSnippetsComponent.prototype.isActive = function (index) {
                    return this._active === index;
                };
                VariationSnippetsComponent.prototype.doActivate = function (index) {
                    // @todo test boundaries
                    this._active = index;
                };
                VariationSnippetsComponent.prototype.onTabClick = function (index) {
                    this.doActivate(index);
                    this.activated.emit({
                        index: index,
                        variation: this.variation
                    });
                    this.snippetsService.activate(index, this.variation);
                };
                VariationSnippetsComponent.prototype.update = function () {
                    this.data.persist(500);
                };
                VariationSnippetsComponent.prototype.create = function () {
                    var _this = this;
                    this.data.create(variation_snippet_5.VariationSnippet, this.experiment, { number: this.experiment.snippets.length }, false)
                        .then(function (snippet) {
                        _this.experiment.snippets.push(snippet);
                        var done = 0;
                        _this.experiment.variations.forEach(function (variation) {
                            _this.data.create(snippet_value_5.SnippetValue, snippet, { variation: variation.number })
                                .then(function (value) {
                                variation.snippetValues.push(value);
                                if (++done == _this.experiment.variations.length) {
                                    _this.doActivate(snippet.number);
                                    _this.snippetsService.activate(snippet.number, _this.variation);
                                }
                            })
                                .catch(function (error) {
                                _this.notifications.error('Error occurred while creating variation snippet. Check error console for details.');
                            });
                        });
                    })
                        .catch(function (error) {
                        _this.notifications.error('Error occurred while creating variation snippet. Check error console for details.');
                    });
                };
                //noinspection ReservedWordAsName
                VariationSnippetsComponent.prototype.delete = function (index) {
                    var _this = this;
                    if (this.variation.snippetValues.length == 1) {
                        this.notifications.error('Cannot delete the only snippet.');
                        return;
                    }
                    var snippets = this.experiment.snippets.items;
                    var snippet = snippets[index];
                    if (snippet == undefined) {
                        this.notifications.error('Error has occurred while removing the snippet. Check error console for details.');
                        return console.error("Can't find snippet " + index);
                    }
                    snippets.splice(snippet.number, 1);
                    this.experiment.variations.forEach(function (variation) {
                        variation.snippetValues.splice(snippet.number, 1);
                    });
                    this.data.remove(snippet)
                        .then(function () {
                        //
                    })
                        .catch(function (error) {
                        _this.notifications.error('Error has occurred while removing snippet. Check error console for details.');
                        console.error(error);
                    });
                    for (var n = index; n < snippets.length; n++) {
                        snippets[n].number = n;
                    }
                    var $this = this;
                    var next = index == 0 ? 0 : index - 1;
                    this.doActivate(next);
                    this.snippetsService.activate(next, this.variation);
                };
                VariationSnippetsComponent.prototype.onPopoverInit = function (component) {
                    component.setOwner(this);
                };
                VariationSnippetsComponent.prototype.snippetTabPopover = function (options, component, service) {
                    var id = this.variation.number == 0 ?
                        'ab_experiment_details.snippet_tab_header_control' :
                        'ab_experiment_details.snippet_tab_header_variation';
                    var message = service.message(id);
                    var content = service.message(component.uiPopover);
                    return message.content + '<br>' + content.content;
                };
                VariationSnippetsComponent.prototype.deleteSnippetPopover = function (options, component, service) {
                    var content = service.message(component.uiPopover);
                    if (this.variation.snippetValues.length > 1) {
                        return content.content;
                    }
                    var message = service.message('ab_experiment_details.cant_delete_last_snippet');
                    return content.content + '<br>' + 'NOTICE: ' + message.content;
                };
                VariationSnippetsComponent.prototype.blur = function (event) {
                    jQuery(event.srcElement).blur();
                };
                __decorate([
                    core_30.Input(), 
                    __metadata('design:type', variation_6.Variation)
                ], VariationSnippetsComponent.prototype, "variation", void 0);
                __decorate([
                    core_30.Output(), 
                    __metadata('design:type', core_30.EventEmitter)
                ], VariationSnippetsComponent.prototype, "activated", void 0);
                VariationSnippetsComponent = __decorate([
                    core_30.Component({
                        selector: 'variation-snippets',
                        templateUrl: 'templates/ab-experiment-details/variation-snippets.html',
                        directives: [
                            ui_edit_inline_directive_2.UiEditInlineDirective,
                            ui_popover_component_5.UiPopoverComponent
                        ]
                    }), 
                    __metadata('design:paramtypes', [core_30.ElementRef, data_service_5.DataService, ab_experiment_factory_service_1.AbExperimentFactoryService, variation_snippets_service_1.VariationSnippetsService, auth_6.AuthService, notification_service_4.NotificationService])
                ], VariationSnippetsComponent);
                return VariationSnippetsComponent;
            }());
            exports_59("VariationSnippetsComponent", VariationSnippetsComponent);
        }
    }
});
System.register("app/ab-experiment-details/experiment-variation.component", ['angular2/core', "app/dom-disabled.directive", "app/ab-experiment-details/variation-weight.component", "app/ab-experiment-details/variation-snippets.component", "app/model/variation", "app/ui-popover.component", "app/ui-edit-inline.directive", "app/em/data.service"], function(exports_60, context_60) {
    "use strict";
    var __moduleName = context_60 && context_60.id;
    var core_31, dom_disabled_directive_2, variation_weight_component_1, variation_snippets_component_1, variation_7, ui_popover_component_6, ui_edit_inline_directive_3, data_service_6;
    var ExperimentVariationComponent;
    return {
        setters:[
            function (core_31_1) {
                core_31 = core_31_1;
            },
            function (dom_disabled_directive_2_1) {
                dom_disabled_directive_2 = dom_disabled_directive_2_1;
            },
            function (variation_weight_component_1_1) {
                variation_weight_component_1 = variation_weight_component_1_1;
            },
            function (variation_snippets_component_1_1) {
                variation_snippets_component_1 = variation_snippets_component_1_1;
            },
            function (variation_7_1) {
                variation_7 = variation_7_1;
            },
            function (ui_popover_component_6_1) {
                ui_popover_component_6 = ui_popover_component_6_1;
            },
            function (ui_edit_inline_directive_3_1) {
                ui_edit_inline_directive_3 = ui_edit_inline_directive_3_1;
            },
            function (data_service_6_1) {
                data_service_6 = data_service_6_1;
            }],
        execute: function() {
            ExperimentVariationComponent = (function () {
                function ExperimentVariationComponent(data) {
                    this.data = data;
                    this.changeWeight = new core_31.EventEmitter(false);
                    this.onAfterWeightChanged = new core_31.EventEmitter(false);
                    this.snippetActivated = new core_31.EventEmitter();
                    this.onInit = new core_31.EventEmitter();
                    this.onDelete = new core_31.EventEmitter();
                    this.onOpen = new core_31.EventEmitter();
                    this.opened = false;
                }
                ExperimentVariationComponent.prototype.ngOnInit = function () {
                    this.onInit.emit(this);
                };
                ExperimentVariationComponent.prototype.onChangeWeight = function (event) {
                    event.so.variation = this.variation;
                    this.changeWeight.emit(event);
                };
                ExperimentVariationComponent.prototype.afterWeightChanged = function (event) {
                    event.so.variation = this.variation;
                    this.onAfterWeightChanged.emit(event);
                };
                ExperimentVariationComponent.prototype.onSnippetActivated = function (event) {
                    this.snippetActivated.emit(event);
                };
                ExperimentVariationComponent.prototype.update = function () {
                    this.data.persist(500);
                };
                //noinspection ReservedWordAsName
                ExperimentVariationComponent.prototype.delete = function () {
                    this.onDelete.emit(this.variation);
                };
                ExperimentVariationComponent.prototype.open = function () {
                    this.onOpen.emit(this);
                    this.opened = true;
                };
                ExperimentVariationComponent.prototype.close = function () {
                    this.opened = false;
                };
                ExperimentVariationComponent.prototype.toggle = function (event) {
                    event.stopPropagation();
                    if (this.opened) {
                        this.close();
                    }
                    else {
                        this.open();
                    }
                };
                __decorate([
                    core_31.Input(), 
                    __metadata('design:type', variation_7.Variation)
                ], ExperimentVariationComponent.prototype, "variation", void 0);
                __decorate([
                    core_31.Output(), 
                    __metadata('design:type', core_31.EventEmitter)
                ], ExperimentVariationComponent.prototype, "changeWeight", void 0);
                __decorate([
                    core_31.Output('afterWeightChanged'), 
                    __metadata('design:type', core_31.EventEmitter)
                ], ExperimentVariationComponent.prototype, "onAfterWeightChanged", void 0);
                __decorate([
                    core_31.Output(), 
                    __metadata('design:type', core_31.EventEmitter)
                ], ExperimentVariationComponent.prototype, "snippetActivated", void 0);
                __decorate([
                    core_31.Output('init'), 
                    __metadata('design:type', core_31.EventEmitter)
                ], ExperimentVariationComponent.prototype, "onInit", void 0);
                __decorate([
                    core_31.Output('delete'), 
                    __metadata('design:type', core_31.EventEmitter)
                ], ExperimentVariationComponent.prototype, "onDelete", void 0);
                __decorate([
                    core_31.Output('open'), 
                    __metadata('design:type', core_31.EventEmitter)
                ], ExperimentVariationComponent.prototype, "onOpen", void 0);
                ExperimentVariationComponent = __decorate([
                    core_31.Component({
                        selector: 'experiment-variation',
                        templateUrl: 'templates/ab-experiment-details/experiment-variation.html',
                        directives: [
                            ui_edit_inline_directive_3.UiEditInlineDirective,
                            ui_popover_component_6.UiPopoverComponent,
                            dom_disabled_directive_2.DomDisabledDirective,
                            variation_weight_component_1.VariationWeightComponent,
                            variation_snippets_component_1.VariationSnippetsComponent
                        ],
                        encapsulation: core_31.ViewEncapsulation.Emulated
                    }), 
                    __metadata('design:paramtypes', [data_service_6.DataService])
                ], ExperimentVariationComponent);
                return ExperimentVariationComponent;
            }());
            exports_60("ExperimentVariationComponent", ExperimentVariationComponent);
        }
    }
});
System.register("app/ab-experiment-details/experiment-variation-list.component", ['angular2/core', "app/ab-experiment-details/experiment-variation.component", "app/ab-experiment-details/variation-weights.service", "app/model/experiment", "app/model/variation", "app/ab-experiment-details/variation-snippets.service", "app/auth", "app/model/snippet-value", "app/ui-popover.component", "app/ab-experiment-factory.service", "app/em/data.service", "app/notification.service"], function(exports_61, context_61) {
    "use strict";
    var __moduleName = context_61 && context_61.id;
    var core_32, experiment_variation_component_1, variation_weights_service_2, experiment_7, variation_8, variation_snippets_service_2, auth_7, snippet_value_6, ui_popover_component_7, ab_experiment_factory_service_2, data_service_7, notification_service_5;
    var ExperimentVariationListComponent;
    return {
        setters:[
            function (core_32_1) {
                core_32 = core_32_1;
            },
            function (experiment_variation_component_1_1) {
                experiment_variation_component_1 = experiment_variation_component_1_1;
            },
            function (variation_weights_service_2_1) {
                variation_weights_service_2 = variation_weights_service_2_1;
            },
            function (experiment_7_1) {
                experiment_7 = experiment_7_1;
            },
            function (variation_8_1) {
                variation_8 = variation_8_1;
            },
            function (variation_snippets_service_2_1) {
                variation_snippets_service_2 = variation_snippets_service_2_1;
            },
            function (auth_7_1) {
                auth_7 = auth_7_1;
            },
            function (snippet_value_6_1) {
                snippet_value_6 = snippet_value_6_1;
            },
            function (ui_popover_component_7_1) {
                ui_popover_component_7 = ui_popover_component_7_1;
            },
            function (ab_experiment_factory_service_2_1) {
                ab_experiment_factory_service_2 = ab_experiment_factory_service_2_1;
            },
            function (data_service_7_1) {
                data_service_7 = data_service_7_1;
            },
            function (notification_service_5_1) {
                notification_service_5 = notification_service_5_1;
            }],
        execute: function() {
            ExperimentVariationListComponent = (function () {
                function ExperimentVariationListComponent(data, factory, weights, snippets, auth, notifications) {
                    this.data = data;
                    this.factory = factory;
                    this.weights = weights;
                    this.snippets = snippets;
                    this.auth = auth;
                    this.notifications = notifications;
                    this._variationComponents = [];
                }
                ExperimentVariationListComponent.prototype.registerVariationComponent = function (component) {
                    this._variationComponents.push(component);
                };
                ExperimentVariationListComponent.prototype.onVariationOpen = function (component) {
                    this._variationComponents.forEach(function (entry) {
                        if (entry != component) {
                            entry.close();
                        }
                    });
                };
                Object.defineProperty(ExperimentVariationListComponent.prototype, "variations", {
                    get: function () {
                        return this.experiment.variations.items;
                    },
                    enumerable: true,
                    configurable: true
                });
                ExperimentVariationListComponent.prototype.onChangeWeight = function (event) {
                    var value = event.so.value, variation = event.so.variation;
                    event.preventDefault();
                    event.stopPropagation();
                };
                ExperimentVariationListComponent.prototype.afterWeightChanged = function (event) {
                    this.weights.normalize(this.variations, event.so.variation);
                    this.data.persist();
                };
                ExperimentVariationListComponent.prototype.evenWeights = function () {
                    this.weights.even(this.variations);
                    this.data.persist();
                };
                ExperimentVariationListComponent.prototype.create = function () {
                    var _this = this;
                    this.data.create(variation_8.Variation, this.experiment, { number: this.experiment.variations.length })
                        .then(function (variation) {
                        variation.experiment = _this.experiment;
                        variation.experiment.snippets.forEach(function (snippet) {
                            _this.data.create(snippet_value_6.SnippetValue, snippet, { variation: variation.number })
                                .then(function (value) {
                                variation.snippetValues.items[snippet.number] = value;
                                if (snippet.number == _this.snippets.active) {
                                    _this.snippets.activate(snippet.number, null, variation);
                                }
                            })
                                .catch(function (error) {
                                _this.notifications.error('Error occurred while creating experiment variation. Check error console for details.');
                                console.error(error);
                            });
                        });
                        variation.experiment.variations.push(variation);
                    })
                        .catch(function (error) {
                        _this.notifications.error('Error occurred while creating experiment variation. Check error console for details.');
                    });
                };
                //noinspection ReservedWordAsName
                ExperimentVariationListComponent.prototype.delete = function (variation) {
                    var _this = this;
                    if (variation.number == 0) {
                        return this.notifications.error('Cannot delete control variation!');
                    }
                    this.data.remove(variation)
                        .then(function () {
                        var index = _this.experiment.variations.indexOf(variation);
                        _this.experiment.variations.remove(variation);
                        _this.weights.refill(_this.experiment.variations.items);
                        for (var i = index, len = _this.experiment.variations.items.length; i < len; i++) {
                            _this.experiment.variations.items[i].number = i;
                        }
                        _this.data.persist();
                    })
                        .catch(function (error) {
                        _this.notifications.error('Error has occurred while removing the experiment variation. Check error console for details.');
                    });
                };
                __decorate([
                    core_32.Input(), 
                    __metadata('design:type', experiment_7.Experiment)
                ], ExperimentVariationListComponent.prototype, "experiment", void 0);
                ExperimentVariationListComponent = __decorate([
                    core_32.Component({
                        selector: 'experiment-variation-list',
                        templateUrl: 'templates/ab-experiment-details/experiment-variation-list.html',
                        directives: [
                            experiment_variation_component_1.ExperimentVariationComponent,
                            ui_popover_component_7.UiPopoverComponent
                        ],
                        providers: [
                            ab_experiment_factory_service_2.AbExperimentFactoryService,
                            variation_weights_service_2.VariationWeightsService,
                            variation_snippets_service_2.VariationSnippetsService
                        ]
                    }), 
                    __metadata('design:paramtypes', [data_service_7.DataService, ab_experiment_factory_service_2.AbExperimentFactoryService, variation_weights_service_2.VariationWeightsService, variation_snippets_service_2.VariationSnippetsService, auth_7.AuthService, notification_service_5.NotificationService])
                ], ExperimentVariationListComponent);
                return ExperimentVariationListComponent;
            }());
            exports_61("ExperimentVariationListComponent", ExperimentVariationListComponent);
        }
    }
});
System.register("app/ab-experiment-details/experiment-goal.component", ['angular2/core', "app/em/data.service", "app/model/goal", "app/ui-popover.component", "app/ui-edit-inline.directive"], function(exports_62, context_62) {
    "use strict";
    var __moduleName = context_62 && context_62.id;
    var core_33, data_service_8, goal_4, ui_popover_component_8, ui_edit_inline_directive_4;
    var ExperimentGoalComponent;
    return {
        setters:[
            function (core_33_1) {
                core_33 = core_33_1;
            },
            function (data_service_8_1) {
                data_service_8 = data_service_8_1;
            },
            function (goal_4_1) {
                goal_4 = goal_4_1;
            },
            function (ui_popover_component_8_1) {
                ui_popover_component_8 = ui_popover_component_8_1;
            },
            function (ui_edit_inline_directive_4_1) {
                ui_edit_inline_directive_4 = ui_edit_inline_directive_4_1;
            }],
        execute: function() {
            ExperimentGoalComponent = (function () {
                function ExperimentGoalComponent(data) {
                    this.data = data;
                    this.onDelete = new core_33.EventEmitter();
                }
                ExperimentGoalComponent.prototype.update = function () {
                    this.data.persist(500);
                };
                ExperimentGoalComponent.prototype.delete = function () {
                    this.onDelete.emit(this.goal);
                };
                ExperimentGoalComponent.prototype.onPopoverInit = function (component) {
                    component.setOwner(this);
                };
                ExperimentGoalComponent.prototype.deleteGoalPopover = function (options, component, service) {
                    var content = service.message(component.uiPopover);
                    if (this.list.length > 1) {
                        return content.content;
                    }
                    var message = service.message('ab_experiment_details.cant_delete_last_goal');
                    return content.content + '<br>' + 'NOTICE: ' + message.content;
                };
                __decorate([
                    core_33.Input(), 
                    __metadata('design:type', goal_4.Goal)
                ], ExperimentGoalComponent.prototype, "goal", void 0);
                __decorate([
                    core_33.Input(), 
                    __metadata('design:type', Array)
                ], ExperimentGoalComponent.prototype, "list", void 0);
                __decorate([
                    core_33.Output('delete'), 
                    __metadata('design:type', core_33.EventEmitter)
                ], ExperimentGoalComponent.prototype, "onDelete", void 0);
                ExperimentGoalComponent = __decorate([
                    core_33.Component({
                        selector: 'experiment-goal',
                        templateUrl: 'templates/ab-experiment-details/experiment-goal.html',
                        directives: [
                            ui_edit_inline_directive_4.UiEditInlineDirective,
                            ui_popover_component_8.UiPopoverComponent
                        ]
                    }), 
                    __metadata('design:paramtypes', [data_service_8.DataService])
                ], ExperimentGoalComponent);
                return ExperimentGoalComponent;
            }());
            exports_62("ExperimentGoalComponent", ExperimentGoalComponent);
        }
    }
});
System.register("app/ab-experiment-details/experiment-goal-list.component", ['angular2/core', "app/ab-experiment-details/experiment-goal.component", "app/auth", "app/ui-popover.component", "app/model/experiment", "app/model/goal", "app/em/data.service", "app/notification.service", "app/model/goal-destination"], function(exports_63, context_63) {
    "use strict";
    var __moduleName = context_63 && context_63.id;
    var core_34, experiment_goal_component_1, auth_8, ui_popover_component_9, experiment_8, goal_5, data_service_9, notification_service_6, goal_destination_3;
    var ExperimentGoalListComponent;
    return {
        setters:[
            function (core_34_1) {
                core_34 = core_34_1;
            },
            function (experiment_goal_component_1_1) {
                experiment_goal_component_1 = experiment_goal_component_1_1;
            },
            function (auth_8_1) {
                auth_8 = auth_8_1;
            },
            function (ui_popover_component_9_1) {
                ui_popover_component_9 = ui_popover_component_9_1;
            },
            function (experiment_8_1) {
                experiment_8 = experiment_8_1;
            },
            function (goal_5_1) {
                goal_5 = goal_5_1;
            },
            function (data_service_9_1) {
                data_service_9 = data_service_9_1;
            },
            function (notification_service_6_1) {
                notification_service_6 = notification_service_6_1;
            },
            function (goal_destination_3_1) {
                goal_destination_3 = goal_destination_3_1;
            }],
        execute: function() {
            ExperimentGoalListComponent = (function () {
                function ExperimentGoalListComponent(data, auth, notifications) {
                    this.data = data;
                    this.auth = auth;
                    this.notifications = notifications;
                }
                Object.defineProperty(ExperimentGoalListComponent.prototype, "goals", {
                    get: function () {
                        return this.experiment.goals.items;
                    },
                    enumerable: true,
                    configurable: true
                });
                ExperimentGoalListComponent.prototype.create = function () {
                    var _this = this;
                    this.data.create(goal_5.Goal, this.experiment)
                        .then(function (goal) {
                        _this.experiment.goals.push(goal);
                        _this.data.create(goal_destination_3.GoalDestination, goal)
                            .then(function (destination) {
                            goal.destinations.push(destination);
                        })
                            .catch(function (error) {
                            _this.notifications.error('Error has occurred while adding experiment goal. Check error console for details.');
                            console.error(error);
                        });
                    })
                        .catch(function (error) {
                        _this.notifications.error('Error has occurred while adding experiment goal. Check error console for details.');
                        console.error(error);
                    });
                };
                //noinspection ReservedWordAsName
                ExperimentGoalListComponent.prototype.delete = function (goal) {
                    var _this = this;
                    if (this.experiment.goals.length == 1) {
                        return;
                    }
                    this.experiment.goals.remove(goal);
                    this.data.remove(goal)
                        .catch(function (error) {
                        _this.notifications.error('Error has occurred while removing experiment goal. Check error console for details.');
                        console.error(error);
                    });
                };
                __decorate([
                    core_34.Input(), 
                    __metadata('design:type', experiment_8.Experiment)
                ], ExperimentGoalListComponent.prototype, "experiment", void 0);
                ExperimentGoalListComponent = __decorate([
                    core_34.Component({
                        selector: 'experiment-goal-list',
                        templateUrl: 'templates/ab-experiment-details/experiment-goal-list.html',
                        directives: [
                            experiment_goal_component_1.ExperimentGoalComponent,
                            ui_popover_component_9.UiPopoverComponent
                        ]
                    }), 
                    __metadata('design:paramtypes', [data_service_9.DataService, auth_8.AuthService, notification_service_6.NotificationService])
                ], ExperimentGoalListComponent);
                return ExperimentGoalListComponent;
            }());
            exports_63("ExperimentGoalListComponent", ExperimentGoalListComponent);
        }
    }
});
System.register("app/ui-js-field.directive", ['angular2/core'], function(exports_64, context_64) {
    "use strict";
    var __moduleName = context_64 && context_64.id;
    var core_35;
    var UiJsFieldDirective;
    return {
        setters:[
            function (core_35_1) {
                core_35 = core_35_1;
            }],
        execute: function() {
            UiJsFieldDirective = (function () {
                function UiJsFieldDirective(elementRef) {
                    this.elementRef = elementRef;
                }
                Object.defineProperty(UiJsFieldDirective.prototype, "code", {
                    get: function () {
                        return this._code;
                    },
                    set: function (value) {
                        for (var k in this.params) {
                            var v = this.params[k];
                            var re = new RegExp('\\{\\{\\s*' + k + '\\s*}}');
                            value = value.replace(re, v);
                        }
                        this._code = value;
                        if (this.rows == undefined) {
                            this.rows = this._code.split("\n").length;
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                UiJsFieldDirective.prototype.select = function (event) {
                    event.target.select();
                };
                __decorate([
                    core_35.Input(), 
                    __metadata('design:type', Number)
                ], UiJsFieldDirective.prototype, "rows", void 0);
                __decorate([
                    core_35.Input(), 
                    __metadata('design:type', Object)
                ], UiJsFieldDirective.prototype, "params", void 0);
                __decorate([
                    core_35.Input(), 
                    __metadata('design:type', String), 
                    __metadata('design:paramtypes', [String])
                ], UiJsFieldDirective.prototype, "code", null);
                UiJsFieldDirective = __decorate([
                    core_35.Component({
                        selector: 'ui-js-field',
                        template: "\n    <div class=\"component-root\">\n        <textarea class=\"js-field\" rows=\"{{ rows }}\" readonly spellcheck=\"false\"\n            (click)=\"select($event)\">{{ code }}</textarea>\n    </div>\n    ",
                        styles: [
                            "\n        textarea {\n            color: #212F40;\n            width: 100%;\n            background: transparent;\n            border: 0 solid transparent;\n            outline: 0 solid transparent;\n            position: absolute;\n            padding: 0;\n            left: -1px;\n            top: -1px;\n            line-height: 1.4;\n            overflow: hidden;\n            resize: none;\n            position: relative !important;\n            top: 0 !important;\n            border: 1px solid;\n            border-color: #d9d9d9 !important;\n            border-top-color: #c0c0c0;\n            background-color: #fff !important;\n            padding-bottom: 10px !important;\n            -webkit-border-radius: 1px;\n            -moz-border-radius: 1px;\n            border-radius: 1px;\n            -webkit-box-shadow: none;\n            -moz-box-shadow: none;\n            box-shadow: none;\n            color: #333;\n            font-size: 13px;\n            line-height: 18px;\n            padding: 1px 8px;\n        }\n        "
                        ]
                    }), 
                    __metadata('design:paramtypes', [core_35.ElementRef])
                ], UiJsFieldDirective);
                return UiJsFieldDirective;
            }());
            exports_64("UiJsFieldDirective", UiJsFieldDirective);
        }
    }
});
System.register("app/experiment-publish.service", ['angular2/core', 'angular2/http'], function(exports_65, context_65) {
    "use strict";
    var __moduleName = context_65 && context_65.id;
    var core_36, http_4;
    var ExperimentPublishService;
    return {
        setters:[
            function (core_36_1) {
                core_36 = core_36_1;
            },
            function (http_4_1) {
                http_4 = http_4_1;
            }],
        execute: function() {
            ExperimentPublishService = (function () {
                function ExperimentPublishService(http) {
                    this.http = http;
                    this._publishing = false;
                }
                Object.defineProperty(ExperimentPublishService.prototype, "publishing", {
                    get: function () {
                        return this._publishing;
                    },
                    enumerable: true,
                    configurable: true
                });
                ExperimentPublishService.prototype.publish = function (experiment) {
                    var _this = this;
                    this._publishing = true;
                    var req = this.http.get('/cache/experiment/publish/' + experiment.id);
                    return new Promise(function (resolve, reject) {
                        req.subscribe(function (res) {
                            experiment.pristine = true;
                            resolve(res);
                        }, function (err) {
                            console.error(err);
                            reject(err);
                        }, function () {
                            _this._publishing = false;
                        });
                    });
                };
                ExperimentPublishService.prototype.errors = function (experiment) {
                    var errors = [];
                    if (experiment.variations.length == 1) {
                        errors.push('no_variations');
                    }
                    if (!experiment.urls) {
                        errors.push('invalid_url');
                    }
                    var validUrl;
                    experiment.urls.forEach(function (url) {
                        if (url.pattern) {
                            validUrl = true;
                        }
                    });
                    if (!validUrl) {
                        errors.push('invalid_url');
                    }
                    if (!experiment.goals) {
                        errors.push('invalid_goal');
                    }
                    var validGoal;
                    experiment.goals.forEach(function (goal) {
                        goal.destinations.forEach(function (destination) {
                            if (destination.value) {
                                validGoal = true;
                            }
                        });
                    });
                    if (!validGoal) {
                        errors.push('invalid_goal');
                    }
                    return errors;
                };
                ExperimentPublishService = __decorate([
                    core_36.Injectable(), 
                    __metadata('design:paramtypes', [http_4.Http])
                ], ExperimentPublishService);
                return ExperimentPublishService;
            }());
            exports_65("ExperimentPublishService", ExperimentPublishService);
        }
    }
});
System.register("app/ui-ladda.directive", ['angular2/core'], function(exports_66, context_66) {
    "use strict";
    var __moduleName = context_66 && context_66.id;
    var core_37;
    var UiLaddaDirective;
    return {
        setters:[
            function (core_37_1) {
                core_37 = core_37_1;
            }],
        execute: function() {
            UiLaddaDirective = (function () {
                function UiLaddaDirective(elementRef) {
                    this.elementRef = elementRef;
                }
                Object.defineProperty(UiLaddaDirective.prototype, "property", {
                    set: function (value) {
                        if (value == true) {
                            this.start();
                        }
                        else {
                            this.stop();
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                UiLaddaDirective.prototype.start = function () {
                    if (this.ladda != undefined) {
                        jQuery(this.elementRef.nativeElement)
                            .css({ 'pointer-events': 'none' });
                        this.ladda.start();
                    }
                };
                UiLaddaDirective.prototype.stop = function () {
                    if (this.ladda != undefined) {
                        jQuery(this.elementRef.nativeElement)
                            .css({ 'pointer-events': 'auto' });
                        this.ladda.stop();
                    }
                };
                UiLaddaDirective.prototype.ngOnInit = function () {
                    jQuery(this.elementRef.nativeElement)
                        .addClass('ladda-button')
                        .attr('data-style', 'slide-right');
                    this.ladda = Ladda.create(this.elementRef.nativeElement);
                };
                __decorate([
                    core_37.Input('ladda'), 
                    __metadata('design:type', Boolean), 
                    __metadata('design:paramtypes', [Boolean])
                ], UiLaddaDirective.prototype, "property", null);
                UiLaddaDirective = __decorate([
                    core_37.Directive({
                        selector: '[ladda]'
                    }), 
                    __metadata('design:paramtypes', [core_37.ElementRef])
                ], UiLaddaDirective);
                return UiLaddaDirective;
            }());
            exports_66("UiLaddaDirective", UiLaddaDirective);
        }
    }
});
System.register("app/ab-experiment-details.component", ['angular2/core', 'angular2/common', 'angular2/http', "app/dom-disabled.directive", "app/ab-experiment-details/experiment-url-list.component", "app/ab-experiment-details/experiment-variation-list.component", "app/ab-experiment-details/experiment-goal-list.component", "app/auth", "app/ui-js-field.directive", "app/ui-popover.component", "app/experiment-publish.service", "app/ui-ladda.directive", "app/notification.service", "app/ui-edit-inline.directive", "app/em/data.service", "app/config.service"], function(exports_67, context_67) {
    "use strict";
    var __moduleName = context_67 && context_67.id;
    var core_38, common_1, http_5, dom_disabled_directive_3, experiment_url_list_component_1, experiment_variation_list_component_1, experiment_goal_list_component_1, auth_9, ui_js_field_directive_1, ui_popover_component_10, experiment_publish_service_1, ui_ladda_directive_1, notification_service_7, ui_edit_inline_directive_5, data_service_10, config_service_2;
    var AbExperimentDetailsComponent;
    return {
        setters:[
            function (core_38_1) {
                core_38 = core_38_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (http_5_1) {
                http_5 = http_5_1;
            },
            function (dom_disabled_directive_3_1) {
                dom_disabled_directive_3 = dom_disabled_directive_3_1;
            },
            function (experiment_url_list_component_1_1) {
                experiment_url_list_component_1 = experiment_url_list_component_1_1;
            },
            function (experiment_variation_list_component_1_1) {
                experiment_variation_list_component_1 = experiment_variation_list_component_1_1;
            },
            function (experiment_goal_list_component_1_1) {
                experiment_goal_list_component_1 = experiment_goal_list_component_1_1;
            },
            function (auth_9_1) {
                auth_9 = auth_9_1;
            },
            function (ui_js_field_directive_1_1) {
                ui_js_field_directive_1 = ui_js_field_directive_1_1;
            },
            function (ui_popover_component_10_1) {
                ui_popover_component_10 = ui_popover_component_10_1;
            },
            function (experiment_publish_service_1_1) {
                experiment_publish_service_1 = experiment_publish_service_1_1;
            },
            function (ui_ladda_directive_1_1) {
                ui_ladda_directive_1 = ui_ladda_directive_1_1;
            },
            function (notification_service_7_1) {
                notification_service_7 = notification_service_7_1;
            },
            function (ui_edit_inline_directive_5_1) {
                ui_edit_inline_directive_5 = ui_edit_inline_directive_5_1;
            },
            function (data_service_10_1) {
                data_service_10 = data_service_10_1;
            },
            function (config_service_2_1) {
                config_service_2 = config_service_2_1;
            }],
        execute: function() {
            AbExperimentDetailsComponent = (function () {
                function AbExperimentDetailsComponent(data, publishService, http, auth, notifications, config) {
                    this.data = data;
                    this.publishService = publishService;
                    this.http = http;
                    this.auth = auth;
                    this.notifications = notifications;
                    this.config = config;
                    this.onDelete = new core_38.EventEmitter();
                    this.onRun = new core_38.EventEmitter();
                    //noinspection JSUnresolvedLibraryURL,JSUnusedGlobalSymbols
                    this.jsSnippetCode = '<script src="http://{{ host }}/a/{{ uid }}.js" async="async"></script>';
                    this.jsSnippetParams = {};
                    this.jsSnippetParams['host'] = this.config._.cache.cache_host;
                    this.jsSnippetParams['uid'] = this.auth.account.uid;
                }
                AbExperimentDetailsComponent.prototype.routerCanReuse = function (nextInstruction, prevInstruction) {
                    return true;
                };
                AbExperimentDetailsComponent.prototype.ngOnDestroy = function () {
                };
                AbExperimentDetailsComponent.prototype.setExperiment = function (experiment) {
                    this.experiment = experiment;
                };
                AbExperimentDetailsComponent.prototype.update = function () {
                    this.data.persist(500);
                };
                Object.defineProperty(AbExperimentDetailsComponent.prototype, "valid", {
                    get: function () {
                        return this.publishService.errors(this.experiment).length == 0;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(AbExperimentDetailsComponent.prototype, "pristine", {
                    //noinspection JSMethodCanBeStatic
                    get: function () {
                        // @todo implement changes tracking
                        //return this.experiment.pristine;
                        return false;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(AbExperimentDetailsComponent.prototype, "publishing", {
                    //noinspection JSUnusedGlobalSymbols
                    get: function () {
                        return this.publishService.publishing;
                    },
                    enumerable: true,
                    configurable: true
                });
                AbExperimentDetailsComponent.prototype.run = function () {
                    var _this = this;
                    if (!this.valid) {
                        return;
                    }
                    this.experiment.state = 2;
                    this.experiment.startedAt = new Date();
                    this.data.persist().then(function () {
                        _this.onRun.emit(_this.experiment);
                        _this.publishService.publish(_this.experiment);
                    });
                };
                AbExperimentDetailsComponent.prototype.toggle = function () {
                    var _this = this;
                    switch (this.experiment.state) {
                        case 2:
                            this.experiment.state = 3;
                            break;
                        case 3:
                            if (!this.valid) {
                                // return console.warn('Cannot resume invalid experiment');
                                return;
                            }
                            this.experiment.state = 2;
                            break;
                        default:
                            console.error(sprintf('Invalid experiment state %s', this.experiment.state));
                    }
                    this.data.persist()
                        .then(function (_) {
                        _this.publishService.publish(_this.experiment)
                            .then(function (result) {
                            if (_this.experiment.state == 2) {
                                _this.notifications.success('experiment successfully activated');
                            }
                        })
                            .catch(function (reason) {
                            _this.notifications.error('error has occurred');
                        });
                    })
                        .catch(function (_) { return console.error(_); });
                };
                //noinspection ReservedWordAsName
                AbExperimentDetailsComponent.prototype.delete = function () {
                    this.onDelete.emit(this.experiment);
                };
                AbExperimentDetailsComponent.prototype.touch = function () {
                    if (this.experiment.pristine) {
                        this.experiment.pristine = false;
                    }
                };
                AbExperimentDetailsComponent.prototype.publish = function () {
                    var _this = this;
                    if (!this.valid) {
                        return;
                    }
                    this.publishService.publish(this.experiment)
                        .then(function (result) {
                        _this.notifications.success('experiment has been published successfully');
                    })
                        .catch(function (reason) {
                        _this.notifications.error('failed to publish experiment');
                    });
                };
                AbExperimentDetailsComponent.prototype.details = function () {
                };
                AbExperimentDetailsComponent.prototype.onPopoverInit = function (component) {
                    component.setOwner(this);
                };
                //noinspection JSUnusedGlobalSymbols
                AbExperimentDetailsComponent.prototype.runExperimentPopover = function (options, component, service) {
                    var errors = this.publishService.errors(this.experiment);
                    var message = errors.length ?
                        service.message(sprintf('ab_experiment_details.cant_run_experiment.%s', errors[0])).content :
                        service.message('ab_experiment_details.run_experiment_button.ready').content;
                    return service.message(component.uiPopover).content + '<br>' + 'NOTICE: ' + message;
                };
                //noinspection JSUnusedGlobalSymbols
                AbExperimentDetailsComponent.prototype.publishExperimentPopover = function (options, component, service) {
                    var errors = this.publishService.errors(this.experiment);
                    return errors.length ?
                        service.message(sprintf('ab_experiment_details.cant_publish_experiment.%s', errors[0])).content :
                        service.message('ab_experiment_details.publish_experiment_button.ready').content;
                };
                //noinspection JSUnusedGlobalSymbols
                AbExperimentDetailsComponent.prototype.toggleExperimentPopover = function (options, component, service) {
                    if (this.experiment.state == 3) {
                        var errors = this.publishService.errors(this.experiment);
                        if (errors.length) {
                            return service.message(sprintf('ab_experiment_details.cant_run_experiment.%s', errors[0])).content;
                        }
                        return service.message('ab_experiment_details.resume_experiment_button').content;
                    }
                    else {
                        return service.message('ab_experiment_details.pause_experiment_button').content;
                    }
                };
                AbExperimentDetailsComponent = __decorate([
                    core_38.Component({
                        selector: 'so-app',
                        templateUrl: 'templates/ab-experiment-details.html',
                        providers: [
                            experiment_publish_service_1.ExperimentPublishService
                        ],
                        directives: [
                            ui_edit_inline_directive_5.UiEditInlineDirective,
                            ui_popover_component_10.UiPopoverComponent,
                            ui_js_field_directive_1.UiJsFieldDirective,
                            dom_disabled_directive_3.DomDisabledDirective,
                            experiment_url_list_component_1.ExperimentUrlListComponent,
                            experiment_variation_list_component_1.ExperimentVariationListComponent,
                            experiment_goal_list_component_1.ExperimentGoalListComponent,
                            ui_ladda_directive_1.UiLaddaDirective,
                            common_1.FORM_DIRECTIVES
                        ]
                    }), 
                    __metadata('design:paramtypes', [data_service_10.DataService, experiment_publish_service_1.ExperimentPublishService, http_5.Http, auth_9.AuthService, notification_service_7.NotificationService, config_service_2.ConfigService])
                ], AbExperimentDetailsComponent);
                return AbExperimentDetailsComponent;
            }());
            exports_67("AbExperimentDetailsComponent", AbExperimentDetailsComponent);
        }
    }
});
System.register("app/details-sidebar.component", ['angular2/core', 'angular2/router', 'angular2/common', "app/details-sidebar.service"], function(exports_68, context_68) {
    "use strict";
    var __moduleName = context_68 && context_68.id;
    var core_39, router_6, common_2, details_sidebar_service_1;
    var DetailsSidebarComponent;
    return {
        setters:[
            function (core_39_1) {
                core_39 = core_39_1;
            },
            function (router_6_1) {
                router_6 = router_6_1;
            },
            function (common_2_1) {
                common_2 = common_2_1;
            },
            function (details_sidebar_service_1_1) {
                details_sidebar_service_1 = details_sidebar_service_1_1;
            }],
        execute: function() {
            DetailsSidebarComponent = (function () {
                function DetailsSidebarComponent(router, elementRef, service) {
                    var _this = this;
                    this.router = router;
                    this.service = service;
                    this.onOpen = new core_39.EventEmitter();
                    this.onClose = new core_39.EventEmitter();
                    this.isOpen = false;
                    this.service.element = elementRef;
                    this.service.anchor = 'detailsSidebar';
                    service.onLoad(function () {
                        _this.service.open();
                    });
                    service.onOpen(function () {
                        _this.open();
                    });
                    service.onClose(function () {
                        _this.close();
                    });
                    this.router.subscribe(function (path) {
                        // @todo maybe unnecessary
                        _this.service.close();
                    });
                }
                DetailsSidebarComponent.prototype.open = function () {
                    this.isOpen = true;
                    this.onOpen.emit(this);
                };
                DetailsSidebarComponent.prototype.close = function () {
                    this.isOpen = false;
                    this.onClose.emit(this);
                };
                __decorate([
                    core_39.Output('open'), 
                    __metadata('design:type', core_39.EventEmitter)
                ], DetailsSidebarComponent.prototype, "onOpen", void 0);
                __decorate([
                    core_39.Output('close'), 
                    __metadata('design:type', core_39.EventEmitter)
                ], DetailsSidebarComponent.prototype, "onClose", void 0);
                DetailsSidebarComponent = __decorate([
                    core_39.Component({
                        selector: 'details-sidebar',
                        templateUrl: 'templates/details-sidebar.html',
                        providers: [core_39.ElementRef],
                        directives: [common_2.NgClass]
                    }), 
                    __metadata('design:paramtypes', [router_6.Router, core_39.ElementRef, details_sidebar_service_1.DetailsSidebarService])
                ], DetailsSidebarComponent);
                return DetailsSidebarComponent;
            }());
            exports_68("DetailsSidebarComponent", DetailsSidebarComponent);
        }
    }
});
System.register("app/details-sidebar.service", ['angular2/core'], function(exports_69, context_69) {
    "use strict";
    var __moduleName = context_69 && context_69.id;
    var core_40;
    var DetailsSidebarService;
    return {
        setters:[
            function (core_40_1) {
                core_40 = core_40_1;
            }],
        execute: function() {
            DetailsSidebarService = (function () {
                function DetailsSidebarService(dcl) {
                    this.dcl = dcl;
                    this.hidden = false;
                }
                Object.defineProperty(DetailsSidebarService.prototype, "element", {
                    set: function (elementRef) {
                        this.elementRef = elementRef;
                        this.nativeElement = elementRef.nativeElement;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(DetailsSidebarService.prototype, "anchor", {
                    set: function (anchorName) {
                        this.anchorName = anchorName;
                    },
                    enumerable: true,
                    configurable: true
                });
                DetailsSidebarService.prototype.loadComponent = function (type) {
                    var _this = this;
                    this.cleanup();
                    return new Promise(function (resolve, reject) {
                        // @todo assert that elementRef and anchorName are defined
                        _this.dcl.loadIntoLocation(type, _this.elementRef, _this.anchorName)
                            .then(function (ref) {
                            if (_this._onLoad) {
                                _this._onLoad();
                            }
                            _this.currentRef = ref;
                            resolve(ref);
                        })
                            .catch(function (error) { return reject(error); });
                    });
                };
                DetailsSidebarService.prototype.unloadComponent = function () {
                    //this.currentRef.dispose();
                };
                // @todo implement with observer
                DetailsSidebarService.prototype.onLoad = function (callback) {
                    this._onLoad = callback;
                };
                DetailsSidebarService.prototype.onBlur = function () {
                    // @todo set method to dispose current component on `virtual` blur event
                };
                Object.defineProperty(DetailsSidebarService.prototype, "isOpen", {
                    get: function () {
                        return this._isOpen;
                    },
                    enumerable: true,
                    configurable: true
                });
                DetailsSidebarService.prototype.onOpen = function (callback) {
                    this._onOpen = callback;
                };
                DetailsSidebarService.prototype.open = function () {
                    this.nativeElement.style.display = 'block';
                    this._isOpen = true;
                    this._onOpen();
                };
                DetailsSidebarService.prototype.onClose = function (callback) {
                    this._onClose = callback;
                };
                DetailsSidebarService.prototype.close = function () {
                    this._isOpen = false;
                    this.cleanup();
                    this._onClose();
                };
                DetailsSidebarService.prototype.cleanup = function () {
                    if (this.currentRef) {
                        this.currentRef.dispose();
                    }
                };
                DetailsSidebarService = __decorate([
                    core_40.Injectable(),
                    __param(0, core_40.Inject(core_40.DynamicComponentLoader)), 
                    __metadata('design:paramtypes', [core_40.DynamicComponentLoader])
                ], DetailsSidebarService);
                return DetailsSidebarService;
            }());
            exports_69("DetailsSidebarService", DetailsSidebarService);
        }
    }
});
System.register("app/ab-experiment-row.component", ['angular2/core', "app/ab-experiment-details.component", "app/details-sidebar.service", "app/model/experiment", "app/ui-edit-inline.directive", "app/ui-popover.component", "app/em/data.service", "app/experiment-publish.service", "app/notification.service"], function(exports_70, context_70) {
    "use strict";
    var __moduleName = context_70 && context_70.id;
    var core_41, ab_experiment_details_component_1, details_sidebar_service_2, experiment_9, ui_edit_inline_directive_6, ui_popover_component_11, data_service_11, experiment_publish_service_2, notification_service_8;
    var AbExperimentRowComponent;
    return {
        setters:[
            function (core_41_1) {
                core_41 = core_41_1;
            },
            function (ab_experiment_details_component_1_1) {
                ab_experiment_details_component_1 = ab_experiment_details_component_1_1;
            },
            function (details_sidebar_service_2_1) {
                details_sidebar_service_2 = details_sidebar_service_2_1;
            },
            function (experiment_9_1) {
                experiment_9 = experiment_9_1;
            },
            function (ui_edit_inline_directive_6_1) {
                ui_edit_inline_directive_6 = ui_edit_inline_directive_6_1;
            },
            function (ui_popover_component_11_1) {
                ui_popover_component_11 = ui_popover_component_11_1;
            },
            function (data_service_11_1) {
                data_service_11 = data_service_11_1;
            },
            function (experiment_publish_service_2_1) {
                experiment_publish_service_2 = experiment_publish_service_2_1;
            },
            function (notification_service_8_1) {
                notification_service_8 = notification_service_8_1;
            }],
        execute: function() {
            AbExperimentRowComponent = (function () {
                function AbExperimentRowComponent(sidebar, publishService, notifications, data, elementRef) {
                    this.sidebar = sidebar;
                    this.publishService = publishService;
                    this.notifications = notifications;
                    this.data = data;
                    this.elementRef = elementRef;
                    this.onDelete = new core_41.EventEmitter(false);
                    this.onRun = new core_41.EventEmitter(false);
                    this.loading = false;
                }
                AbExperimentRowComponent.prototype.ngOnInit = function () {
                };
                AbExperimentRowComponent.prototype.ngOnDestroy = function () {
                    if (this.sidebar.isOpen) {
                        this.sidebar.close();
                    }
                };
                AbExperimentRowComponent.prototype.onDetails = function () {
                    var _this = this;
                    if (!this.experiment.expanded && !this.experiment.locked) {
                        this.experiment.expand()
                            .catch(function (error) { return console.error(error); });
                    }
                    jQuery(this.elementRef.nativeElement).find('input').focus();
                    this.sidebar.loadComponent(ab_experiment_details_component_1.AbExperimentDetailsComponent)
                        .then(function (ref) {
                        _this.detailsRef = ref;
                        var instance = ref.instance;
                        instance.setExperiment(_this.experiment);
                        instance.onDelete.subscribe(function (experiment) {
                            _this.sidebar.close();
                            _this.onDelete.emit(experiment);
                        });
                        instance.onRun.subscribe(function (experiment) {
                            _this.onRun.emit(experiment);
                        });
                    });
                };
                Object.defineProperty(AbExperimentRowComponent.prototype, "valid", {
                    get: function () {
                        return this.publishService.errors(this.experiment).length == 0;
                    },
                    enumerable: true,
                    configurable: true
                });
                AbExperimentRowComponent.prototype.toggle = function (event) {
                    var _this = this;
                    event.stopPropagation();
                    jQuery(event.currentTarget).mouseleave();
                    switch (this.experiment.state) {
                        case 2:
                            this.experiment.state = 3;
                            break;
                        case 3:
                            if (!this.valid) {
                                // return console.warn('Cannot resume invalid experiment');
                                return;
                            }
                            this.experiment.state = 2;
                            break;
                        default:
                            console.error(sprintf('Invalid experiment state %s', this.experiment.state));
                    }
                    this.data.persist()
                        .then(function (_) {
                        _this.publishService.publish(_this.experiment)
                            .then(function (result) {
                            if (_this.experiment.state == 2) {
                                _this.notifications.success('experiment successfully activated');
                            }
                        })
                            .catch(function (reason) {
                            _this.notifications.error('error has occurred');
                        });
                    })
                        .catch(function (_) { return console.error(_); });
                };
                AbExperimentRowComponent.prototype.onPopoverInit = function (component) {
                    component.setOwner(this);
                };
                Object.defineProperty(AbExperimentRowComponent.prototype, "runningPopover", {
                    get: function () {
                        return this.experiment.state == 2
                            ? 'ab_experiments_list.running_status.running'
                            : 'ab_experiments_list.running_status.paused';
                    },
                    enumerable: true,
                    configurable: true
                });
                AbExperimentRowComponent.prototype.runningPopoverFn = function (options, component, service) {
                    if (!this.experiment) {
                        return 'n/a';
                    }
                    var id = this.experiment.state == 2 ?
                        'ab_experiments_list.running_status.running' :
                        'ab_experiments_list.running_status.paused';
                    var message = service.message(id);
                    return message.content;
                };
                AbExperimentRowComponent.prototype.update = function () {
                    this.data.persist(500);
                };
                AbExperimentRowComponent.prototype.save = function () {
                    this.data.persist();
                };
                AbExperimentRowComponent.prototype.blur = function () {
                    this.sidebar.close();
                };
                __decorate([
                    core_41.Input(), 
                    __metadata('design:type', experiment_9.Experiment)
                ], AbExperimentRowComponent.prototype, "experiment", void 0);
                __decorate([
                    core_41.Output('delete'), 
                    __metadata('design:type', core_41.EventEmitter)
                ], AbExperimentRowComponent.prototype, "onDelete", void 0);
                __decorate([
                    core_41.Output('run'), 
                    __metadata('design:type', core_41.EventEmitter)
                ], AbExperimentRowComponent.prototype, "onRun", void 0);
                AbExperimentRowComponent = __decorate([
                    core_41.Component({
                        selector: 'tr[abExperimentRow]',
                        templateUrl: 'templates/ab-experiment-row.html',
                        providers: [
                            experiment_publish_service_2.ExperimentPublishService
                        ],
                        directives: [
                            ui_edit_inline_directive_6.UiEditInlineDirective,
                            ui_popover_component_11.UiPopoverComponent
                        ]
                    }), 
                    __metadata('design:paramtypes', [details_sidebar_service_2.DetailsSidebarService, experiment_publish_service_2.ExperimentPublishService, notification_service_8.NotificationService, data_service_11.DataService, core_41.ElementRef])
                ], AbExperimentRowComponent);
                return AbExperimentRowComponent;
            }());
            exports_70("AbExperimentRowComponent", AbExperimentRowComponent);
        }
    }
});
System.register("app/ab-experiments-list.component", ['angular2/core', "app/model/experiment", "app/ab-experiment-row.component", "app/auth", "app/details-sidebar.service", "app/ab-experiment-details.component", "app/ui-popover.component", "app/ab-experiment-factory.service", "app/ui-edit-inline.directive", "app/ui-ladda.directive", "app/em/data.service", "app/notification.service", "app/em/experiments.service"], function(exports_71, context_71) {
    "use strict";
    var __moduleName = context_71 && context_71.id;
    var core_42, experiment_10, ab_experiment_row_component_1, auth_10, details_sidebar_service_3, ab_experiment_details_component_2, ui_popover_component_12, ab_experiment_factory_service_3, ui_edit_inline_directive_7, ui_ladda_directive_2, data_service_12, notification_service_9, experiments_service_2;
    var AbExperimentsListComponent;
    return {
        setters:[
            function (core_42_1) {
                core_42 = core_42_1;
            },
            function (experiment_10_1) {
                experiment_10 = experiment_10_1;
            },
            function (ab_experiment_row_component_1_1) {
                ab_experiment_row_component_1 = ab_experiment_row_component_1_1;
            },
            function (auth_10_1) {
                auth_10 = auth_10_1;
            },
            function (details_sidebar_service_3_1) {
                details_sidebar_service_3 = details_sidebar_service_3_1;
            },
            function (ab_experiment_details_component_2_1) {
                ab_experiment_details_component_2 = ab_experiment_details_component_2_1;
            },
            function (ui_popover_component_12_1) {
                ui_popover_component_12 = ui_popover_component_12_1;
            },
            function (ab_experiment_factory_service_3_1) {
                ab_experiment_factory_service_3 = ab_experiment_factory_service_3_1;
            },
            function (ui_edit_inline_directive_7_1) {
                ui_edit_inline_directive_7 = ui_edit_inline_directive_7_1;
            },
            function (ui_ladda_directive_2_1) {
                ui_ladda_directive_2 = ui_ladda_directive_2_1;
            },
            function (data_service_12_1) {
                data_service_12 = data_service_12_1;
            },
            function (notification_service_9_1) {
                notification_service_9 = notification_service_9_1;
            },
            function (experiments_service_2_1) {
                experiments_service_2 = experiments_service_2_1;
            }],
        execute: function() {
            AbExperimentsListComponent = (function () {
                function AbExperimentsListComponent(data, experiments, factory, auth, sidebar, notifications) {
                    this.data = data;
                    this.experiments = experiments;
                    this.factory = factory;
                    this.auth = auth;
                    this.sidebar = sidebar;
                    this.notifications = notifications;
                    this.creating = false;
                }
                AbExperimentsListComponent.prototype.ngOnInit = function () {
                };
                Object.defineProperty(AbExperimentsListComponent.prototype, "ready", {
                    get: function () {
                        return this.experiments.ready;
                    },
                    enumerable: true,
                    configurable: true
                });
                AbExperimentsListComponent.prototype.details = function (experiment) {
                    var _this = this;
                    this.sidebar.loadComponent(ab_experiment_details_component_2.AbExperimentDetailsComponent)
                        .then(function (ref) {
                        var component = ref.instance;
                        component.setExperiment(experiment);
                        component.onDelete.subscribe(function (experiment) {
                            _this.sidebar.close();
                            _this.delete(experiment);
                        });
                        component.onRun.subscribe(function (experiment) {
                            _this.run(experiment);
                        });
                    });
                };
                AbExperimentsListComponent.prototype.create = function () {
                    var _this = this;
                    this.creating = true;
                    this.data.create(experiment_10.Experiment)
                        .then(function (experiment) {
                        _this.experiments.push(experiment);
                        _this.creating = false;
                        _this.details(experiment);
                    })
                        .catch(function (error) {
                        _this.notifications.error("Error has occurred while creating experiment. See error console for details.");
                        console.error(error);
                    });
                };
                //noinspection ReservedWordAsName
                AbExperimentsListComponent.prototype.delete = function (experiment) {
                    var _this = this;
                    this.experiments.remove(experiment);
                    this.data.remove(experiment)
                        .then(function (result) {
                        // @todo hide from the list first, remove on success
                    })
                        .catch(function (error) {
                        _this.notifications.error('Failed to delete experiment. See error console for details.');
                    });
                };
                AbExperimentsListComponent.prototype.run = function (experiment) {
                    //let index = this.experiments.new.indexOf(experiment);
                    //this.experiments.new.splice(index, 1);
                    //this.experiments.active.push(experiment);
                    //this.details(experiment);
                    this.experiments.reindex('state');
                };
                AbExperimentsListComponent = __decorate([
                    core_42.Component({
                        templateUrl: 'templates/ab-experiments-list.html',
                        selector: 'ab-experiments-list',
                        providers: [
                            ab_experiment_factory_service_3.AbExperimentFactoryService
                        ],
                        directives: [
                            ab_experiment_row_component_1.AbExperimentRowComponent,
                            ui_popover_component_12.UiPopoverComponent,
                            ui_edit_inline_directive_7.UiEditInlineDirective,
                            ui_ladda_directive_2.UiLaddaDirective
                        ]
                    }), 
                    __metadata('design:paramtypes', [data_service_12.DataService, experiments_service_2.ExperimentsService, ab_experiment_factory_service_3.AbExperimentFactoryService, auth_10.AuthService, details_sidebar_service_3.DetailsSidebarService, notification_service_9.NotificationService])
                ], AbExperimentsListComponent);
                return AbExperimentsListComponent;
            }());
            exports_71("AbExperimentsListComponent", AbExperimentsListComponent);
        }
    }
});
System.register("app/menu-top.component", ['angular2/core', 'angular2/router', "app/auth"], function(exports_72, context_72) {
    "use strict";
    var __moduleName = context_72 && context_72.id;
    var core_43, router_7, auth_11;
    var MenuTopComponent;
    return {
        setters:[
            function (core_43_1) {
                core_43 = core_43_1;
            },
            function (router_7_1) {
                router_7 = router_7_1;
            },
            function (auth_11_1) {
                auth_11 = auth_11_1;
            }],
        execute: function() {
            MenuTopComponent = (function () {
                function MenuTopComponent(auth, elementRef) {
                    this.auth = auth;
                    this.elementRef = elementRef;
                }
                MenuTopComponent.prototype.ngOnInit = function () {
                    var userHeader = jQuery(this.elementRef.nativeElement)
                        .find('#user-header').find('.dropdown-toggle');
                    userHeader.dropdownHover();
                };
                Object.defineProperty(MenuTopComponent.prototype, "account", {
                    get: function () {
                        return this.auth.account;
                    },
                    enumerable: true,
                    configurable: true
                });
                MenuTopComponent.prototype.logout = function () {
                    this.auth.logout();
                };
                MenuTopComponent = __decorate([
                    core_43.Component({
                        selector: 'menu-top',
                        templateUrl: 'templates/menu-top.html',
                        directives: [
                            router_7.ROUTER_DIRECTIVES
                        ]
                    }), 
                    __metadata('design:paramtypes', [auth_11.AuthService, core_43.ElementRef])
                ], MenuTopComponent);
                return MenuTopComponent;
            }());
            exports_72("MenuTopComponent", MenuTopComponent);
        }
    }
});
System.register("app/login-page.component", ['angular2/core', "app/login-form.component", "app/auth"], function(exports_73, context_73) {
    "use strict";
    var __moduleName = context_73 && context_73.id;
    var core_44, login_form_component_1, auth_12;
    var LoginPageComponent;
    return {
        setters:[
            function (core_44_1) {
                core_44 = core_44_1;
            },
            function (login_form_component_1_1) {
                login_form_component_1 = login_form_component_1_1;
            },
            function (auth_12_1) {
                auth_12 = auth_12_1;
            }],
        execute: function() {
            LoginPageComponent = (function () {
                function LoginPageComponent(auth) {
                    this.auth = auth;
                }
                LoginPageComponent = __decorate([
                    core_44.Component({
                        selector: 'login-page',
                        templateUrl: 'templates/login-page.html',
                        directives: [login_form_component_1.LoginFormComponent]
                    }), 
                    __metadata('design:paramtypes', [auth_12.AuthService])
                ], LoginPageComponent);
                return LoginPageComponent;
            }());
            exports_73("LoginPageComponent", LoginPageComponent);
        }
    }
});
System.register("app/profile/profile-account-user.component", ['angular2/core', "app/auth"], function(exports_74, context_74) {
    "use strict";
    var __moduleName = context_74 && context_74.id;
    var core_45, auth_13;
    var ProfileAccountUserComponent;
    return {
        setters:[
            function (core_45_1) {
                core_45 = core_45_1;
            },
            function (auth_13_1) {
                auth_13 = auth_13_1;
            }],
        execute: function() {
            ProfileAccountUserComponent = (function () {
                function ProfileAccountUserComponent(auth) {
                    this.auth = auth;
                    this.account = {};
                    this.formErrors = {};
                    this.active = true;
                }
                ProfileAccountUserComponent.prototype.ngOnInit = function () {
                    this.account = this.clone(this.auth.account);
                };
                ProfileAccountUserComponent.prototype.clone = function (data) {
                    var account = {};
                    for (var prop in data) {
                        account[prop] = data[prop];
                    }
                    return account;
                };
                ProfileAccountUserComponent.prototype.update = function () {
                    var _this = this;
                    this.auth.updateAccount(this.account)
                        .then(function (result) {
                        _this.reset();
                    })
                        .catch(function (reason) {
                        console.error(reason);
                    });
                };
                ProfileAccountUserComponent.prototype.reset = function () {
                    var _this = this;
                    this.account = this.clone(this.auth.account);
                    this.active = false;
                    setTimeout(function () { return _this.active = true; }, 0);
                };
                ProfileAccountUserComponent = __decorate([
                    core_45.Component({
                        selector: 'profile-account-user',
                        templateUrl: 'templates/profile/profile-account-user.html'
                    }), 
                    __metadata('design:paramtypes', [auth_13.AuthService])
                ], ProfileAccountUserComponent);
                return ProfileAccountUserComponent;
            }());
            exports_74("ProfileAccountUserComponent", ProfileAccountUserComponent);
        }
    }
});
System.register("app/profile/profile-account-password.component", ['angular2/core', "app/auth", "app/notification.service", "app/ui-ladda.directive"], function(exports_75, context_75) {
    "use strict";
    var __moduleName = context_75 && context_75.id;
    var core_46, auth_14, notification_service_10, ui_ladda_directive_3;
    var ProfileAccountPasswordComponent;
    return {
        setters:[
            function (core_46_1) {
                core_46 = core_46_1;
            },
            function (auth_14_1) {
                auth_14 = auth_14_1;
            },
            function (notification_service_10_1) {
                notification_service_10 = notification_service_10_1;
            },
            function (ui_ladda_directive_3_1) {
                ui_ladda_directive_3 = ui_ladda_directive_3_1;
            }],
        execute: function() {
            ProfileAccountPasswordComponent = (function () {
                function ProfileAccountPasswordComponent(auth, notifications) {
                    this.auth = auth;
                    this.notifications = notifications;
                    this.password = {
                        current: '',
                        new: '',
                        confirm: ''
                    };
                    this.formErrors = {};
                    this.active = true;
                    this.updating = false;
                }
                Object.defineProperty(ProfileAccountPasswordComponent.prototype, "valid", {
                    get: function () {
                        return !(this.formErrors['current'] ||
                            this.formErrors['new'] ||
                            this.formErrors['confirm']);
                    },
                    enumerable: true,
                    configurable: true
                });
                ProfileAccountPasswordComponent.prototype.refresh = function () {
                    this.validate();
                };
                ProfileAccountPasswordComponent.prototype.validate = function () {
                    this.formErrors = {};
                    if (!this.password.current.length) {
                        this.formErrors['current'] = true;
                    }
                    if (!this.password.new.length) {
                        this.formErrors['new'] = true;
                    }
                    if (this.password.confirm != this.password.new) {
                        this.formErrors['confirm'] = true;
                    }
                    // @todo check that new password is not the same as current
                };
                ProfileAccountPasswordComponent.prototype.update = function () {
                    var _this = this;
                    if (!this.valid) {
                        return;
                    }
                    this.updating = true;
                    this.auth.changePassword(this.password)
                        .then(function (result) {
                        // @todo show success message
                        _this.reset();
                        _this.updating = false;
                        _this.notifications.success('password successfully updated.');
                    })
                        .catch(function (error) {
                        // @todo show error message
                        if (error['field'] == 'current') {
                            _this.formErrors['current'] = true;
                            _this.notifications.error('incorrect current password');
                        }
                        else {
                            _this.notifications.error('failed to change password');
                            console.error(error);
                        }
                        _this.updating = false;
                    });
                };
                ProfileAccountPasswordComponent.prototype.reset = function () {
                    var _this = this;
                    this.formErrors = {};
                    this.password = {
                        current: '',
                        new: '',
                        confirm: ''
                    };
                    this.active = false;
                    setTimeout(function () { return _this.active = true; }, 0);
                };
                ProfileAccountPasswordComponent = __decorate([
                    core_46.Component({
                        selector: 'profile-account-password',
                        templateUrl: 'templates/profile/profile-account-password.html',
                        directives: [
                            ui_ladda_directive_3.UiLaddaDirective
                        ]
                    }), 
                    __metadata('design:paramtypes', [auth_14.AuthService, notification_service_10.NotificationService])
                ], ProfileAccountPasswordComponent);
                return ProfileAccountPasswordComponent;
            }());
            exports_75("ProfileAccountPasswordComponent", ProfileAccountPasswordComponent);
        }
    }
});
System.register("app/profile/profile-account.component", ['angular2/core', "app/profile/profile-account-user.component", "app/profile/profile-account-password.component"], function(exports_76, context_76) {
    "use strict";
    var __moduleName = context_76 && context_76.id;
    var core_47, profile_account_user_component_1, profile_account_password_component_1;
    var ProfileAccountComponent;
    return {
        setters:[
            function (core_47_1) {
                core_47 = core_47_1;
            },
            function (profile_account_user_component_1_1) {
                profile_account_user_component_1 = profile_account_user_component_1_1;
            },
            function (profile_account_password_component_1_1) {
                profile_account_password_component_1 = profile_account_password_component_1_1;
            }],
        execute: function() {
            ProfileAccountComponent = (function () {
                function ProfileAccountComponent() {
                }
                ProfileAccountComponent = __decorate([
                    core_47.Component({
                        selector: 'profile-account',
                        templateUrl: 'templates/profile/profile-account.html',
                        directives: [
                            profile_account_user_component_1.ProfileAccountUserComponent,
                            profile_account_password_component_1.ProfileAccountPasswordComponent
                        ]
                    }), 
                    __metadata('design:paramtypes', [])
                ], ProfileAccountComponent);
                return ProfileAccountComponent;
            }());
            exports_76("ProfileAccountComponent", ProfileAccountComponent);
        }
    }
});
System.register("app/profile.component", ['angular2/core', "app/profile/profile-account.component"], function(exports_77, context_77) {
    "use strict";
    var __moduleName = context_77 && context_77.id;
    var core_48, profile_account_component_1;
    var ProfileComponent;
    return {
        setters:[
            function (core_48_1) {
                core_48 = core_48_1;
            },
            function (profile_account_component_1_1) {
                profile_account_component_1 = profile_account_component_1_1;
            }],
        execute: function() {
            ProfileComponent = (function () {
                function ProfileComponent() {
                }
                ProfileComponent = __decorate([
                    core_48.Component({
                        selector: 'profile-page',
                        templateUrl: 'templates/profile.html',
                        directives: [
                            profile_account_component_1.ProfileAccountComponent
                        ]
                    }), 
                    __metadata('design:paramtypes', [])
                ], ProfileComponent);
                return ProfileComponent;
            }());
            exports_77("ProfileComponent", ProfileComponent);
        }
    }
});
System.register("app/keep-alive.service", ['angular2/core', "app/config.service", "app/xhr.service", "app/auth"], function(exports_78, context_78) {
    "use strict";
    var __moduleName = context_78 && context_78.id;
    var core_49, config_service_3, xhr_service_2, auth_15;
    var KeepAliveService;
    return {
        setters:[
            function (core_49_1) {
                core_49 = core_49_1;
            },
            function (config_service_3_1) {
                config_service_3 = config_service_3_1;
            },
            function (xhr_service_2_1) {
                xhr_service_2 = xhr_service_2_1;
            },
            function (auth_15_1) {
                auth_15 = auth_15_1;
            }],
        execute: function() {
            KeepAliveService = (function () {
                function KeepAliveService(config, xhr, auth) {
                    this.config = config;
                    this.xhr = xhr;
                    this.auth = auth;
                    this.baseUrl = this.config._.keep_alive.service_base_url;
                }
                KeepAliveService.prototype.initialize = function () {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        _this.xhr.post(_this.url('initialize'), {})
                            .then(function (response) {
                            var data = response.json().data;
                            _this.timeout = data['timeout'];
                            _this.max = data['max'];
                            _this.reset();
                            _this.interval = setInterval(_this.refresh.bind(_this), _this.timeout * 1000);
                            resolve();
                        })
                            .catch(function (error) {
                            console.error(error);
                            reject();
                        });
                    });
                };
                KeepAliveService.prototype.reset = function () {
                    var time = new Date();
                    time.setSeconds(time.getSeconds() + this.max);
                    this.expires = time;
                };
                KeepAliveService.prototype.refresh = function () {
                    var _this = this;
                    var time = new Date();
                    if (time >= this.expires) {
                        clearInterval(this.interval);
                        this.close();
                    }
                    else {
                        this.xhr.post(this.url('refresh'), {})
                            .then(function (response) {
                            //
                        })
                            .catch(function (error) {
                            // console.error(error);
                            if (error.status == 'authentication_required') {
                                _this.auth.logout();
                            }
                        });
                    }
                };
                KeepAliveService.prototype.close = function () {
                    var _this = this;
                    this.xhr.post(this.url('close'), {})
                        .then(function (response) {
                        _this.auth.logout();
                    })
                        .catch(function (error) {
                        console.error(error);
                    });
                };
                KeepAliveService.prototype.url = function (method) {
                    return this.baseUrl + method;
                };
                KeepAliveService = __decorate([
                    core_49.Injectable(), 
                    __metadata('design:paramtypes', [config_service_3.ConfigService, xhr_service_2.XhrService, auth_15.AuthService])
                ], KeepAliveService);
                return KeepAliveService;
            }());
            exports_78("KeepAliveService", KeepAliveService);
        }
    }
});
System.register("app/persistance/DTO", [], function(exports_79, context_79) {
    "use strict";
    var __moduleName = context_79 && context_79.id;
    var DTO;
    return {
        setters:[],
        execute: function() {
            DTO = (function () {
                function DTO(_data) {
                    this._data = _data;
                }
                DTO.prototype.data = function () {
                    return this._data;
                };
                DTO.prototype.setData = function (data) {
                    this._data = data;
                };
                return DTO;
            }());
            exports_79("DTO", DTO);
        }
    }
});
System.register("app/persistance/Serializable", [], function(exports_80, context_80) {
    "use strict";
    var __moduleName = context_80 && context_80.id;
    return {
        setters:[],
        execute: function() {
        }
    }
});
System.register("app/persistance/CollectionDAO", ["app/persistance/ElementDAO"], function(exports_81, context_81) {
    "use strict";
    var __moduleName = context_81 && context_81.id;
    var ElementDAO_1;
    var CollectionDAO;
    return {
        setters:[
            function (ElementDAO_1_1) {
                ElementDAO_1 = ElementDAO_1_1;
            }],
        execute: function() {
            CollectionDAO = (function () {
                function CollectionDAO(service, collection, path) {
                    this._service = service;
                    this._collection = collection;
                    this._path = path;
                }
                CollectionDAO.prototype.getService = function () {
                    return this._service;
                };
                CollectionDAO.prototype.all = function () {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        var element = _this._service.all(_this._path);
                        element.getList()
                            .then(function (result) {
                            var list = [];
                            for (var _i = 0, result_1 = result; _i < result_1.length; _i++) {
                                var item = result_1[_i];
                                list.push(new ElementDAO_1.ElementDAO(_this._service, item));
                            }
                            resolve(list);
                        })
                            .catch(function (error) { return reject(error); });
                    });
                };
                CollectionDAO.prototype.create = function (dto) {
                    var data = dto ? dto.data() : {};
                    var element = this._service.restangularizeElement(this._service, data, '999');
                    return new ElementDAO_1.ElementDAO(this._service, element);
                };
                return CollectionDAO;
            }());
            exports_81("CollectionDAO", CollectionDAO);
        }
    }
});
System.register("app/persistance/ElementDAO", ["app/persistance/DTO", "app/persistance/CollectionDAO"], function(exports_82, context_82) {
    "use strict";
    var __moduleName = context_82 && context_82.id;
    var DTO_1, CollectionDAO_1;
    var ElementDAO;
    return {
        setters:[
            function (DTO_1_1) {
                DTO_1 = DTO_1_1;
            },
            function (CollectionDAO_1_1) {
                CollectionDAO_1 = CollectionDAO_1_1;
            }],
        execute: function() {
            ElementDAO = (function () {
                function ElementDAO(service, element) {
                    this._service = service;
                    this._element = element;
                }
                ElementDAO.prototype.collection = function (path) {
                    var collection = this._service.restangularizeCollection(this._element, [], path);
                    return new CollectionDAO_1.CollectionDAO(this._service, collection, path);
                };
                Object.defineProperty(ElementDAO.prototype, "dto", {
                    /*serialize(): DTO {
                        return new DTO(this._element.plain());
                    }*/
                    get: function () {
                        return new DTO_1.DTO(this._element.plain());
                    },
                    enumerable: true,
                    configurable: true
                });
                return ElementDAO;
            }());
            exports_82("ElementDAO", ElementDAO);
        }
    }
});
System.register("app/persistance/EntityRepository", [], function(exports_83, context_83) {
    "use strict";
    var __moduleName = context_83 && context_83.id;
    var EntityRepository;
    return {
        setters:[],
        execute: function() {
            EntityRepository = (function () {
                function EntityRepository() {
                }
                return EntityRepository;
            }());
            exports_83("EntityRepository", EntityRepository);
        }
    }
});
System.register("app/persistance/ModelEntity", [], function(exports_84, context_84) {
    "use strict";
    var __moduleName = context_84 && context_84.id;
    var ModelEntity;
    return {
        setters:[],
        execute: function() {
            ModelEntity = (function () {
                function ModelEntity(dao) {
                    if (dao) {
                        this._dao = dao;
                        this.build(dao);
                    }
                }
                ModelEntity.prototype.attach = function (repository) {
                    // let dto = repository.createDTO(this);
                    // this.setDTO(dto);
                };
                return ModelEntity;
            }());
            exports_84("ModelEntity", ModelEntity);
        }
    }
});
// import {Injectable} from 'angular2/core';
// import {Http} from 'angular2/http';
System.register("app/persistance/RestangularBackend", [], function(exports_85, context_85) {
    "use strict";
    var __moduleName = context_85 && context_85.id;
    var RestangularBackend, IPostElement, IPostCollection;
    return {
        setters:[],
        execute: function() {
            RestangularBackend = (function () {
                function RestangularBackend() {
                    var $http = window['legacyProviders']['http'];
                    this.service = window['restangularConstructor']($http, Q);
                    this.service.setBaseUrl('/api/-/rest/v1');
                }
                RestangularBackend.prototype.all = function (route, base) {
                    //let path = name + 's';
                    var element = base ? base.all(route) : this.service.all(route);
                    return new Promise(function (resolve, reject) {
                        element.getList()
                            .then(function (result) {
                            resolve(result);
                        })
                            .catch(function (error) { return reject(error); });
                    });
                };
                //noinspection JSMethodCanBeStatic
                RestangularBackend.prototype.save = function (entity) {
                    return new Promise(function (resolve, reject) {
                        entity.save()
                            .then(function (element) { return resolve(element); })
                            .catch(function (error) { return reject(error); });
                    });
                };
                //noinspection JSMethodCanBeStatic
                RestangularBackend.prototype.remove = function (entity) {
                    return new Promise(function (resolve, reject) {
                        entity.remove()
                            .then(function (element) { return resolve(element); })
                            .catch(function (error) { return reject(error); });
                    });
                };
                return RestangularBackend;
            }());
            exports_85("RestangularBackend", RestangularBackend);
            IPostElement = (function () {
                function IPostElement(element) {
                    this.element = element;
                }
                IPostElement.prototype.post = function (entity) {
                    return new Promise(function (resolve, reject) {
                        /*this.element.post<IElement>(entity.route, entity.plain())
                            .then((element) => {
                                element['parentResource'] = this.element;
                                // entity.ielement = element;
                                resolve(entity);
                            })
                            .catch(error => reject(error));*/
                    });
                };
                return IPostElement;
            }());
            exports_85("IPostElement", IPostElement);
            IPostCollection = (function () {
                function IPostCollection(collection) {
                    this.collection = collection;
                }
                IPostCollection.prototype.post = function (entity) {
                    return new Promise(function (resolve, reject) {
                        /*return this.collection.post(entity.plain())
                            .then((element) => {
                                entity.ielement = element;
                                resolve(entity);
                            })
                            .catch(error => reject(error));*/
                    });
                };
                return IPostCollection;
            }());
            exports_85("IPostCollection", IPostCollection);
        }
    }
});
System.register("app/persistance/EntityManager", [], function(exports_86, context_86) {
    "use strict";
    var __moduleName = context_86 && context_86.id;
    var EntityManager;
    return {
        setters:[],
        execute: function() {
            EntityManager = (function () {
                function EntityManager() {
                }
                EntityManager.prototype.createRootDTO = function (type) {
                    return null;
                };
                EntityManager.prototype.getDTO = function (type) {
                    return null;
                };
                EntityManager.prototype.getRepository = function (type) {
                    return null;
                };
                return EntityManager;
            }());
            exports_86("EntityManager", EntityManager);
        }
    }
});
System.register("app/aggregate/DomainEntity", ["app/persistance/ModelEntity"], function(exports_87, context_87) {
    "use strict";
    var __moduleName = context_87 && context_87.id;
    var ModelEntity_1;
    var DomainEntity;
    return {
        setters:[
            function (ModelEntity_1_1) {
                ModelEntity_1 = ModelEntity_1_1;
            }],
        execute: function() {
            DomainEntity = (function (_super) {
                __extends(DomainEntity, _super);
                function DomainEntity() {
                    _super.apply(this, arguments);
                }
                return DomainEntity;
            }(ModelEntity_1.ModelEntity));
            exports_87("DomainEntity", DomainEntity);
        }
    }
});
System.register("app/aggregate/AggregateRoot", ["app/aggregate/DomainEntity"], function(exports_88, context_88) {
    "use strict";
    var __moduleName = context_88 && context_88.id;
    var DomainEntity_1;
    var AggregateRoot;
    return {
        setters:[
            function (DomainEntity_1_1) {
                DomainEntity_1 = DomainEntity_1_1;
            }],
        execute: function() {
            AggregateRoot = (function (_super) {
                __extends(AggregateRoot, _super);
                function AggregateRoot() {
                    _super.apply(this, arguments);
                }
                return AggregateRoot;
            }(DomainEntity_1.DomainEntity));
            exports_88("AggregateRoot", AggregateRoot);
        }
    }
});
System.register("app/common/Index", [], function(exports_89, context_89) {
    "use strict";
    var __moduleName = context_89 && context_89.id;
    var Index;
    return {
        setters:[],
        execute: function() {
            Index = (function () {
                function Index(propertyName, items) {
                    if (items === void 0) { items = []; }
                    this.propertyName = propertyName;
                    this._map = new Map();
                    this.build(items);
                }
                Index.prototype.push = function (item) {
                    this.get(item[this.propertyName]).push(item);
                };
                Index.prototype.remove = function (item) {
                    this._map.forEach(function (items, key) {
                        var i = items.indexOf(item);
                        if (i !== -1) {
                            items.splice(i, 1);
                        }
                    });
                };
                Index.prototype.get = function (key) {
                    if (!this._map.has(key)) {
                        this._map.set(key, []);
                    }
                    return this._map.get(key);
                };
                //noinspection ReservedWordAsName
                Index.prototype.in = function (keys) {
                    var _this = this;
                    var result = [];
                    keys.forEach(function (key) {
                        result = result.concat(_this.get(key));
                    });
                    return result;
                };
                Index.prototype.rebuild = function (items) {
                    this.clear();
                    this.build(items);
                };
                Index.prototype.build = function (items) {
                    var _this = this;
                    items.forEach(function (item) { return _this.push(item); });
                };
                Index.prototype.clear = function () {
                    this._map.clear();
                };
                return Index;
            }());
            exports_89("Index", Index);
        }
    }
});
System.register("app/common/IndexedCollection", ["app/common/Index"], function(exports_90, context_90) {
    "use strict";
    var __moduleName = context_90 && context_90.id;
    var Index_1;
    var IndexedCollection;
    return {
        setters:[
            function (Index_1_1) {
                Index_1 = Index_1_1;
            }],
        execute: function() {
            IndexedCollection = (function () {
                function IndexedCollection(items) {
                    this._items = [];
                    this._cursor = -1;
                    this._indexes = new Map();
                    if (items) {
                        this._items = items;
                    }
                }
                IndexedCollection.prototype.all = function () {
                    return this.items;
                };
                IndexedCollection.prototype.findBy = function (criteria) {
                    var result = [];
                    var _loop_1 = function(k) {
                        //noinspection JSUnfilteredForInLoop
                        var index = this_1.index(k).get(criteria[k]);
                        if (index.length == 0) {
                            return { value: [] };
                        }
                        else if (result.length == 0) {
                            result = index;
                        }
                        else {
                            //noinspection JSUnusedAssignment
                            result.filter(function (item) {
                                return index.indexOf(item) !== -1;
                            });
                            if (result.length == 0) {
                                return { value: [] };
                            }
                        }
                    };
                    var this_1 = this;
                    for (var k in criteria) {
                        var state_1 = _loop_1(k);
                        if (typeof state_1 === "object") return state_1.value;
                    }
                    return result;
                };
                IndexedCollection.prototype.push = function (item) {
                    var length = this._items.push(item);
                    this._indexes.forEach(function (index) {
                        index.push(item);
                    });
                    return length;
                };
                IndexedCollection.prototype.splice = function (start, deleteCount) {
                    return this._items.splice(start, deleteCount);
                };
                IndexedCollection.prototype.remove = function (item) {
                    this._indexes.forEach(function (index) {
                        index.remove(item);
                    });
                    var i = this._items.indexOf(item);
                    this._items.splice(i, 1);
                };
                IndexedCollection.prototype.indexOf = function (searchElement) {
                    return this._items.indexOf(searchElement);
                };
                IndexedCollection.prototype.forEach = function (callbackfn, thisArg) {
                    this._items.forEach(callbackfn, this);
                };
                Object.defineProperty(IndexedCollection.prototype, "items", {
                    get: function () {
                        return this._items;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(IndexedCollection.prototype, "length", {
                    get: function () {
                        return this._items.length;
                    },
                    enumerable: true,
                    configurable: true
                });
                IndexedCollection.prototype.next = function (value) {
                    return this._cursor < this._items.length
                        ? { done: false, value: this._items[this._cursor++] }
                        : { done: true };
                };
                IndexedCollection.prototype.index = function (property) {
                    if (!this._indexes.has(property)) {
                        this._indexes.set(property, new Index_1.Index(property, this._items));
                    }
                    return this._indexes.get(property);
                };
                IndexedCollection.prototype.reindex = function (property) {
                    var _this = this;
                    if (property) {
                        this.index(property).rebuild(this._items);
                    }
                    else {
                        this._indexes.forEach(function (index) {
                            index.rebuild(_this._items);
                        });
                    }
                };
                return IndexedCollection;
            }());
            exports_90("IndexedCollection", IndexedCollection);
        }
    }
});
System.register("app/persistance/SerializableCollection", [], function(exports_91, context_91) {
    "use strict";
    var __moduleName = context_91 && context_91.id;
    return {
        setters:[],
        execute: function() {
        }
    }
});
System.register("app/persistance/EntityCollection", ["app/common/IndexedCollection", "app/persistance/DTO"], function(exports_92, context_92) {
    "use strict";
    var __moduleName = context_92 && context_92.id;
    var IndexedCollection_1, DTO_2;
    var EntityCollection;
    return {
        setters:[
            function (IndexedCollection_1_1) {
                IndexedCollection_1 = IndexedCollection_1_1;
            },
            function (DTO_2_1) {
                DTO_2 = DTO_2_1;
            }],
        execute: function() {
            EntityCollection = (function (_super) {
                __extends(EntityCollection, _super);
                function EntityCollection(dao) {
                    _super.call(this);
                    if (dao) {
                        this.initialize(dao);
                    }
                }
                Object.defineProperty(EntityCollection.prototype, "dao", {
                    get: function () {
                        return this._dao;
                    },
                    enumerable: true,
                    configurable: true
                });
                EntityCollection.prototype.initialize = function (dao) {
                    if (this._dao) {
                        throw new Error('Collection already initialized');
                    }
                    this._dao = dao;
                };
                EntityCollection.prototype.restore = function (dto) {
                    for (var _i = 0, _a = dto.data(); _i < _a.length; _i++) {
                        var item = _a[_i];
                        this.push(this.restoreItem(new DTO_2.DTO(item)));
                    }
                };
                EntityCollection.prototype.serialize = function () {
                    var data = [];
                    for (var _i = 0, _a = this.all(); _i < _a.length; _i++) {
                        var item = _a[_i];
                        data.push(item.serialize().data());
                    }
                    return new DTO_2.DTO(data);
                };
                return EntityCollection;
            }(IndexedCollection_1.IndexedCollection));
            exports_92("EntityCollection", EntityCollection);
        }
    }
});
System.register("app/experiments/ExperimentURL", ["app/persistance/ModelEntity", "app/persistance/DTO"], function(exports_93, context_93) {
    "use strict";
    var __moduleName = context_93 && context_93.id;
    var ModelEntity_2, DTO_3;
    var ExperimentURL;
    return {
        setters:[
            function (ModelEntity_2_1) {
                ModelEntity_2 = ModelEntity_2_1;
            },
            function (DTO_3_1) {
                DTO_3 = DTO_3_1;
            }],
        execute: function() {
            ExperimentURL = (function (_super) {
                __extends(ExperimentURL, _super);
                function ExperimentURL() {
                    _super.apply(this, arguments);
                }
                ExperimentURL.prototype.build = function (dao) {
                    this.restore(dao.dto);
                };
                ExperimentURL.prototype.restore = function (dto) {
                    var data = dto.data();
                    this.id = data['id'];
                    this.pattern = data['pattern'];
                };
                ExperimentURL.prototype.serialize = function () {
                    return new DTO_3.DTO({
                        id: this.id,
                        pattern: this.pattern
                    });
                };
                return ExperimentURL;
            }(ModelEntity_2.ModelEntity));
            exports_93("ExperimentURL", ExperimentURL);
        }
    }
});
System.register("app/experiments/ExperimentURLCollection", ["app/persistance/EntityCollection", "app/experiments/ExperimentURL"], function(exports_94, context_94) {
    "use strict";
    var __moduleName = context_94 && context_94.id;
    var EntityCollection_1, ExperimentURL_1;
    var ExperimentURLCollection;
    return {
        setters:[
            function (EntityCollection_1_1) {
                EntityCollection_1 = EntityCollection_1_1;
            },
            function (ExperimentURL_1_1) {
                ExperimentURL_1 = ExperimentURL_1_1;
            }],
        execute: function() {
            ExperimentURLCollection = (function (_super) {
                __extends(ExperimentURLCollection, _super);
                function ExperimentURLCollection() {
                    _super.apply(this, arguments);
                }
                ExperimentURLCollection.prototype.restoreItem = function (dto) {
                    return new ExperimentURL_1.ExperimentURL(this.dao.create(dto));
                };
                return ExperimentURLCollection;
            }(EntityCollection_1.EntityCollection));
            exports_94("ExperimentURLCollection", ExperimentURLCollection);
        }
    }
});
System.register("app/experiments/Variation", ["app/persistance/ModelEntity", "app/persistance/DTO"], function(exports_95, context_95) {
    "use strict";
    var __moduleName = context_95 && context_95.id;
    var ModelEntity_3, DTO_4;
    var Variation;
    return {
        setters:[
            function (ModelEntity_3_1) {
                ModelEntity_3 = ModelEntity_3_1;
            },
            function (DTO_4_1) {
                DTO_4 = DTO_4_1;
            }],
        execute: function() {
            Variation = (function (_super) {
                __extends(Variation, _super);
                function Variation() {
                    _super.apply(this, arguments);
                }
                Variation.prototype.build = function (dao) {
                    this.restore(dao.dto);
                };
                Variation.prototype.restore = function (dto) {
                    var data = dto.data();
                    this.id = data['id'];
                    this.number = data['number'];
                    this.title = data['title'];
                    this.state = data['state'];
                    this.weight = data['weight'];
                    this.enabled = data['enabled'];
                };
                Variation.prototype.serialize = function () {
                    return new DTO_4.DTO({
                        id: this.id,
                        number: this.number,
                        title: this.title,
                        state: this.state,
                        weight: this.weight,
                        enabled: this.enabled
                    });
                };
                return Variation;
            }(ModelEntity_3.ModelEntity));
            exports_95("Variation", Variation);
        }
    }
});
System.register("app/experiments/VariationCollection", ["app/persistance/EntityCollection", "app/experiments/Variation"], function(exports_96, context_96) {
    "use strict";
    var __moduleName = context_96 && context_96.id;
    var EntityCollection_2, Variation_1;
    var VariationCollection;
    return {
        setters:[
            function (EntityCollection_2_1) {
                EntityCollection_2 = EntityCollection_2_1;
            },
            function (Variation_1_1) {
                Variation_1 = Variation_1_1;
            }],
        execute: function() {
            VariationCollection = (function (_super) {
                __extends(VariationCollection, _super);
                function VariationCollection() {
                    _super.apply(this, arguments);
                }
                VariationCollection.prototype.restoreItem = function (dto) {
                    return new Variation_1.Variation(this.dao.create(dto));
                };
                return VariationCollection;
            }(EntityCollection_2.EntityCollection));
            exports_96("VariationCollection", VariationCollection);
        }
    }
});
System.register("app/experiments/SnippetValue", ["app/persistance/ModelEntity", "app/persistance/DTO"], function(exports_97, context_97) {
    "use strict";
    var __moduleName = context_97 && context_97.id;
    var ModelEntity_4, DTO_5;
    var SnippetValue;
    return {
        setters:[
            function (ModelEntity_4_1) {
                ModelEntity_4 = ModelEntity_4_1;
            },
            function (DTO_5_1) {
                DTO_5 = DTO_5_1;
            }],
        execute: function() {
            SnippetValue = (function (_super) {
                __extends(SnippetValue, _super);
                function SnippetValue() {
                    _super.apply(this, arguments);
                }
                SnippetValue.prototype.build = function (dao) {
                    this.restore(dao.dto);
                };
                SnippetValue.prototype.restore = function (dto) {
                    var data = dto.data();
                    this.id = data['id'];
                    this.value = data['value'];
                };
                SnippetValue.prototype.serialize = function () {
                    return new DTO_5.DTO({
                        id: this.id,
                        value: this.value
                    });
                };
                return SnippetValue;
            }(ModelEntity_4.ModelEntity));
            exports_97("SnippetValue", SnippetValue);
        }
    }
});
System.register("app/experiments/SnippetValueCollection", ["app/persistance/EntityCollection", "app/experiments/SnippetValue"], function(exports_98, context_98) {
    "use strict";
    var __moduleName = context_98 && context_98.id;
    var EntityCollection_3, SnippetValue_1;
    var SnippetValueCollection;
    return {
        setters:[
            function (EntityCollection_3_1) {
                EntityCollection_3 = EntityCollection_3_1;
            },
            function (SnippetValue_1_1) {
                SnippetValue_1 = SnippetValue_1_1;
            }],
        execute: function() {
            SnippetValueCollection = (function (_super) {
                __extends(SnippetValueCollection, _super);
                function SnippetValueCollection() {
                    _super.apply(this, arguments);
                }
                SnippetValueCollection.prototype.restoreItem = function (dto) {
                    return new SnippetValue_1.SnippetValue(this.dao.create(dto));
                };
                return SnippetValueCollection;
            }(EntityCollection_3.EntityCollection));
            exports_98("SnippetValueCollection", SnippetValueCollection);
        }
    }
});
System.register("app/experiments/Snippet", ["app/persistance/ModelEntity", "app/persistance/DTO", "app/experiments/SnippetValueCollection"], function(exports_99, context_99) {
    "use strict";
    var __moduleName = context_99 && context_99.id;
    var ModelEntity_5, DTO_6, SnippetValueCollection_1;
    var Snippet;
    return {
        setters:[
            function (ModelEntity_5_1) {
                ModelEntity_5 = ModelEntity_5_1;
            },
            function (DTO_6_1) {
                DTO_6 = DTO_6_1;
            },
            function (SnippetValueCollection_1_1) {
                SnippetValueCollection_1 = SnippetValueCollection_1_1;
            }],
        execute: function() {
            Snippet = (function (_super) {
                __extends(Snippet, _super);
                function Snippet() {
                    _super.apply(this, arguments);
                }
                Snippet.prototype.build = function (dao) {
                    // this.values = new SnippetValueCollection(dao.collection('values'));
                    this.values = new SnippetValueCollection_1.SnippetValueCollection();
                    this.values.initialize(dao.collection('values'));
                    this.restore(dao.dto);
                };
                Snippet.prototype.restore = function (dto) {
                    var data = dto.data();
                    this.id = data['id'];
                    this.number = data['number'];
                    this.title = data['title'];
                    this.values.restore(new DTO_6.DTO(data['values']));
                };
                Snippet.prototype.serialize = function () {
                    return new DTO_6.DTO({
                        id: this.id,
                        number: this.number,
                        title: this.title
                    });
                };
                return Snippet;
            }(ModelEntity_5.ModelEntity));
            exports_99("Snippet", Snippet);
        }
    }
});
System.register("app/experiments/SnippetCollection", ["app/persistance/EntityCollection", "app/experiments/Snippet"], function(exports_100, context_100) {
    "use strict";
    var __moduleName = context_100 && context_100.id;
    var EntityCollection_4, Snippet_1;
    var SnippetCollection;
    return {
        setters:[
            function (EntityCollection_4_1) {
                EntityCollection_4 = EntityCollection_4_1;
            },
            function (Snippet_1_1) {
                Snippet_1 = Snippet_1_1;
            }],
        execute: function() {
            SnippetCollection = (function (_super) {
                __extends(SnippetCollection, _super);
                function SnippetCollection() {
                    _super.apply(this, arguments);
                }
                SnippetCollection.prototype.restoreItem = function (dto) {
                    return new Snippet_1.Snippet(this.dao.create(dto));
                };
                return SnippetCollection;
            }(EntityCollection_4.EntityCollection));
            exports_100("SnippetCollection", SnippetCollection);
        }
    }
});
System.register("app/experiments/GoalDestination", ["app/persistance/ModelEntity", "app/persistance/DTO"], function(exports_101, context_101) {
    "use strict";
    var __moduleName = context_101 && context_101.id;
    var ModelEntity_6, DTO_7;
    var GoalDestination;
    return {
        setters:[
            function (ModelEntity_6_1) {
                ModelEntity_6 = ModelEntity_6_1;
            },
            function (DTO_7_1) {
                DTO_7 = DTO_7_1;
            }],
        execute: function() {
            GoalDestination = (function (_super) {
                __extends(GoalDestination, _super);
                function GoalDestination() {
                    _super.apply(this, arguments);
                }
                GoalDestination.prototype.build = function (dao) {
                    this.restore(dao.dto);
                };
                GoalDestination.prototype.restore = function (dto) {
                    var data = dto.data();
                    this.id = data['id'];
                    this.value = data['value'];
                };
                GoalDestination.prototype.serialize = function () {
                    return new DTO_7.DTO({
                        id: this.id,
                        value: this.value
                    });
                };
                return GoalDestination;
            }(ModelEntity_6.ModelEntity));
            exports_101("GoalDestination", GoalDestination);
        }
    }
});
System.register("app/experiments/GoalDestinationCollection", ["app/persistance/EntityCollection", "app/experiments/GoalDestination"], function(exports_102, context_102) {
    "use strict";
    var __moduleName = context_102 && context_102.id;
    var EntityCollection_5, GoalDestination_1;
    var GoalDestinationCollection;
    return {
        setters:[
            function (EntityCollection_5_1) {
                EntityCollection_5 = EntityCollection_5_1;
            },
            function (GoalDestination_1_1) {
                GoalDestination_1 = GoalDestination_1_1;
            }],
        execute: function() {
            GoalDestinationCollection = (function (_super) {
                __extends(GoalDestinationCollection, _super);
                function GoalDestinationCollection() {
                    _super.apply(this, arguments);
                }
                GoalDestinationCollection.prototype.restoreItem = function (dto) {
                    return new GoalDestination_1.GoalDestination(this.dao.create(dto));
                };
                return GoalDestinationCollection;
            }(EntityCollection_5.EntityCollection));
            exports_102("GoalDestinationCollection", GoalDestinationCollection);
        }
    }
});
System.register("app/experiments/Goal", ["app/persistance/ModelEntity", "app/persistance/DTO", "app/experiments/GoalDestinationCollection"], function(exports_103, context_103) {
    "use strict";
    var __moduleName = context_103 && context_103.id;
    var ModelEntity_7, DTO_8, GoalDestinationCollection_1;
    var Goal;
    return {
        setters:[
            function (ModelEntity_7_1) {
                ModelEntity_7 = ModelEntity_7_1;
            },
            function (DTO_8_1) {
                DTO_8 = DTO_8_1;
            },
            function (GoalDestinationCollection_1_1) {
                GoalDestinationCollection_1 = GoalDestinationCollection_1_1;
            }],
        execute: function() {
            Goal = (function (_super) {
                __extends(Goal, _super);
                function Goal() {
                    _super.apply(this, arguments);
                }
                Goal.prototype.build = function (dao) {
                    // this.destinations = new GoalDestinationCollection(dao.collection('destinations'));
                    this.destinations = new GoalDestinationCollection_1.GoalDestinationCollection();
                    this.destinations.initialize(dao.collection('destinations'));
                    this.restore(dao.dto);
                };
                Goal.prototype.restore = function (dto) {
                    var data = dto.data();
                    this.id = data['id'];
                    this.destinations.restore(new DTO_8.DTO([data['destination']]));
                };
                Goal.prototype.serialize = function () {
                    return new DTO_8.DTO({
                        id: this.id,
                        title: this.title
                    });
                };
                return Goal;
            }(ModelEntity_7.ModelEntity));
            exports_103("Goal", Goal);
        }
    }
});
System.register("app/experiments/GoalCollection", ["app/persistance/EntityCollection", "app/experiments/Goal"], function(exports_104, context_104) {
    "use strict";
    var __moduleName = context_104 && context_104.id;
    var EntityCollection_6, Goal_1;
    var GoalCollection;
    return {
        setters:[
            function (EntityCollection_6_1) {
                EntityCollection_6 = EntityCollection_6_1;
            },
            function (Goal_1_1) {
                Goal_1 = Goal_1_1;
            }],
        execute: function() {
            GoalCollection = (function (_super) {
                __extends(GoalCollection, _super);
                function GoalCollection() {
                    _super.apply(this, arguments);
                }
                GoalCollection.prototype.restoreItem = function (dto) {
                    return new Goal_1.Goal(this.dao.create(dto));
                };
                return GoalCollection;
            }(EntityCollection_6.EntityCollection));
            exports_104("GoalCollection", GoalCollection);
        }
    }
});
System.register("app/experiments/Experiment", ["app/persistance/DTO", "app/aggregate/AggregateRoot", "app/experiments/ExperimentURLCollection", "app/experiments/VariationCollection", "app/experiments/SnippetCollection", "app/experiments/GoalCollection"], function(exports_105, context_105) {
    "use strict";
    var __moduleName = context_105 && context_105.id;
    var DTO_9, AggregateRoot_1, ExperimentURLCollection_1, VariationCollection_1, SnippetCollection_1, GoalCollection_1;
    var Experiment;
    return {
        setters:[
            function (DTO_9_1) {
                DTO_9 = DTO_9_1;
            },
            function (AggregateRoot_1_1) {
                AggregateRoot_1 = AggregateRoot_1_1;
            },
            function (ExperimentURLCollection_1_1) {
                ExperimentURLCollection_1 = ExperimentURLCollection_1_1;
            },
            function (VariationCollection_1_1) {
                VariationCollection_1 = VariationCollection_1_1;
            },
            function (SnippetCollection_1_1) {
                SnippetCollection_1 = SnippetCollection_1_1;
            },
            function (GoalCollection_1_1) {
                GoalCollection_1 = GoalCollection_1_1;
            }],
        execute: function() {
            Experiment = (function (_super) {
                __extends(Experiment, _super);
                function Experiment() {
                    _super.apply(this, arguments);
                }
                /*constructor(dao?: ElementDAO) {
                    this.urls = new ExperimentURLCollection();
                    super(dao);
                }*/
                Experiment.prototype.build = function (dao) {
                    console.log('build experiment');
                    this.urls = new ExperimentURLCollection_1.ExperimentURLCollection(dao.collection('urls'));
                    this.variations = new VariationCollection_1.VariationCollection(dao.collection('variations'));
                    this.snippets = new SnippetCollection_1.SnippetCollection(dao.collection('snippets'));
                    this.goals = new GoalCollection_1.GoalCollection(dao.collection('goals'));
                    // this.urls.initialize(dao.collection('urls'));
                    // this.variations.initialize(dao.collection('variations'));
                    // this.snippets.initialize(dao.collection('snippets'));
                    // this.goals.initialize(dao.collection('goals'));
                    this.restore(dao.dto);
                };
                Experiment.prototype.restore = function (dto) {
                    var data = dto.data();
                    this.id = data['id'];
                    this.title = data['title'];
                    this.urls.restore(new DTO_9.DTO(data['urls']));
                    this.variations.restore(new DTO_9.DTO(data['variations']));
                    this.snippets.restore(new DTO_9.DTO(data['snippets']));
                    this.goals.restore(new DTO_9.DTO(data['goals']));
                };
                Experiment.prototype.serialize = function () {
                    return new DTO_9.DTO({
                        id: this.id,
                        title: this.title,
                        urls: this.urls.serialize(),
                        variations: this.variations.serialize()
                    });
                };
                return Experiment;
            }(AggregateRoot_1.AggregateRoot));
            exports_105("Experiment", Experiment);
        }
    }
});
System.register("app/persistance/RootDAO", ["app/persistance/CollectionDAO"], function(exports_106, context_106) {
    "use strict";
    var __moduleName = context_106 && context_106.id;
    var CollectionDAO_2;
    var RootDAO;
    return {
        setters:[
            function (CollectionDAO_2_1) {
                CollectionDAO_2 = CollectionDAO_2_1;
            }],
        execute: function() {
            RootDAO = (function (_super) {
                __extends(RootDAO, _super);
                function RootDAO(service, path) {
                    _super.call(this, service, null, path);
                }
                return RootDAO;
            }(CollectionDAO_2.CollectionDAO));
            exports_106("RootDAO", RootDAO);
        }
    }
});
System.register("app/persistance/DomainCollection", ["app/persistance/EntityCollection"], function(exports_107, context_107) {
    "use strict";
    var __moduleName = context_107 && context_107.id;
    var EntityCollection_7;
    var DomainCollection;
    return {
        setters:[
            function (EntityCollection_7_1) {
                EntityCollection_7 = EntityCollection_7_1;
            }],
        execute: function() {
            DomainCollection = (function (_super) {
                __extends(DomainCollection, _super);
                function DomainCollection() {
                    _super.apply(this, arguments);
                }
                DomainCollection.prototype.find = function (id) {
                    var items = this.findBy({ id: id });
                    if (items.length > 1) {
                        throw new Error('Duplicate entity id');
                    }
                    return items.length == 0 ? null : items[0];
                };
                return DomainCollection;
            }(EntityCollection_7.EntityCollection));
            exports_107("DomainCollection", DomainCollection);
        }
    }
});
System.register("app/persistance/DomainRepository", ["app/persistance/EntityRepository"], function(exports_108, context_108) {
    "use strict";
    var __moduleName = context_108 && context_108.id;
    var EntityRepository_1;
    var DomainRepository;
    return {
        setters:[
            function (EntityRepository_1_1) {
                EntityRepository_1 = EntityRepository_1_1;
            }],
        execute: function() {
            DomainRepository = (function (_super) {
                __extends(DomainRepository, _super);
                function DomainRepository(dao, collection) {
                    _super.call(this);
                    this._rootDAO = dao;
                    this._list = collection;
                    this.init();
                }
                DomainRepository.prototype.add = function (entity) {
                    this._list.push(entity);
                };
                DomainRepository.prototype.all = function () {
                    return this._list.all();
                };
                DomainRepository.prototype.attach = function (entity) {
                    entity.attach(this);
                };
                DomainRepository.prototype.build = function (source, destination) {
                    for (var _i = 0, source_1 = source; _i < source_1.length; _i++) {
                        var item = source_1[_i];
                        destination.push(this.buildItem(item));
                    }
                };
                /**
                 * @todo
                 */
                DomainRepository.prototype.createQueryBuilder = function () {
                };
                Object.defineProperty(DomainRepository.prototype, "dao", {
                    get: function () {
                        return this._rootDAO;
                    },
                    enumerable: true,
                    configurable: true
                });
                DomainRepository.prototype.find = function (id) {
                    return this._list.find(id);
                };
                DomainRepository.prototype.findBy = function (criteria) {
                    return this._list.findBy(criteria);
                };
                DomainRepository.prototype.init = function () {
                    var _this = this;
                    this.dao.all()
                        .then(function (result) {
                        _this.build(result, _this._list);
                        console.log(_this._list);
                    })
                        .catch(function (error) {
                        console.error(error);
                    });
                };
                return DomainRepository;
            }(EntityRepository_1.EntityRepository));
            exports_108("DomainRepository", DomainRepository);
        }
    }
});
System.register("app/experiments/ExperimentCollection", ["app/persistance/DomainCollection", "app/experiments/Experiment"], function(exports_109, context_109) {
    "use strict";
    var __moduleName = context_109 && context_109.id;
    var DomainCollection_1, Experiment_1;
    var ExperimentCollection;
    return {
        setters:[
            function (DomainCollection_1_1) {
                DomainCollection_1 = DomainCollection_1_1;
            },
            function (Experiment_1_1) {
                Experiment_1 = Experiment_1_1;
            }],
        execute: function() {
            ExperimentCollection = (function (_super) {
                __extends(ExperimentCollection, _super);
                function ExperimentCollection() {
                    _super.apply(this, arguments);
                }
                ExperimentCollection.prototype.restoreItem = function (dto) {
                    return new Experiment_1.Experiment(this.dao.create(dto));
                };
                return ExperimentCollection;
            }(DomainCollection_1.DomainCollection));
            exports_109("ExperimentCollection", ExperimentCollection);
        }
    }
});
System.register("app/experiments/ExperimentRepository", ["app/experiments/Experiment", "app/persistance/DomainRepository", "app/experiments/ExperimentCollection"], function(exports_110, context_110) {
    "use strict";
    var __moduleName = context_110 && context_110.id;
    var Experiment_2, DomainRepository_1, ExperimentCollection_1;
    var ExperimentRepository;
    return {
        setters:[
            function (Experiment_2_1) {
                Experiment_2 = Experiment_2_1;
            },
            function (DomainRepository_1_1) {
                DomainRepository_1 = DomainRepository_1_1;
            },
            function (ExperimentCollection_1_1) {
                ExperimentCollection_1 = ExperimentCollection_1_1;
            }],
        execute: function() {
            ExperimentRepository = (function (_super) {
                __extends(ExperimentRepository, _super);
                function ExperimentRepository(dao) {
                    _super.call(this, dao, new ExperimentCollection_1.ExperimentCollection(dao));
                }
                ExperimentRepository.prototype.buildItem = function (source) {
                    return new Experiment_2.Experiment(source);
                };
                return ExperimentRepository;
            }(DomainRepository_1.DomainRepository));
            exports_110("ExperimentRepository", ExperimentRepository);
        }
    }
});
System.register("app/common/Factory", [], function(exports_111, context_111) {
    "use strict";
    var __moduleName = context_111 && context_111.id;
    var Factory;
    return {
        setters:[],
        execute: function() {
            Factory = (function () {
                function Factory(__ct) {
                    this.__ct = __ct;
                }
                Factory.prototype.create = function () {
                    return new this.__ct();
                };
                return Factory;
            }());
            exports_111("Factory", Factory);
        }
    }
});
System.register("app/aggregate/EntityFactory", ["app/common/Factory"], function(exports_112, context_112) {
    "use strict";
    var __moduleName = context_112 && context_112.id;
    var Factory_1;
    var EntityFactory;
    return {
        setters:[
            function (Factory_1_1) {
                Factory_1 = Factory_1_1;
            }],
        execute: function() {
            EntityFactory = (function (_super) {
                __extends(EntityFactory, _super);
                function EntityFactory() {
                    _super.apply(this, arguments);
                }
                EntityFactory.prototype.create = function (data) {
                    // @todo data?
                    return _super.prototype.create.call(this);
                };
                return EntityFactory;
            }(Factory_1.Factory));
            exports_112("EntityFactory", EntityFactory);
        }
    }
});
System.register("app/aggregate/DomainFactory", ["app/aggregate/EntityFactory"], function(exports_113, context_113) {
    "use strict";
    var __moduleName = context_113 && context_113.id;
    var EntityFactory_1;
    var DomainFactory;
    return {
        setters:[
            function (EntityFactory_1_1) {
                EntityFactory_1 = EntityFactory_1_1;
            }],
        execute: function() {
            DomainFactory = (function (_super) {
                __extends(DomainFactory, _super);
                function DomainFactory() {
                    _super.apply(this, arguments);
                }
                return DomainFactory;
            }(EntityFactory_1.EntityFactory));
            exports_113("DomainFactory", DomainFactory);
        }
    }
});
System.register("app/experiments/ExperimentFactory", ["app/aggregate/DomainFactory", "app/experiments/Experiment"], function(exports_114, context_114) {
    "use strict";
    var __moduleName = context_114 && context_114.id;
    var DomainFactory_1, Experiment_3;
    var ExperimentFactory;
    return {
        setters:[
            function (DomainFactory_1_1) {
                DomainFactory_1 = DomainFactory_1_1;
            },
            function (Experiment_3_1) {
                Experiment_3 = Experiment_3_1;
            }],
        execute: function() {
            ExperimentFactory = (function (_super) {
                __extends(ExperimentFactory, _super);
                function ExperimentFactory() {
                    _super.call(this, Experiment_3.Experiment);
                }
                return ExperimentFactory;
            }(DomainFactory_1.DomainFactory));
            exports_114("ExperimentFactory", ExperimentFactory);
        }
    }
});
System.register("app/experiments/ExperimentService", ['angular2/core', "app/persistance/EntityManager", "app/experiments/ExperimentRepository", "app/experiments/ExperimentFactory", "app/persistance/RootDAO", "app/persistance/RestangularBackend"], function(exports_115, context_115) {
    "use strict";
    var __moduleName = context_115 && context_115.id;
    var core_50, EntityManager_1, ExperimentRepository_1, ExperimentFactory_1, RootDAO_1, RestangularBackend_1;
    var ExperimentService;
    return {
        setters:[
            function (core_50_1) {
                core_50 = core_50_1;
            },
            function (EntityManager_1_1) {
                EntityManager_1 = EntityManager_1_1;
            },
            function (ExperimentRepository_1_1) {
                ExperimentRepository_1 = ExperimentRepository_1_1;
            },
            function (ExperimentFactory_1_1) {
                ExperimentFactory_1 = ExperimentFactory_1_1;
            },
            function (RootDAO_1_1) {
                RootDAO_1 = RootDAO_1_1;
            },
            function (RestangularBackend_1_1) {
                RestangularBackend_1 = RestangularBackend_1_1;
            }],
        execute: function() {
            ExperimentService = (function () {
                function ExperimentService(backend, em) {
                    this._backend = backend;
                    this._em = em;
                    var dao = new RootDAO_1.RootDAO(this._backend.service, 'experiments');
                    this._repository = new ExperimentRepository_1.ExperimentRepository(dao);
                    this._factory = new ExperimentFactory_1.ExperimentFactory();
                }
                ExperimentService.prototype.create = function (data) {
                    var entity = this._factory.create(data);
                    entity.attach(this._repository);
                    return entity;
                };
                Object.defineProperty(ExperimentService.prototype, "list", {
                    get: function () {
                        return this._repository.all();
                    },
                    enumerable: true,
                    configurable: true
                });
                ExperimentService = __decorate([
                    core_50.Injectable(),
                    __param(0, core_50.Inject(RestangularBackend_1.RestangularBackend)),
                    __param(1, core_50.Inject(EntityManager_1.EntityManager)), 
                    __metadata('design:paramtypes', [RestangularBackend_1.RestangularBackend, EntityManager_1.EntityManager])
                ], ExperimentService);
                return ExperimentService;
            }());
            exports_115("ExperimentService", ExperimentService);
        }
    }
});
System.register("app/tests/ExperimentComponent", ['angular2/core', "app/persistance/RestangularBackend", "app/persistance/EntityManager", "app/experiments/ExperimentService", "app/ui-edit-inline.directive"], function(exports_116, context_116) {
    "use strict";
    var __moduleName = context_116 && context_116.id;
    var core_51, RestangularBackend_2, EntityManager_2, ExperimentService_1, ui_edit_inline_directive_8;
    var ExperimentComponent;
    return {
        setters:[
            function (core_51_1) {
                core_51 = core_51_1;
            },
            function (RestangularBackend_2_1) {
                RestangularBackend_2 = RestangularBackend_2_1;
            },
            function (EntityManager_2_1) {
                EntityManager_2 = EntityManager_2_1;
            },
            function (ExperimentService_1_1) {
                ExperimentService_1 = ExperimentService_1_1;
            },
            function (ui_edit_inline_directive_8_1) {
                ui_edit_inline_directive_8 = ui_edit_inline_directive_8_1;
            }],
        execute: function() {
            ExperimentComponent = (function () {
                function ExperimentComponent(experiments) {
                    this.experiments = experiments;
                }
                ExperimentComponent.prototype.ngOnInit = function () {
                    var experiment = this.experiments.create();
                    console.log(experiment);
                };
                ExperimentComponent.prototype.update = function (event) {
                    console.log(event);
                };
                ExperimentComponent = __decorate([
                    core_51.Component({
                        selector: 'experiment-component',
                        template: "\n<!--suppress HtmlUnknownAttribute -->\n<div class=\"component-root\">\n  <h3>Experiments</h3>\n  <div *ngFor=\"#item of experiments.list\" class=\"experiment-item\">\n    <h4>{{ item.title }}</h4>\n    <div class=\"urls\">\n        <div *ngFor=\"#url of item.urls.items\">\n            <input [edit-inline]\n                   [(ngModel)]=\"url.pattern\"\n                   (change)=\"update(url)\"\n                   [placeholder]=\"'Experiment URL'\"/>\n        </div>\n    </div>\n    <div class=\"variations\">\n        <div *ngFor=\"#variation of item.variations.items\">\n            <h4><input [edit-inline]\n                       [(ngModel)]=\"variation.title\"\n                       (change)=\"update(variation)\"\n                       [placeholder]=\"'Variation Title'\"/></h4>\n        </div>\n    </div>\n  </div>\n</div>\n    ",
                        providers: [
                            RestangularBackend_2.RestangularBackend,
                            EntityManager_2.EntityManager,
                            ExperimentService_1.ExperimentService
                        ],
                        directives: [
                            ui_edit_inline_directive_8.UiEditInlineDirective
                        ]
                    }), 
                    __metadata('design:paramtypes', [ExperimentService_1.ExperimentService])
                ], ExperimentComponent);
                return ExperimentComponent;
            }());
            exports_116("ExperimentComponent", ExperimentComponent);
        }
    }
});
System.register("app/app.component", ['angular2/core', 'angular2/router', 'angular2/common', "app/auth", "app/dashboard.component", "app/notification-bar.component", "app/menu-sidebar.component", "app/ab-experiments.component", "app/ab-experiments-list.component", "app/details-sidebar.component", "app/details-sidebar.service", "app/menu-top.component", "app/login-page.component", "app/em/experiments.service", "app/em/experiment-stats.service", "app/profile.component", "app/config.service", "app/messages.service", "app/keep-alive.service", "app/notification.service", "app/xhr.service", "app/em/restangular.service", "app/em/data.service", "app/em/entity-manager", "app/tests/ExperimentComponent"], function(exports_117, context_117) {
    "use strict";
    var __moduleName = context_117 && context_117.id;
    var core_52, router_8, router_9, common_3, auth_16, dashboard_component_1, notification_bar_component_1, menu_sidebar_component_1, ab_experiments_component_1, ab_experiments_list_component_1, details_sidebar_component_1, details_sidebar_service_4, menu_top_component_1, login_page_component_1, experiments_service_3, experiment_stats_service_3, profile_component_1, config_service_4, messages_service_2, keep_alive_service_1, notification_service_11, xhr_service_3, restangular_service_4, data_service_13, entity_manager_2, ExperimentComponent_1;
    var AppComponent;
    return {
        setters:[
            function (core_52_1) {
                core_52 = core_52_1;
            },
            function (router_8_1) {
                router_8 = router_8_1;
                router_9 = router_8_1;
            },
            function (common_3_1) {
                common_3 = common_3_1;
            },
            function (auth_16_1) {
                auth_16 = auth_16_1;
            },
            function (dashboard_component_1_1) {
                dashboard_component_1 = dashboard_component_1_1;
            },
            function (notification_bar_component_1_1) {
                notification_bar_component_1 = notification_bar_component_1_1;
            },
            function (menu_sidebar_component_1_1) {
                menu_sidebar_component_1 = menu_sidebar_component_1_1;
            },
            function (ab_experiments_component_1_1) {
                ab_experiments_component_1 = ab_experiments_component_1_1;
            },
            function (ab_experiments_list_component_1_1) {
                ab_experiments_list_component_1 = ab_experiments_list_component_1_1;
            },
            function (details_sidebar_component_1_1) {
                details_sidebar_component_1 = details_sidebar_component_1_1;
            },
            function (details_sidebar_service_4_1) {
                details_sidebar_service_4 = details_sidebar_service_4_1;
            },
            function (menu_top_component_1_1) {
                menu_top_component_1 = menu_top_component_1_1;
            },
            function (login_page_component_1_1) {
                login_page_component_1 = login_page_component_1_1;
            },
            function (experiments_service_3_1) {
                experiments_service_3 = experiments_service_3_1;
            },
            function (experiment_stats_service_3_1) {
                experiment_stats_service_3 = experiment_stats_service_3_1;
            },
            function (profile_component_1_1) {
                profile_component_1 = profile_component_1_1;
            },
            function (config_service_4_1) {
                config_service_4 = config_service_4_1;
            },
            function (messages_service_2_1) {
                messages_service_2 = messages_service_2_1;
            },
            function (keep_alive_service_1_1) {
                keep_alive_service_1 = keep_alive_service_1_1;
            },
            function (notification_service_11_1) {
                notification_service_11 = notification_service_11_1;
            },
            function (xhr_service_3_1) {
                xhr_service_3 = xhr_service_3_1;
            },
            function (restangular_service_4_1) {
                restangular_service_4 = restangular_service_4_1;
            },
            function (data_service_13_1) {
                data_service_13 = data_service_13_1;
            },
            function (entity_manager_2_1) {
                entity_manager_2 = entity_manager_2_1;
            },
            function (ExperimentComponent_1_1) {
                ExperimentComponent_1 = ExperimentComponent_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent(auth, keepAlive, data, router) {
                    this.auth = auth;
                    this.keepAlive = keepAlive;
                    this.data = data;
                    this.router = router;
                    this.isLoggedIn = false;
                    //router.navigate(['Dashboard']);
                }
                AppComponent.prototype.ngOnInit = function () {
                    this.keepAlive.initialize();
                };
                AppComponent.prototype.loadData = function () {
                };
                AppComponent = __decorate([
                    core_52.Component({
                        selector: 'so-app',
                        providers: [
                            config_service_4.ConfigService,
                            xhr_service_3.XhrService,
                            auth_16.AuthService,
                            restangular_service_4.RestangularService,
                            data_service_13.DataService,
                            entity_manager_2.EntityManager,
                            keep_alive_service_1.KeepAliveService,
                            messages_service_2.MessagesService,
                            notification_service_11.NotificationService,
                            details_sidebar_service_4.DetailsSidebarService,
                            experiments_service_3.ExperimentsService,
                            experiment_stats_service_3.ExperimentStatsService
                        ],
                        templateUrl: 'templates/app.html',
                        directives: [
                            router_8.ROUTER_DIRECTIVES,
                            common_3.FORM_DIRECTIVES,
                            menu_top_component_1.MenuTopComponent,
                            menu_sidebar_component_1.MenuSidebarComponent,
                            notification_bar_component_1.NotificationBarComponent,
                            details_sidebar_component_1.DetailsSidebarComponent,
                            login_page_component_1.LoginPageComponent
                        ]
                    }),
                    router_8.RouteConfig([
                        { path: '/dashboard', name: 'Dashboard', component: dashboard_component_1.DashboardComponent, useAsDefault: true },
                        { path: '/login', name: 'LoginPage', component: login_page_component_1.LoginPageComponent },
                        { path: '/profile', name: 'ProfilePage', component: profile_component_1.ProfileComponent },
                        { path: '/experiments/ab/...', name: 'AbExperiments', component: ab_experiments_component_1.AbExperimentsComponent },
                        { path: '/experiments/ab', name: 'AbExperimentsList', component: ab_experiments_list_component_1.AbExperimentsListComponent },
                        { path: '/test/experiment', name: 'ExperimentTest', component: ExperimentComponent_1.ExperimentComponent }
                    ]), 
                    __metadata('design:paramtypes', [auth_16.AuthService, keep_alive_service_1.KeepAliveService, data_service_13.DataService, router_9.Router])
                ], AppComponent);
                return AppComponent;
            }());
            exports_117("AppComponent", AppComponent);
        }
    }
});
System.register("app/boot", ['angular2/core', 'angular2/platform/browser', 'angular2/router', 'angular2/http', "app/app.component"], function(exports_118, context_118) {
    "use strict";
    var __moduleName = context_118 && context_118.id;
    var core_53, browser_1, router_10, http_6, app_component_1;
    return {
        setters:[
            function (core_53_1) {
                core_53 = core_53_1;
            },
            function (browser_1_1) {
                browser_1 = browser_1_1;
            },
            function (router_10_1) {
                router_10 = router_10_1;
            },
            function (http_6_1) {
                http_6 = http_6_1;
            },
            function (app_component_1_1) {
                app_component_1 = app_component_1_1;
            }],
        execute: function() {
            core_53.enableProdMode();
            browser_1.bootstrap(app_component_1.AppComponent, [router_10.ROUTER_PROVIDERS, http_6.HTTP_PROVIDERS, core_53.provide(router_10.APP_BASE_HREF, { useValue: '/' })]);
        }
    }
});
//# sourceMappingURL=bundle.js.map