const gulp = require('gulp');
const del = require('del');
var uglify = require('gulp-uglify');
var rename = require("gulp-rename");


gulp.task('clean:libs', function () {
    return del('dist/lib/**/*');
});

gulp.task('copy:libs', ['clean:libs'], function () {
    return gulp.src([
            'node_modules/es6-shim/es6-shim.min.js',
            'node_modules/es6-shim/es6-shim.map',
            'node_modules/systemjs/dist/system-polyfills.js',
            'node_modules/systemjs/dist/system-polyfills.js.map',
            'node_modules/angular2/es6/dev/src/testing/shims_for_IE.js',
            'node_modules/angular2/bundles/angular2-polyfills.js',
            'node_modules/systemjs/dist/system.src.js',
            'node_modules/rxjs/bundles/Rx.js',
            'node_modules/angular2/bundles/angular2.dev.js',
            'node_modules/angular2/bundles/router.dev.js',
            'node_modules/angular2/bundles/http.dev.js',
            'node_modules/sprintf-js/dist/sprintf.min.js',
            'node_modules/sprintf-js/dist/sprintf.min.map',
            'node_modules/jquery/dist/jquery.min.js',
            'node_modules/q/q.js',
            'node_modules/lodash/lodash.min.js',
            'node_modules/angular/angular.min.js',
            'node_modules/angular/angular.min.js.map',
            'src/lib/restangular.js',
            'node_modules/bootstrap/dist/js/bootstrap.min.js',
            'node_modules/ladda/dist/ladda.min.js',
            'node_modules/spin.js/spin.min.js'
        ])
        .pipe(gulp.dest('dist/lib'));
});

gulp.task('copy:config', function () {
    return gulp.src([
            'src/app.config.js'
        ])
        .pipe(gulp.dest('dist'));
});

gulp.task('compress', function () {
    return gulp.src('dist/bundle.js')
        .pipe(uglify({
            mangle: false,
            compress: false
        }))
        .pipe(rename('bundle.min.js'))
        .pipe(gulp.dest('dist'));
});


gulp.task('build', ['copy:libs', 'copy:config', 'compress']);
gulp.task('default', ['build']);
