import {Component, Output, EventEmitter} from 'angular2/core';

import {NotificationMessage} from "./notification-message";
import {NotificationService} from "./notification.service";
import {NotificationSubscriberInterface} from "./notification-subscriber";


@Component({
    selector: 'notification-bar',
    templateUrl: 'templates/notification-bar.html'
})
export class NotificationBarComponent {

    messages:NotificationMessage[] = [];

    active:NotificationMessage[] = [];

    open:boolean = false;

    private closing:any;

    @Output('open')
    onOpen: EventEmitter<any> = new EventEmitter();

    @Output('close')
    onClose: EventEmitter<any> = new EventEmitter();


    constructor(service:NotificationService) {
        service.subscribe(NotificationSubscriber.create((message:NotificationBarMessage) => {
            this.show(message);
        }));
    }

    show(message:NotificationBarMessage):void {
        this.messages.push(message);
        this.active.push(message);
        this.open = true;
        this.onOpen.emit(null);

        setTimeout(() => {
            if (this.active.length == 1) {
                this.open = false;
                this.onClose.emit(null);
            }
            message.closing = true;
        }, 4000);

        setTimeout(() => {
            this.active.splice(this.active.indexOf(message), 1);
        }, 4500);
    }
}

class NotificationSubscriber implements NotificationSubscriberInterface {

    constructor(private callback:Function) {
    }

    static create(callback:Function):NotificationSubscriber {
        return new NotificationSubscriber(callback);
    }

    next(message:NotificationMessage):void {
        this.callback(new NotificationBarMessage(message));
    }
}

export class NotificationBarMessage extends NotificationMessage {

    private _classes:string[] = ['success', 'info', 'info', 'warning', 'danger'];
    private _prefixes:string[] = ['Success', 'Info', 'Notice', 'Warning', 'Error'];

    closing:boolean = false;

    constructor(message:NotificationMessage) {
        super(message.content, message.type);
    }

    get classes():string {
        return 'alert-' + this._classes[this.type] + (this.closing ? ' closing' : '');
    }

    get prefix():string {
        return this._prefixes[this.type];
    }
}
