import {Component, Input} from 'angular2/core';

import {ExperimentGoalComponent} from "./experiment-goal.component";
import {ExperimentsService} from "../em/experiments.service";
import {AuthService} from "../auth";
import {UiPopoverComponent} from "../ui-popover.component";
import {Experiment} from '../model/experiment';
import {Goal} from '../model/goal';
import {DataService} from '../em/data.service';
import {NotificationService} from '../notification.service';
import {GoalDestination} from '../model/goal-destination';


@Component({
    selector: 'experiment-goal-list',
    templateUrl: 'templates/ab-experiment-details/experiment-goal-list.html',
    directives: [
        ExperimentGoalComponent,
        UiPopoverComponent
    ]
})
export class ExperimentGoalListComponent {

    @Input()
    experiment: Experiment;

    constructor(private data: DataService,
                private auth: AuthService,
                private notifications: NotificationService) {
    }

    get goals(): Goal[] {
        return this.experiment.goals.items;
    }

    create() {

        this.data.create<Goal>(Goal, this.experiment)
            .then(goal => {
                this.experiment.goals.push(goal);
                this.data.create<GoalDestination>(GoalDestination, goal)
                    .then(destination => {
                        goal.destinations.push(destination);
                    })
                    .catch(error => {
                        this.notifications.error('Error has occurred while adding experiment goal. Check error console for details.');
                        console.error(error);
                    })
            })
            .catch(error => {
                this.notifications.error('Error has occurred while adding experiment goal. Check error console for details.');
                console.error(error);
            });
    }

    //noinspection ReservedWordAsName
    delete(goal: Goal) {

        if (this.experiment.goals.length == 1) {
            return;
        }

        this.experiment.goals.remove(goal);
        this.data.remove(goal)
            .catch(error => {
                this.notifications.error('Error has occurred while removing experiment goal. Check error console for details.');
                console.error(error);
            });
    }
}
