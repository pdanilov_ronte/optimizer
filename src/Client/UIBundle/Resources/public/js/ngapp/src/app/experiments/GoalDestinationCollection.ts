import {EntityCollection} from '../persistance/EntityCollection';
import {DTO} from '../persistance/DTO';
import {GoalDestination} from "./GoalDestination";


export class GoalDestinationCollection extends EntityCollection<GoalDestination> {
    restoreItem(dto: DTO): GoalDestination {
        return new GoalDestination(this.dao.create(dto));
    }
}
