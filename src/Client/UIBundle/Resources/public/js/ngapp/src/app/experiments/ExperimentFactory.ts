import {DomainFactory} from '../aggregate/DomainFactory';
import {Experiment} from './Experiment';


export class ExperimentFactory extends DomainFactory<Experiment> {

    constructor() {
        super(Experiment);
    }
}
