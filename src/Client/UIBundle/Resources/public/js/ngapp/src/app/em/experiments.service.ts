import {Inject, Injectable} from 'angular2/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/share';

//import {EntityManager} from "so/dao/EntityManager";

import {AuthService} from "../auth";
import {EntityManager} from './entity-manager';

import {Experiment} from "../model/experiment";
import {DataService} from './data.service';
import {IndexedCollection} from './indexed-collection';
import {NotificationService} from '../notification.service';


@Injectable()
export class ExperimentsService {

    private experiments = new IndexedCollection<Experiment>();

    ready: boolean = false;

    constructor(private data: DataService,
                private notifications: NotificationService) {

        this.data.all<Experiment>(Experiment)
            .then(collection => {
                this.experiments = collection;
                this.ready = true;
            })
            .catch(error => {
                this.notifications.error('Failed to load experiments list. Check error console for details.');
                console.error(error);
            });
    }

    get all(): Experiment[] {
        return this.experiments.items;
    }

    get created(): Experiment[] {
        return this.experiments.index('state').get(1);
    }

    get active(): Experiment[] {
        return this.experiments.index('state').in([2, 3]);
    }

    push(item: Experiment): void {
        this.experiments.push(item);
    }

    remove(item: Experiment): void {
        this.experiments.remove(item);
    }

    // @todo excessively open
    reindex(prop: string): void {
        this.experiments.reindex(prop);
    }
}
