import {Directive, Input, Output, EventEmitter, ElementRef, OnInit} from 'angular2/core';

import {PopoverService} from "./popover.service";

declare let jQuery: any;


@Directive({
    selector: '[uiPopover]',
    providers: [
        PopoverService
    ]
})
export class UiPopoverComponent implements OnInit {

    @Output('popoverInit')
    onInit: EventEmitter<any> = new EventEmitter(false);

    @Input()
    uiPopover: string;

    @Input()
    popoverPlacement: string;

    @Input()
    popoverClass: string;

    @Input()
    popoverStyle: string;

    @Input()
    popoverArrowStyle: string;

    @Input()
    popoverFn: Function;

    private popoverOwner: any;

    private template: string = `
    <div class="popover%s" style="%s" role="tooltip">
        <div class="arrow"></div>
        <h3 class="popover-title"></h3>
        <div class="popover-content"></div>
    </div>
    `;

    private defaultStyle = 'min-width:250px';

    constructor(private elementRef: ElementRef,
                private popovers: PopoverService) {
    }

    ngOnInit() {

        this.onInit.emit(this);

        let options = this.popovers.options(this.uiPopover);

        options.placement = this.popoverPlacement || 'auto';

        let popoverClass = this.popoverClass ? ' ' + this.popoverClass : '';

        let style = this.popoverStyle || this.defaultStyle;

        options.template = sprintf(this.template, popoverClass, style);


        if (this.popoverFn) {
            options.content = this.popoverFn.bind(this.popoverOwner, options, this, this.popovers);
        }

        jQuery(this.elementRef.nativeElement).popover(options);

        jQuery(this.elementRef.nativeElement).click((event: any) => {
            jQuery(event['currentTarget']).popover('hide');
        });
    }

    public setOwner(owner: any) {
        this.popoverOwner = owner;
    }
}
