import {Component, Input, Output, OnInit, EventEmitter} from 'angular2/core';

import {ExperimentStatsService} from "../em/experiment-stats.service";
import {Experiment} from "../model/experiment";
import {Variation} from "../model/variation";


@Component({
    selector: '[dashboardAbStatsBreakdown]',
    templateUrl: 'templates/dashboard/dashboard-ab-stats-breakdown.html'
})
export class DashboardAbStatsBreakdownComponent implements OnInit {

    @Input()
    experiment: Experiment;

    @Output('init')
    onInit: EventEmitter<any> = new EventEmitter();

    stats: any[] = [];

    opened: boolean = false;

    constructor(private service: ExperimentStatsService) {
    }

    show() {
        this.opened = true;
    }

    hide() {
        this.opened = false;
    }

    toggle() {
        this.opened = !this.opened;
    }

    ngOnInit(): any {
        this.onInit.emit(this);
    }

    showStats(data: any[]) {

        let stats:any[] = [];

        this.experiment.variations.items.forEach((variation: Variation) => {
            stats[variation.number] = {
                title: variation.title,
                number: variation.number,
                hits: 0,
                visitors: 0,
                xviews: 0,
                uxviews: 0,
                goals: 0,
                conversion: 0
            };
        });

        data.forEach((entry: any) => {
            let number = entry.variation;
            stats[number].hits += entry.hits;
            stats[number].visitors += entry.visitors;
            stats[number].xviews += entry.xviews;
            stats[number].uxviews += entry.uxviews;
            stats[number].goals += entry.goals;
            stats[number].conversion = (stats[number].uxviews > 0)
                ? Math.round(stats[number].goals / stats[number].uxviews * 100) : 0;
        });

        this.stats = stats;
    }
}
