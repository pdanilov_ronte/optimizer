var CONFIG = {
    "auth": {
        "login_check": "http://app.master.so/security/login_check",
        "account_update": "http://app.master.so/security-profile/edit",
        "change_password": "http://app.master.so/security-password/change-password"
    },
    "keep_alive": {
        "service_base_url": "http://app.master.so/service/-/keep-alive/"
    },
    "dao": {
        "breeze_api": "http://app.master.so/api/odp"
    },
    "cache": {
        "cache_host": "cache.master.ng"
    }
};
