import {EntityCollection} from '../persistance/EntityCollection';
import {DTO} from '../persistance/DTO';
import {Snippet} from "./Snippet";


export class SnippetCollection extends EntityCollection<Snippet> {
    restoreItem(dto: DTO): Snippet {
        return new Snippet(this.dao.create(dto));
    }
}
