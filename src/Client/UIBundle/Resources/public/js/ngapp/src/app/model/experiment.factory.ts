import {AbstractEntityFactory} from '../em/entity-factory';

import {Experiment} from './experiment';
import {Variation} from './variation';
import {ExperimentUrl} from './experiment-url';
import {VariationSnippet} from './variation-snippet';
import {Goal} from './goal';
import {EntityFactory} from '../em/entity-factory';


//noinspection JSUnusedGlobalSymbols
@EntityFactory()
export class ExperimentFactory extends AbstractEntityFactory<Experiment> {

    create(complete: boolean = false, params?: any): Promise<Experiment> {

        let experiment = new Experiment();
        experiment.title = 'New Experiment';
        experiment.type = 1;
        experiment.state = 1;
        experiment.method = 1;
        experiment.mode = 2;

        experiment.em = this.em;

        return new Promise((resolve, reject) => {
            if (complete) {
                this.complete(experiment)
                    .then(() => {
                        experiment.expanded = true;
                        //let number = 0;
                        experiment.variations.forEach((variation: Variation) => {
                            variation.experiment = experiment;
                            experiment.snippets.items.forEach((snippet: VariationSnippet) => {
                                variation.snippetValues.push(snippet.values.items[variation.number]);
                            });
                        });
                        resolve(experiment);
                    })
                    .catch(error => {
                        reject(error);
                    });
            } else {
                resolve(experiment);
            }
        });
    }

    // @todo simplify
    complete(entity: Experiment): Promise<Experiment> {
        return new Promise<Experiment>((resolve, reject) => {

            let tasks = 0;
            let done = () => {
                if (++tasks == 3) {
                    resolve(entity);
                }
            };

            // url
            this.em.getFactory<ExperimentUrl>(ExperimentUrl)
                .create(true)
                .then(url => {
                    entity.urls.push(url);
                    done();
                })
                .catch(error => {
                    reject(error);
                });

            // variation
            this.em.getFactory<Variation>(Variation)
                .create(true, {title: 'Control Variation', number: 0, weight: 100})
                .then(variation => {
                    entity.variations.push(variation);
                    done();
                })
                .catch(error => {
                    reject(error);
                });

            // snippet
            this.em.getFactory<VariationSnippet>(VariationSnippet)
                .create(true)
                .then(snippet => {
                    entity.snippets.push(snippet);
                    done();
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    fromIElement(ielement: restangular.IElement): Experiment {

        let experiment = new Experiment();

        experiment.id = ielement['id'];
        experiment.title = ielement['title'];
        experiment.type = ielement['type'];
        experiment.state = ielement['state'];
        experiment.method = ielement['method'];
        experiment.mode = ielement['mode'];

        experiment.startedAt = new Date(ielement['started_at']);

        for (let entry of ielement['variations']) {
            let variation = this.em.getFactory<Variation>(Variation).fromIElement(entry);
            experiment.variations.push(variation);
        }

        for (let entry of ielement['goals']) {
            let goal = this.em.getFactory<Goal>(Goal).fromIElement(entry);
            experiment.goals.push(goal);
        }

        experiment.em = this.em;
        experiment.ielement = ielement;

        return experiment;
    }
}
