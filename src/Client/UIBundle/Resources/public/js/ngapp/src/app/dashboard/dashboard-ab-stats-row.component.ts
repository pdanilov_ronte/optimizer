import {Component, Input, Output, EventEmitter} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';

import {Experiment} from "../model/experiment";
import {ExperimentStatsService} from "../em/experiment-stats.service";
import {DashboardAbStatsBreakdownComponent} from "./dashboard-ab-stats-breakdown.component";


@Component({
    selector: '[dashboardAbStatsRow]',
    templateUrl: 'templates/dashboard/dashboard-ab-stats-row.html',
    providers: [],
    directives: [
        ROUTER_DIRECTIVES
    ]
})
export class DashboardAbStatsRowComponent {

    @Input()
    experiment: Experiment;

    @Output('init')
    onInit: EventEmitter<DashboardAbStatsRowComponent> = new EventEmitter();

    @Output('ready')
    onReady: EventEmitter<any> = new EventEmitter();

    @Output('details')
    onDetails: EventEmitter<DashboardAbStatsRowComponent> = new EventEmitter();

    breakdown: DashboardAbStatsBreakdownComponent;

    stats: {} = {};

    ready: boolean = false;

    constructor(private service: ExperimentStatsService) {
        this.onInit.emit(this);
    }

    ngOnInit(): any {

        this.onInit.emit(this);

        this.service.experiment(this.experiment.id)
            .then(result => {
                this.showStats(result);
                this.ready = true;
                this.onReady.emit(result);
            });
    }

    setBreakdown(component: DashboardAbStatsBreakdownComponent) {
        this.breakdown = component;
    }

    toggleBreakdown() {
        this.breakdown.toggle();
    }

    details() {
        this.onDetails.emit(this);
    }

    private showStats(data: any[]) {

        let stats = {
            hits: 0,
            visitors: 0,
            xviews: 0,
            uxviews: 0,
            goals: 0,
            conversion: 0
        };

        data.forEach((entry: any) => {
            stats.hits += entry.hits;
            stats.visitors += entry.visitors;
            stats.xviews += entry.xviews;
            stats.uxviews += entry.uxviews;
            stats.goals += entry.goals;
        });

        stats.conversion = stats.uxviews > 0
            ? Math.round(stats.goals / stats.uxviews * 100) : 0;

        this.stats = stats;
    }
}
