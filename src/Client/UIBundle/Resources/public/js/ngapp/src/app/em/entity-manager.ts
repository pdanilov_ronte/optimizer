import {Injectable} from 'angular2/core';

import {ModelEntity} from './entity';
import {EntityRepository} from './entity-repository';
import {RestangularService} from './restangular.service';
import {AbstractEntityFactory} from './entity-factory';
import {IndexedCollection} from './indexed-collection';
import {PropertyContract} from './contract.decorator';
import {EntityTypeRegistry} from './entity-type.decorator';
import {ChangeTracker} from './change-tracker';
import ICollection = restangular.ICollection;
import IElement = restangular.IElement;
import {IPost, IPostElement, IPostCollection} from './restangular.service';
import {FactoryRegistry} from './entity-factory';


@Injectable()
export class EntityManager {

    private _repositories = new Map<typeof ModelEntity, EntityRepository<any>>();
    private _factories = new Map<typeof ModelEntity, AbstractEntityFactory<any>>();
    private _types = new Map<string, typeof ModelEntity>();
    private _services = new Map<typeof ModelEntity, IPost>();

    private _tracked: ModelEntity[] = [];

    private _tracker = new ChangeTracker();

    constructor(private rest: RestangularService) {
        for (let type of EntityTypeRegistry.types) {
            this._types.set(type['name'], type);
        }
    }

    getRepository<T extends ModelEntity>(type: typeof ModelEntity): EntityRepository<T> {
        let repository = this._repositories.get(type);
        if (!repository) {
            repository = new EntityRepository<T>();
            this._repositories.set(type, repository);
        }
        return repository;
    }

    getFactory<T extends ModelEntity>(type: typeof ModelEntity): AbstractEntityFactory<T> {
        let factory = this._factories.get(type);
        if (!factory) {
            let factoryClass: typeof AbstractEntityFactory = FactoryRegistry.getClass(type);
            factory = new factoryClass(this);
            this._factories.set(type, factory);
        }
        return factory;
    }

    getType(name: string): typeof ModelEntity {
        return this._types.get(name);
    }

    // @todo -?complete flag
    // @todo -?parent
    create<T extends ModelEntity>(type: typeof ModelEntity, parent?: ModelEntity, params?: any, complete: boolean = true): Promise<T> {
        return new Promise((resolve, reject) => {
            if (!parent && !this._services.has(type)) {
                return reject(
                    sprintf('Attempt to create `%s` as a root entity prior to service initialization', type['name']));
            }
            let service: IPost = parent
                ? parent.service
                : this._services.get(type);
            let factory = this.getFactory<T>(type);
            factory.create(complete, params)
                .then(entity => {
                    this.attach<T>(entity, service)
                        .then((entity: T) => {
                            entity.id = entity.ielement['id'];
                            resolve(entity);
                        })
                        .catch(error => reject(error));
                })
                .catch(error => reject(error));
        });
    }

    all<T extends ModelEntity>(type: typeof ModelEntity, entity?: T): Promise<IndexedCollection<T>> {
        let route = type.ENTITY_ROUTE;
        //noinspection TypeScriptValidateTypes
        return new Promise<IndexedCollection<T>>((resolve, reject) => {
            this.rest.all(route, entity ? entity.ielement : null)
                .then((result: ICollection) => {
                    // @todo -direct services access
                    // @todo +move to rest service
                    if (!this._services.has(type)) {
                        this._services.set(type, new IPostCollection(result));
                    }
                    // @todo +repository cache
                    let collection = new IndexedCollection<T>();
                    let factory = this.getFactory<T>(type);
                    result.forEach((entry: IElement) => {
                        let entity = factory.fromIElement(entry);
                        collection.push(entity);
                        entity.observe(this.changed.bind(this));
                    });
                    resolve(collection);
                })
                .catch(error => reject(error));
        });
    }

    persist(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this._tracker.queue.length == 0) {
                resolve([]);
            }
            let entity: ModelEntity;
            while (entity = this._tracker.shift()) {
                entity.serialize();
                this.rest.save(entity.ielement)
                    .then(result => {
                        // @todo
                        resolve(result);
                    })
                    .catch(error => {
                        console.error(error);
                        // @todo return reject(error);
                        reject(error);
                    });
            }
            // resolve();
        });
    }

    expand<T extends ModelEntity>(type: typeof ModelEntity, target: T): Promise<T> {

        return new Promise<T>((resolve, reject) => {
            let contracts = ModelEntity.getContracts(type);
            let fulfilled = 0;
            contracts.forEach((contract: PropertyContract<T>) => {
                let delegate = contract.bind(target, this);
                delegate.transfer(this.all.bind(this))
                    .then((result: IndexedCollection<T>) => {
                        contract.fulfill(result, delegate);
                        delegate.expand(this.expand.bind(this), result)
                            .then(result => {
                                if (++fulfilled == contracts.length) {
                                    resolve(target);
                                }
                            })
                            .catch(error => {
                                console.error(error);
                                reject(error);
                            })
                    })
                    .catch(error => {
                        console.error(error);
                        reject(error)
                    });
            });
        });
    }

    remove<T extends ModelEntity>(entity: T): Promise<T> {
        return new Promise((resolve, reject) => {
            this.rest.remove(entity.ielement)
                .then(() => resolve(entity))
                .catch(error => reject(error));
        });
    }

    // @todo no post/save assumed
    private attach<T extends ModelEntity>(entity: ModelEntity, service: IPost): Promise<T> {
        return new Promise((resolve, reject) => {
            service.post(entity)
                .then((element: any) => {
                    entity.traverse(this.attach.bind(this))
                        .then(() => {
                            entity.observe(this.changed.bind(this));
                            resolve(entity);
                        })
                        .catch(error => reject(error));
                })
                .catch(error => reject(error));
        });
    }

    private changed(changes: any[]): any {
        changes.forEach((entry: {object: ModelEntity, name: string})=> {
            if (ModelEntity.isObserved(entry.object, entry.name)) {
                //this._tracked.push(entry.object);
                this._tracker.track(entry.object);
            }
        });
    }
}
