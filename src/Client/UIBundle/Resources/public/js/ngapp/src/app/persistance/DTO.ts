export class DTO {

    constructor(private _data?: any) {
    }

    public data(): any {
        return this._data;
    }

    public setData(data: any) {
        this._data = data;
    }
}
