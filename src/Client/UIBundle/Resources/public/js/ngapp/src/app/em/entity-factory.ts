import {EntityManager} from './entity-manager';
import IElement = restangular.IElement;
import {ModelEntity} from './entity';


export class AbstractEntityFactory<T extends ModelEntity> {

    constructor(private _em: EntityManager) {
    }

    create(complete: boolean = false, params?: any): Promise<T> {
        throw new Error('You must implement `create` method in ' + this.constructor['name'] + ' class');
    }

    complete(entity: T): Promise<T> {
        throw new Error('You must implement `complete` method in ' + this.constructor['name'] + ' class');
    }

    fromIElement(element: IElement): T {
        throw new Error('You must implement `fromIElement` method in ' + this.constructor['name'] + ' class');
    }

    protected get em(): EntityManager {
        return this._em;
    }

    static get ENTITY_NAME(): string {
        return undefined;
    };
}


export class FactoryRegistry {

    private static factoryClasses = new Map<typeof ModelEntity, typeof AbstractEntityFactory>();

    static register(type: typeof ModelEntity, factoryClass: typeof AbstractEntityFactory): void {
        if (FactoryRegistry.factoryClasses.has(type)) {
            throw new Error('Factory for type ' + type['name'] + ' already registered');
        }
        FactoryRegistry.factoryClasses.set(type, factoryClass);
    }

    static getClass(type: typeof ModelEntity): typeof AbstractEntityFactory {
        if (!FactoryRegistry.factoryClasses.has(type)) {
            throw new Error('Factory for type ' + type['name'] + ' is not registered');
        }
        return FactoryRegistry.factoryClasses.get(type);
    }
}


class EntityFactoryMetadata {

    constructor() {
    }
}

class EntityFactoryDecorator {

    constructor(public metadata: EntityFactoryMetadata) {
    }

    decorate(): void {
    }
}

export let EntityFactory = function () {
    return function (target) {
        let metadata = new EntityFactoryMetadata();
        let decorator = new EntityFactoryDecorator(metadata);
        decorator.decorate();
    }
};
