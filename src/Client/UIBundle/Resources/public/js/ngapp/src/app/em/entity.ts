import {EntityManager} from './entity-manager';
import {AbstractEntityFactory} from './entity-factory';
import IElement = restangular.IElement;
import {PropertyContract} from './contract.decorator';
import {ContractMetadata} from './contract.decorator';
import {IndexedCollection} from './indexed-collection';
import {Expandable} from './expandable.decorator';
import ICollection = restangular.ICollection;
import {IPost} from './restangular.service';
import {IPostElement} from './restangular.service';
import {FactoryRegistry} from './entity-factory';


export abstract class ModelEntity {

    public id: number;

    protected _em: EntityManager;
    protected _ielement: IElement;
    protected _iservice: IPost;

    // @todo smart?
    expanded: boolean = false;
    private _locked: boolean = false;

    set em(em: EntityManager) {
        this._em = em;
    }

    static getContracts(type: typeof ModelEntity): PropertyContract<ModelEntity>[] {
        let reflect: {} = Reflect['getMetadata']('propMetadata', type);
        let result: PropertyContract<ModelEntity>[] = [];
        for (let key in reflect) {
            //noinspection JSUnfilteredForInLoop
            let list: ContractMetadata[] = reflect[key];
            for (let meta of list) {
                // @todo smart
                if (meta.contract) {
                    result.push(meta.contract);
                }
            }
        }
        return result;
    }

    static isObserved(entity: ModelEntity, name: string): boolean {
        let reflect: {} = Reflect['getMetadata']('propMetadata', entity.constructor);
        if (!reflect || !reflect[name]) {
            return false;
        }
        for (let meta of reflect[name]) {
            if (meta.persist) {
                return true;
            }
        }
        return false;
    }

    static isExpandable(entity: ModelEntity): boolean {
        let reflect: any[] = Reflect['getMetadata']('annotations', entity.constructor);
        if (!reflect) {
            return false;
        }
        for (let meta of reflect) {
            if (meta.constructor['name'] == 'ExpandableMetadata') {
                return true;
            }
        }
        return false;
    }

    protected lock() {
        this._locked = true;
    }

    protected unlock() {
        this._locked = false;
    }

    get locked(): boolean {
        return this._locked;
    }

    // @todo replace with `is expandable` check (descendant's decorator?)
    expand(): Promise<ModelEntity> {
        return new Promise<ModelEntity>((resolve) => {
            resolve(this);
        });
    }

    observe(observer: Function): void {
        this._shrinkIElement();
        // @todo replace with defineProperty
        Object['observe'](this, observer);
    }

    serialize(): void {
        for (let prop in this.plain()) {
            if (prop != 'id') {
                //noinspection JSUnfilteredForInLoop
                this._ielement[prop] = this[prop];
            }
        }
    }

    plain(): any {
        let plain = {};
        for (let prop in this) {
            if (!this.hasOwnProperty(prop)) {
                continue;
            }
            if (ModelEntity.isObserved(this, prop)) {
                plain[prop] = this[prop];
            }
        }
        return plain;
    }

    // @todo -callback / +promise interface
    // @todo +contracts to generalize traversal
    traverse(callback: Function): Promise<ModelEntity> {
        return new Promise((resolve) => {
            resolve(this);
        });
    }

    // @todo
    private _shrinkIElement(): void {
        for (let prop in this._ielement.plain()) {
            //noinspection JSUnfilteredForInLoop
            if (prop != 'id' && !ModelEntity.isObserved(this, prop)) {
                //noinspection JSUnfilteredForInLoop
                delete this._ielement[prop];
            }
        }
    }

    // @todo hide / encapsulate
    set ielement(ielement: IElement) {
        this._ielement = ielement;
        this._iservice = new IPostElement(this._ielement);
    }

    get ielement(): IElement {
        return this._ielement;
    }

    get service(): IPost {
        return this._iservice;
    }

    get base(): ICollection {
        return this._ielement['getParentList']
            ? this._ielement['getParentList']()
            : undefined;
    }

    get route(): string {
        return undefined;
    }

    protected doExpand<T extends ModelEntity>(type: typeof ModelEntity, target: T): Promise<T> {
        return new Promise((resolve, reject) => {
            if (this.expanded) {
                return reject('Entity ' + type['name'] + ' already expanded');
            }
            if (this.locked) {
                return reject('Entity ' + type['name'] + ' is locked');
            }
            this.lock();
            this._em.expand(type, target)
                .then(result => {
                    this.expanded = true;
                    this.unlock();
                    resolve(result);
                })
                .catch(error => reject(error));
        })
    }

    protected doTraverse(tasks: IndexedCollection<ModelEntity>[], callback: Function): Promise<ModelEntity> {
        return new Promise<ModelEntity>((resolve, reject) => {
            let done = 0;
            for (let collection of tasks) {
                if (collection.length == 0) {
                    if (++done == tasks.length)
                        resolve(this);
                    continue;
                }
                this._doTraverseCollection(collection, callback)
                    .then(() => {
                        if (++done == tasks.length)
                            resolve(this);
                    })
                    .catch(error => reject(error));
            }
        });
    }

    private _doTraverseCollection(collection: IndexedCollection<ModelEntity>, callback: Function): Promise<any> {
        return new Promise((resolve, reject) => {
            let done = 0;
            collection.forEach(entity => {
                callback(entity, this.service)
                    .then(() => {
                        if (++done == collection.length)
                            resolve();
                    })
                    .catch((error: any) => reject(error));
            });
        });
    }

    /**
     * @deprecated
     */
    static get FACTORY_CLASS(): typeof AbstractEntityFactory {
        return AbstractEntityFactory;
    };

    static get ENTITY_ROUTE(): string {
        return undefined;
    };
}


export interface ExpandableInterface<T extends ModelEntity> {
    expand(): Promise<T>;
}


class EntityMetadata {
    constructor(public type: typeof ModelEntity,
                public factoryClass: typeof AbstractEntityFactory) {
    }
}

class EntityDecorator {

    constructor(public metadata: EntityMetadata) {
    }

    decorate(): void {
        // register entity type and its factory class
        FactoryRegistry.register(this.metadata.type, this.metadata.factoryClass);
    }
}

export let Entity = function (factoryClass: typeof AbstractEntityFactory) {
    return function (target) {
        let metadata = new EntityMetadata(target, factoryClass);
        let decorator = new EntityDecorator(metadata);
        decorator.decorate();
    }
};
