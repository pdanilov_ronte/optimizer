import {Component, Input, Output, EventEmitter, OnInit, OnDestroy, ComponentRef, ElementRef} from 'angular2/core';

import {AbExperimentDetailsComponent} from "./ab-experiment-details.component";
import {DetailsSidebarService} from "./details-sidebar.service";

import {Experiment} from './model/experiment';
//import {ExperimentsService} from "./em/experiments.service";
import {UiEditInlineDirective} from "./ui-edit-inline.directive";
import {UiPopoverComponent} from "./ui-popover.component";
import {PopoverService} from "./popover.service";
import {DataService} from './em/data.service';
import {ExperimentPublishService} from './experiment-publish.service';
import {NotificationService} from './notification.service';


@Component({
    selector: 'tr[abExperimentRow]',
    templateUrl: 'templates/ab-experiment-row.html',
    providers: [
        ExperimentPublishService
    ],
    directives: [
        UiEditInlineDirective,
        UiPopoverComponent
    ]
})
export class AbExperimentRowComponent implements OnInit, OnDestroy {

    @Input() experiment: Experiment;

    @Output('delete')
    onDelete: EventEmitter<Experiment> = new EventEmitter(false);

    @Output('run')
    onRun: EventEmitter<Experiment> = new EventEmitter(false);

    private details: AbExperimentDetailsComponent;
    private detailsRef: ComponentRef;

    loading: boolean = false;

    constructor(private sidebar: DetailsSidebarService,
                private publishService: ExperimentPublishService,
                private notifications: NotificationService,
                private data: DataService,
                private elementRef: ElementRef) {
    }

    ngOnInit(): any {
    }

    ngOnDestroy(): any {
        if (this.sidebar.isOpen) {
            this.sidebar.close();
        }
    }

    onDetails() {

        if (!this.experiment.expanded && !this.experiment.locked) {
            this.experiment.expand()
                .catch(error => console.error(error));
        }

        jQuery(this.elementRef.nativeElement).find('input').focus();

        this.sidebar.loadComponent(AbExperimentDetailsComponent)
            .then((ref: ComponentRef) => {
                this.detailsRef = ref;
                let instance: AbExperimentDetailsComponent = ref.instance;
                instance.setExperiment(this.experiment);
                instance.onDelete.subscribe((experiment: Experiment) => {
                    this.sidebar.close();
                    this.onDelete.emit(experiment);
                });
                instance.onRun.subscribe((experiment: Experiment) => {
                    this.onRun.emit(experiment);
                });
            });
    }

    get valid(): boolean {
        return this.publishService.errors(this.experiment).length == 0;
    }

    toggle(event: MouseEvent): void {
        event.stopPropagation();
        jQuery(event.currentTarget).mouseleave();
        switch (this.experiment.state) {
            case 2:
                this.experiment.state = 3;
                break;
            case 3:
                if (!this.valid) {
                    // return console.warn('Cannot resume invalid experiment');
                    return;
                }
                this.experiment.state = 2;
                break;
            default:
                console.error(sprintf('Invalid experiment state %s', this.experiment.state));
        }
        this.data.persist()
            .then(_ => {
                this.publishService.publish(this.experiment)
                    .then(result => {
                        if (this.experiment.state == 2) {
                            this.notifications.success('experiment successfully activated');
                        }
                    })
                    .catch(reason => {
                        this.notifications.error('error has occurred');
                    });
            })
            .catch(_ => console.error(_));
    }

    onPopoverInit(component: UiPopoverComponent) {
        component.setOwner(this);
    }

    get runningPopover(): string {
        return this.experiment.state == 2
            ? 'ab_experiments_list.running_status.running'
            : 'ab_experiments_list.running_status.paused';
    }

    runningPopoverFn(options: any, component: UiPopoverComponent, service: PopoverService) {

        if (!this.experiment) {
            return 'n/a';
        }

        let id = this.experiment.state == 2 ?
            'ab_experiments_list.running_status.running' :
            'ab_experiments_list.running_status.paused';

        let message = service.message(id);

        return message.content;
    }

    update() {
        this.data.persist(500);
    }

    save() {
        this.data.persist();
    }

    blur() {
        this.sidebar.close();
    }
}
