import {CollectionDAO} from './CollectionDAO';
import IService = restangular.IService;


export class RootDAO extends CollectionDAO {
    constructor(service: IService, path: string) {
        super(service, null, path);
    }
}