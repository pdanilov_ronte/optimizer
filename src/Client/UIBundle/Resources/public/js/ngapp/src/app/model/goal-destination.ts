import {ModelEntity, Entity} from '../em/entity';
import {Persist} from '../em/persist.decorator';
import {GoalDestinationFactory} from './goal-destination.factory';


@Entity(GoalDestinationFactory)
export class GoalDestination extends ModelEntity {

    @Persist()
    value: string;

    constructor() {
        super();
    }

    get route(): string {
        return GoalDestination.ENTITY_ROUTE;
    }

    static get ENTITY_ROUTE(): string {
        return 'destinations';
    }
}
