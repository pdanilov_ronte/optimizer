import {Component} from 'angular2/core';
import {RouteConfig, RouterOutlet} from 'angular2/router';

import {AuthService} from "./auth";
import {LoginFormComponent} from "./login-form.component";


@Component({
    template: `
    <h1>Login Page</h1>
    <router-outlet></router-outlet>
    `,
    directives: [RouterOutlet],
    providers: [AuthService]
})
export class LoginComponent {
}
