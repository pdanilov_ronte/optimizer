import {DomainCollection} from '../persistance/DomainCollection';
import {Experiment} from './Experiment';
import {DTO} from '../persistance/DTO';


export class ExperimentCollection extends DomainCollection<Experiment> {
    restoreItem(dto: DTO): Experiment {
        return new Experiment(this.dao.create(dto));
    }
}
