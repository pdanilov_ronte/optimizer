import {Component} from 'angular2/core';

import {ProfileAccountUserComponent} from "./profile-account-user.component";
import {ProfileAccountPasswordComponent} from "./profile-account-password.component";


@Component({
    selector: 'profile-account',
    templateUrl: 'templates/profile/profile-account.html',
    directives: [
        ProfileAccountUserComponent,
        ProfileAccountPasswordComponent
    ]
})
export class ProfileAccountComponent {

}
