import {Injectable} from 'angular2/core';

import {MessagesService} from "./messages.service";


@Injectable()
export class PopoverService {

    constructor(private messages: MessagesService) {
    }

    public options(id: string): any {

        let message = this.messages._[id];

        let options = {
            trigger: 'hover',
            html: true
        };

        if (message['title']) {
            options['title'] = message['title'];
        }

        if (message['content']) {
            options['content'] = message['content'];
        }

        return options;
    }

    public message(id: string): any {
        return this.messages._[id];
    }
}
