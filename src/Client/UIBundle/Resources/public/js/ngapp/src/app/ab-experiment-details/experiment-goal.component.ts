import {Component, Input, Output, EventEmitter} from 'angular2/core';

import {DataService} from '../em/data.service';
import {Goal} from "../model/goal";
import {UiPopoverComponent} from "../ui-popover.component";
import {PopoverService} from "../popover.service";
import {UiEditInlineDirective} from "../ui-edit-inline.directive";


@Component({
    selector: 'experiment-goal',
    templateUrl: 'templates/ab-experiment-details/experiment-goal.html',
    directives: [
        UiEditInlineDirective,
        UiPopoverComponent
    ]
})
export class ExperimentGoalComponent {

    @Input()
    goal: Goal;

    @Input()
    list: Goal[];

    @Output('delete')
    onDelete: EventEmitter<any> = new EventEmitter();

    constructor(private data: DataService) {
    }

    update() {
        this.data.persist(500);
    }

    delete() {
        this.onDelete.emit(this.goal);
    }

    onPopoverInit(component: UiPopoverComponent) {
        component.setOwner(this);
    }

    deleteGoalPopover(options: any, component: UiPopoverComponent, service: PopoverService) {

        let content = service.message(component.uiPopover);

        if (this.list.length > 1) {
            return content.content;
        }

        let message = service.message('ab_experiment_details.cant_delete_last_goal');

        return content.content + '<br>' + 'NOTICE: ' + message.content;
    }
}
