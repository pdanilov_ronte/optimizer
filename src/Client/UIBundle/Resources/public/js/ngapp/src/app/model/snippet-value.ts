import {ModelEntity, Entity} from '../em/entity';
import {SnippetValueFactory} from './snippet-value.factory';
import {Persist} from '../em/persist.decorator';


@Entity(SnippetValueFactory)
export class SnippetValue extends ModelEntity {

    @Persist()
    value: string;

    @Persist()
    variation: number;

    constructor() {
        super();
    }

    get route(): string {
        return SnippetValue.ENTITY_ROUTE;
    }

    static get ENTITY_ROUTE(): string {
        return 'values';
    }
}
