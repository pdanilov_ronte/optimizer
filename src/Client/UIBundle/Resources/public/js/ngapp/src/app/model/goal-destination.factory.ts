import {AbstractEntityFactory} from '../em/entity-factory';
import {GoalDestination} from './goal-destination';


export class GoalDestinationFactory extends AbstractEntityFactory<GoalDestination> {

    create(complete: boolean = false, params?: any): Promise<GoalDestination> {
        let destination = new GoalDestination();

        return new Promise((resolve, reject) => {
            this.em.persist()
                .then(() => {
                    resolve(destination);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    fromIElement(element: restangular.IElement): GoalDestination {
        let destination = new GoalDestination();

        destination.value = element['value'];

        destination.em = this.em;
        destination.ielement = element;

        return destination;
    }
}
