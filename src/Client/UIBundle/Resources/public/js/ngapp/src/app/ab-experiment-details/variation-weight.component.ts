import {Component, Input, Output, EventEmitter, OnInit} from 'angular2/core';
import {UiSliderDirective} from "../ui-slider.directive";
import {Variation} from "../model/variation";
import {VariationWeightsService} from "./variation-weights.service";
import {UiPopoverComponent} from "../ui-popover.component";
import {DataService} from '../em/data.service';


@Component({
    selector: 'variation-weight',
    templateUrl: 'templates/ab-experiment-details/variation-weight.html',
    directives: [
        UiSliderDirective,
        UiPopoverComponent
    ]
})
export class VariationWeightComponent implements OnInit {

    @Input()
    variation: Variation;

    @Output()
    changeWeight: EventEmitter<any> = new EventEmitter(false);

    @Output()
    afterWeightChanged: EventEmitter<any> = new EventEmitter(false);

    config = {
        min: 0,
        max: 100,
        step: 1
    };

    constructor(private weights: VariationWeightsService, private data: DataService) {
    }

    ngOnInit(): any {
    }

    onSlide(event: any) {
        event.preventDefault();
        event.stopPropagation();
        if (this.variation.weightLocked) {
            return;
        }
        this.weights.applyChange(event.ui.value, this.variation);
    }

    onSlideStop(event: any) {
        event.so = {};
        this.afterWeightChanged.emit(event);
    }

    toggleLock(event: any) {
        event.stopPropagation();
        this.variation.weightLocked = !this.variation.weightLocked;
        this.weights.saveState(this.variation);
        this.data.persist();
    }
}
