import {ModelEntity, Entity} from '../em/entity';
import {IndexedCollection} from '../em/indexed-collection';
import {VariationSnippetFactory} from './variation-snippet.factory';
import {SnippetValue} from './snippet-value';
import {Contract} from '../em/contract.decorator';
import {Expandable} from '../em/expandable.decorator';
import {ExpandableInterface} from '../em/entity';
import {Persist} from '../em/persist.decorator';


@Expandable()
@Entity(VariationSnippetFactory)
export class VariationSnippet extends ModelEntity implements ExpandableInterface<VariationSnippet> {

    @Persist()
    number: number = 0;

    @Persist()
    title: string = '';

    @Contract(SnippetValue)
    values: IndexedCollection<SnippetValue> = new IndexedCollection<SnippetValue>();

    constructor() {
        super();
    }

    traverse(callback: Function): Promise<VariationSnippet> {
        return this.doTraverse([this.values], callback);
    }

    get route(): string {
        return VariationSnippet.ENTITY_ROUTE;
    }

    static get ENTITY_ROUTE(): string {
        return 'snippets';
    }

    public expand(): Promise<VariationSnippet> {
        return this.doExpand(VariationSnippet, this);
    }
}
