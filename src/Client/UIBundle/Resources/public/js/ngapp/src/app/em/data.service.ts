import {Injectable} from 'angular2/core';

import {EntityManager} from './entity-manager';

import {Experiment} from '../model/experiment';
import {ModelEntity} from './entity';
import {IndexedCollection} from './indexed-collection';
import {AbstractEntityFactory} from './entity-factory';


@Injectable()
export class DataService {

    private _deferred: number;

    constructor(private _em: EntityManager) {
        // @todo make official
        window['data'] = this;
    }

    getType(name: string): typeof ModelEntity {
        return this._em.getType(name);
    }

    getFactory<T extends ModelEntity>(type: typeof ModelEntity): AbstractEntityFactory<T> {
        return this._em.getFactory<T>(type);
    }

    /**
     * @returns {EntityManager}
     * @deprecated
     */
    getManager(): EntityManager {
        return this._em;
    }

    // @todo -params
    create<T extends ModelEntity>(type: typeof ModelEntity, parent?: ModelEntity, params?: any, complete: boolean = true): Promise<T> {
        return this._em.create<T>(type, parent, params, complete);
    }

    // @todo create sync decorator version
    all<T extends ModelEntity>(type: typeof ModelEntity): Promise<IndexedCollection<T>> {
        return this._em.all<T>(type);
    }

    remove<T extends ModelEntity>(entity: T): Promise<T> {
        return this._em.remove<T>(entity);
    }

    persist(delay: number = 0): Promise<any> {
        if (this._deferred) {
            window.clearTimeout(this._deferred);
        }
        return new Promise((resolve, reject) => {
            this._deferred = window.setTimeout(() => {
                this._em.persist()
                    .then(result => resolve(result))
                    .catch(error => reject(error));
            }, delay);
        });
    }
}
