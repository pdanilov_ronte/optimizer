import {Directive, Input, ElementRef, OnInit} from 'angular2/core';
import {Output, EventEmitter} from 'angular2/core';


@Directive({
    selector: '[edit-inline]',
    host: {
        '(mouseenter)': 'focus()',
        '(mouseleave)': 'blur()',
        '(focus)': 'focus(true)',
        '(blur)': 'blur(true)',
        '(keyup)': 'keyup($event)'
    }
})
export class UiEditInlineDirective implements OnInit {

    @Input('edit-inline')
    model: string;

    @Input('prefix')
    prefix: string;

    @Input('content-type')
    type: string = 'string';

    @Output('focus')
    onFocus: EventEmitter<any> = new EventEmitter();

    @Output('blur')
    onBlur: EventEmitter<any> = new EventEmitter();

    @Output('change')
    onChange: EventEmitter<any> = new EventEmitter();

    @Output('enter')
    onEnter: EventEmitter<any> = new EventEmitter();

    @Output('escape')
    onEscape: EventEmitter<any> = new EventEmitter();

    focused: boolean = false;

    private prefixElement: any;

    private highlighted: boolean = false;

    constructor(private elementRef: ElementRef) {
    }

    ngOnInit(): any {
        this.elementRef.nativeElement.spellcheck = false;
        jQuery(this.elementRef.nativeElement).css({
            color: '#212F40',
            width: '100%',
            background: 'transparent',
            border: '0 solid transparent',
            borderBottom: '1px solid transparent',
            outline: '0 solid transparent',
            position: 'relative',
            padding: '0 0 2px 0',
            left: '-1px',
            top: '-1px',
            overflow: 'hidden',
            resize: 'none',
            transition: 'border-color 0.1s ease'
        });

        if (this.type == 'code') {
            jQuery(this.elementRef.nativeElement).css({
                borderLeft: '1px solid rgba(0, 0, 0, 0.25)',
                paddingLeft: '10px'
            });
        }

        if (this.prefix) {
            this.showPrefix();
        }

        this.model = this.value;
    }

    get value(): string {
        return this.elementRef.nativeElement.value;
    }

    focus(fix: boolean = false): void {
        if (!this.focused) {
            if (this.type != 'code') {
                let color = fix ? 'rgba(0, 0, 0, 0.4)' : 'rgba(0, 0, 0, 0.15)';
                jQuery(this.elementRef.nativeElement).css({
                    borderBottom: '1px solid ' + color
                });
            }
            if (fix) {
                this.focused = true;
                this.highlightPrefix();
                //this.onFocus.emit(this.elementRef.nativeElement);
            }
        }
    }

    blur(release: boolean = false): void {
        if (!this.focused || release) {
            jQuery(this.elementRef.nativeElement).css({
                borderBottom: '1px solid transparent'
            });
            if (release) {
                this.focused = false;
                //this.onBlur.emit(this.elementRef.nativeElement);
                if (this.value.length == 0) {
                    this.highlightPrefix(false);
                }
            }
        }
    }

    keyup(event: KeyboardEvent): void {
        event.preventDefault();
        event.stopPropagation();
        switch (event['code']) {
            case 'Enter':
                this.onEnter.emit(event);
                break;
            case 'Escape':
                jQuery(this.elementRef.nativeElement).blur();
                this.onEscape.emit(event);
                break;
            default:
                if (this.model != this.value) {
                    console.log(this.model);
                    console.log(this.value);
                    /*let stripped = this.stripPrefix(this.value);
                    if (stripped != this.value) {
                        jQuery(this.elementRef.nativeElement).val(stripped);
                    }*/
                    this.onChange.emit(null);
                }
        }
    }

    /**
     * @deprecated
     */
    private showPrefix() {

        let span: any = jQuery('<span style="position:absolute;visibility:hidden">' + this.prefix + '</span>')
            .insertBefore(this.elementRef.nativeElement);
        let width: number = span.width();
        let position: JQueryCoordinates = jQuery(this.elementRef.nativeElement).position();
        jQuery(span).remove();

        let element = jQuery('<input readonly/>').insertBefore(this.elementRef.nativeElement)
            .css({
                color: '#212F40',
                opacity: '0.25',
                width: width + 'px',
                background: 'transparent',
                border: '0 solid transparent',
                borderBottom: '1px solid transparent',
                outline: '0 solid transparent',
                position: 'absolute',
                padding: '0 0 2px 0',
                left: position.left,
                top: position.top,
                overflow: 'hidden',
                resize: 'none'
            })
            .val(this.prefix);

        this.prefixElement = element[0];

        jQuery(this.elementRef.nativeElement).css({
            paddingLeft: '' + width + 'px'
        });

        jQuery(this.prefixElement).focus((event: FocusEvent) => {
            jQuery(this.elementRef.nativeElement).focus();
        });

        if (this.value.length) {
            this.highlightPrefix();
        }
    }

    private highlightPrefix(on: boolean = true) {
        if (this.prefixElement && (this.highlighted != on)) {
            jQuery(this.prefixElement).css({
                opacity: (on ? '1' : '0.4')
            });
            this.highlighted = on;
        }
    }

    private stripPrefix(value: string): string {
        if (!this.prefix) {
            return value;
        }
        return value.substr(0, this.prefix.length) == this.prefix
            ? value.substr(this.prefix.length)
            : value;
    }
}
