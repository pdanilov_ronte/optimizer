import {Component, Input, Output, EventEmitter, ElementRef, OnInit} from 'angular2/core';

declare let jQuery: any;


@Component({
    selector: 'ui-slider',
    template: '<div class="ui-slider"></div>'
})
export class UiSliderDirective {

    @Input()
    config = {};

    @Output()
    slide: EventEmitter<any> = new EventEmitter(false);

    @Output()
    stop: EventEmitter<any> = new EventEmitter(false);

    private _model: number;

    private el: any;

    constructor(private elementRef: ElementRef) {
    }

    ngOnInit() {
        this.el = jQuery(this.elementRef.nativeElement).children('.ui-slider');
        this.el.slider(this.config);
        this.el.slider('value', this._model);
        this.el.on('slide', this.uiOnSlide. bind(this));
        this.el.on('slidestop', this.uiOnSlideStop.bind(this));
    }

    uiOnSlide(event:any, ui:any) {
        event.ui = ui;
        this.slide.emit(event);
    }

    uiOnSlideStop(event:any, ui:any) {
        event.ui = ui;
        this.stop.emit(event);
    }

    @Input()
    set model(value: number) {
        this._model = value;
        if (this.el) {
            this.el.slider('value', this._model);
        }
    }

    get model(): number {
        return this._model;
    }

    @Input()
    set disabled(value: boolean) {
        if (this.el) {
            this.el.slider('option', 'disabled', value);
        }
    }
}
