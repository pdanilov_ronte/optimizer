import {ModelEntity, Entity, ExpandableInterface} from '../em/entity';
import {ExperimentFactory} from './experiment.factory';

import {Variation} from './variation';
import {IndexedCollection} from '../em/indexed-collection';
import {Goal} from './goal';
import {ExperimentUrl} from './experiment-url';
import {VariationSnippet} from './variation-snippet';

import {Expandable} from '../em/expandable.decorator';
import {Contract, PropertyContract} from '../em/contract.decorator';
import {Persist} from '../em/persist.decorator';
import {EntityType} from '../em/entity-type.decorator';


@EntityType()
@Expandable()
@Entity(ExperimentFactory)
export class Experiment extends ModelEntity implements ExpandableInterface<Experiment> {

    @Persist()
    title: string;

    type: number;
    method: number;
    mode: number;
    pristine: boolean;

    @Persist()
    startedAt: Date;

    @Persist()
    state: number;

    @Contract(ExperimentUrl)
    urls: IndexedCollection<ExperimentUrl> = new IndexedCollection<ExperimentUrl>();

    @Contract(Goal)
    goals: IndexedCollection<Goal> = new IndexedCollection<Goal>();

    @Contract(Variation)
    variations: IndexedCollection<Variation> = new IndexedCollection<Variation>();

    @Contract(VariationSnippet)
    snippets: IndexedCollection<VariationSnippet> = new IndexedCollection<VariationSnippet>();

    constructor() {
        super();
    }

    expand(): Promise<Experiment> {
        return new Promise<Experiment>((resolve, reject) => {
            this.doExpand<Experiment>(Experiment, this)
                .then(experiment => {
                    experiment.variations.forEach((variation: Variation) => {
                        variation.experiment = experiment;
                        experiment.snippets.items.forEach((snippet: VariationSnippet) => {
                            variation.snippetValues.push(snippet.values.items[variation.number]);
                        });
                    });
                    resolve(experiment);
                })
                .catch(error => reject(error));
        })
    }

    traverse(callback: Function): Promise<Experiment> {
        let tasks = [
            this.urls, this.goals, this.variations, this.snippets
        ];
        return this.doTraverse(tasks, callback);
    }

    //noinspection JSUnusedGlobalSymbols
    addVariation(variation: Variation): number {
        return this.variations.push(variation);
    }

    get route(): string {
        return Experiment.ENTITY_ROUTE;
    }

    // @todo use class decorator
    //noinspection JSUnusedGlobalSymbols
    static get ENTITY_ROUTE(): string {
        return 'experiments';
    }
}
