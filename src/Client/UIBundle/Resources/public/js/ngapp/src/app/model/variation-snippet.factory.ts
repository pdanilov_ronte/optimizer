import {AbstractEntityFactory} from '../em/entity-factory';
import {VariationSnippet} from './variation-snippet';
import {SnippetValue} from './snippet-value';


//noinspection JSUnusedGlobalSymbols
export class VariationSnippetFactory extends AbstractEntityFactory<VariationSnippet> {

    create(complete: boolean = false, params?: any): Promise<VariationSnippet> {
        let snippet = new VariationSnippet();
        snippet.title = 'New Snippet';

        if (params && (params['number'] !== undefined)) {
            snippet.number = params['number'];
        }

        return new Promise((resolve, reject) => {
            if (complete) {
                this.complete(snippet)
                    .then(() => {
                        this.em.persist()
                            .then(() => {
                                resolve(snippet);
                            })
                            .catch(error => {
                                reject(error)
                            });
                    })
                    .catch(error => reject(error));
            } else {
                this.em.persist()
                    .then(() => {
                        resolve(snippet);
                    })
                    .catch(error => {
                        reject(error)
                    });
            }
        });
    }

    complete(entity: VariationSnippet): Promise<VariationSnippet> {
        return new Promise<VariationSnippet>((resolve, reject) => {
            this.em.getFactory<SnippetValue>(SnippetValue)
                .create(true)
                .then(value => {
                    entity.values.push(value);
                    resolve(entity);
                })
                .catch(error => {
                    reject(error)
                });
        });
    }

    fromIElement(element: restangular.IElement): VariationSnippet {
        let snippet = new VariationSnippet();

        snippet.number = element['number'];
        snippet.title = element['title'];

        snippet.em = this.em;
        snippet.ielement = element;

        return snippet;
    }
}
