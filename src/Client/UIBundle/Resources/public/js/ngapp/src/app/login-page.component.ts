import {Component} from 'angular2/core';

import {LoginFormComponent} from "./login-form.component";
import {AuthService} from "./auth";


@Component({
    selector: 'login-page',
    templateUrl: 'templates/login-page.html',
    directives: [LoginFormComponent]
})
export class LoginPageComponent {

    constructor(private auth: AuthService) {
    }
}
