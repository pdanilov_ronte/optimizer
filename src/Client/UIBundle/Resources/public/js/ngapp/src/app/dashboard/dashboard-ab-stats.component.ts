
import {Component, OnInit} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';
;
import {DashboardAbStatsRowComponent} from "./dashboard-ab-stats-row.component";
import {DashboardAbStatsBreakdownComponent} from "./dashboard-ab-stats-breakdown.component";
import {Experiment} from "../model/experiment";
import {UiPopoverComponent} from "../ui-popover.component";
import {ExperimentsService} from '../em/experiments.service';


@Component({
    selector: 'dashboard-ab-stats',
    templateUrl: 'templates/dashboard/dashboard-ab-stats.html',
    providers: [],
    directives: [
        DashboardAbStatsRowComponent,
        DashboardAbStatsBreakdownComponent,
        UiPopoverComponent,
        ROUTER_DIRECTIVES
    ]
})
export class DashboardAbStatsComponent implements OnInit {

    private rows = new Map<number, DashboardAbStatsRowComponent>();

    private breakdown = new Map<number, DashboardAbStatsBreakdownComponent>();

    constructor(private experiments: ExperimentsService) {
    }

    ngOnInit(): any {
    }

    registerRow(component: DashboardAbStatsRowComponent, index: number) {
        this.rows.set(index, component);
    }

    registerBreakdown(component: DashboardAbStatsBreakdownComponent, index: number) {
        this.breakdown.set(index, component);
        this.rows.get(index).setBreakdown(component);
    }

    showBreakdown(data: any[], index: number) {
        this.breakdown.get(index).showStats(data);
    }

    toggleDetails(row: DashboardAbStatsRowComponent) {
    }
}
