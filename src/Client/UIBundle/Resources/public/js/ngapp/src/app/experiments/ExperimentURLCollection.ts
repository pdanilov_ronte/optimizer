import {EntityCollection} from '../persistance/EntityCollection';
import {ExperimentURL} from './ExperimentURL';
import {DTO} from '../persistance/DTO';


export class ExperimentURLCollection extends EntityCollection<ExperimentURL> {
    restoreItem(dto: DTO): ExperimentURL {
        return new ExperimentURL(this.dao.create(dto));
    }
}
