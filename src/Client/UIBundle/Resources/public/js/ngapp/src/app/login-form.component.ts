import {Component} from 'angular2/core';
import {Router, RouteConfig} from 'angular2/router';

import {AuthService} from "./auth";
import {DashboardComponent} from "./dashboard.component";


@Component({
    selector: 'login-form',
    templateUrl: 'templates/login-form.html',
    providers: [AuthService]
})
export class LoginFormComponent {

    username: string;
    password: string;

    constructor(private auth: AuthService, private router: Router) {
    }

    login() {

        let success = () => {
            this.router.navigate(['Dashboard'])
        };

        this.auth.login(this.username, this.password)
            .then(function (data) {
                success();
            })
            .catch(function (reason) {
                console.error('Login failed: ' + reason);
            });
    }
}
