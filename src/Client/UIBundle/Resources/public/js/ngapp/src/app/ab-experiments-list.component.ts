import {Component, Inject, OnInit, Input, ComponentRef, ChangeDetectionStrategy} from 'angular2/core';

import {Experiment} from "./model/experiment";
import {AbExperimentRowComponent} from "./ab-experiment-row.component";
import {AuthService} from "./auth";
import {ExperimentUrl} from "./model/experiment-url";
import {VariationSnippet} from "./model/variation-snippet";
import {DetailsSidebarService} from "./details-sidebar.service";
import {AbExperimentDetailsComponent} from "./ab-experiment-details.component";
import {UiPopoverComponent} from "./ui-popover.component";
import {AbExperimentFactoryService} from "./ab-experiment-factory.service";
import {UiEditInlineDirective} from "./ui-edit-inline.directive";
import {UiLaddaDirective} from "./ui-ladda.directive";
import {DataService} from './em/data.service';
import {IndexedCollection} from './em/indexed-collection';
import {NotificationService} from './notification.service';
import {ExperimentsService} from './em/experiments.service';


@Component({
    templateUrl: 'templates/ab-experiments-list.html',
    selector: 'ab-experiments-list',
    providers: [
        AbExperimentFactoryService
    ],
    directives: [
        AbExperimentRowComponent,
        UiPopoverComponent,
        UiEditInlineDirective,
        UiLaddaDirective
    ]
})
export class AbExperimentsListComponent implements OnInit {

    creating: boolean = false;

    constructor(private data: DataService,
                public experiments: ExperimentsService,
                private factory: AbExperimentFactoryService,
                private auth: AuthService,
                private sidebar: DetailsSidebarService,
                private notifications: NotificationService) {
    }

    ngOnInit(): any {
    }

    get ready(): boolean {
        return this.experiments.ready;
    }

    details(experiment: Experiment) {
        this.sidebar.loadComponent(AbExperimentDetailsComponent)
            .then((ref: ComponentRef) => {
                let component: AbExperimentDetailsComponent = ref.instance;
                component.setExperiment(experiment);
                component.onDelete.subscribe((experiment: Experiment) => {
                    this.sidebar.close();
                    this.delete(experiment);
                });
                component.onRun.subscribe((experiment: Experiment) => {
                    this.run(experiment);
                });
            });
    }

    create() {
        this.creating = true;
        this.data.create<Experiment>(Experiment)
            .then(experiment => {
                this.experiments.push(experiment);
                this.creating = false;
                this.details(experiment);
            })
            .catch(error => {
                this.notifications.error("Error has occurred while creating experiment. See error console for details.");
                console.error(error);
            });
    }

    //noinspection ReservedWordAsName
    delete(experiment: Experiment) {

        this.experiments.remove(experiment);

        this.data.remove<Experiment>(experiment)
            .then(result => {
                // @todo hide from the list first, remove on success
            })
            .catch(error => {
                this.notifications.error('Failed to delete experiment. See error console for details.');
            });
    }

    run(experiment: Experiment) {

        //let index = this.experiments.new.indexOf(experiment);

        //this.experiments.new.splice(index, 1);
        //this.experiments.active.push(experiment);

        //this.details(experiment);
        this.experiments.reindex('state');
    }
}
