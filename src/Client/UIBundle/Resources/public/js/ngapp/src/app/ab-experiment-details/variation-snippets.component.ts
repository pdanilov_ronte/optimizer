import {Component, Input, Output, EventEmitter, ElementRef, OnInit} from 'angular2/core';

import {VariationSnippetsService} from "./variation-snippets.service";
import {Variation} from "../model/variation";
import {VariationSnippet} from "../model/variation-snippet";
import {AuthService} from "../auth";
import {SnippetValue} from "../model/snippet-value";
import {Experiment} from "../model/experiment";
import {UiPopoverComponent} from "../ui-popover.component";
import {PopoverService} from "../popover.service";
import {AbExperimentFactoryService} from "../ab-experiment-factory.service";
import {UiEditInlineDirective} from "../ui-edit-inline.directive";
import {DataService} from '../em/data.service';
import {NotificationService} from '../notification.service';

declare let jQuery: any;


@Component({
    selector: 'variation-snippets',
    templateUrl: 'templates/ab-experiment-details/variation-snippets.html',
    directives: [
        UiEditInlineDirective,
        UiPopoverComponent
    ]
})
export class VariationSnippetsComponent implements OnInit {

    @Input()
    variation: Variation;

    @Output()
    activated: EventEmitter<any> = new EventEmitter();

    private el: any;

    private _active: number;

    constructor(private elementRef: ElementRef,
                private data: DataService,
                private factory: AbExperimentFactoryService,
                private snippetsService: VariationSnippetsService,
                private auth: AuthService,
                private notifications: NotificationService) {
    }

    ngOnInit() {
        if (this.variation.snippetValues.length > 0) {
            this._active = 0;
        }
        this.snippetsService.addComponent(this);
    }

    get experiment(): Experiment {
        return this.variation.experiment;
    }

    get snippetValues(): SnippetValue[] {
        return this.variation.snippetValues.items;
    }

    isActive(index: number): boolean {
        return this._active === index;
    }

    doActivate(index: number) {
        // @todo test boundaries
        this._active = index;
    }

    onTabClick(index: number) {
        this.doActivate(index);
        this.activated.emit({
            index: index,
            variation: this.variation
        });
        this.snippetsService.activate(index, this.variation);
    }

    update() {
        this.data.persist(500);
    }

    create() {

        this.data.create<VariationSnippet>(VariationSnippet, this.experiment, {number: this.experiment.snippets.length}, false)
            .then(snippet => {
                this.experiment.snippets.push(snippet);
                let done = 0;
                this.experiment.variations.forEach(variation => {
                    this.data.create<SnippetValue>(SnippetValue, snippet, {variation: variation.number})
                        .then(value => {
                            variation.snippetValues.push(value);
                            if (++done == this.experiment.variations.length) {
                                this.doActivate(snippet.number);
                                this.snippetsService.activate(snippet.number, this.variation);
                            }
                        })
                        .catch(error => {
                            this.notifications.error('Error occurred while creating variation snippet. Check error console for details.')
                        });
                })
            })
            .catch(error => {
                this.notifications.error('Error occurred while creating variation snippet. Check error console for details.')
            })


    }

    //noinspection ReservedWordAsName
    delete(index: number) {

        if (this.variation.snippetValues.length == 1) {
            this.notifications.error('Cannot delete the only snippet.');
            return;
        }

        let snippets = this.experiment.snippets.items;
        let snippet = snippets[index];

        if (snippet == undefined) {
            this.notifications.error('Error has occurred while removing the snippet. Check error console for details.');
            return console.error("Can't find snippet " + index);
        }

        snippets.splice(snippet.number, 1);

        this.experiment.variations.forEach(variation => {
            variation.snippetValues.splice(snippet.number, 1);
        });

        this.data.remove(snippet)
            .then(() => {
                //
            })
            .catch(error => {
                this.notifications.error('Error has occurred while removing snippet. Check error console for details.');
                console.error(error);
            });

        for (let n = index; n < snippets.length; n++) {
            snippets[n].number = n;
        }

        let $this = this;

        let next = index == 0 ? 0 : index - 1;
        this.doActivate(next);
        this.snippetsService.activate(next, this.variation);
    }

    onPopoverInit(component: UiPopoverComponent) {
        component.setOwner(this);
    }

    snippetTabPopover(options: any, component: UiPopoverComponent, service: PopoverService) {

        let id = this.variation.number == 0 ?
            'ab_experiment_details.snippet_tab_header_control' :
            'ab_experiment_details.snippet_tab_header_variation';

        let message = service.message(id);
        let content = service.message(component.uiPopover);

        return message.content + '<br>' + content.content;
    }

    deleteSnippetPopover(options: any, component: UiPopoverComponent, service: PopoverService) {

        let content = service.message(component.uiPopover);

        if (this.variation.snippetValues.length > 1) {
            return content.content;
        }

        let message = service.message('ab_experiment_details.cant_delete_last_snippet');

        return content.content + '<br>' + 'NOTICE: ' + message.content;
    }

    blur(event: KeyboardEvent) {
        jQuery(event.srcElement).blur();
    }
}
