import {Component, Input, Output, EventEmitter} from 'angular2/core';

import {ExperimentUrl} from "../model/experiment-url";
import {DomDisabledDirective} from "../dom-disabled.directive";
import {UiPopoverComponent} from "../ui-popover.component";
import {PopoverService} from "../popover.service";
import {UiEditInlineDirective} from "../ui-edit-inline.directive";
import {DataService} from '../em/data.service';


@Component({
    selector: 'experiment-url',
    templateUrl: 'templates/ab-experiment-details/experiment-url.html',
    directives: [
        UiEditInlineDirective,
        DomDisabledDirective,
        UiPopoverComponent
    ]
})
export class ExperimentUrlComponent {

    @Input()
    url: ExperimentUrl;

    @Input()
    list: ExperimentUrl[] = [];

    @Input()
    private last: boolean;

    @Output('create')
    onCreate: EventEmitter<any> = new EventEmitter();

    @Output('delete')
    onDelete: EventEmitter<any> = new EventEmitter();


    constructor(private data: DataService) {
    }

    create() {
        this.onCreate.emit(null);
    }

    //noinspection ReservedWordAsName
    delete() {
        this.onDelete.emit(this.url);
    }

    update() {
        this.data.persist(500);
    }

    onPopoverInit(popover: UiPopoverComponent) {
        popover.setOwner(this);
    }

    deleteUrlButtonPopover(options: any, popover: UiPopoverComponent, service: PopoverService) {

        if (this.list.length > 1) {
            return service.message(popover.uiPopover).content;
        }

        return service.message(popover.uiPopover).content + '<br>NOTICE: ' +
            service.message('ab_experiment_details.cant_delete_last_url').content;
    }
}
