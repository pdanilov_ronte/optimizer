import {Component, Input} from 'angular2/core';
import {NgForm} from 'angular2/common';

import {AuthService} from "../auth";


@Component({
    selector: 'profile-account-user',
    templateUrl: 'templates/profile/profile-account-user.html'
})
export class ProfileAccountUserComponent {

    account: any = {};

    formErrors = {};

    active = true;

    form: NgForm;

    constructor(private auth: AuthService) {
    }

    ngOnInit() {
        this.account = this.clone(this.auth.account);
    }

    clone(data: any) {
        let account = {};
        for (let prop in data) {
            account[prop] = data[prop];
        }
        return account;
    }

    update() {
        this.auth.updateAccount(this.account)
            .then(result => {
                this.reset();
            })
            .catch(reason => {
                console.error(reason);
            });
    }

    reset() {

        this.account = this.clone(this.auth.account);

        this.active = false;
        setTimeout(() => this.active = true, 0);
    }
}
