var CONFIG = {
    "auth": {
        "login_check": "http://app.strongoptimizer.com/security/login_check",
        "account_update": "http://app.strongoptimizer.com/security-profile/edit",
        "change_password": "http://app.strongoptimizer.com/security-password/change-password"
    },
    "keep_alive": {
        "service_base_url": "http://app.strongoptimizer.com/service/-/keep-alive/"
    },
    "dao": {
        "breeze_api": "http://app.strongoptimizer.com/api/odp"
    },
    "cache": {
        "cache_host": "cache.strongoptimizer.com"
    }
};
