import {Component, Input, Output, EventEmitter, ElementRef} from 'angular2/core';
import {Router} from 'angular2/router';
import {NgClass} from 'angular2/common';

import {DetailsSidebarService} from "./details-sidebar.service";


@Component({
    selector: 'details-sidebar',
    templateUrl: 'templates/details-sidebar.html',
    providers: [ElementRef],
    directives: [NgClass]
})
export class DetailsSidebarComponent {

    @Output('open')
    onOpen: EventEmitter<any> = new EventEmitter();

    @Output('close')
    onClose: EventEmitter<any> = new EventEmitter();

    isOpen: boolean = false;

    constructor(private router: Router, elementRef: ElementRef, private service: DetailsSidebarService) {

        this.service.element = elementRef;
        this.service.anchor = 'detailsSidebar';

        service.onLoad(() => {
            this.service.open();
        });
        service.onOpen(() => {
            this.open();
        });
        service.onClose(() => {
            this.close();
        });
        this.router.subscribe(path => {
            // @todo maybe unnecessary
            this.service.close();
        });
    }

    public open(): void {
        this.isOpen = true;
        this.onOpen.emit(this);
    }

    public close(): void {
        this.isOpen = false;
        this.onClose.emit(this);
    }
}
