import IService = restangular.IService;
import IElement = restangular.IElement;

import {DTO} from './DTO';
import {CollectionDAO} from './CollectionDAO';


export class ElementDAO {

    private _service: IService;
    private _element: IElement;

    constructor(service: IService, element: IElement) {
        this._service = service;
        this._element = element;
    }

    public collection(path: string): CollectionDAO {
        let collection = this._service.restangularizeCollection(this._element, [], path);
        return new CollectionDAO(this._service, collection, path);
    }

    /*serialize(): DTO {
        return new DTO(this._element.plain());
    }*/

    get dto(): DTO {
        return new DTO(this._element.plain());
    }
}
