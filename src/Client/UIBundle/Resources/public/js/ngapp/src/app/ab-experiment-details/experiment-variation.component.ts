import {Component, Input, Output, EventEmitter, ViewEncapsulation, OnInit} from 'angular2/core';

import {DomDisabledDirective} from "../dom-disabled.directive";
import {VariationWeightComponent} from "./variation-weight.component";
import {VariationSnippetsComponent} from "./variation-snippets.component";

import {Variation} from "../model/variation";
import {UiPopoverComponent} from "../ui-popover.component";
import {UiEditInlineDirective} from "../ui-edit-inline.directive";
import {DataService} from '../em/data.service';


@Component({
    selector: 'experiment-variation',
    templateUrl: 'templates/ab-experiment-details/experiment-variation.html',
    directives: [
        UiEditInlineDirective,
        UiPopoverComponent,
        DomDisabledDirective,
        VariationWeightComponent,
        VariationSnippetsComponent
    ],
    encapsulation: ViewEncapsulation.Emulated
})
export class ExperimentVariationComponent implements OnInit {

    @Input()
    variation: Variation;

    @Output()
    changeWeight: EventEmitter<any> = new EventEmitter(false);

    @Output('afterWeightChanged')
    onAfterWeightChanged: EventEmitter<any> = new EventEmitter(false);

    @Output()
    snippetActivated: EventEmitter<any> = new EventEmitter();

    @Output('init')
    onInit: EventEmitter<any> = new EventEmitter();

    @Output('delete')
    onDelete: EventEmitter<any> = new EventEmitter();

    @Output('open')
    onOpen: EventEmitter<any> = new EventEmitter();

    opened = false;

    constructor(private data: DataService) {
    }

    ngOnInit() {
        this.onInit.emit(this);
    }

    onChangeWeight(event: any) {
        event.so.variation = this.variation;
        this.changeWeight.emit(event);
    }

    afterWeightChanged(event: any) {
        event.so.variation = this.variation;
        this.onAfterWeightChanged.emit(event);
    }

    onSnippetActivated(event: any) {
        this.snippetActivated.emit(event);
    }

    update() {
        this.data.persist(500);
    }

    //noinspection ReservedWordAsName
    delete() {
        this.onDelete.emit(this.variation);
    }

    open() {
        this.onOpen.emit(this);
        this.opened = true;
    }

    close() {
        this.opened = false;
    }

    toggle(event: any) {
        event.stopPropagation();
        if (this.opened) {
            this.close();
        } else {
            this.open();
        }
    }
}
