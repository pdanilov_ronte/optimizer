import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';

import {AuthService} from "./auth";


@Component({
    selector: 'menu-sidebar',
    templateUrl: 'templates/menu-sidebar.html',
    directives: [
        ROUTER_DIRECTIVES
    ]
})
export class MenuSidebarComponent {

    constructor(private auth: AuthService) {
    }

    logout() {
        this.auth.logout();
    }
}
