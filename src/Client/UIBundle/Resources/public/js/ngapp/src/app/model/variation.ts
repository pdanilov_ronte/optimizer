import {ModelEntity, Entity} from '../em/entity';
import {VariationFactory} from './variation.factory';
import {IndexedCollection} from '../em/indexed-collection';
import {SnippetValue} from './snippet-value';
import {Experiment} from './experiment';
import {Contract} from '../em/contract.decorator';
import {Persist} from '../em/persist.decorator';


@Entity(VariationFactory)
export class Variation extends ModelEntity {

    static states = {
        WEIGHT_LOCKED: 0x1,
        WEIGHT_DISABLED: 0x2
    };

    @Persist()
    number: number = 0;

    @Persist()
    title: string = '';

    @Persist()
    weight: number = 0;

    @Persist()
    state: number = 0;

    private _weightLocked: boolean;

    weightDisabled: boolean = false;

    experiment: Experiment;

    snippetValues: IndexedCollection<SnippetValue> = new IndexedCollection<SnippetValue>();

    constructor() {
        super();
    }

    get weightLocked(): boolean {
        return this._weightLocked;
    }

    set weightLocked(value: boolean) {
        if (value == this._weightLocked) {
            return;
        }
        if (value == true) {
            this.state |= Variation.states.WEIGHT_LOCKED;
        } else {
            this.state = this.state & ~Variation.states.WEIGHT_LOCKED;
        }
        this._weightLocked = value;
    }

    get route(): string {
        return Variation.ENTITY_ROUTE;
    }

    static get ENTITY_ROUTE(): string {
        return 'variations';
    }
}
