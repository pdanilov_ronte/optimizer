import {DomainEntity} from '../aggregate/DomainEntity';
import {EntityCollection} from './EntityCollection';


export abstract class DomainCollection<T extends DomainEntity> extends EntityCollection<T> {

    find(id: number): T {
        let items = this.findBy({id: id});
        if (items.length > 1) {
            throw new Error('Duplicate entity id');
        }
        return items.length == 0 ? null : items[0];
    }
}
