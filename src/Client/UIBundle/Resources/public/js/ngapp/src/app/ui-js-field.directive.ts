import {Component, Input, ElementRef} from 'angular2/core';


@Component({
    selector: 'ui-js-field',
    template: `
    <div class="component-root">
        <textarea class="js-field" rows="{{ rows }}" readonly spellcheck="false"
            (click)="select($event)">{{ code }}</textarea>
    </div>
    `,
    styles: [
        `
        textarea {
            color: #212F40;
            width: 100%;
            background: transparent;
            border: 0 solid transparent;
            outline: 0 solid transparent;
            position: absolute;
            padding: 0;
            left: -1px;
            top: -1px;
            line-height: 1.4;
            overflow: hidden;
            resize: none;
            position: relative !important;
            top: 0 !important;
            border: 1px solid;
            border-color: #d9d9d9 !important;
            border-top-color: #c0c0c0;
            background-color: #fff !important;
            padding-bottom: 10px !important;
            -webkit-border-radius: 1px;
            -moz-border-radius: 1px;
            border-radius: 1px;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            color: #333;
            font-size: 13px;
            line-height: 18px;
            padding: 1px 8px;
        }
        `
    ]
})
export class UiJsFieldDirective {

    @Input()
    rows: number;

    @Input()
    params: any;

    private _code: string;

    constructor(private elementRef: ElementRef) {
    }

    @Input()
    set code(value: string) {

        for (let k in this.params) {
            let v = this.params[k];
            let re = new RegExp('\\{\\{\\s*' + k + '\\s*}}');
            value = value.replace(re, v);
        }

        this._code = value;

        if (this.rows == undefined) {
            this.rows = this._code.split("\n").length;
        }
    }

    get code(): string {
        return this._code;
    }

    select(event:any) {
        event.target.select();
    }
}
