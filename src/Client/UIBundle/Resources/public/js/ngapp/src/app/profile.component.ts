import {Component} from 'angular2/core';

import {ProfileAccountComponent} from "./profile/profile-account.component";


@Component({
    selector: 'profile-page',
    templateUrl: 'templates/profile.html',
    directives: [
        ProfileAccountComponent
    ]
})
export class ProfileComponent {

}
