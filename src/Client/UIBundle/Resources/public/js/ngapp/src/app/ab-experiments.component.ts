import {Component} from 'angular2/core';


@Component({
    template: '<router-outlet></router-outlet>',
    providers: []
})
export class AbExperimentsComponent {

    constructor() {
    }
}
