import {DecoratorMetadata, makeTypeDecorator} from '../decorator';
import {ModelEntity} from './entity';


export class EntityTypeMetadata implements DecoratorMetadata<ModelEntity> {

    private type: typeof ModelEntity;

    constructor() {
    }

    decorateTarget(target: any, name: string): void {
        this.type = target;
        EntityTypeRegistry.register(target);
    }
}

export interface EntityTypeDecorator {
    (): any;
    new (): any;
}

export class EntityTypeRegistry {

    static types: typeof ModelEntity[] = [];

    static register(type: typeof ModelEntity): void {
        if (EntityTypeRegistry.types.indexOf(type) !== undefined) {
            EntityTypeRegistry.types.push(type);
        }
    }
}

/**
 * @deprecated
 */
export let EntityType: EntityTypeDecorator = makeTypeDecorator(EntityTypeMetadata);
