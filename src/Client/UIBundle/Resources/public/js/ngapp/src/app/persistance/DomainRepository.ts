import {RootDAO} from './RootDAO';
import {ElementDAO} from './ElementDAO';
import {DomainEntity} from '../aggregate/DomainEntity';
import {DomainCollection} from './DomainCollection';
import {EntityRepository} from './EntityRepository';


export abstract class DomainRepository<T extends DomainEntity> extends EntityRepository<T> {

    private _rootDAO: RootDAO;
    private _list: DomainCollection<T>;

    constructor(dao: RootDAO, collection: DomainCollection<T>) {
        super();
        this._rootDAO = dao;
        this._list = collection;
        this.init();
    }

    public add(entity: T): void {
        this._list.push(entity);
    }

    public all(): T[] {
        return this._list.all();
    }

    public attach(entity: T): void {
        entity.attach(this);
    }

    protected build(source: ElementDAO[], destination: DomainCollection<T>): void {
        for (let item of source) {
            destination.push(this.buildItem(item));
        }
    }

    protected abstract buildItem(source: ElementDAO): T;

    /**
     * @todo
     */
    public createQueryBuilder(): void {
    }

    protected get dao(): RootDAO {
        return this._rootDAO;
    }

    public find(id: number): T {
        return this._list.find(id);
    }

    public findBy(criteria: any): T[] {
        return this._list.findBy(criteria);
    }

    protected init() {
        this.dao.all()
            .then(result => {
                this.build(result, this._list);
                console.log(this._list);
            })
            .catch(error => {
                console.error(error)
            });
    }
}
