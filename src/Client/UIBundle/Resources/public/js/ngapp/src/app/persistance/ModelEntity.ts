import {Serializable} from './Serializable';
import {DTO} from './DTO';
import {ElementDAO} from './ElementDAO';
import {EntityRepository} from './EntityRepository';


export abstract class ModelEntity implements Serializable {

    private _dao: ElementDAO;

    constructor(dao?: ElementDAO) {
        if (dao) {
            this._dao = dao;
            this.build(dao);
        }
    }

    public attach(repository: EntityRepository<ModelEntity>): void {
        // let dto = repository.createDTO(this);
        // this.setDTO(dto);
    }

    protected abstract build(dao: ElementDAO): void;

    public abstract restore(dto: DTO): void;

    public abstract serialize(): DTO;
}
