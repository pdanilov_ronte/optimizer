import {Component, Input, Output, EventEmitter, ElementRef, OnInit} from 'angular2/core';

declare let jQuery:any;


@Component({
    selector: 'ui-edit-inline',
    template: `
    <div class="directive-root edit-inline-container" [ngClass]="{ 'edit-code': contentType == 'code' }">
        <textarea rows="{{ rows }}" style="resize:none" spellcheck="false"
        [(ngModel)]="model" placeholder="{{ placeholder }}"
        (keydown)="onKeydown($event)" (keyup)="onKeyup($event)"></textarea>
    </div>
    `,
    styles: [`
        .edit-inline-container {
            position: relative;
            min-height: 19px;
        }

        .edit-code-inline-container {
            position: relative;
            min-height: 143px;
        }

        .edit-inline-container textarea {
            color: #212F40;
            width: 100%;
            background: transparent;
            border: 0 solid transparent;
            outline: 0 solid transparent;
            position: absolute;
            padding: 0;
            left: -1px;
            top: -1px;
            line-height: 1.4;
            overflow: hidden;
            resize: none;
        }

        .edit-inline-container textarea:hover {
            border-bottom: 1px solid rgba(0, 0, 0, 0.25);
        }

        .edit-inline-container textarea[disabled] {
            color: rgba(0, 0, 0, 0.65);
        }

        .edit-inline-container textarea[disabled]:hover {
            border-bottom: none;
        }

        .edit-inline-container.edit-code textarea {
            position: relative;
            border-left: 1px solid rgba(0, 0, 0, 0.25);
            padding-left: 10px;
            /*top: 10px;*/
        }

        .edit-inline-container.edit-code textarea:hover {
            border-bottom: none;
        }
    `]
})
export class UiEditInlineComponent implements OnInit {

    private _model:string;

    private _textarea:HTMLTextAreaElement;

    @Output()
    change:EventEmitter<any> = new EventEmitter();

    @Input()
    rows:number = 1;

    @Input()
    contentType:string = 'string';

    @Input()
    placeholder:string = '';

    @Input('model')
    model:string;

    @Output('model')
    modelChange:EventEmitter<string> = new EventEmitter();

    @Output('enter')
    onEnter:EventEmitter<any> = new EventEmitter();

    @Output('escape')
    onEscape:EventEmitter<any> = new EventEmitter();

    private _lock:any;
    private _change:any;


    constructor(private elementRef:ElementRef) {
    }

    ngOnInit() {
        this._textarea = jQuery(this.elementRef.nativeElement).find('textarea')[0];
    }

    onKeydown(event:KeyboardEvent) {
        switch (event['code']) {
            case 'Enter':
                if (this.contentType == 'string') {
                    event.preventDefault();
                    event.stopPropagation();
                    this.onEnter.emit({component: this, event: event});
                }
                break;
            case 'Escape':
                if (this.contentType == 'string') {
                    event.preventDefault();
                    event.stopPropagation();
                    this._textarea.blur();
                    this.onEscape.emit({component: this, event: event});
                }
                break;
        }
    }

    onKeyup(event:KeyboardEvent) {
        switch (event['code']) {
            case 'Enter':
            case 'Escape':
                break;
            default:
                this.modelChange.emit(this._textarea.value);
                //this.change.emit(this._textarea.value);
        }
    }
}
