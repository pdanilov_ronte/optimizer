import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';


@Injectable()
export class ExperimentStatsService {

    private _stats:{} = {};

    constructor(private http:Http) {
    }

    experiment(id:number):Promise<any> {

        if (this._stats[id] != undefined) {
            return new Promise((resolve) => {
                resolve(this._stats[id]);
            });
        }

        return new Promise((resolve, reject) => {
            this.load(id)
                .then(result => {
                    resolve(result);
                })
                .catch(reason => {
                    reject(reason);
                });
        })
    }

    private load(experiment_id:number):Promise<any> {

        let url = '/bundles/stats/variation_date.php';

        let request = this.http.get(url, {search: 'experiment_id=' + experiment_id + '&_qt=' + Date.now()});

        return new Promise((resolve, reject) => {

            request.subscribe(
                (res: Response) => {
                    let json = res.json();
                    resolve(res.json());
                },
                (err: Response) => {
                    reject(err.json());
                }
            )
        });
    }
}
