import {Type, Injectable, Inject, DynamicComponentLoader, ElementRef, ComponentRef} from 'angular2/core';
import {DetailsSidebarComponent} from "./details-sidebar.component";


@Injectable()
export class DetailsSidebarService {

    private elementRef: ElementRef;
    private nativeElement: HTMLElement;

    private anchorName: string;

    private currentRef: ComponentRef;

    private _onLoad: Function;

    private _onOpen: Function;
    private _onClose: Function;

    private _isOpen: boolean;

    hidden: boolean = false;

    constructor(@Inject(DynamicComponentLoader) private dcl: DynamicComponentLoader) {
    }

    public set element(elementRef: ElementRef) {
        this.elementRef = elementRef;
        this.nativeElement = elementRef.nativeElement;
    }

    public set anchor(anchorName: string) {
        this.anchorName = anchorName;
    }

    public loadComponent(type: Type): Promise<ComponentRef> {

        this.cleanup();

        return new Promise<ComponentRef>((resolve, reject) => {

            // @todo assert that elementRef and anchorName are defined
            this.dcl.loadIntoLocation(type, this.elementRef, this.anchorName)
                .then((ref: ComponentRef) => {
                    if (this._onLoad) {
                        this._onLoad();
                    }
                    this.currentRef = ref;
                    resolve(ref);
                })
                .catch(error => reject(error));
        });
    }

    public unloadComponent() {
        //this.currentRef.dispose();
    }

    // @todo implement with observer
    public onLoad(callback: Function) {
        this._onLoad = callback;
    }

    public onBlur() {
        // @todo set method to dispose current component on `virtual` blur event
    }

    get isOpen(): boolean {
        return this._isOpen;
    }

    public onOpen(callback: Function) {
        this._onOpen = callback;
    }

    public open() {
        this.nativeElement.style.display = 'block';
        this._isOpen = true;
        this._onOpen();
    }

    public onClose(callback: Function) {
        this._onClose = callback;
    }

    public close() {
        this._isOpen = false;
        this.cleanup();
        this._onClose();
    }

    public cleanup() {
        if (this.currentRef) {
            this.currentRef.dispose();
        }
    }
}
