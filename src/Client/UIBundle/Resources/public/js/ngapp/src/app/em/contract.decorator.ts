import {ModelEntity} from './entity';
import {IndexedCollection} from './indexed-collection';
import {EntityManager} from './entity-manager';
import IElement = restangular.IElement;
import ICollection = restangular.ICollection;
import {DecoratorMetadata, makePropDecorator} from '../decorator';


export class PropertyContract<T extends ModelEntity> {

    private _fulfilled: boolean = false;

    constructor(public entityType: typeof ModelEntity,
                public propertyName: string) {
    }

    bind(entity: T, em: EntityManager): ContractDelegate<T> {
        return new ContractDelegate<T>(this, entity, em);
    }

    fulfill(data: IndexedCollection<T>, delegate: ContractDelegate<T>): void {
        delegate.deliver(this.property.bind(this), data);
        this._fulfilled = true;
    }

    get fulfilled(): boolean {
        return this._fulfilled;
    }

    protected property(entity: T): IndexedCollection<T> {
        return entity[this.propertyName];
    }
}

export class ContractDelegate<T extends ModelEntity> {

    constructor(private contract: PropertyContract<T>,
                private entity: T,
                private em: EntityManager) {
    }

    transfer(method: Function): Promise<IndexedCollection<T>> {
        return method(this.contract.entityType, this.entity);
    }

    deliver(property: Function, data: IndexedCollection<T>): void {
        let destination: IndexedCollection<T> = property(this.entity);
        if (destination.length) {
            destination.items.splice(0);
        }
        for (let item of data.items) {
            destination.push(item);
        }
    }

    expand(method: Function, data: IndexedCollection<T>): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (!this.contract.fulfilled) {
                return reject('Attempt to expand unfulfilled contract');
            }
            if (!data.length) {
                // @todo empty promise results
                return resolve();
            }
            if (!ModelEntity.isExpandable(data.items[0])) {
                return resolve();
            }
            let expanded = 0;
            data.forEach((entity: T) => {
                entity.expand()
                    .then(result => {
                        if (++expanded == data.length) {
                            resolve(this.entity);
                        }
                    })
                    .catch(error => {
                        reject(error);
                    });
            });
        });
    }
}


class ContractFactory {
    static create(meta: ContractMetadata): PropertyContract<ModelEntity> {
        return new PropertyContract(meta.entityType, meta.propertyName);
    }
}

export class ContractMetadata implements DecoratorMetadata<ModelEntity> {

    public entityType: typeof ModelEntity;
    public targetClass: typeof ModelEntity;
    public propertyName: string;

    public contract: PropertyContract<ModelEntity>;

    constructor() {
    }

    decorateTarget(targetClass: typeof ModelEntity, propertyName: string, args?: any): void {
        this.targetClass = targetClass;
        this.propertyName = propertyName;
        this.entityType = args[0];
        this.contract = ContractFactory.create(this);
    }
}

export interface ContractFactoryDecorator {
    (entityType: typeof ModelEntity): any;
    new (entityType: typeof ModelEntity): any;
}

export let Contract: ContractFactoryDecorator = makePropDecorator(ContractMetadata);
