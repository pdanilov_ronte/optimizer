import {ModelEntity} from '../persistance/ModelEntity';
import {ElementDAO} from '../persistance/ElementDAO';
import {DTO} from '../persistance/DTO';


export class Variation extends ModelEntity {

    id: number;
    number: number;
    title: string;
    state: number;
    weight: number;
    enabled: boolean;

    protected build(dao: ElementDAO): void {
        this.restore(dao.dto);
    }

    public restore(dto: DTO): void {
        let data = dto.data();
        this.id = data['id'];
        this.number = data['number'];
        this.title = data['title'];
        this.state = data['state'];
        this.weight = data['weight'];
        this.enabled = data['enabled'];
    }

    public serialize(): DTO {
        return new DTO({
            id: this.id,
            number: this.number,
            title: this.title,
            state: this.state,
            weight: this.weight,
            enabled: this.enabled
        });
    }
}
