import {Injectable} from 'angular2/core';

import {ConfigService} from "./config.service";
import {XhrService} from "./xhr.service";
import {AuthService} from "./auth";


@Injectable()
export class KeepAliveService {

    private baseUrl: string;

    private timeout: number;
    private max: number;

    private expires: Date;

    private interval: number;

    constructor(private config: ConfigService,
                private xhr: XhrService,
                private auth: AuthService) {

        this.baseUrl = this.config._.keep_alive.service_base_url;
    }

    public initialize(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.xhr.post(this.url('initialize'), {})
                .then((response) => {
                    let data = response.json().data;
                    this.timeout = data['timeout'];
                    this.max = data['max'];
                    this.reset();
                    this.interval = setInterval(this.refresh.bind(this), this.timeout * 1000);
                    resolve();

                })
                .catch((error) => {
                    console.error(error);
                    reject();
                });
        })
    }

    public reset() {
        let time = new Date();
        time.setSeconds(time.getSeconds() + this.max);
        this.expires = time;
    }

    public refresh() {

        let time = new Date();

        if (time >= this.expires) {

            clearInterval(this.interval);
            this.close();

        } else {

            this.xhr.post(this.url('refresh'), {})
                .then((response) => {
                    //
                })
                .catch((error) => {
                    // console.error(error);
                    if (error.status == 'authentication_required') {
                        this.auth.logout();
                    }
                });
        }
    }

    public close() {
        this.xhr.post(this.url('close'), {})
            .then((response) => {
                this.auth.logout();
            })
            .catch((error) => {
                console.error(error);
            });
    }

    private url(method: string): string {
        return this.baseUrl + method;
    }
}
