import {Serializable} from './Serializable';
import {DTO} from './DTO';


export interface SerializableCollection<T> extends Serializable {
    restoreItem(dto: DTO): T;
}
