import {DecoratorMetadata, makePropDecorator} from '../decorator';
import {ModelEntity} from './entity';


export class PropertyPersist<T extends ModelEntity> {

    constructor(public propertyName: string) {
    }
}

class PersistFactory {
    static create(meta: PersistMetadata): PropertyPersist<ModelEntity> {
        return new PropertyPersist(meta.propertyName);
    }
}

export class PersistMetadata implements DecoratorMetadata<ModelEntity> {

    public targetClass: typeof ModelEntity;
    public propertyName: string;

    public persist: PropertyPersist<ModelEntity>;


    constructor() {
    }

    decorateTarget(targetClass: typeof ModelEntity, propertyName: string): void {
        this.targetClass = targetClass;
        this.propertyName = propertyName;
        this.persist = PersistFactory.create(this);
    }
}

export interface PersistFactoryDecorator {
    (): any;
    new (): any;
}

export let Persist: PersistFactoryDecorator = makePropDecorator(PersistMetadata);
