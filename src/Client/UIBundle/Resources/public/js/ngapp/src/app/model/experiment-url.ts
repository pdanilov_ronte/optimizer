import {ModelEntity, Entity} from '../em/entity';
import {ExperimentUrlFactory} from './experiment-url.factory';
import {Persist} from '../em/persist.decorator';


@Entity(ExperimentUrlFactory)
export class ExperimentUrl extends ModelEntity {

    @Persist()
    pattern: string;

    constructor() {
        super();
    }

    get route(): string {
        return ExperimentUrl.ENTITY_ROUTE;
    }

    static get ENTITY_ROUTE(): string {
        return 'urls';
    }
}
