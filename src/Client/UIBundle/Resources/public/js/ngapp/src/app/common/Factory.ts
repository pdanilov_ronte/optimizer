interface CT<T> {
    new(): T;
}

export abstract class Factory<T> {

    constructor(private __ct: CT<T>) {
    }

    public create(): T {
        return new this.__ct();
    }
}
