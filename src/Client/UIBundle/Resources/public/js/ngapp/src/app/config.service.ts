import {Injectable} from 'angular2/core';

declare let CONFIG: any;
declare let CONFIG_EXT: any;


@Injectable()
export class ConfigService {

    private _config: any = {};

    constructor() {
        if (CONFIG == undefined) {
            throw new Error('Config file is not loaded');
        }
        this._config = CONFIG;
        this._config.app = (typeof CONFIG_EXT != 'undefined') ? CONFIG_EXT.app : {};
    }

    public get _(): any {
        return this._config;
    }
}
