import {Component} from 'angular2/core';

import {RestangularBackend} from '../persistance/RestangularBackend';
import {EntityManager} from '../persistance/EntityManager';
import {ExperimentService} from '../experiments/ExperimentService';
import {UiEditInlineDirective} from "../ui-edit-inline.directive";


@Component({
    selector: 'experiment-component',
    template: `
<!--suppress HtmlUnknownAttribute -->
<div class="component-root">
  <h3>Experiments</h3>
  <div *ngFor="#item of experiments.list" class="experiment-item">
    <h4>{{ item.title }}</h4>
    <div class="urls">
        <div *ngFor="#url of item.urls.items">
            <input [edit-inline]
                   [(ngModel)]="url.pattern"
                   (change)="update(url)"
                   [placeholder]="'Experiment URL'"/>
        </div>
    </div>
    <div class="variations">
        <div *ngFor="#variation of item.variations.items">
            <h4><input [edit-inline]
                       [(ngModel)]="variation.title"
                       (change)="update(variation)"
                       [placeholder]="'Variation Title'"/></h4>
        </div>
    </div>
  </div>
</div>
    `,
    providers: [
        RestangularBackend,
        EntityManager,
        ExperimentService
    ],
    directives: [
        UiEditInlineDirective
    ]
})
export class ExperimentComponent {

    constructor(public experiments: ExperimentService) {
    }

    public ngOnInit(): void {
        let experiment = this.experiments.create();
        console.log(experiment);
    }

    public update(event: any): void {
        console.log(event);
    }
}
