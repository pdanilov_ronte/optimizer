import {ModelEntity, Entity} from '../em/entity';
import {IndexedCollection} from '../em/indexed-collection';
import {GoalFactory} from './goal.factory';
import {GoalDestination} from './goal-destination';
import {Expandable} from '../em/expandable.decorator';
import {Contract} from '../em/contract.decorator';
import {Persist} from '../em/persist.decorator';


@Expandable()
@Entity(GoalFactory)
export class Goal extends ModelEntity {

    @Persist()
    title: string;

    pattern: string = '';

    @Contract(GoalDestination)
    destinations = new IndexedCollection<GoalDestination>();

    constructor() {
        super();
    }

    get route(): string {
        return Goal.ENTITY_ROUTE;
    }

    static get ENTITY_ROUTE(): string {
        return 'goals';
    }

    public expand(): Promise<Goal> {
        return this.doExpand(Goal, this);
    }
}
