import {AbstractEntityFactory} from '../em/entity-factory';
import {SnippetValue} from './snippet-value';


//noinspection JSUnusedGlobalSymbols
export class SnippetValueFactory extends AbstractEntityFactory<SnippetValue> {

    create(complete: boolean = false, params?: any): Promise<SnippetValue> {
        let value = new SnippetValue();

        // @todo -
        if (params && (params['variation'] !== undefined)) {
            value.variation = params['variation'];
        }

        return new Promise((resolve, reject) => {
            this.em.persist()
                .then(() => {
                    resolve(value)
                })
                .catch(error => {
                    reject(error)
                });
        });
    }

    fromIElement(element: restangular.IElement): SnippetValue {
        let value = new SnippetValue();

        value.value = element['value'];

        value.em = this.em;
        value.ielement = element;

        return value;
    }
}
