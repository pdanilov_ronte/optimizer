import {EntityCollection} from '../persistance/EntityCollection';
import {DTO} from '../persistance/DTO';
import {SnippetValue} from "./SnippetValue";


export class SnippetValueCollection extends EntityCollection<SnippetValue> {
    restoreItem(dto: DTO): SnippetValue {
        return new SnippetValue(this.dao.create(dto));
    }
}
