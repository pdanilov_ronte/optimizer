import {Component} from 'angular2/core';
import {Router, RouteConfig} from 'angular2/router';

import {DashboardAbStatsComponent} from "./dashboard/dashboard-ab-stats.component";
import {Experiment} from './model/experiment';


@Component({
    selector: 'dashboard',
    templateUrl: 'templates/dashboard.html',
    providers: [],
    directives: [
        DashboardAbStatsComponent
    ]
})
export class DashboardComponent {
}
