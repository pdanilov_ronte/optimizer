import {AbstractEntityFactory} from '../em/entity-factory';
import {ExperimentUrl} from './experiment-url';


//noinspection JSUnusedGlobalSymbols
export class ExperimentUrlFactory extends AbstractEntityFactory<ExperimentUrl> {

    create(complete: boolean = false, params?: any): Promise<ExperimentUrl> {
        let url = new ExperimentUrl();

        url.pattern = '';

        return new Promise((resolve, reject) => {
            this.em.persist()
                .then(() => {
                    resolve(url);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    fromIElement(element: restangular.IElement): ExperimentUrl {
        let url = new ExperimentUrl();

        url.pattern = element['pattern'];

        url.em = this.em;
        url.ielement = element;

        return url;
    }
}
