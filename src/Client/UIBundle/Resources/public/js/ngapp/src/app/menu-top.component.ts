import {Component, ElementRef, OnInit} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';

import {AuthService} from "./auth";


@Component({
    selector: 'menu-top',
    templateUrl: 'templates/menu-top.html',
    directives: [
        ROUTER_DIRECTIVES
    ]
})
export class MenuTopComponent implements OnInit {

    constructor(private auth:AuthService, private elementRef:ElementRef) {
    }

    ngOnInit():any {
        let userHeader:any = jQuery(this.elementRef.nativeElement)
            .find('#user-header').find('.dropdown-toggle');

        userHeader.dropdownHover();
    }

    get account() {
        return this.auth.account;
    }

    logout() {
        this.auth.logout();
    }
}
