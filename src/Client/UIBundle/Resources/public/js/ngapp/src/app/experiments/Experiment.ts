import {DTO} from '../persistance/DTO';
import {ElementDAO} from '../persistance/ElementDAO';
import {AggregateRoot} from '../aggregate/AggregateRoot';
import {ExperimentURLCollection} from './ExperimentURLCollection';
import {VariationCollection} from "./VariationCollection";
import {SnippetCollection} from "./SnippetCollection";
import {GoalCollection} from "./GoalCollection";


export class Experiment extends AggregateRoot {

    id: number;
    title: string;

    urls: ExperimentURLCollection;
    variations: VariationCollection;
    snippets: SnippetCollection;
    goals: GoalCollection;

    /*constructor(dao?: ElementDAO) {
        this.urls = new ExperimentURLCollection();
        super(dao);
    }*/

    protected build(dao: ElementDAO): void {
        console.log('build experiment');
        this.urls = new ExperimentURLCollection(dao.collection('urls'));
        this.variations = new VariationCollection(dao.collection('variations'));
        this.snippets = new SnippetCollection(dao.collection('snippets'));
        this.goals = new GoalCollection(dao.collection('goals'));
        // this.urls.initialize(dao.collection('urls'));
        // this.variations.initialize(dao.collection('variations'));
        // this.snippets.initialize(dao.collection('snippets'));
        // this.goals.initialize(dao.collection('goals'));
        this.restore(dao.dto);
    }

    public restore(dto: DTO): void {
        let data = dto.data();
        this.id = data['id'];
        this.title = data['title'];
        this.urls.restore(new DTO(data['urls']));
        this.variations.restore(new DTO(data['variations']));
        this.snippets.restore(new DTO(data['snippets']));
        this.goals.restore(new DTO(data['goals']));
    }

    public serialize(): DTO {
        return new DTO({
            id: this.id,
            title: this.title,
            urls: this.urls.serialize(),
            variations: this.variations.serialize()
        });
    }
}
