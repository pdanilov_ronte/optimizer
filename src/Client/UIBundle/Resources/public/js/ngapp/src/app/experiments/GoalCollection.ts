import {EntityCollection} from '../persistance/EntityCollection';
import {DTO} from '../persistance/DTO';
import {Goal} from "./Goal";


export class GoalCollection extends EntityCollection<Goal> {
    restoreItem(dto: DTO): Goal {
        return new Goal(this.dao.create(dto));
    }
}
