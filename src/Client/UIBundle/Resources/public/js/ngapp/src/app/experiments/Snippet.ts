import {ModelEntity} from '../persistance/ModelEntity';
import {ElementDAO} from '../persistance/ElementDAO';
import {DTO} from '../persistance/DTO';
import {SnippetValueCollection} from "./SnippetValueCollection";


export class Snippet extends ModelEntity {

    id: number;
    number: number;
    title: string;

    values: SnippetValueCollection;

    protected build(dao: ElementDAO): void {
        // this.values = new SnippetValueCollection(dao.collection('values'));
        this.values = new SnippetValueCollection();
        this.values.initialize(dao.collection('values'));
        this.restore(dao.dto);
    }

    public restore(dto: DTO): void {
        let data = dto.data();
        this.id = data['id'];
        this.number = data['number'];
        this.title = data['title'];
        this.values.restore(new DTO(data['values']));
    }

    public serialize(): DTO {
        return new DTO({
            id: this.id,
            number: this.number,
            title: this.title
        });
    }
}
