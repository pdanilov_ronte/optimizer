import {Component, EventEmitter, OnDestroy} from 'angular2/core';
import {CanReuse, ComponentInstruction} from 'angular2/router';
import {FORM_DIRECTIVES} from 'angular2/common';
import {Http} from 'angular2/http';

import {DomDisabledDirective} from "./dom-disabled.directive";
import {ExperimentUrlListComponent} from "./ab-experiment-details/experiment-url-list.component";
import {ExperimentVariationListComponent} from "./ab-experiment-details/experiment-variation-list.component";

import {Experiment} from "./model/experiment";
import {ExperimentGoalListComponent} from "./ab-experiment-details/experiment-goal-list.component";
import {AuthService} from "./auth";
import {UiJsFieldDirective} from "./ui-js-field.directive";
import {UiPopoverComponent} from "./ui-popover.component";
import {PopoverService} from "./popover.service";
import {ExperimentPublishService} from "./experiment-publish.service";
import {UiLaddaDirective} from "./ui-ladda.directive";
import {NotificationService} from "./notification.service";
import {UiEditInlineDirective} from "./ui-edit-inline.directive";
import {DataService} from './em/data.service';
import {ConfigService} from './config.service';


@Component({
    selector: 'so-app',
    templateUrl: 'templates/ab-experiment-details.html',
    providers: [
        ExperimentPublishService
    ],
    directives: [
        UiEditInlineDirective,
        UiPopoverComponent,
        UiJsFieldDirective,
        DomDisabledDirective,
        ExperimentUrlListComponent,
        ExperimentVariationListComponent,
        ExperimentGoalListComponent,
        UiLaddaDirective,
        FORM_DIRECTIVES
    ]
})
export class AbExperimentDetailsComponent implements CanReuse, OnDestroy {

    experiment: Experiment;

    onDelete: EventEmitter<Experiment> = new EventEmitter();

    onRun: EventEmitter<Experiment> = new EventEmitter();

    //noinspection JSUnresolvedLibraryURL,JSUnusedGlobalSymbols
    jsSnippetCode = '<script src="http://{{ host }}/a/{{ uid }}.js" async="async"></script>';
    jsSnippetParams = {};

    constructor(private data: DataService,
                private publishService: ExperimentPublishService,
                private http: Http,
                private auth: AuthService,
                private notifications: NotificationService,
                private config: ConfigService) {

        this.jsSnippetParams['host'] = this.config._.cache.cache_host;
        this.jsSnippetParams['uid'] = this.auth.account.uid;
    }

    routerCanReuse(nextInstruction: ComponentInstruction, prevInstruction: ComponentInstruction): any {
        return true;
    }

    ngOnDestroy(): any {
    }

    public setExperiment(experiment: Experiment) {
        this.experiment = experiment;
    }

    public update() {
        this.data.persist(500);
    }

    get valid(): boolean {
        return this.publishService.errors(this.experiment).length == 0;
    }

    //noinspection JSMethodCanBeStatic
    get pristine(): boolean {
        // @todo implement changes tracking
        //return this.experiment.pristine;
        return false;
    }

    //noinspection JSUnusedGlobalSymbols
    get publishing(): boolean {
        return this.publishService.publishing;
    }

    public run() {

        if (!this.valid) {
            return;
        }

        this.experiment.state = 2;
        this.experiment.startedAt = new Date();

        this.data.persist().then(() => {
            this.onRun.emit(this.experiment);
            this.publishService.publish(this.experiment);
        });
    }

    public toggle(): void {
        switch (this.experiment.state) {
            case 2:
                this.experiment.state = 3;
                break;
            case 3:
                if (!this.valid) {
                    // return console.warn('Cannot resume invalid experiment');
                    return;
                }
                this.experiment.state = 2;
                break;
            default:
                console.error(sprintf('Invalid experiment state %s', this.experiment.state));
        }

        this.data.persist()
            .then(_ => {
                this.publishService.publish(this.experiment)
                    .then(result => {
                        if (this.experiment.state == 2) {
                            this.notifications.success('experiment successfully activated');
                        }
                    })
                    .catch(reason => {
                        this.notifications.error('error has occurred');
                    });
            })
            .catch(_ => console.error(_));
    }

    //noinspection ReservedWordAsName
    public delete() {
        this.onDelete.emit(this.experiment);
    }

    public touch() {
        if (this.experiment.pristine) {
            this.experiment.pristine = false;
        }
    }

    public publish() {
        if (!this.valid) {
            return;
        }

        this.publishService.publish(this.experiment)
            .then(result => {
                this.notifications.success('experiment has been published successfully');
            })
            .catch(reason => {
                this.notifications.error('failed to publish experiment');
            });
    }

    public details() {
    }

    onPopoverInit(component: UiPopoverComponent) {
        component.setOwner(this);
    }

    //noinspection JSUnusedGlobalSymbols
    runExperimentPopover(options: any, component: UiPopoverComponent, service: PopoverService) {

        let errors = this.publishService.errors(this.experiment);

        let message = errors.length ?
            service.message(sprintf('ab_experiment_details.cant_run_experiment.%s', errors[0])).content :
            service.message('ab_experiment_details.run_experiment_button.ready').content;

        return service.message(component.uiPopover).content + '<br>' + 'NOTICE: ' + message;
    }

    //noinspection JSUnusedGlobalSymbols
    publishExperimentPopover(options: any, component: UiPopoverComponent, service: PopoverService) {

        let errors = this.publishService.errors(this.experiment);

        return errors.length ?
            service.message(sprintf('ab_experiment_details.cant_publish_experiment.%s', errors[0])).content :
            service.message('ab_experiment_details.publish_experiment_button.ready').content;
    }

    //noinspection JSUnusedGlobalSymbols
    toggleExperimentPopover(options: any, component: UiPopoverComponent, service: PopoverService) {

        if (this.experiment.state == 3) {
            let errors = this.publishService.errors(this.experiment);
            if (errors.length) {
                return service.message(sprintf('ab_experiment_details.cant_run_experiment.%s', errors[0])).content;
            }
            return service.message('ab_experiment_details.resume_experiment_button').content;
        } else {
            return service.message('ab_experiment_details.pause_experiment_button').content;
        }
    }
}
