import {DomainEntity} from './DomainEntity';
import {EntityFactory} from './EntityFactory';


export abstract class DomainFactory<T extends DomainEntity> extends EntityFactory<T> {
}
