import {DTO} from './DTO';
import {ModelEntity} from './ModelEntity';
import {EntityRepository} from './EntityRepository';


export class EntityManager {

    private _repositories: Map<typeof ModelEntity, EntityRepository<ModelEntity>>;

    public createRootDTO(type: typeof ModelEntity): DTO {
        return null;
    }

    public getDTO(type: typeof ModelEntity): DTO {
        return null;
    }

    public getRepository(type: typeof ModelEntity): EntityRepository<ModelEntity> {
        return null;
    }
}
