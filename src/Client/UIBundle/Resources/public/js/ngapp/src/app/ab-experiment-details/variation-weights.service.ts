import {Variation} from "../model/variation";

export class VariationWeightsService {

    static states = {
        LOCKED: 0x1,
        DISABLED: 0x2
    };

    applyChange(value: number, variation: Variation): number {

        let delta: number = value - variation.weight,
            limit: number = 100,
            subjects: Variation[] = [],
            list = variation.experiment.variations;

        if (delta == 0) {
            return variation.weight;
        }

        list.forEach((entry) => {
            if (entry == variation) {
                return;
            }
            if (entry.weightLocked) {
                limit -= entry.weight;
            } else if (delta > 0) {
                limit -= 1;
                if (entry.weight > 1) {
                    subjects.push(entry);
                }
            } else {
                subjects.push(entry);
            }
        });

        if (subjects.length == 0) {
            return variation.weight;
        }

        if (delta < 0) {
            limit = variation.number == 0 ? 1 : 0;
        }

        let applyValue: number;
        if (delta > 0) {
            applyValue = value > limit ? limit : value;
        } else {
            applyValue = value < limit ? limit : value;
        }

        // apply value
        let applyDelta = applyValue - variation.weight;
        variation.weight = applyValue;

        // redistribute subject's weights
        this.applyDelta(applyDelta, subjects);

        return applyValue;
    }

    public refill(list: Variation[]): void {

        let subjects: Variation[] = [],
            delta: number = -100,
            total: number = 100;

        list.forEach((entry: Variation) => {
            delta += entry.weight;
            if (!entry.weightLocked) {
                subjects.push(entry);
            } else {
                total -= entry.weight;
            }
        });

        if (subjects.length == 0) {
            subjects = list;
            total = 100;
        }

        this.applyDelta(delta, subjects);
        this.roundOff(subjects, total);
    }

    public even(list: Variation[]): void {

        let subjects: Variation[] = [],
            amount: number = 100;

        for (let i = 0; i < list.length; i++) {
            let entry = list[i];
            if (entry.weightLocked) {
                amount -= entry.weight;
                continue;
            }
            subjects.push(entry);
        }

        if (subjects.length == 0) {
            //return console.error('No subjects to distribute weights among');
            return;
        }

        let share = amount / subjects.length;

        for (let i = 0; i < subjects.length; i++) {
            subjects[i].weight = share;
        }

        this.roundOff(subjects, amount);
    }

    public normalize(list: Variation[], exclude?: Variation): void {
        let subjects: Variation[] = [],
            total: number = 100;

        list.forEach((entry: Variation) => {
            if ((exclude == entry) || entry.weightLocked) {
                total -= entry.weight;
            } else {
                subjects.push(entry);
            }
        });

        if (subjects.length) {
            this.roundOff(subjects, total);
        }
    }

    private applyDelta(delta: number, subjects: Variation[]): void {

        let overflow = 0,
            splice: number[] = [],
            share = delta / subjects.length;

        for (let i = 0; i < subjects.length; i++) {

            let entry: Variation = subjects[i],
                value = entry.weight - share;

            if (delta > 0 && value < 1) {
                overflow += (-1 * value + 1);
                value = 1;
                splice.push(i);
            }

            entry.weight = value;
        }

        if (overflow > 0) {
            splice.map(
                n => subjects.splice(n, 1)
            );
            this.applyDelta(overflow, subjects);
        }
    }

    private roundOff(subjects: Variation[], total: number = 100): void {
        let rem = 0;
        subjects.forEach((entry: Variation) => {
            if (rem > 0) {
                entry.weight -= 1 - rem;
            }
            rem = entry.weight % 1;
            entry.weight = Math.ceil(entry.weight);
            total -= entry.weight;
            if (entry.weight == -1) {
                entry.weight = 0;
                total -= 1;
            }
        });
        if (total < 0) {
            subjects[subjects.length - 1].weight += total;
        }
    }

    saveState(variation: Variation) {
        let states = VariationWeightsService.states,
            state = 0x0;

        if (variation.weightLocked) {
            state = state | states.LOCKED;
        }
        if (variation.weightDisabled) {
            state = state | states.DISABLED;
        }
        variation.state = state;
    }

    restoreState(variation: Variation) {
        let states = VariationWeightsService.states,
            state = variation.state;

        if (state & states.LOCKED) {
            variation.weightLocked = true;
        }
        if (state & states.DISABLED) {
            variation.weightDisabled = true;
        }
    }

    getSubjects(value: number, variation: Variation, list: Variation[]): Variation[] {

        let subjects: Variation[] = [],
            delta: number = variation.weight - value;

        if (delta == 0) {
            throw new Error('Variation weight delta cannot be zero');
        }

        for (let i = 0; i < list.length; i++) {

            let entry: Variation = list[i];

            if (entry.id == variation.id
                || entry.weightLocked
                || (delta > 1 && entry.weight == 1)) {

                continue;
            }

            subjects.push(entry);
        }

        return subjects;
    }
}
