import {Injectable} from 'angular2/core';

//import {ExperimentsService} from "./em/experiments.service";
import {AuthService} from "./auth";

import {Experiment} from "./model/experiment";
import {ExperimentUrl} from "./model/experiment-url";
import {Variation} from "./model/variation";
import {VariationSnippet} from "./model/variation-snippet";
import {SnippetValue} from "./model/snippet-value";


@Injectable()
export class AbExperimentFactoryService {

    constructor(/*private data: ExperimentsService,*/ private auth: AuthService) {
    }

    public createExperiment(): Experiment {
        return new Experiment();
    }

    public createExperimentUrl(experiment: Experiment): ExperimentUrl {
        return new ExperimentUrl();
    }

    public createVariation(experiment: Experiment) {

        return new Variation();
    }

    public createVariationSnippet(experiment: Experiment): VariationSnippet {

        return new VariationSnippet();
    }

    public createSnippetValue(variation: Variation, snippet: VariationSnippet): SnippetValue {

        return new SnippetValue();
    }
}
