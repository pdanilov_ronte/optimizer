export let MESSAGES = {
    'ab_experiments_list.button_new': {
        title: 'Create new A/B test experiment',
        content: `
        New experiment will be created in inactive state.<br>
        To activate and run your experiment you will need to add at least one variation,
        define experiment page URL and experiment goal,
        and then press <b>"Run"</b> button.
        `
    },
    'ab_experiments_list.running_status.running': {
        content: `Experiment is running`
    },
    'ab_experiments_list.running_status.paused': {
        content: `Experiment is paused`
    },
    'ab_experiment_details.add_url_button': {
        title: 'Add experiment URL',
        content: `
        Experiment URL is the page(s) where your experiment will run on. This means that experiment code won't be executed on any other pages.
        You can have as many page URLs as you need.
        `
    },
    'ab_experiment_details.delete_url_button': {
        title: 'Delete this URL',
        content: `
        Removes page(s) at this url from the experiment.
        `
    },
    'ab_experiment_details.cant_delete_last_url': {
        content: `
        You can't delete this URL because at least one must be present to run an experiment.
        `
    },
    'ab_experiment_details.url_input_field': {
        title: 'Experiment URL / URL mask',
        content: `
        You can use wildcard symbol "<samp>*</samp>" in the URL string, which means "any character".<br>
        For example, <samp>http://example.com/category1/*</samp> <br> will match any page under <samp>http://example.com/category1/</samp>.
        `
    },
    'ab_experiment_details.variation_list_title': {
        title: 'List of experiment variations',
        content: `
        Variations are different presentations of some elements on your experiment page.
        Each A/B experiment includes one "control variation" which is simply an unmodified page, and one or more variations
        that present some modifications. Upon finishing experiment you can choose the most effective variation based on the results.
        `
    },
    'ab_experiment_details.create_variation_button': {
        title: 'Add new variation',
        content: `
        You can add as many variation as you see fit, but it is a good practice in general to keep 3-5 variations per experiment including control variation.
        `
    },
    'ab_experiment_details.distribute_weights_button': {
        title: 'Distribute weights evenly',
        content: `
        This will evenly distribute weights between all <em>unlocked</em> variations.
        Locked variations weights remain untouched so if you have, for example, 3 variations with one of them locked at 50%
        the 2 other variations weights will be set to 25%.
        `
    },
    'ab_experiment_details.variation_weight_number': {
        title: 'Variation weight',
        content: `
        Variation weight determines number of times this variation will be shown to experiment page visitors as relative to other variations.

        `
    },
    'ab_experiment_details.weight_lock_button': {
        title: 'Lock variation weight',
        content: `
        When variation weight is locked, it cannot be changed by any means, neither explicitly (using weight handler)
        nor implicitly (e.g. when "distribute weights" button is pressed).
        `
    },
    'ab_experiment_details.change_weight_handler': {
        content: 'Change variation weight'
    },
    'ab_experiment_details.delete_variation_button': {
        title: 'Delete variation',
        content: `
        Remove this variation and collected data from experiment.<br>
        NOTICE: You must have at least one variation aside from control variation to be able to publish your changes.
        `
    },
    'ab_experiment_details.snippet_tab_header': {
        title: 'Variation snippet',
        content: `Variation snippet can be either html code or text.`
    },
    'ab_experiment_details.snippet_tab_header_control': {
        content: `
        For control variation, this snippet defines html code or plain text that will be searched on experiment page
        and replaced with corresponding snippet from effective variation.
        `
    },
    'ab_experiment_details.snippet_tab_header_variation': {
        content: `
        For any variation except control, this snippet defines html code or plain text that will replace corresponding
        snippet from control variation.
        `
    },
    'ab_experiment_details.delete_snippet_button': {
        title: 'Delete snippet',
        content: `
        Delete this variation snippet from all variations including control variation. Note that at least one snippet
        must be present.
        `
    },
    'ab_experiment_details.add_snippet_button': {
        title: 'Add new snippet',
        content: `
        A new snippet will be created for all variations including control variation.
        `
    },
    'ab_experiment_details.cant_delete_last_snippet': {
        content: `
        You can't delete this snippet because at least one must be present to run an experiment.
        `
    },
    'ab_experiment_details.goals_list_title': {
        title: 'Experiment goals',
        content: `
        Experiment goal presents a page that must be reached by a visitor for experiment session to be counted as successful.
        `
    },
    'ab_experiment_details.add_goal_button': {
        content: 'Add new experiment goal'
    },
    'ab_experiment_details.delete_goal_button': {
        title: 'Delete goal',
        content: `
        Delete this goal from experiment. Note that at least one goal must be present.
        `
    },
    'ab_experiment_details.cant_delete_last_goal': {
        content: `
        You can't delete this goal because at least one must be present to run an experiment.
        `
    },
    'ab_experiment_details.run_experiment_button': {
        title: 'Run experiment',
        content: `
        Activate and run the experiment.
        `
    },
    'ab_experiment_details.run_experiment_button.ready': {
        content: 'Note that once experiment is activated, any changes made afterwards must be published explicitly to' +
        ' take effect.'
    },
    'ab_experiment_details.cant_run_experiment.no_variations': {
        content: `
        You can't run this experiment because <strong>you have no variations</strong> present.
        Create at least one variation <em>aside from control variation</em> to be able to activate the experiment.
        `
    },
    'ab_experiment_details.cant_run_experiment.invalid_url': {
        content: `
        You can't run this experiment because <strong>you have no valid experiment URL</strong> present.
        Define valid experiment URL to be able to activate the experiment.
        `
    },
    'ab_experiment_details.cant_run_experiment.invalid_goal': {
        content: `
        You can't run this experiment because <strong>you have no valid experiment goal</strong> present.
        Define valid experiment goal to be able to activate the experiment.
        `
    },
    'ab_experiment_details.delete_experiment_button': {
        title: 'Delete experiment',
        content: `
        Delete this experiment and all its data. Note that once experiment is activated, it cannot be deleted.
        `
    },
    'ab_experiment_details.toggle_experiment_button': {
        content: `
        Pause/Resume experiment
        `
    },
    'ab_experiment_details.pause_experiment_button': {
        content: `
        Pause experiment
        `
    },
    'ab_experiment_details.resume_experiment_button': {
        content: `
        Resume experiment
        `
    },
    'ab_experiment_details.publish_experiment_button': {
        title: 'Publish experiment',
        content: ''
    },
    'ab_experiment_details.publish_experiment_button.ready': {
        content: `
        Once you published the experiment, any changes made will take effect on target pages.
        `
    },
    'ab_experiment_details.cant_publish_experiment.no_variations': {
        content: `
        You can't publish this experiment because <strong>you have no variations</strong>.
        Please, create at least one variation <em>aside from control variation</em>.
        `
    },
    'ab_experiment_details.cant_publish_experiment.invalid_url': {
        content: `
        You can't publish this experiment because <strong>you have no valid experiment URL</strong>.
        Please, define a valid experiment URL.
        `
    },
    'ab_experiment_details.cant_publish_experiment.invalid_goal': {
        content: `
        You can't publish this experiment because <strong>you have no valid experiment goal</strong>.
        Please, define a valid experiment goal.
        `
    },
    'ab_experiment_details.js_code_snippet_title': {
        content: `
        Insert this code snippet to all of your experiment pages. You can safely insert it to all pages on your site
        &nbsp;&mdash;&nbsp;the experiment will only run on pages defined by experiment URL field(s).<br>
        If you are unsure where to insert this code, a good practice is to put it closer to the top of a page,
        &nbsp;inside of "header" tag or right after opening "body" tag.
        `
    },
    'dashboard.ab_experiments_table.th.views': {
        title: 'Experiment page views',
        content: `
        This value shows the number of times when any of experiment pages has been viewed.<br>
        The number in brackets shows <b>unique&nbsp;views</b> only.
        `
    },
    'dashboard.ab_experiments_table.th.goals': {
        title: 'Experiment goals reached',
        content: `
        This value shows the number of times when any of experiment goals has been reached.<br>
        For destination goal, only first hit is counted.
        `
    },
    'dashboard.ab_experiments_table.th.conversion': {
        title: 'Experiment conversion',
        content: `
        This value shows <em>conversion ratio</em> for the experiment.<br>
        Ratio is calculated as <span style="white-space:nowrap">[ <em>(unique views) / (goals reached)</em> ]</span>.
        `
    },
    'dashboard.ab_experiments_table.th.significance': {
        title: 'Experiment significance',
        content: `
        This value shows <em>statistical&nbsp;significance</em> for the experiment.<br>
        The higher significance means higher probability to determine a winning variation.
        `
    }
};
