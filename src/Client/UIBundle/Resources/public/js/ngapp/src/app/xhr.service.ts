import {Injectable} from 'angular2/core';
import {Http, Headers, RequestOptionsArgs, Response} from 'angular2/http';


@Injectable()
export class XhrService {

    constructor(private http: Http) {
    }

    public get(url: string, search?: any): Promise<Response> {

        let args: RequestOptionsArgs = {};
        args.headers = new Headers({
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json'
        });

        if (typeof search == 'object') {
            let plain = '';
            for (let prop in search) {
                if (search.hasOwnProperty(prop)) {
                    plain += (plain.length ? '&' : '') + prop + '=' + search[prop];
                }
            }
            args.search = plain;
        }

        let request = this.http.get(url, args);

        return new Promise((resolve, reject) => {
            request.subscribe(
                response => {
                    resolve(response);
                },
                error => {
                    reject(error.json());
                }
            );
        });
    }

    public post(url: string, data: any): Promise<Response> {

        let headers = new Headers({
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json'
        });

        let json = JSON.stringify(data);

        let options = <RequestOptionsArgs> {
            headers: headers
        };

        let request = this.http.post(url, json, options);

        return new Promise<Response>((resolve, reject) => {
            request.subscribe(
                response => {
                    resolve(response);
                },
                error => {
                    reject(error.json());
                }
            );
        });
    }
}
