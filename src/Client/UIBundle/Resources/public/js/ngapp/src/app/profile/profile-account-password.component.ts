import {Component} from 'angular2/core';

import {AuthService} from "../auth";
import {NotificationService} from "../notification.service";
import {UiLaddaDirective} from "../ui-ladda.directive";


@Component({
    selector: 'profile-account-password',
    templateUrl: 'templates/profile/profile-account-password.html',
    directives: [
        UiLaddaDirective
    ]
})
export class ProfileAccountPasswordComponent {

    password: {
        current: string;
        new: string;
        confirm: string;
    } = {
        current: '',
        new: '',
        confirm: ''
    };

    formErrors = {};

    active = true;

    updating:boolean = false;

    constructor(private auth: AuthService, private notifications: NotificationService) {
    }

    get valid(): boolean {
        return !(
            this.formErrors['current'] ||
            this.formErrors['new'] ||
            this.formErrors['confirm']
        );
    }

    refresh() {
        this.validate();
    }

    validate() {
        this.formErrors = {};

        if (!this.password.current.length) {
            this.formErrors['current'] = true;
        }

        if (!this.password.new.length) {
            this.formErrors['new'] = true;
        }

        if (this.password.confirm != this.password.new) {
            this.formErrors['confirm'] = true;
        }

        // @todo check that new password is not the same as current
    }

    update() {

        if (!this.valid) {
            return;
        }

        this.updating = true;

        this.auth.changePassword(this.password)
            .then((result) => {
                // @todo show success message
                this.reset();
                this.updating = false;
                this.notifications.success('password successfully updated.');
            })
            .catch((error) => {

                // @todo show error message
                if (error['field'] == 'current') {
                    this.formErrors['current'] = true;
                    this.notifications.error('incorrect current password');
                } else {
                    this.notifications.error('failed to change password');
                    console.error(error);
                }
                this.updating = false;
            });
    }

    reset() {
        this.formErrors = {};
        this.password = {
            current: '',
            new: '',
            confirm: ''
        };
        this.active = false;
        setTimeout(() => this.active = true, 0);
    }
}
