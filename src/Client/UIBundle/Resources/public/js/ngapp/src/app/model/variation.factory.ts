import {AbstractEntityFactory} from '../em/entity-factory';

import {Variation} from './variation';


//noinspection JSUnusedGlobalSymbols
export class VariationFactory extends AbstractEntityFactory<Variation> {

    create(complete: boolean = false, params?: any): Promise<Variation> {
        let variation = new Variation();

        for (let prop in params) {
            if (params.hasOwnProperty(prop)) {
                variation[prop] = params[prop];
            }
        }

        return new Promise((resolve, reject) => {
            this.em.persist()
                .then(() => {
                    resolve(variation);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    fromIElement(element: restangular.IElement): Variation {
        let variation = new Variation();

        variation.number = element['number'];
        variation.title = element['title'];
        variation.weight = element['weight'];
        variation.state = element['state'];

        variation.weightLocked = (variation.state & Variation.states.WEIGHT_LOCKED) != 0;

        variation.em = this.em;
        variation.ielement = element;

        return variation;
    }
}
