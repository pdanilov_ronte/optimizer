import {ModelEntity} from '../persistance/ModelEntity';
import {ElementDAO} from '../persistance/ElementDAO';
import {DTO} from '../persistance/DTO';
import {GoalDestinationCollection} from "./GoalDestinationCollection";


export class Goal extends ModelEntity {

    id: number;
    title: string;

    destinations: GoalDestinationCollection;

    protected build(dao: ElementDAO): void {
        // this.destinations = new GoalDestinationCollection(dao.collection('destinations'));
        this.destinations = new GoalDestinationCollection();
        this.destinations.initialize(dao.collection('destinations'));
        this.restore(dao.dto);
    }

    public restore(dto: DTO): void {
        let data = dto.data();
        this.id = data['id'];
        this.destinations.restore(new DTO([data['destination']]));
    }

    public serialize(): DTO {
        return new DTO({
            id: this.id,
            title: this.title
        });
    }
}
