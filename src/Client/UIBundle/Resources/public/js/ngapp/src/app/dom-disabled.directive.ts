import {Directive, ElementRef, Input, OnInit} from 'angular2/core';


@Directive({
    selector: '[domDisabled]'
})
export class DomDisabledDirective implements OnInit {

    @Input('domDisabled')
    expression: boolean = false;

    constructor(private el: ElementRef) {
    }

    ngOnInit(): any {
        this.el.nativeElement.disabled = this.expression;
    }
}
