import {Injectable} from 'angular2/core';

import {NotificationMessage} from "./notification-message";
import {NotificationSubscriberInterface} from "./notification-subscriber";


@Injectable()
export class NotificationService {

    private _subscribers:NotificationSubscriberInterface[] = [];

    public subscribe(subscriber:NotificationSubscriberInterface):void {
        this._subscribers.push(subscriber);
    }

    message(message:NotificationMessage):void {
        this._subscribers.forEach((subscriber:NotificationSubscriberInterface) => {
            subscriber.next(message);
        });
    }

    success(content:string):void {
        this.message(NotificationService.factory(content, NotificationMessage.SUCCESS));
    }

    error(content:string):void {
        this.message(NotificationService.factory(content, NotificationMessage.ERROR));
    }

    notice(content:string):void {
        this.message(NotificationService.factory(content, NotificationMessage.NOTICE));
    }

    warning(content:string):void {
        this.message(NotificationService.factory(content, NotificationMessage.WARNING));
    }

    static factory(content:string, type?:number):NotificationMessage {
        return new NotificationMessage(content, type);
    }
}
