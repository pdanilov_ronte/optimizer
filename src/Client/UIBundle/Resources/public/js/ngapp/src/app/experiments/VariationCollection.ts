import {EntityCollection} from '../persistance/EntityCollection';
import {DTO} from '../persistance/DTO';
import {Variation} from "./Variation";


export class VariationCollection extends EntityCollection<Variation> {
    restoreItem(dto: DTO): Variation {
        return new Variation(this.dao.create(dto));
    }
}
