import {Component, Input} from 'angular2/core';

import {ExperimentVariationComponent} from "./experiment-variation.component";
import {VariationWeightsService} from "./variation-weights.service";

import {Experiment} from "../model/experiment";
import {Variation} from "../model/variation";
import {VariationSnippetsService} from "./variation-snippets.service";
import {ExperimentsService} from "../em/experiments.service";
import {AuthService} from "../auth";
import {SnippetValue} from "../model/snippet-value";
import {UiPopoverComponent} from "../ui-popover.component";
import {AbExperimentFactoryService} from "../ab-experiment-factory.service";
import {DataService} from '../em/data.service';
import {NotificationService} from '../notification.service';


@Component({
    selector: 'experiment-variation-list',
    templateUrl: 'templates/ab-experiment-details/experiment-variation-list.html',
    directives: [
        ExperimentVariationComponent,
        UiPopoverComponent
    ],
    providers: [
        AbExperimentFactoryService,
        VariationWeightsService,
        VariationSnippetsService
    ]
})
export class ExperimentVariationListComponent {

    @Input()
    experiment: Experiment;

    private _variationComponents: ExperimentVariationComponent[] = [];

    constructor(private data: DataService,
                private factory: AbExperimentFactoryService,
                private weights: VariationWeightsService,
                private snippets: VariationSnippetsService,
                private auth: AuthService,
                private notifications: NotificationService) {
    }

    registerVariationComponent(component: ExperimentVariationComponent) {
        this._variationComponents.push(component);
    }

    onVariationOpen(component: ExperimentVariationComponent) {
        this._variationComponents.forEach((entry: ExperimentVariationComponent) => {
            if (entry != component) {
                entry.close();
            }
        })
    }

    get variations(): Variation[] {
        return this.experiment.variations.items;
    }

    onChangeWeight(event: any) {
        let value: number = event.so.value,
            variation: Variation = event.so.variation;

        event.preventDefault();
        event.stopPropagation();
    }

    afterWeightChanged(event: any) {
        this.weights.normalize(this.variations, event.so.variation);
        this.data.persist();
    }

    evenWeights() {
        this.weights.even(this.variations);
        this.data.persist();
    }

    create() {
        this.data.create<Variation>(Variation, this.experiment, {number: this.experiment.variations.length})
            .then((variation) => {
                variation.experiment = this.experiment;
                variation.experiment.snippets.forEach((snippet) => {
                    this.data.create<SnippetValue>(SnippetValue, snippet, {variation: variation.number})
                        .then((value) => {
                            variation.snippetValues.items[snippet.number] = value;
                            if (snippet.number == this.snippets.active) {
                                this.snippets.activate(snippet.number, null, variation);
                            }
                        })
                        .catch(error => {
                            this.notifications.error('Error occurred while creating experiment variation. Check error console for details.');
                            console.error(error);
                        });
                });
                variation.experiment.variations.push(variation);
            })
            .catch(error => {
                this.notifications.error('Error occurred while creating experiment variation. Check error console for details.');
            });
    }

    //noinspection ReservedWordAsName
    delete(variation: Variation) {

        if (variation.number == 0) {
            return this.notifications.error('Cannot delete control variation!');
        }

        this.data.remove(variation)
            .then(() => {
                let index = this.experiment.variations.indexOf(variation);
                this.experiment.variations.remove(variation);
                this.weights.refill(this.experiment.variations.items);
                for (let i = index, len = this.experiment.variations.items.length; i < len; i++) {
                    this.experiment.variations.items[i].number = i;
                }
                this.data.persist();
            })
            .catch(error => {
                this.notifications.error('Error has occurred while removing the experiment variation. Check error console for details.');
            });
    }
}
