import Type = angular.Type;

export function makeTypeDecorator(annotationCls: any): any {
    function TypeDecoratorFactory(objOrType: any): any {
        var annotationInstance: DecoratorMetadata<any> = new annotationCls(objOrType);
        if (this instanceof annotationCls) {
            return annotationInstance;
        } else {
            var chainAnnotation = this.annotations instanceof Array ? this.annotations : [];
            chainAnnotation.push(annotationInstance);
            var TypeDecorator = function TypeDecorator(cls: any) {
                var annotations = Reflect['getOwnMetadata']('annotations', cls);
                annotations = annotations || [];
                annotations.push(annotationInstance);
                Reflect['defineMetadata']('annotations', annotations, cls);
                annotationInstance.decorateTarget(cls, null);
                return cls;
            };
            TypeDecorator['annotations'] = chainAnnotation;
            return TypeDecorator;
        }
    }

    TypeDecoratorFactory.prototype = Object.create(annotationCls.prototype);
    return TypeDecoratorFactory;
}

export function makePropDecorator(decoratorCls: Type): any {

    function PropDecoratorFactory(...args: any[]): any {
        var decoratorInstance: DecoratorMetadata<any> = Object.create(decoratorCls.prototype);

        if (this instanceof decoratorCls) {
            return decoratorInstance;
        } else {
            return function PropDecorator(target: any, name: string) {
                var meta = Reflect['getOwnMetadata']('propMetadata', target.constructor);
                meta = meta || {};
                meta[name] = meta[name] || [];
                meta[name].unshift(decoratorInstance);
                Reflect['defineMetadata']('propMetadata', meta, target.constructor);
                decoratorInstance.decorateTarget(target.constructor, name, args);
            };
        }
    }

    PropDecoratorFactory.prototype = Object.create(decoratorCls.prototype);
    return PropDecoratorFactory;
}

/**
 * Metadata
 */
interface DecoratorMetadataInterface<T> {
    decorateTarget(target: any, name: string, args?: any): void;
}

export abstract class DecoratorMetadata<T> implements DecoratorMetadataInterface<T> {
    abstract decorateTarget(target: any, name: string, args?: any): void;
}

export class TypeDecoratorMetadata<T> extends DecoratorMetadata<T> {

    decorateTarget(target: T): void {
    }
}


/**
 * Decorator
 */
export interface Decorator {
    decorate(target: any): void;
}

export abstract class AbstractDecorator implements Decorator {
    private metadata: DecoratorMetadata<any>;

    abstract decorate(target: any): void;
}

export class TypeDecorator extends AbstractDecorator {

    decorate(target: any): void {
    }
}


/**
 * Factory
 */
export abstract class DecoratorFactory {
    static build(type: typeof DecoratorMetadata): TypeDecorator {
        let factory = new TypeDecoratorFactory();
        let metadata = new TypeDecoratorMetadata();
        let o_metadata = Object.create(type);
        return factory.build(metadata);
    }
}

interface DecoratorFactoryInterface {
    build(metadata: DecoratorMetadata<any>): Decorator;
}

export class TypeDecoratorFactory implements DecoratorFactoryInterface {
    build(metadata: DecoratorMetadata<any>): TypeDecorator {
        return new TypeDecorator();
    }
}