import {DecoratorMetadata, makeTypeDecorator} from '../decorator';
import {ModelEntity} from './entity';


export class EntityExpandable<T extends ModelEntity> {

    constructor() {
    }
}

class ExpandableFactory {
    static create(meta: ExpandableMetadata): EntityExpandable<ModelEntity> {
        return new EntityExpandable();
    }
}

export class ExpandableMetadata implements DecoratorMetadata<ModelEntity> {

    public targetClass: typeof ModelEntity;

    public expandable: EntityExpandable<ModelEntity>;


    constructor() {
    }

    decorateTarget(targetClass: typeof ModelEntity): void {
        this.targetClass = targetClass;
        this.expandable = ExpandableFactory.create(this);
    }
}

export interface ExpandableFactoryDecorator {
    (): any;
    new (): any;
}

/**
 * @deprecated
 */
export let Expandable: ExpandableFactoryDecorator = makeTypeDecorator(ExpandableMetadata);
