import {VariationSnippetsComponent} from "./variation-snippets.component";
import {Variation} from "../model/variation";


export class VariationSnippetsService {

    private _components: VariationSnippetsComponent[] = [];

    private _active: number = 0;

    addComponent(component: VariationSnippetsComponent) {
        this._components.push(component);
    }

    get active(): number {
        return this._active;
    }

    activate(index: number, exclude?: Variation, include?: Variation) {
        for (let i = 0; i < this._components.length; i++) {
            let entry = this._components[i];
            if (exclude && entry.variation.number == exclude.number) {
                continue;
            } else if (include && entry.variation.number != include.number) {
                continue;
            }
            entry.doActivate(index);
            this._active = index;
        }
    }
}
