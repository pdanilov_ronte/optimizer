import {ModelEntity} from '../persistance/ModelEntity';
import {ElementDAO} from '../persistance/ElementDAO';
import {DTO} from '../persistance/DTO';


export class ExperimentURL extends ModelEntity {

    id: number;
    pattern: string;

    protected build(dao: ElementDAO): void {
        this.restore(dao.dto);
    }

    restore(dto: DTO): void {
        let data = dto.data();
        this.id = data['id'];
        this.pattern = data['pattern'];
    }

    serialize(): DTO {
        return new DTO({
            id: this.id,
            pattern: this.pattern
        });
    }
}
