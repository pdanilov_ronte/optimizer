import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';
import {Http, Headers, Request, RequestMethod, RequestOptionsArgs, Response} from 'angular2/http';

import {ConfigService} from "./config.service";
import {XhrService} from './xhr.service';


@Injectable()
export class AuthService {

    private _account: any = {};

    constructor(private http: Http, private config: ConfigService, private xhr: XhrService) {
        this.xhr.get('/service/-/auth/account')
            .then(result => {
                let data = result.json();
                this._account = data['account'];
            })
            .catch(error => console.error(error));
    }

    get account(): any {
        return this._account;
    }

    public isLoggedIn(): boolean {
        return Boolean(parseInt(localStorage.getItem('so.auth.is_logged_in')));
    }

    public login(username: string, password: string) {

        let headers = new Headers({
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        });

        let body = '_username=' + username + '&_password=' + password;

        let json = JSON.stringify({
            _username: username,
            _password: password
        });

        let options = <RequestOptionsArgs> {
            method: RequestMethod.Get,
            headers: headers,
            search: '_username=' + username + '&_password=' + password
        };

        let request = new Request({
            method: RequestMethod.Get,
            url: this.config._.auth.login_check,
            headers: headers,
            search: '_username=' + username + '&_password=' + password
        });

        let post = this.http.get(this.config._.auth.login_check,
            {search: '_username=' + username + '&_password=' + password});

        let $this = this;

        return new Promise<Response>(function (resolve, reject) {
            post
            //.map(res => res.json())
                .subscribe(
                    res => {
                        let json = res.json();
                        $this._account = json.user;
                        localStorage.setItem('so.auth.logged', '1');
                        localStorage.setItem('so.auth.account', JSON.stringify(json.user));
                        resolve(res);
                        window.location.href = '/';
                    },
                    err => {
                        let json = err.json();
                        reject(json.error);
                    },
                    () => {
                    }
                );
        });
    }

    public logout() {
        localStorage.clear();
        window.location.href = '/logout';
    }

    public updateAccount(data: any): Promise<any> {

        let account = {
            name: '',
            email: ''
        };

        let error: string;

        for (let prop in account) {
            if (!this._account.hasOwnProperty(prop)) {
                error = sprintf('Account has no property %s', prop);
                console.error(error);
                return new Promise((resolve, reject) => {
                    reject(error);
                });
            }
            account[prop] = data[prop];
        }

        //account['_token'] = this.config._.app.csrf.fos_user_profile_form;

        let headers = new Headers({
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json'
        });

        let json = JSON.stringify(account);

        let options = <RequestOptionsArgs> {
            headers: headers
        };

        let request = this.http.post(
            this.config._.auth.account_update,
            json,
            options
        );

        return new Promise<Response>((resolve, reject) => {
            request
            //.map(res => res.json())
                .subscribe(
                    res => {
                        let json = res.json();
                        this._account = json.account;
                        localStorage.setItem('so.auth.account', JSON.stringify(json.account));
                        resolve(json);
                    },
                    err => {
                        reject(err.json().error);
                    },
                    () => {
                    }
                );
        });
    }

    public changePassword(data: any): Promise<any> {

        let password = {
            current: '',
            new: '',
            confirm: ''
        };

        let error: string;

        for (let prop in password) {
            if (!data.hasOwnProperty(prop)) {
                error = sprintf('Data has no property %s', prop);
                console.error(error);
                return new Promise((resolve, reject) => {
                    reject(error);
                });
            }
            password[prop] = data[prop];
        }

        let headers = new Headers({
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json'
        });

        let json = JSON.stringify(password);

        let options = <RequestOptionsArgs> {
            headers: headers
        };

        let request = this.http.post(
            this.config._.auth.change_password,
            json,
            options
        );

        return new Promise<Response>((resolve, reject) => {
            request
            //.map(res => res.json())
                .subscribe(
                    res => {
                        let json = res.json();
                        //this._account = json.account;
                        //localStorage.setItem('so.auth.account', JSON.stringify(json.account));
                        resolve(json);
                    },
                    err => {
                        reject(err.json());
                    },
                    () => {
                    }
                );
        });
    }
}
