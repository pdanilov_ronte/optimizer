import {IndexedCollection} from '../common/IndexedCollection';
import {ModelEntity} from './ModelEntity';
import {CollectionDAO} from './CollectionDAO';
import {DTO} from './DTO';
import {SerializableCollection} from './SerializableCollection';


export abstract class EntityCollection<T extends ModelEntity> extends IndexedCollection<T> implements SerializableCollection<T> {

    private _dao: CollectionDAO;

    constructor(dao?: CollectionDAO) {
        super();
        if (dao) {
            this.initialize(dao);
        }
    }

    protected get dao(): CollectionDAO {
        return this._dao;
    }

    public initialize(dao: CollectionDAO): void {
        if (this._dao) {
            throw new Error('Collection already initialized');
        }
        this._dao = dao;
    }

    public restore(dto: DTO): void {
        for (let item of dto.data()) {
            this.push(this.restoreItem(new DTO(item)));
        }
    }

    public abstract restoreItem(dto: DTO): T;

    public serialize(): DTO {
        let data: any[] = [];
        for (let item of this.all()) {
            data.push(item.serialize().data());
        }
        return new DTO(data);
    }
}
