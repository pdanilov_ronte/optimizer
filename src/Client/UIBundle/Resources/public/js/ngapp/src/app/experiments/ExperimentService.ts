import {Injectable, Inject} from 'angular2/core';

import {EntityManager} from '../persistance/EntityManager';
import {ExperimentRepository} from './ExperimentRepository';
import {ExperimentFactory} from './ExperimentFactory';
import {Experiment} from './Experiment';
import {RootDAO} from '../persistance/RootDAO';
import {RestangularBackend} from '../persistance/RestangularBackend';


@Injectable()
export class ExperimentService {

    private _backend: RestangularBackend;
    private _em: EntityManager;
    private _factory: ExperimentFactory;
    private _repository: ExperimentRepository;

    constructor(@Inject(RestangularBackend) backend: RestangularBackend,
                @Inject(EntityManager) em: EntityManager) {
        this._backend = backend;
        this._em = em;
        let dao = new RootDAO(this._backend.service, 'experiments');
        this._repository = new ExperimentRepository(dao);
        this._factory = new ExperimentFactory();
    }

    public create(data?: any): Experiment {
        let entity = this._factory.create(data);
        entity.attach(this._repository);
        return entity;
    }

    get list(): Experiment[] {
        return this._repository.all();
    }
}
