import {ModelEntity} from './entity';

export class ChangeTracker {

    // @todo
    private _map = new Map<ModelEntity, Map<string, any>>();

    private _queue: ModelEntity[] = [];

    get queue(): ModelEntity[] {
        return this._queue;
    }

    track(entity: ModelEntity): number {
        // @todo track
        let index = this._queue.indexOf(entity);
        if (index !== -1) {
            // @todo necessary to update?
            this._queue[index] = entity;
            return index;
        }
        return this._queue.push(entity);
    }

    shift(): ModelEntity {
        return this._queue.shift();
    }

    contains(entity: ModelEntity): boolean {
        return this._queue.indexOf(entity) !== -1;
    }
}
