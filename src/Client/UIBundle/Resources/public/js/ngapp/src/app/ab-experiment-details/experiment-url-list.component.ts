import {Component, Input, Output} from 'angular2/core';

import {ExperimentUrlComponent} from "./experiment-url.component";
import {ExperimentsService} from "../em/experiments.service";
import {AuthService} from "../auth";

import {Experiment} from "../model/experiment";
import {ExperimentUrl} from "../model/experiment-url";
import {UiPopoverComponent} from "../ui-popover.component";
import {DataService} from '../em/data.service';
import {NotificationService} from '../notification.service';


@Component({
    selector: 'experiment-url-list',
    templateUrl: 'templates/ab-experiment-details/experiment-url-list.html',
    directives: [
        ExperimentUrlComponent,
        UiPopoverComponent
    ]
})
export class ExperimentUrlListComponent {

    @Input()
    experiment: Experiment;

    constructor(private data: DataService,
                private auth: AuthService,
                private notification: NotificationService) {
    }

    get urls(): ExperimentUrl[] {
        return this.experiment.urls.items;
    }

    create() {

        this.data.create<ExperimentUrl>(ExperimentUrl, this.experiment)
            .then(url => {
                this.experiment.urls.push(url);
            })
            .catch(error => {
                this.notification.error('Failed to add experiment URL. Check error console for details.')
            });
    }

    //noinspection ReservedWordAsName
    delete(url: ExperimentUrl) {

        this.data.remove(url)
            .then(() => {
                this.experiment.urls.remove(url);
            })
            .catch(error => {
                this.notification.error('Error has occurred while removing the experiment URL. Check error console for details.');
            });
    }
}
