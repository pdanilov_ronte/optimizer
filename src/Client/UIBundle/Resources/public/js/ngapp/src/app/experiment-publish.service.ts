import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';

import {Experiment} from "./model/experiment";


@Injectable()
export class ExperimentPublishService {

    private _publishing:boolean = false;


    constructor(private http:Http) {
    }

    get publishing():boolean {
        return this._publishing;
    }

    public publish(experiment:Experiment):Promise<Response> {

        this._publishing = true;

        let req = this.http.get('/cache/experiment/publish/' + experiment.id);

        return new Promise<Response>((resolve, reject) => {
            req.subscribe(
                res => {
                    experiment.pristine = true;
                    resolve(res);
                },
                err => {
                    console.error(err);
                    reject(err);
                },
                () => {
                    this._publishing = false;
                }
            );
        });
    }

    public errors(experiment:Experiment):string[] {

        let errors:string[] = [];

        if (experiment.variations.length == 1) {
            errors.push('no_variations');
        }

        if (!experiment.urls) {
            errors.push('invalid_url');
        }

        let validUrl:boolean;

        experiment.urls.forEach(url => {
            if (url.pattern) {
                validUrl = true;
            }
        });

        if (!validUrl) {
            errors.push('invalid_url');
        }

        if (!experiment.goals) {
            errors.push('invalid_goal');
        }

        let validGoal:boolean;

        experiment.goals.forEach(goal => {
            goal.destinations.forEach(destination => {
                if (destination.value) {
                    validGoal = true;
                }
            });
        });

        if (!validGoal) {
            errors.push('invalid_goal');
        }

        return errors;
    }
}
