import {NotificationMessage} from "./notification-message";

export interface NotificationSubscriberInterface {
    next(message:NotificationMessage): void;
}
