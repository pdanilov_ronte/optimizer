export class NotificationMessage {

    static SUCCESS:number = 0;
    static INFO:number = 1;
    static NOTICE:number = 2;
    static WARNING:number = 3;
    static ERROR:number = 4;

    constructor(public content?:string, public type:number = NotificationMessage.NOTICE) {
    }
}