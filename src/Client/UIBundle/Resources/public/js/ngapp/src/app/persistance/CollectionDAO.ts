import IService = restangular.IService;
import ICollection = restangular.ICollection;

import {ElementDAO} from './ElementDAO';
import {DTO} from './DTO';


export class CollectionDAO {

    private _service: IService;
    private _collection: ICollection;
    private _path: string;

    constructor(service: IService, collection: ICollection, path: string) {
        this._service = service;
        this._collection = collection;
        this._path = path;
    }

    public getService(): IService {
        return this._service;
    }

    public all(): Promise<ElementDAO[]> {
        return new Promise((resolve, reject) => {
            let element = this._service.all(this._path);
            element.getList()
                .then((result: ICollection) => {
                    let list: ElementDAO[] = [];
                    for (let item of result) {
                        list.push(new ElementDAO(this._service, item));
                    }
                    resolve(list);
                })
                .catch((error: any) => reject(error));
        });
    }

    public create(dto?: DTO): ElementDAO {
        let data = dto ? dto.data() : {};
        let element = this._service.restangularizeElement(this._service, data, '999');
        return new ElementDAO(this._service, element);
    }
}
