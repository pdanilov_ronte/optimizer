import {Directive, Input, ElementRef, OnInit} from 'angular2/core';


@Directive({
    selector: '[ladda]'
})
export class UiLaddaDirective implements OnInit {

    private ladda:ILaddaButton;

    constructor(private elementRef:ElementRef) {
    }

    @Input('ladda')
    set property(value:boolean) {
        if (value == true) {
            this.start();
        } else {
            this.stop();
        }
    }

    public start():void {
        if (this.ladda != undefined) {
            jQuery(this.elementRef.nativeElement)
                .css({'pointer-events': 'none'});
            this.ladda.start();
        }
    }

    public stop():void {
        if (this.ladda != undefined) {
            jQuery(this.elementRef.nativeElement)
                .css({'pointer-events': 'auto'});
            this.ladda.stop();
        }
    }

    ngOnInit():any {

        jQuery(this.elementRef.nativeElement)
            .addClass('ladda-button')
            .attr('data-style', 'slide-right');

        this.ladda = Ladda.create(this.elementRef.nativeElement);
    }
}
