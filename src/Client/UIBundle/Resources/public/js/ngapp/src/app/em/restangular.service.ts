import {Injectable} from 'angular2/core';
import {Http} from 'angular2/http';

import IService = restangular.IService;
import IProvider = restangular.IProvider;
import ICollectionPromise = restangular.ICollectionPromise;
import ICollection = restangular.ICollection;
import IElement = restangular.IElement;
import {ModelEntity} from './entity';


@Injectable()
export class RestangularService {

    private service: IService;

    constructor() {

        let $http = window['legacyProviders']['http'];

        this.service = window['restangularConstructor']($http, Q);
        this.service.setBaseUrl('/api/-/rest/v1');
    }

    all(route: string, base?: IElement): Promise<ICollection> {
        //let path = name + 's';
        let element = base ? base.all(route) : this.service.all(route);
        return new Promise((resolve, reject) => {
            element.getList()
                .then((result: ICollection) => {
                    resolve(result);
                })
                .catch((error: any) => reject(error));
        });
    }

    //noinspection JSMethodCanBeStatic
    save(entity: IElement): Promise<IElement> {
        return new Promise((resolve, reject) => {
            entity.save()
                .then(element => resolve(element))
                .catch(error => reject(error));
        })
    }

    //noinspection JSMethodCanBeStatic
    remove(entity: IElement): Promise<IElement> {
        return new Promise((resolve, reject) => {
            entity.remove()
                .then(element => resolve(element))
                .catch(error => reject(error));
        });
    }
}

export interface IPost {
    post<T extends ModelEntity>(entity: T): Promise<T>;
}

export class IPostElement implements IPost {

    constructor(private element: IElement) {
    }

    post<T extends ModelEntity>(entity: T): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            this.element.post<IElement>(entity.route, entity.plain())
                .then((element) => {
                    element['parentResource'] = this.element;
                    entity.ielement = element;
                    resolve(entity);
                })
                .catch(error => reject(error));
        })
    }
}

export class IPostCollection implements IPost {

    constructor(private collection: ICollection) {
    }

    post<T extends ModelEntity>(entity: T): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            return this.collection.post(entity.plain())
                .then((element) => {
                    entity.ielement = element;
                    resolve(entity);
                })
                .catch(error => reject(error));
        })
    }
}
