import {Experiment} from './Experiment';
import {ElementDAO} from '../persistance/ElementDAO';
import {DomainRepository} from '../persistance/DomainRepository';
import {RootDAO} from '../persistance/RootDAO';
import {ExperimentCollection} from './ExperimentCollection';


export class ExperimentRepository extends DomainRepository<Experiment> {

    constructor(dao: RootDAO) {
        super(dao, new ExperimentCollection(dao));
    }

    protected buildItem(source: ElementDAO): Experiment {
        return new Experiment(source);
    }
}
