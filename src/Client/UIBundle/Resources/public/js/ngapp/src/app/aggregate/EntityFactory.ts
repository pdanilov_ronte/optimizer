import {ModelEntity} from '../persistance/ModelEntity';
import {Factory} from '../common/Factory';


export abstract class EntityFactory<T extends ModelEntity> extends Factory<T> {

    public create(data?: any): T {
        // @todo data?
        return <T> super.create();
    }
}
