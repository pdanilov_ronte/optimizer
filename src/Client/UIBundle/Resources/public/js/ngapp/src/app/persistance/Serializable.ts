import {DTO} from './DTO';


export interface Serializable {
    restore(dto: DTO): void;
    serialize(): DTO;
}
