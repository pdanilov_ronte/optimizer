import {AbstractEntityFactory} from '../em/entity-factory';
import {Goal} from './goal';


//noinspection JSUnusedGlobalSymbols
export class GoalFactory extends AbstractEntityFactory<Goal> {

    create(complete: boolean = false, params?: any): Promise<Goal> {
        let goal = new Goal();

        return new Promise((resolve, reject) => {
            this.em.persist()
                .then(() => {
                    resolve(goal);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    fromIElement(element: restangular.IElement): Goal {
        let goal = new Goal();

        goal.title = element['title'];

        goal.em = this.em;
        goal.ielement = element;

        return goal;
    }
}
