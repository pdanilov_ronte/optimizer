import {Injectable} from 'angular2/core';

import {MESSAGES} from './app.messages';


@Injectable()
export class MessagesService {

    private _messages: any = {};

    constructor() {
        this._messages = MESSAGES;
    }

    public get _(): any {
        return this._messages;
    }
}
