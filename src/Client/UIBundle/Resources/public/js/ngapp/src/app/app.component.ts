import {Component, OnInit} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';
import {Router, RouteParams} from 'angular2/router';
import {FORM_DIRECTIVES} from 'angular2/common';

import {AuthService} from './auth';
import {LoginComponent} from './login.component';
import {DashboardComponent} from "./dashboard.component";

import {LoginFormComponent} from "./login-form.component";
import {NotificationBarComponent} from "./notification-bar.component";
import {MenuSidebarComponent} from "./menu-sidebar.component";
import {AbExperimentsComponent} from "./ab-experiments.component";

import {AbExperimentsListComponent} from "./ab-experiments-list.component";
import {DetailsSidebarComponent} from "./details-sidebar.component";
import {DetailsSidebarService} from "./details-sidebar.service";
import {MenuTopComponent} from "./menu-top.component";
import {LoginPageComponent} from "./login-page.component";
import {ExperimentsService} from './em/experiments.service';
import {ExperimentStatsService} from "./em/experiment-stats.service";
import {ProfileComponent} from "./profile.component";
import {ConfigService} from "./config.service";
import {MessagesService} from "./messages.service";
import {KeepAliveService} from "./keep-alive.service";
import {NotificationService} from "./notification.service";
import {XhrService} from "./xhr.service";

import {RestangularService} from "./em/restangular.service";
import {DataService} from "./em/data.service";
import {EntityManager} from './em/entity-manager';
import {ExperimentComponent} from './tests/ExperimentComponent';


@Component({
    selector: 'so-app',
    providers: [
        ConfigService,
        XhrService,
        AuthService,
        RestangularService,
        DataService,
        EntityManager,
        KeepAliveService,
        MessagesService,
        NotificationService,
        DetailsSidebarService,
        ExperimentsService,
        ExperimentStatsService
    ],
    templateUrl: 'templates/app.html',
    directives: [
        ROUTER_DIRECTIVES,
        FORM_DIRECTIVES,
        MenuTopComponent,
        MenuSidebarComponent,
        NotificationBarComponent,
        DetailsSidebarComponent,
        LoginPageComponent
    ]
})
@RouteConfig([
    {path: '/dashboard', name: 'Dashboard', component: DashboardComponent, useAsDefault: true},
    {path: '/login', name: 'LoginPage', component: LoginPageComponent},
    {path: '/profile', name: 'ProfilePage', component: ProfileComponent},
    {path: '/experiments/ab/...', name: 'AbExperiments', component: AbExperimentsComponent},
    {path: '/experiments/ab', name: 'AbExperimentsList', component: AbExperimentsListComponent},
    {path: '/test/experiment', name: 'ExperimentTest', component: ExperimentComponent}
])
export class AppComponent implements OnInit {

    isLoggedIn: boolean = false;

    constructor(private auth: AuthService,
                private keepAlive: KeepAliveService,
                private data: DataService,
                private router: Router) {
        //router.navigate(['Dashboard']);
    }

    ngOnInit(): any {
        this.keepAlive.initialize();
    }

    private loadData() {
    }
}
