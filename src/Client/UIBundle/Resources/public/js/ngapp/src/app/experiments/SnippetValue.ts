import {ModelEntity} from '../persistance/ModelEntity';
import {ElementDAO} from '../persistance/ElementDAO';
import {DTO} from '../persistance/DTO';


export class SnippetValue extends ModelEntity {

    id: number;
    value: string;

    protected build(dao: ElementDAO): void {
        this.restore(dao.dto);
    }

    public restore(dto: DTO): void {
        let data = dto.data();
        this.id = data['id'];
        this.value = data['value'];
    }

    public serialize(): DTO {
        return new DTO({
            id: this.id,
            value: this.value
        });
    }
}
