<?php

namespace Client\UIBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ClientUIBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
