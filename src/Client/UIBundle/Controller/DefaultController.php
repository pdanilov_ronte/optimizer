<?php

namespace Client\UIBundle\Controller;

use Base\UsersBundle\Entity\Account;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use /** @noinspection PhpUnusedAliasInspection */
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use /** @noinspection PhpUnusedAliasInspection */
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Csrf\CsrfProvider\CsrfTokenManagerAdapter;
use Symfony\Component\Security\Csrf\CsrfTokenManager;


/**
 * @package Client\UIBundle\Controller
 * @author  PM:/ <pm@spiral.ninja>
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="client_ui_index")
     * @Template()
     */
    public function indexAction()
    {
        /** @type Account $user */
        $user = $this->container->get('security.context')->getToken()->getUser();

        /** @type CsrfTokenManager $provider */
        $provider = $this->get('security.csrf.token_manager');

        return [
            'user' => $user,
            'token' => $provider->getToken('change_password')
        ];
    }

    /**
     * @Route("/{any}", name="client_ui_subroute", requirements={"any" = ".+"})
     * @Template("ClientUIBundle:Default:index.html.twig")
     */
    public function subrouteAction()
    {
        /** @type Account $user */
        $user = $this->container->get('security.context')->getToken()->getUser();

        /** @type CsrfTokenManager $provider */
        $provider = $this->get('security.csrf.token_manager');

        return [
            'user' => $user,
            'token' => $provider->getToken('change_password')
        ];
    }
}
