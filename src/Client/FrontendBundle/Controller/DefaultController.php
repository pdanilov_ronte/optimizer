<?php

namespace Client\FrontendBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;


class DefaultController extends Controller
{
    /**
     * @Route("/{any}", name="client_frontend_index", requirements={"any" = ".*"})
     */
    public function indexAction()
    {
        /** @type UserInterface $account */
        $account = $this->getUser();

        /** @type AuthorizationChecker $checker */
        $checker = $this->get('security.authorization_checker');

        $authorized =
            !is_null($account)
            && $account->isEnabled()
            && $checker->isGranted('ROLE_USER');

        $target = $authorized
            ? 'ClientUIBundle:Default:index'
            : 'ClientLoginBundle:Default:index';

        return $this->forward($target);
    }
}
