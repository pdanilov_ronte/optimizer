import {Component, OnInit, ElementRef} from 'angular2/core';
import {RouterLink} from 'angular2/router';
import {RegistrationService} from "./registration.service";

declare let jQuery: any;


@Component({
    selector: 'register-page',
    templateUrl: 'templates/register-page.html',
    providers: [
        RegistrationService
    ],
    directives: [
        RouterLink
    ]
})
export class RegisterPageComponent implements OnInit {

    private name: string;
    private email: string;
    private password_first: string;
    private password_second: string;
    private invitation_code: string;

    private errors: string[] = [];

    constructor(private service: RegistrationService, private elementRef: ElementRef) {
    }

    ngOnInit(): any {

        let getURLParameter = (name: string) => {
            return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(
                    location.search) || [, ""])[1].replace(/\+/g, '%20')) || null
        };

        let email: string = getURLParameter('email');
        let code: string = getURLParameter('invitation');

        // @todo implement w/o jquery involved

        if (email != undefined) {
            this.email = email;
            jQuery(this.elementRef.nativeElement).find('input[name=email]')[0].readOnly = true;
        }

        if (code != undefined) {
            this.invitation_code = code;
            jQuery(this.elementRef.nativeElement).find('input[name=invitation_code]')[0].readOnly = true;
        }
    }

    public register() {

        this.errors = [];

        // validate data

        if (!this.email) {
            return this.errors.push('Please enter E-mail');
        }

        if (!this.invitation_code) {
            return this.errors.push('Please enter invitation code');
        }

        if (!this.password_first) {
            return this.errors.push('Please enter password');
        }

        if (this.password_first != this.password_second) {
            return this.errors.push('Passwords do not match');
        }

        this.service.register({
                name: this.name,
                email: this.email,
                password: this.password_first,
                code: this.invitation_code
            })
            .then(result => {
                window.location.pathname = '/';

            })
            .catch(reason => {
                console.error(reason);
                this.errors.push(reason);
            });
    }
}
