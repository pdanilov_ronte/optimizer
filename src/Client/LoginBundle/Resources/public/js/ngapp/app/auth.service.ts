import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';
import {Http, Headers, Request, RequestMethod, RequestOptionsArgs, Response} from 'angular2/http';
import {ConfigService} from "./config.service";


@Injectable()
export class AuthService {

    private _account: any;

    constructor(private config: ConfigService, private http: Http) {
        if (!localStorage.getItem('so.auth.logged')) {
            localStorage.setItem('so.auth.logged', '0');
        }
        if (localStorage.getItem('so.auth.account')) {
            this._account = JSON.parse(localStorage.getItem('so.auth.account'));
        }
    }

    get account(): any {
        return this._account;
    }

    public isLoggedIn(): boolean {
        return Boolean(parseInt(localStorage.getItem('so.auth.is_logged_in')));
    }

    public login(username: string, password: string) {

        let headers = new Headers({
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        });

        let body = '_username=' + username + '&_password=' + password;

        let json = JSON.stringify({
            _username: username,
            _password: password
        });

        let options = <RequestOptionsArgs> {
            method: RequestMethod.Get,
            headers: headers,
            search: '_username=' + username + '&_password=' + password
        };

        let request = new Request({
            method: RequestMethod.Get,
            url: this.config._.auth.login_check,
            headers: headers,
            search: '_username=' + username + '&_password=' + password
        });

        let post = this.http.get(this.config._.auth.login_check,
            {search: '_username=' + username + '&_password=' + password});

        let $this = this;

        return new Promise<Response>((resolve, reject) => {
            post
                //.map(res => res.json())
                .subscribe(
                    res => {
                        let json = res.json();
                        this._account = json.user;
                        localStorage.setItem('so.auth.logged', '1');
                        localStorage.setItem('so.auth.account', JSON.stringify(this._account));
                        resolve(res);
                    },
                    err => {
                        let json = err.json();
                        reject(json.error);
                    },
                    () => {
                    }
                );
        });
    }

    public logout() {
        localStorage.clear();
        window.location.href = '/logout';
    }
}
