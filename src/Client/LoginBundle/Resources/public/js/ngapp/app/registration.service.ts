import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core';
import {Http, Headers, Request, RequestMethod, RequestOptionsArgs, Response} from 'angular2/http';
import {ConfigService} from "./config.service";


@Injectable()
export class RegistrationService {

    private _account: any;

    constructor(private config: ConfigService, private http: Http) {
    }

    public register(data: any): Promise<Response> {

        let headers = new Headers({
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json'
        });

        let json = JSON.stringify(data);

        let options = <RequestOptionsArgs> {
            headers: headers
        };

        let post = this.http.post(
            this.config._.auth.register,
            json,
            options
        );

        let $this = this;

        return new Promise<Response>(function (resolve, reject) {
            post
                //.map(res => res.json())
                .subscribe(
                    res => {
                        let body = JSON.parse(res['_body']);
                        let json = res.json();
                        localStorage.setItem('so.auth.logged', '1');
                        localStorage.setItem('so.auth.account', JSON.stringify(json.account));
                        resolve(json);
                    },
                    err => {
                        reject(err.json().error);
                    },
                    () => {
                    }
                );
        });
    }
}
