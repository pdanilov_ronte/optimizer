import {Injectable} from 'angular2/core';
import {CONFIG} from './app.config';

declare let CONFIG_EXT: any;


export class ConfigService {

    private _config: any = {};

    constructor() {
        this._config = CONFIG;
        this._config.app = CONFIG_EXT.app;
    }

    public get _(): any {
        return this._config;
    }
}
