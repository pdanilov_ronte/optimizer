import {Component} from 'angular2/core';
import {RouterLink} from 'angular2/router';
import {ConfigService} from "./config.service";
import {AuthService} from "./auth.service";


@Component({
    selector: 'login-page',
    templateUrl: 'templates/login-page.html',
    directives: [
        RouterLink
    ]
})
export class LoginPageComponent {

    private csrf_token: string;

    private username: string;
    private password: string;

    private errors: string[] = [];

    constructor(private config: ConfigService, private auth: AuthService) {
        this.csrf_token = config._.app.csrf_token;
    }

    public login() {

        this.errors = [];

        let success = () => {
            window.location.pathname = '/';
        };

        this.auth.login(this.username, this.password)
            .then((data) => {
                success();
            })
            .catch((reason) => {
                console.error(reason);
                this.errors.push(reason);
            });
    }
}
