import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';
import {Router} from 'angular2/router';

import {ConfigService} from "./config.service";
import {AuthService} from "./auth.service";
import {LoginPageComponent} from "./login-page.component";
import {RegisterPageComponent} from "./register-page.component";


@Component({
    selector: 'login-app',
    template: `
    <router-outlet></router-outlet>
    `,
    providers: [
        ConfigService,
        AuthService
    ],
    directives: [
        ROUTER_DIRECTIVES
    ]
})
@RouteConfig([
    {path: '/', name: 'LoginPage', component: LoginPageComponent, useAsDefault: true},
    {path: '/register', name: 'RegisterPage', component: RegisterPageComponent}
])
export class AppComponent {

    public isLoggedIn: boolean = false;

    constructor(private router: Router) {
    }
}
