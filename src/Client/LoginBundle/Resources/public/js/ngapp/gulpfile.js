const gulp = require('gulp');
const del = require('del');


gulp.task('clean:libs', function () {
    return del('dist/libs/**/*');
});

gulp.task('copy:libs', ['clean:libs'], function () {
    return gulp.src([
            'node_modules/es6-shim/es6-shim.min.js',
            'node_modules/es6-shim/es6-shim.map',
            'node_modules/systemjs/dist/system-polyfills.js',
            'node_modules/systemjs/dist/system-polyfills.js.map',
            'node_modules/angular2/bundles/angular2-polyfills.js',
            'node_modules/systemjs/dist/system.src.js',
            'node_modules/rxjs/bundles/Rx.js',
            'node_modules/jquery/dist/jquery.min.js',
            'node_modules/angular2/bundles/angular2.dev.js',
            'node_modules/angular2/bundles/router.dev.js',
            'node_modules/angular2/bundles/http.dev.js'
        ])
        .pipe(gulp.dest('dist/lib'));
});


gulp.task('build', ['copy:libs']);
gulp.task('default', ['build']);
