<?php

namespace Client\LoginBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/{any}", name="client_login_app", requirements={"any" = ".*"})
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        if (!in_array($request->getPathInfo(), ['/', '/register'])) {
            return new RedirectResponse('/');
        }

        /** @var \Symfony\Component\Security\Csrf\CsrfTokenManagerInterface $csrf */
        $csrf = $this->get('security.csrf.token_manager');

        $token = $csrf->getToken('login_form');

        return array(
            'csrf_token' => $token,
            'request_path' => $request->getRequestUri()
        );
    }
}
