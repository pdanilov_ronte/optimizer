<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new FOS\RestBundle\FOSRestBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new \Jmikola\AutoLoginBundle\JmikolaAutoLoginBundle(),
            /** Application bundles */
            new Shared\SoaBundle\SharedSoaBundle(),
            new Shared\TestBundle\SharedTestBundle(),
            new Base\ExperimentsBundle\BaseExperimentsBundle(),
            new Base\GoalsBundle\BaseGoalsBundle(),
            new Client\JSBundle\ClientJSBundle(),
            new Client\ServiceBundle\ClientServiceBundle(),
            new Client\RestBundle\ClientRestBundle(),
            new Client\OdpBundle\ClientOdpBundle(),
            new Base\UsersBundle\BaseUsersBundle(),
            new Stats\ReceivingBundle\StatsReceivingBundle(),
            new Client\CacheBundle\ClientCacheBundle(),
            new Cache\NotifyBundle\CacheNotifyBundle(),
            new Cache\UpdateBundle\CacheUpdateBundle(),
            new Client\TestBundle\ClientTestBundle(),
            new Client\FrontendBundle\ClientFrontendBundle(),
            new Client\LoginBundle\ClientLoginBundle(),
            new Client\UIBundle\ClientUIBundle(),
            new Client\TrackBundle\ClientTrackBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Shared\DebugBundle\SharedDebugBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
